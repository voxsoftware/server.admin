
F= (body)->
	global= F.global
	if global.context.server
		if(global.context.server.isMaster)
	
			if process.argv.indexOf("--forever") < 0
				throw global.Exception.create("This process is not suitable for reboot. You need start kowix with --forever argument").putCode("NOT_AVAILBLE")


			setTimeout ()->
				process.kill(process.pid, 'SIGTERM')
			, 100
			return 
				queued: yes 

		else 
			await global.userFunction("passwd.check").invoke(body)
			global.context.server.channel.invokeMethod((await global.getSite()).idSite, "app.reboot", body)

	

module.exports= (global)->
	F.global= global 
	F

