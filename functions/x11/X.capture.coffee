import Child from 'child_process'
import Os from 'os'
import Path from 'path'
Fs= core.System.IO.Fs 


F= (body)->
	global= F.global 


	# password check 
	await global.userFunction("passwd.check").invoke body

	tmp= global.UserContext.getHomeDir()
	tmp= Path.join tmp, "x11"
	if not global.publicContext.x11_checked 
		if not Fs.sync.exists(tmp)
			await Fs.async.mkdir tmp
		global.publicContext.x11_checked = yes

	global.UserContext.setRequireMode("npm")
	Sharp= await global.UserContext.require("sharp","0.21.3")
	X11= await global.UserContext.require("x11","2.3.0")

	promisify= (func)->
		return (...args)->
			task= new core.VW.Task()
			args.push (er,data)->
				task.result= data
				task.exception= er if er
				task.finish()

			func.apply this, args
			return task 


	if not X11.createClientAsync
		X11.createClientAsync = promisify X11.createClient

	 


	client= await X11.createClientAsync()
	uname= ''
	X= client.client
	X.on "error", (er)->
		console.error er


	screen= client.screen[0]
	windowId= screen.root 

	geo = null
	if not X.GetGeometryAsync 
		X.GetGeometryAsync= promisify X.GetGeometry
		X.GetImageAsync= promisify X.GetImage
	if not X.GetCursorImageAsync
		X.GetCursorImageAsync= promisify X.GetCursorImage
	lastsrc1= null 
	compare= (a,b)->
		if a.length != b.length 
			return false
		for val,i in a 
			if val isnt b[i]
				return false 
		
		return true
	
	geo= await X.GetGeometryAsync windowId

	create_mode0= (asBuffer)->
		uname= global.uniqid() + ".jpg"
		name= Path.join tmp, uname
		

		try			
			task= new core.VW.Task()
			p= Child.spawn("import", ["-screen", "-window","root", "-quality", "90", name])
			p.on "error", (e)->
				task.exception= e 
				task.finish()
			p.on "exit", task.finish.bind(task)
			await task 
			if asBuffer
				return await  Fs.async.readFile(name)
			name= null 

		catch e 
			throw e
		finally 
			try 
				await Fs.async.unlink name if name

	create_mode1= (asBuffer)->
		
		uname= global.uniqid() + "." + (body.format or "webp")
		name2= Path.join tmp,uname
		try
			
			
			
			bmp= await X.GetImageAsync 2, windowId, 0, 0, geo.width, geo.height, 0xFFFFFFFF
			busrc= bmp.data 

			# transform data
			#raw= Buffer.allocUnsafe(busrc.length) 
			z=0
			for y in [0...geo.height]
				for x in [0...geo.width]
					a= busrc[(x + geo.width * y) * 4 + 3]
					r= busrc[(x + geo.width * y) * 4 + 2]
					#g= busrc[(x + geo.width * y) * 4 + 1]
					b= busrc[(x + geo.width * y) * 4 + 0]
					
					busrc[z]=r
					#busrc[z+1]=g
					busrc[z+2]=b
					busrc[z+3]= 255
					z+= 4


			if lastsrc1
				if compare lastsrc1, busrc
					return null
			
			lastsrc1= busrc

			buf= await Sharp(busrc, {
				raw:{
					width: geo.width
					height: geo.height
					channels: 4
					
				}
			})[body.format or "webp"]
					quality:50
				.toBuffer()

			if asBuffer
				return buf

			await Fs.async.writeFile name2, buf

		catch e 
			throw e
		finally
			### 
			try 
				await Fs.async.unlink name
			###

	create= create_mode0

	if body?.socket
		
		ended= no
		body.bufferCount= body.bufferCount or 0
		body.socket.on "disconnect", ()->
			ended= yes 	
		
		body.socket.on "error", (er)->
			console.error "Socket error:", er.message

		
		# attach to keyboard and mouse
		serror= (e)->
			body.socket.emit "code.error",
				message: e.message 
				code: e.code
		try 
			XManager= await global.userFunction("x11/X.management").invoke
				Xclient: client
			body.socket.on "mousedown", (clickcode)->
				try 
					await XManager.mouseDown clickcode
				catch e 
					serror e 
					console.error "Failed mousedown: " + e.message
			body.socket.on "mouseup", (clickcode)->
				try 
					await XManager.mouseUp clickcode
				catch e 
					serror e 
					console.error "Failed mouseup: " + e.message
			body.socket.on "keyup", (c)->
				try 
					await XManager.keyUp c?.keycode or c, c?.secure
				catch e 
					serror e
					console.error "Failed keyup: " + e.message
			body.socket.on "keydown", (c)->
				try 
					await XManager.keyDown c?.keycode or c, c?.secure
				catch e 
					serror e
					console.error "Failed keydown: " + e.message
			body.socket.on "move", (location)->
				try 
					# move according to percent ...
					x= geo.width * location.xpercent / 100
					y= geo.height * location.ypercent / 100
					await XManager.move x, y
				catch e 
					console.error "Failed move: " + e.message
		catch e
			body.socket.emit "code.error",
				message: e.message 
				code: e.code


		# attach to screen capture
		buffering= []
		getequal= (src)->
			i= buffering.indexOf src 
			i= undefined if i < 0
			return i

			


		obuf= ''
		while not ended 
			try 
				time= Date.now()
				obuf= await create(yes)
				if obuf isnt  null 
					src= obuf.toString('binary')
					if lastsrc is src 
						# emit same image

					else if body.bufferCount > 0 and ((y = getequal(src)) isnt undefined)
						body.socket.emit "cache.image", y
					else 
						body.socket.emit "image", src 
						if body.bufferCount > 0
							buffering.push src
							if buffering.length > body.bufferCount
								buffering.shift()
						

					lastsrc= src

				
				if Date.now() - time < 60
					await core.VW.Task.sleep 60 - (Date.now()-time)
				

			catch e 
				not ended and body.socket.emit "code.error",
					message: e.message 
					code: e.code

	else 
		await create()
		req= global.context.request 
		serv= new core.VW.Http.StaticServe.Server()
		serv.addPath tmp 
		req.url= "/" + uname 
		await serv.handle global.context


module.exports= (global)->
	F.global= global 
	F 