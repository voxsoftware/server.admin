exceptionKeys = 
	"null": ""
	

class KeyMapper 

	constructor:(list,min)->
		@keymap= @buildASCIIToXKeyMap(list, min)

	buildASCIIToXKeyMap:(XKeysMap,min)->
		asciiToX = {}
		for keyo,i in XKeysMap
			for key,j in keyo 
				value= i + min
				if key isnt 0
					asciiToX[key] = value
				
		return asciiToX

	mapKey: (keyCode)->
		keyCode = exceptionKeys[keyCode] if exceptionKeys[keyCode]
		key = @keymap[keyCode]
		key = 0 if not key 
		key




F= (body)->
	client= body.Xclient
	if not client 
		return 
		
	X= client.client
	root= client.screen[0].root


	promisify= (func)->
		return (...args)->
			task= new core.VW.Task()
			args.push (er,data)->
				task.result= data
				task.exception= er if er
				task.finish()

			func.apply this, args
			return task


	min=X.display.min_keycode
	max=X.display.max_keycode
	if not X.GetKeyboardMappingAsync
		X.GetKeyboardMappingAsync=  promisify X.GetKeyboardMapping
		X.WarpPointerAsync=  promisify X.WarpPointer
		X.require=  promisify X.require
	

	list= await X.GetKeyboardMappingAsync min, max-min
	keyMapper= new KeyMapper(list,min)

	geo= body.geo 

	XManager= 
		resize: (size)->
			mode= ''
			task= new core.VW.Task()
			p= Child.spawn "gtf", [size.width.toString(), size.height.toString(), "59.9"]
			p.stdout.on "data", (s)->
				mode+= s.toString()
			p.on "error", (er)->
				task.exception=er 
				task.finish()
			p.on "exit", task.finish.bind task
			await task 

			t= mode.indexOf("Modeline")
			if t > 0
				mode= mode.substring(t + 8).trim()

				screen=''
				p= Child.spawn "xrandr",["-q"]
				p.stdout.on "data", (s)->
					screen+= s.toString()
				p.on "error", (er)->
					task.exception=er 
					task.finish()
				p.on "exit", task.finish.bind task
				await task 

				parts= screen.split ""




		move: (x,y)->
			await X.WarpPointerAsync(0,root,0,0,0,0,x,y)
		cursor: ()->
			fixes= await X.require "fixes"
			#return Object.keys fixes
			task= new core.VW.Task()
			fixes.FixesGetCursorImage  (er,data)->
				task.result= data 
				task.exception= er if er 
				task.finish()
			await task 
		keyDown: (keyCode,secure)->
			input= await X.require "xtest"
			task= new core.VW.Task()
			code= if secure then keyCode else keyMapper.mapKey(keyCode)
			input.FakeInput input.KeyPress, code, 0, root, 0,0, (er,data)->
				task.result= data 
				task.exception= er if er 
				task.finish()
			await task 
		keyUp: (keyCode,secure)->
			input= await X.require "xtest"
			task= new core.VW.Task()
			code= if secure then keyCode else keyMapper.mapKey(keyCode)
			input.FakeInput input.KeyRelease, code, 0, root, 0,0, (er,data)->
				task.result= data 
				task.exception= er if er 
				task.finish()
			await task 

		mouseDown: (clickCode)->
			input= await X.require "xtest"
			task= new core.VW.Task()
			input.FakeInput input.ButtonPress, clickCode, 0, root, 0,0, (er,data)->
				task.result= data 
				task.exception= er if er 
				task.finish()
			await task 

		mouseUp: (clickCode)->
			input= await X.require "xtest"
			task= new core.VW.Task()
			input.FakeInput input.ButtonRelease, clickCode, 0, root, 0,0, (er,data)->
				task.result= data 
				task.exception= er if er 
				task.finish()
			await task 
			
	return XManager



module.exports=(global)->
	F.global= global 
	F