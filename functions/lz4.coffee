import Child from 'child_process'
import {EventEmitter} from 'events'

class St extends EventEmitter

	constructor:(p)->
		super()
		@init(p)
	
	init:(p1)->
		@p1= p1
		self= @
		p1.on "error", (er)->
			self.emit "error", er 
		
		p1.on "exit", ()->
			self.emit "finish"
		
		p1.stdin.on "end",	()->
			self.emit "end"
		
		p1.stdout.on "data", (data)->
			self.emit "data", data 
		
	write: (data)->
		return @p1.stdin.write data
	
	pause: ()->
		@p1.stdin.pause()
	
	resume: ()->
		@p1.stdin.resume()
	
	end: ()->
		@p1.stdin.end()
	
	
	
	pipe: (streamOut)->
		return @p1.stdout.pipe streamOut
	
	

F= ()->
	p1= Child.spawn "lz4", []
	st= new St p1
	return st
	
module.exports= (global)->
	F.global= global 
	F