var F, pbody 

class ClientSocket{
	
	constructor(client){
		this.client= client
		client.on("disconnect",()=>{
			this.client= null
		})
	}
	
	
	p(e){
		var er={
			stack:e.stack,
			message: e.message|| e,
			code: e.code 
		}
		return er
	}
	
	
	async event(data){
		
		var task , er
		if(data.method){
			try{
				data.body = data.body || {}
				data.body.socket= this.client
				task= F.global.userFunction(data.method).invoke(data.body)
			}catch(e) {
				er= e
			}
			if(data.wait){
				if(task && typeof task.then=="function"){
					task.then((result)=>{
						this.client.emit("event", {
							cid: data.cid ,
							result
						})
					}).catch((e)=>{
						this.client.emit("event", {
							cid: data.cid ,
							result:{
								error: this.p(e)
							}
						})
					})
				}else{
					this.client.emit("event", {
						cid: data.cid ,
						result:er ? {error:this.p(er)} : "received"
					})
				}
			}
		}else{
			console.error("Not valid action")
		}
	}
}



F= async function(body){
	
	if(body.client){
		
		var client= body.client
		var Client= new ClientSocket(client)
		client.on('event', Client.event.bind(Client))
		client.on('disconnect', function(){
			Client= null 
		})
		
		if(body.event=="event"){
			Client.event(body.data)
		}
		return 
		
	}
	return ClientSocket
}
module.exports= function(global){
	F.global=global
	return F  
}