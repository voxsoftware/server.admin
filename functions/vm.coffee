import Path from 'path'
getcode= (code)->
	ctx= await F.global.getSiteContext "kowix"
	return await ctx.userFunction("script.compile").invoke code

F= (body)->
	global= F.global 
	
	# check password ...
	await global.userFunction("passwd.check").invoke(body)
	
	if not body.name 
		throw global.Exception.create "Specify a long name including extension for script"	
	
	if not body.source 
		throw global.Exception.create "Specify source code"	
	
	body.file= Path.join __dirname, body.name
	if not body.mtime
		body.mtime= parseInt((Date.now() / 10000) * 10000)
		
	Cmp= await getcode(body)
	if Cmp?.code 
		return await  eval Cmp.code 
	
		
module.exports= (global)->
	F.global= global 
	F 