import Child from 'child_process'

F= (body)->
	global= F.global 
	
	# check password ...
	await global.userFunction("passwd.check").invoke(body)
	
	task= new core.VW.Task()
	r= task.result= {}
	
	
	
	
	try 
		op= {}
		for id of body.options 
			op[id]= body.options[id]
			
		if not (body.arguments instanceof Array)
			if typeof body.arguments is "string"
				body.arguments= body.arguments.split " "
			else 
				body.arguments= []
		
		
		
		resp_stream= global.context?.response
		resp_stream?.setTimeout(0)
		if global.context?.request?.socket 
			g= ()->
				return if g.yet
				g.yet= true 
				p and p.kill 'SIGKILL'
				p1 and p1.kill 'SIGKILL'
			global.context.request.socket.on "close", g
			global.context.request.on "close", g
		
		
		createW= (resp_stream)->
			(data)->
				
				if resp_stream.write(data)  is false
					if not write1.paused
						try 
							p.stdout.pause()
							p.stderr.pause()
							write1.paused= true
							
							resp_stream.once "drain", ()->
								try 
									p.stdout.resume()
									p.stderr.resume()
								catch 
								
								write1.paused= false
						catch
							
		write1= createW resp_stream
		
		
		p1= undefined 
		
		if body.mode is "stream" and body.compression is "lz4"
			LZ4= await global.UserFunction("lz4").invoke()
			LZ4.on "error", (er)->
				task2= null 
				task.exception= er 
				task.finish()
				
			
			resp_stream_copy= resp_stream
			resp_stream= LZ4
			write2= write1 
			write1= LZ4.write.bind LZ4 
			LZ4.on "data", write2 
			LZ4.on "finish", ()->
				resp_stream= resp_stream_copy
				#resp_stream_copy.end()
				task2.finish()
			task2= new core.VW.Task()
			
			
		###
		if body.mode is "stream" and body.encoding is "lz4"
			
			resp_stream_copy= resp_stream
			resp_stream= p1.stdin
			
			
			write2= write1 
			write1= (data)->
				p1.stdin.write data 
			
			
			p1.stdout.on "data", (data)->
				write2 data 
			p1.on "exit", ()->
				
			
			task2= new core.VW.Task()
			await core.VW.Task.sleep 600
		
		###
		
	
		p= Child.spawn body.cmd, body.arguments, body.options
		p.on "error", (er)->
			body.mode = ''
			task.exception= er 
			task.finish() 
		
		
		
		p.stdout.on "data", (data)->
			if body.mode is "stream"
				if body.stream isnt "stderr"
					write1 data 
				return 
				
				
			r.stdout= r.stdout or []
			r.stdout.push data 
		
		p.stderr.on "data", (data)->
			if body.mode is "stream"
				if body.stream is "stderr" or body.stream is "all"
					if parseInt(body.stderr_as_json) is 1 
						write1 JSON.stringify
							err: data.toString()
					else 
						write1 data 
					
					
				return 
				
			r.stderr= r.stderr or []
			r.stderr.push data 
		
		p.on "exit", ()->
			task.finish()
			LZ4.end() if LZ4
				
		
		if not body.timeout 
			body.timeout= 60000
		
		
		
		if body.timeout 
			r.time= setTimeout ()->
				delete r.time 
				r.err = 'TIMEDOUT'
				p.kill('SIGKILL')
				task.finish() 
			, body.timeout 
			
		
		
		await task 
		await task2 if task2 
		
		if body.mode is "stream" 
			# esto permite saber si el stream termina correctamente 
			if body.termination
				resp_stream.write body.termination
			
			
			resp_stream.end()
		
		
	catch e 
		throw e 
	finally 
		if r.time 
			clearTimeout r.time 
			delete r.time 
		
	if body.mode isnt "stream" 
		r.stderr= Buffer.concat r.stderr if r.stderr
		r.stdout= Buffer.concat r.stdout if r.stdout
		if body.encoding 
			r.stderr= r.stderr.toString body.encoding if r.stderr
			r.stdout= r.stdout.toString body.encoding if r.stdout
			
	return r
		
module.exports= (global)->
	F.global= global 
	F 