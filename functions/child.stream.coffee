import Child from 'child_process'
import {EventEmitter} from 'events'

class St extends EventEmitter

	constructor:(p)->
		super()
		@init(p)
	
	init:(p1)->
		@p1= p1
		self= @
		p1.on "error", (er)->
			self.emit "error", er 
		
		p1.on "exit", ()->
			self.emit "finish"
		
		p1.stdin.on "end",	()->
			self.emit "end"
		
		###
		p1.stdout.on "data", (data)->
			self.emit "data", data 
		###
		
	destroy: ()->
		@p1.kill 'SIGKILL'
		
	write: (data)->
		return @p1.stdin.write data
	
	pause: ()->
		@p1.stdin.pause()
	
	resume: ()->
		@p1.stdin.resume()
	
	end: ()->
		@p1.stdin.end()
	
	pipe0: ()->
		if not @ce0
			@p1.stdin.on "error", @emit.bind(@, "error")
			@ce0= true 
			
		return @p1.stdin
	
	pipe1: ()->
		if not @ce1
			@p1.stdout.on "error", @emit.bind(@, "error")
			@ce1= true 
			
		return @p1.stdout
	
	pipe2: ()->
		if not @ce2
			@p1.stderr.on "error", @emit.bind(@, "error")
			@ce2= true 
			
		return @p1.stderr
	
	pipe: (streamOut)->
		return @p1.stdout.pipe streamOut
	
	

F= (body)->
	
	op= {}
	for id of body.options 
		op[id]= body.options[id]
		
	if not (body.arguments instanceof Array)
		if typeof body.arguments is "string" and body.arguments
			body.arguments= body.arguments.split " "
		else 
			body.arguments= []
			
			
	p1= Child.spawn body.cmd, body.arguments, body.options
	st= new St p1
	return st
	
module.exports= (global)->
	F.global= global 
	F