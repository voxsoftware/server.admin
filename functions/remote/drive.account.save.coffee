F= (body)->
	
	global= F.global
	account= body.account 
	
	await global.userFunction("passwd.check").invoke body
	
	
	# find userdata in database 
	localDB= await global.userFunction("LocalDB").invoke()
	await localDB.put("drive.account.#{body.name or account.name}", JSON.stringify account)
	

module.exports= (global)->
	F.global= global 
	F
    	