
driveCode= ()->
	
	getFile1= (e)->
		json= null 
		try 
			###
			if e.params.folderid 
				folder = DriveApp.getFolderById(e.params.folderid)
			else if e.params.id 
				folder = DriveApp.getFolderById(e.params.id)
			else if e.params.folder 
				e.params.path= '$remote.drive/' + e.params.folder 
			###
			
			if e.params.path 
				e.params.folders= e.params.path.split("/").filter (a)->
					return not not a 
			
			
			
			for fname,i in e.params.folders
				
				if i is (e.params.folders.length - 1)
					file= (foldery or DriveApp).getFilesByName(fname)
					if file.hasNext()
						file= file.next()
					else
						file= null
						folder= (foldery or DriveApp).getFoldersByName(fname)
						if folder.hasNext()
							folder= folder.next()
						else
							folder= null
				else
					folder1= (foldery or DriveApp).getFoldersByName fname 
					if not folder1.hasNext()
						throw new Error "File not found: " + e.params.path
					else 
						foldery= folder1.next()
			
			if folder 
				file= folder 
				
				
			if file 
				json= {}
				json.id= file.getId()
				json.deleted= true 
				
				if e.params.internal 
					file.setTrashed yes 
				else 
					folder1= DriveApp.getFoldersByName ".trash"
					if folder1.hasNext()
						folder1 = folder1.next()
					else 
						folder1= DriveApp.createFolder ".trash"
					
					if folder
						file.getParents().next().removeFolder file 
						folder1.addFolder file 
					else 
						file.getParents().next().removeFile file 
						folder1.addFile file 
				
			
			
		catch e 
			json= {}
			json.error = 
				message: e.message,
				stack: e.stack,
				code: e.code
		
		
		return json 
		
	getFile= (e)->
		json= getFile1 e 
		return ContentService.createTextOutput(JSON.stringify(json)).setMimeType(ContentService.MimeType.JSON)
	
	
	return getFile(e)

F= (body)->
	body= body or {}
	code1= driveCode.toString()
	code1= code1.substring(0, code1.length - 1)
	
	code= '(\n' + code1 + "})"
	#return code 
	
	body.code = code
	return await F.global.UserFunction("remote/drive.eval").invoke(body)



module.exports = (global)->
	F.global = global
	F
