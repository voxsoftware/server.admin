F= (body)->
	body= body or {}
	
	global= F.global 
	body.accountfile= body.accountfile or body.account 
	if not body.accountfile 
		throw global.Exception.create("Invalid argument account").putCode("INVALID_ARGUMENT")
	
	
	if typeof body.accountfile is "object"
		data= body.accountfile
	else 
		
		# find userdata in database 
		localDB= await global.userFunction("LocalDB").invoke()
		account= await localDB.tryGetString("drive.account.#{body.accountfile}")
		if not account 
			throw global.Exception.create("Invalid account #{body.accountfile}").putCode("INVALID_ACCOUNTFILE")
		
		data= JSON.parse account 
		#data= await global.userFunction(body.accountfile).invoke()
		
	if not data?.urls
		throw global.Exception.create("Invalid account file").putCode("INVALID_ACCOUNTFILE")
	
	
	
	random= parseInt Math.random()* data.urls.length
	if random > data.urls.length
		random= 0
	
	url= data.urls[random]
	nbody= JSON.parse(JSON.stringify(body ))
	nbody.pass= data.password 
	
	
	req= new core.VW.Http.Request url 
	req.method='POST'
	req.body= nbody
	req.contentType='application/json'
	req.headers["accept-language"]='en,q=1;'
    
	try
		response= await req.getResponseAsync()
		if response.headers.location 
			delete req.innerRequest.callback if req.innerRequest
			req= new core.VW.Http.Request response.headers.location 
			response= await req.getResponseAsync()
    	
		if typeof response.body is "string"
			if response.headers["content-type"].indexOf("application/json") >= 0
				try 
					json= true 
					response.body= JSON.parse response.body 
				catch e 
					return response.body
			
		if response.body?.error
			er= global.Exception.create(response.body.error.message)
			er.code= response.body.error.code 
			
		if not json 
			global.context.response.write(Buffer.from(response.body,'utf8'))
			global.context.response.end() 
		
		else if response.body?.response_redirect
			global.context.response.redirect(response.body.response_redirect)
			global.context.response.end() 
		
		return response.body
		
	catch e 
		throw e 
	
	finally 
		delete req.innerRequest.callback if req.innerRequest
	

module.exports= (global)->
	F.global= global 
	F
    	