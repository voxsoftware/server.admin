
driveCode= ()->
	
	


	moveFiles= (source_folder, dest_folder)->
		files= source_folder.getFiles()
		while files.hasNext()
			file= files.next()
			dest_folder.addFile file 
			source_folder.removeFile file 
		
	
	getFile1= (e)->
		json= null 
		try 
			
			if e.params.path 
				e.params.folders= e.params.path.split("/").filter (a)->
					return not not a 
			
			uname= e.params.folders[e.params.folders.length - 1]
			for fname,i in e.params.folders
				
				if i is (e.params.folders.length - 1)
					file= (foldery or DriveApp).getFilesByName(fname)
					if file.hasNext()
						file= file.next()
					else
						file=null 
						folder= (foldery or DriveApp).getFoldersByName(fname)
						if folder.hasNext()
							folder= folder.next()
						else
							folder= null
				else
					folder1= (foldery or DriveApp).getFoldersByName fname 
					if not folder1.hasNext()
						throw new Error "File or folder not found: " + e.params.path
					else 
						foldery= folder1.next()
			
			if file 
				json= 
					id: file.getId()
				file.makeCopy e.params.dest
				file.setTrashed yes
			
			if folder 
				
				file= folder 
				json= 
					id: file.getId()
				parent= file.getParents().next()
				
				folder1= parent.getFoldersByName e.params.dest 
				if folder1.hasNext()
					throw new Error "Folder " + e.params.dest + " exists"
				else 
					folder1= parent.createFolder e.params.dest 
				
				moveFiles folder, folder1
				folder.setTrashed yes
					
			
		catch e 
			json= {}
			json.error = 
				message: e.message,
				stack: e.stack,
				code: e.code
		
		
		return json 
		
	getFile= (e)->
		json= getFile1 e 
		return ContentService.createTextOutput(JSON.stringify(json)).setMimeType(ContentService.MimeType.JSON)
	
	
	return getFile(e)

F= (body)->
	body= body or {}
	code1= driveCode.toString()
	code1= code1.substring(0, code1.length - 1)
	
	code= '(\n' + code1 + "})"
	#return code 
	
	body.code = code
	return await F.global.UserFunction("remote/drive.eval").invoke(body)



module.exports = (global)->
	F.global = global
	F
