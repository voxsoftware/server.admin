
driveCode= ()->
	
	getFile1= (e)->
		json= null 
		try 
			
			
			file= DriveApp.getFileById e.params.id 
			try 
				desc= file.getDescription()
				if desc 
					desc= JSON.parse desc
			
			catch 
			
			if desc?.mtime >= Date.now() - 120000
				json= 
					href: desc.href
					id: e.params.id
				return json
			
			response= UrlFetchApp.fetch "https://drive.google.com/uc?id=" + e.params.id,
				followRedirects:false
			headers= response.getAllHeaders()
			json= 
				href: headers.Location or headers.location
				id: e.params.id
			
			
			try 
				desc= 
					href: json.href
					mtime: Date.now()
				file.setDescription JSON.stringify desc
				
			
			
		catch e 
			json= {}
			json.error = 
				message: e.message,
				stack: e.stack,
				code: e.code
		
		
		return json 
		
	getFile= (e)->
		json= getFile1 e 
		return ContentService.createTextOutput(JSON.stringify(json)).setMimeType(ContentService.MimeType.JSON)
	
	
	return getFile(e)

F= (body)->
	body= body or {}
	code1= driveCode.toString()
	code1= code1.substring(0, code1.length - 1)
	
	code= '(\n' + code1 + "})"
	#return code 
	
	body.code = code
	return await F.global.UserFunction("remote/drive.eval").invoke(body)



module.exports = (global)->
	F.global = global
	F
