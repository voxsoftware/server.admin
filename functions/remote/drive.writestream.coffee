IO= core.System.IO
import Stream from 'stream'
import Path from 'path'
import fs from 'fs'
class DriveFileStream extends Stream.Writable 
	
	constructor:(accountFile, path, options)->
		super()
		@options= options or {}
		if not @options.mimeType 
			@options.mimeType= F.Mime.lookup(path) 
		@account= accountFile
		@path= path 
		@temp_uid= F.global.uniqid()
		@fwriting= 0
		@part= 0
		@max= 14
		@parts= []
		
		
	finish: ()->
		console.log "FINISHING"
		if @buffer 
			await @_write Buffer.zero, ()->
				
			
		if @stream 
			await @_writeCurrentStreamToDrive yes
		
	
	
	_raiseError: (e,code)->
		
		@writing= false 
		@fwriting= 0
		@haveError= true
		@stream.close() if @stream 
			
		if @file1
			try 
				await IO.Fs.async.unlink @file1
			catch 
			@file1= null
			
		er= F.global.Exception.create e.message or e.toString()
		if e?.code or code
			er.putCode e?.code or code 
		@emit "error", er
		
		
		# delete folder 
		try 
			await F.global.userFunction("remote/drive.delete").invoke
				accountfile: @account
				internal: yes 
				path: "$remote.drive/" + @temp_uid
		catch e
			console.error "ERROR Deleting temp folder: ", e
		
		
	
	_writeCurrentStreamToDrive	: (finishing)->
		
		console.log "WRITING PART TO DRIVE"
		
		stream= @stream 
		file= @file1 
		
		@stream= null 
		@file1= null 
		
		part= @part 
		@part++
		
		
		try 
			@fwriting++
			#@writing= true 
			if @haveError 
				return 
			
			if finishing
				while @fwriting > 1
					await core.VW.Task.sleep 50
					
			options = @options if finishing 
			idSite= (await F.global.getSite()).idSite
			url= "http://" + F.global.publicContext.PublicIP + ":#{F.global.context.httpServer.$server.address().port}/site/"  + idSite + "/api/function/c/remote@drive.writestream?f=" + encodeURIComponent(Path.basename file) 
			console.log "URL: ", url
			json= @filedata= await F.global.userFunction("remote/drive.upload").invoke
				url: url
				accountfile: @account
				part: part 
				parts: @parts
				filedata: @filedata
				createdesc: finishing
				name: @temp_uid
				path: @path.split("/")[...-1].join("/")
				options: options
			
			delete @filedata.parts 
			
			
			if not json.added 
				throw F.global.Exception.create "Failed putting bytes: " + JSON.stringify(json)
			@parts[part]= json.added
			
			
			if @haveError 
				return 
				
			if finishing
				k= @path.split("/")
				
				## rename to official 
				await F.global.userFunction("remote/drive.rename").invoke
					accountfile: @account
					dest: k[k.length - 1]
					path: k[...-1].join("/") + "/" + @temp_uid
			
					
		catch e 
			@_raiseError e 
		finally 
			
			#@writing= no
			@fwriting--
			stream?.close?()
			if file
				try 
					await IO.Fs.async.unlink file
				catch
					
	
	end: (chunk,enc,next)->
		if typeof enc is "function"
			next= enc 
			enc= null	
			
		while @fwriting > 0
			await core.VW.Task.sleep 50
		
		return if @haveError
		if chunk
			@_write chunk, enc 
		return if @haveError
		
		await @finish()
		next() if next
		@emit "finish"
		
	
		
	_write: (chunk, enc, next)->
		
		
		if typeof enc is "function"
			next= enc 
		
		#if not chunk 
		#	return @finish()
		
		if not Buffer.isBuffer chunk
			chunk= Buffer.from chunk, enc 
		
		
		if not @yet1 and @fwriting > 0
			@buffer=[] if not @buffer
			@buffer.push chunk 
			next()
			return false 
			
		
		if @fwriting > @max
			@buffer=[] if not @buffer
			@buffer.push chunk 
			while @fwriting > @max 
				await core.VW.Task.sleep 50
			next()
			return false 
		
		
		
		if not @stream 
			@writed=0
			@file1= @gettmpFile()
			@stream= new IO.FileStream @file1, IO.FileMode.OpenOrCreate, IO.FileAccess.Write
			@stream.position= 0
			@stream.setLength 0 
		
		
		
		
		
		if @buffer?.length
			@buffer.push chunk
			chunck= Buffer.concat @buffer 
			@buffer= null 
		
		escrito= await @stream.writeAsync chunk, 0, chunk.length
		if escrito < chunk.length 
			next F.global.Exception.create("Write stream error", "WRITE_ERROR") if next 
			@_raiseError "Write stream error", "WRITE_ERROR"
			return false 
			
		
		@writed+= escrito
		
		if @writed >= 20*1024*1024
			
			
			task1= @_writeCurrentStreamToDrive()
			await task1 

			###
			try 
				if not @yet1
					await task1
				else 
					await core.VW.Task.sleep 800
			catch e 
				throw e 
			finally 
				@yet1= yes 
			###
				
			#@writing= true
			#try 
			
			#catch e 
			#	@_raiseError e
			#finally 
			#	@writing= false 
			
		
		
		@emit "drain"
		next()
		return true 
		
			
	
	gettmpFile: ()->
		home= F.global.UserContext.getHomeDir()
		file= Path.join home, F.global.uniqid()
		return file 


F= (body)->
	
	global= F.global 
	
	
	F.Mime= require("mime-types")
	
	
	body= body or {}
	if body.f 
		fileServe= new core.VW.Http.StaticServe.Server()
		fileServe.addPath(global.UserContext.getHomeDir())
		if global.context?.request?.method is "HEAD"
			fileServe.getHead= true
		global.context?.request?.url= "/" + body.f
		await fileServe.handle(global.context)
		task= new core.VW.Task()
		global.context.response.on("finish" ,task.finish.bind task)
		await task
		return 
		
	
	
		
	if not global.publicContext.PublicIP
		PublicIP= await global.UserContext.require "public-ip", "3.0.0"
		global.publicContext.PublicIP= await PublicIP.v4()
	
	
	
	if body.account and body.path 
		
		St= new DriveFileStream body.account, body.path, body.options
		
		if body.emit 
			task= new core.VW.Task()
			rst= global.context.request
			rst.on "error", (e )->
				task.exception= e
				task.finish()
			St.on "error", (e )->
				task.exception= e
				task.finish()
			St.on "finish", task.finish.bind task 
			rst.pipe St
			await task 
			return yes
		
		return St
	
	return DriveFileStream
	
	

module.exports= (global)->
	F.global= global 
	F 