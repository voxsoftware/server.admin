IO= core.System.IO
import Stream from 'stream'
import Path from 'path'
import fs from 'fs'


F= (body)->
	
	global= F.global 
	cacheMax= 3.4* 1024 * 1024 * 1024
	free= 80 * 1024 * 1024  
	part= body.part

	F.checkFileExists= (path)->
		return new Promise (resolve,reject)->
			fs.access path, fs.F_OK, (error)->
				resolve not error 
	
	
	if not global.publicContext.LocalDB 
		await global.userFunction("LocalDB").invoke()
		
	
	
	
	t= global.publicContext.LocalDB
	
	config= await t.tryGetString("drive.cache.0")
	config= JSON.parse config.toString() if config 
	
	if not config 
		config=
			cacheMax: 3.4 * 1024 * 1024 * 1024 
			freeAmount: 80 * 1024 * 1024
		await t.put "drive.cache.0", JSON.stringify(config)
	else 
		cacheMax= config.cacheMax
		free= config.freeAmount


	
	
	home= F.global.UserContext.getHomeDir()
	home= Path.join home, ".remote.cache"
	if not (await F.checkFileExists(home))
		await IO.Fs.async.mkdir home
	else 
		
	
	
	
	file= Path.join home, part.id 
	if not (await F.checkFileExists(file))
		rfiles = []
		
		
		r= await t.tryGetString("drive.cache.1")
		r= JSON.parse r if r 
		
		
		if not r
			
			# enabling drive.cache.config 
			r= 
				control: 1
				size: 0 
			await t.put "drive.cache.1", JSON.stringify(r)
			
			
			files= await IO.Fs.async.readdir home 
			for file in files 
				ufile= Path.join home, file
				stat = await IO.Fs.async.stat ufile 
				
				
				file_u= 
					mtime: stat.mtime.getTime()
					file: ufile
					name: file 
					size: stat.size 
				
				rfiles.push file_u
				r.size += stat.size
				
		rfiles.sort (a,b)->
			return if a.mtime > b.mtime then 1 else (if b.mtime > a.mtime then -1 else 0)
		
		for file, i in rfiles
			file= 
				type: 'put'
				key: 'drive.cache.2.' + file.name 
				value: JSON.stringify file
			
			rfiles[i]= file
		
		# insert all files 
		await t.batch rfiles
		
		
		
			
		
		console.log "File not cached. Making cache"
		filet= Path.join home, part.id + ".cache"
		file= Path.join home, part.id
		
		try 
			
			response= await F.global.userFunction("remote/drive.get_href").invoke
				accountfile: body.accountfile
				id: part.id
			
			
			task= new core.VW.Task()
			st= fs.createWriteStream filet 
			st.on "error", (e)->
				task.exception= e 
				task.finish()
			
			st.on "finish", task.finish.bind task 
			
			
			
			req= new core.VW.Http.Request response.href
			req.analizeResponse= false 
			req.beginGetResponse()
			req.innerRequest.pipe st 
			
			await task 
			await core.VW.Task.sleep 50
			stat= await IO.Fs.async.stat filet 
			if stat.size isnt part.size 
				if body.retry is undefined
					body.retry= 0
				body.retry++
				if body.retry > 2
					throw global.Exception.create "Failed caching the response: #{part.id} #{stat.size} #{part.size}", "WRITE_ERROR"
				else
					return await global.userFunction("drive.cache").invoke body
			await IO.Fs.async.rename filet, file
			
			
			info= 
				mtime: stat.mtime.getTime()
				file: file 
				name: part.id 
				size: stat.size 
				
				
			await t.put "drive.cache.2." + part.id, JSON.stringify(info)
			r.size += stat.size 
			await t.put "drive.cache.1", JSON.stringify(r)
			
			
			
			
		catch e
			try 
				if await F.checkFileExists filet 
					await IO.Fs.async.unlink filet 
				else if await F.checkFileExists file
					await IO.Fs.async.unlink file
			
			
			throw e
	
	
	if r and r.size > cacheMax
		
		freed = 0
		i = 0
		
		
		# get some info 
		result= await t.readMany 
			gte: "drive.cache.2."
			lt: "drive.cache.3"
			limit: 20
		
		
		
		if result.data.length
			
			
			# convert to array
			cache= []
			for d in result.data 
				d= d.value.toString()
				d= JSON.parse d 
				cache.push d 
			
			toremove= []
			while freed < free 
				f= cache[i]
				if not f
					break 
					
					
				if f.name isnt part.id 
				
					try 
						
						toremove.push f.name
						
						await IO.Fs.async.unlink f.file 
						freed += f.size
						r.size -= f.size 
						
					catch e 
						console.error "ERROR FREEING CACHE ... ", e
				i++
			
			
			if toremove.length
				for id, i in toremove
					toremove[i]= 
						type:'del'
						key: "drive.cache.2.#{id}"
				await t.batch toremove
			
			await t.put "drive.cache.1", JSON.stringify(r)
			console.log "Cached freed. ", freed
		
	
	return file 

module.exports= (global)->
	F.global= global 
	F 