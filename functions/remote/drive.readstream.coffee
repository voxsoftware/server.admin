IO= core.System.IO
import Stream from 'stream'
import Path from 'path'
import fs from 'fs'
class DriveFileStream extends Stream.Readable 
	
	constructor:(accountFile, path, options)->
		super()
		@options= options
		@account= accountFile
		@path= path 
		
	

	
	_raiseError: (e,code)->
		
		@reading= false 
		@haveError= true
			
		er= F.global.Exception.create e.message or e.toString()
		if e?.code or code
			er.putCode e?.code or code 
		@emit "error", er
		
	
	emitHeaders: ()->
		headers = {}
		
		headers.status= 200
		headers["content-disposition"] = "attachment;filename=" + Path.basename(@path)
		if not @fileinfo 
			throw  F.global.Exception.create("File not found: #{@path}").putCode("FILE_NOT_FOUND")
			
		if @fileinfo.type or @options.mimeType
			headers["content-type"] = @fileinfo.type or @options.mimeType
		
		if @fileinfo.size 
			headers["content-length"]= @fileinfo.size
		
		@emit "headers", headers
		
	_read: ()->
		try 
			
			# get info from file 
			if not @fileinfo 
				@fileinfo= await F.global.userFunction("remote/drive.get").invoke
					accountfile: @account
					path: @path
			
				
				@emitHeaders()
				
			
			
				@parts= @fileinfo.parts 
				if not @parts 
					@push null
					return 
				
				@size=0
				for part in @parts 
					@size+= @parts.size 
				@cpart= 0
			
			
			@begin()
		catch e 
			@_raiseError e
	
	
	begin:()->
		
		try 
			
			return if @_destroyed 
				
			return @str.resume() if @str 
				
			part= @parts[@cpart]
			if not part
				@push null 
				return 
				
			await @getCached part
		catch e 
			@_raiseError e
	
	_destroy: ()->
		console.log "Destroying ..."
		@_destroyed= yes
		if @str 
			@str.close()
		
		
	
	getCached: (part)->
		
		file= await F.global.userFunction("remote/drive.cache").invoke
			accountfile: @account
			part: part
		
		self= @
		console.log "File cached: ", file	
		str= @str= fs.createReadStream file 
		str.on "data", (data)->
			
			if self._destroyed
				return 
				
			if self.push(data) is no 
				# pause ...
				str.pause()
		
		str.on "error", (e)->
			self._raiseError e
		
		str.on "end", ()->
			delete self.str
			self.cpart++
			self.begin()
		
		
	
	###
	getCacheDir: ()->
		if not @cachedir
			home= F.global.UserContext.getHomeDir()
			home= Path.join home, ".remote.cache"
			@cachedir= home 
		
		return @cachedir
	###

F= (body)->
	
	global= F.global 
	
	
	if body.account and body.path
		St= new DriveFileStream body.account, body.path, body.options
		if body.emit 
			g= ()->
				return if g.good
				g.good= yes 
				St.destroy() if St
				
			global.context.request.once "close", g 
			global.context.request.socket.once "close", g 
			
		
			task= new core.VW.Task()
			
			St.on "headers", (headers)->
				if headers.status 
					global.context.response.statusCode= headers.status
					
				for id,val of headers
					global.context.response.setHeader id, val
			St.on "error", (e )->
				task.exception= e
				task.finish()
			global.context.response.on "finish", task.finish.bind task 
			St.pipe global.context.response
			return await task 
		return St 
		
	return DriveFileStream

module.exports= (global)->
	F.global= global 
	F 