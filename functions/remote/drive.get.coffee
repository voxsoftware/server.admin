
driveCode= ()->
	
	getFile1= (e)->
		json= null 
		try 
			
			if e.params.path 
				e.params.folders= e.params.path.split("/").filter (a)->
					return not not a 
			
			uname= e.params.folders[e.params.folders.length - 1]
			for fname,i in e.params.folders
				
				if i is (e.params.folders.length - 1)
					file= (foldery or DriveApp).getFilesByName(fname)
					if file.hasNext()
						file= file.next()
					else
						file= null
						folder= (foldery or DriveApp).getFoldersByName(fname)
						if folder.hasNext()
							folder= folder.next()
						else
							folder= null
				else
					folder1= (foldery or DriveApp).getFoldersByName fname 
					if not folder1.hasNext()
						throw new Error "File not found: " + e.params.path
					else 
						foldery= folder1.next()
			
			if file 
				if e.params.mode is "readdir"
					throw new Error "File is not a directory"
				json= file.getBlob().getDataAsString()
				json= JSON.parse json 
				
			if folder 
				if e.params.mode is "readdir"
					json= []
					foldersu= folder.getFolders()
					while foldersu.hasNext()
						folderu= foldersu.next()
						obj= 
							name: folderu.getName()
							id: folderu.getId()
							type: "dostream/vnd.application-folder"
						json.push obj 
					
					filesu = folder.getFilesByType("text/kodhe-conf")
					while filesu.hasNext()
						fileu= filesu.next()
						con= fileu.getBlob().getDataAsString()
						con= JSON.parse con 
						obj= 
							name: fileu.getName()
							id: fileu.getId()
							type: con.type
							size: con.size
						json.push obj 
				else 
					json= {}
					json.id= folder.getId()
					json.type= "dostream/vnd.application-folder"
					json.path= e.params.path 
			
		catch e 
			json= {}
			json.error = 
				message: e.message,
				stack: e.stack,
				code: e.code
		
		
		return json 
		
	getFile= (e)->
		json= getFile1 e 
		return ContentService.createTextOutput(JSON.stringify(json)).setMimeType(ContentService.MimeType.JSON)
	
	
	return getFile(e)

F= (body)->
	body= body or {}
	code1= driveCode.toString()
	code1= code1.substring(0, code1.length - 1)
	
	code= '(\n' + code1 + "})"
	#return code 
	
	body.code = code
	return await F.global.UserFunction("remote/drive.eval").invoke(body)



module.exports = (global)->
	F.global = global
	F
