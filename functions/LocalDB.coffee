
F= (body)->
	
	global= F.global 
	body= body or {}
	
	
	
	if global.publicContext.LocalDB 
		return global.publicContext.LocalDB 
	
	
	# use kowix.share great function 
	ctx = await global.getSiteContext("kowix")
	localDb= await ctx.userFunction("share").invoke
		ctxSite: (await global.getSite()).idSite
		method: "LocalDB_low"
		
	console.info "response from share:", localDb
	
	if localDb
		#  means that is clustered
		global.publicContext.LocalDB= localDb
	else 
		global.publicContext.LocalDB= await global.userFunction("LocalDB_low").invoke()
	return global.publicContext.LocalDB 


module.exports= (global)->
	F.global = global 
	F