import fs from 'fs' 

class LXD

	networkInfo: ({network})->
		if not network and arguments[0]
			network= arguments[0]
		
		pro= await F.childStream
			cmd: "lxc"
			arguments: ["network","show", network]
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("LXD_ERROR")
		
		data= content.stdout.toString 'utf8'
		return F.yaml.parse data 

	start: (name)->
		name= name.name or name
		pro= await F.childStream
			cmd: "lxc"
			arguments: ["start", name]
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("LXD_ERROR")
		
		data= content.stdout.toString 'utf8'
		return 
			content: data
	
	stop: ({name, force})->
		args= ["stop", name] 
		if force 
			args.push "--force"
		pro= await F.childStream
			cmd: "lxc"
			arguments: args 
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("LXD_ERROR")
		
		data= content.stdout.toString 'utf8'
		return 
			content: data

	list: (filter)->
		pro= await F.childStream
			cmd: "lxc"
			arguments: "list"
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("LXD_ERROR")
		
		data= content.stdout.toString 'utf8'
		return await @processContent data, filter

	destroy: (name)->
		return @delete name, yes 

	delete: (name, force)->
		args= ["delete", name]
		if force 
			args.push "--force"
		pro= await F.childStream
			cmd: "lxc"
			arguments: args 
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("LXD_ERROR")
		
		data= content.stdout.toString 'utf8'
		return 
			content: data 

			
	storageList: (filter)->
		pro= await F.childStream
			cmd: "lxc"
			arguments: ["storage","list"]
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("LXD_ERROR")
		
		data= content.stdout.toString 'utf8'
		return await @processContent data, filter

	networkList: (filter)->
		pro= await F.childStream
			cmd: "lxc"
			arguments: ["network","list"]
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("LXD_ERROR")
		
		data= content.stdout.toString 'utf8'
		
		return await @processContent data, filter
	
	imageList: (filter)->
		pro= await F.childStream
			cmd: "lxc"
			arguments: ["image","list"]
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("LXD_ERROR")
		
		data= content.stdout.toString 'utf8'
		return await @processContent data, filter


	importImage: ({file, alias})->
		pro= await F.childStream
			cmd: "lxc"
			arguments: ["image","import", file, "--alias", alias]
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("LXD_ERROR")
		
		data= content.stdout.toString 'utf8'
		return await @processContent data

	storageCreate: ({name, backend='dir', params})->
		cmd= 'lxc'
		args= ['storage','create', name, backend]
		if params 
			for id,val of params 
				args.push "#{id}=#{val}"
		
		pro= await F.childStream
			cmd: cmd 
			arguments:args 
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("LXD_ERROR")
		
		data= content.stdout.toString 'utf8'
		#return await @processContent data
		return 
			content: data

	networkCreate: ({name, params})->
		cmd= 'lxc'
		args= ['network','create', name]
		if params 
			for id,val of params 
				args.push "#{id}=#{val}"
		
		pro= await F.childStream
			cmd: cmd 
			arguments:args 
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("LXD_ERROR")
		
		data= content.stdout.toString 'utf8'
		#return await @processContent data
		return 
			content: data


	launch: (options)->
		options.autostart= yes
		return @init(options)

	init: ({name, image, params, storage, network, autostart})->

		cmd= 'lxc'
		args= [(if autostart then 'launch' else "init"), image, name]
		if params
			for id, val of params 
				args.push "-c"
				args.push "#{id}=#{val}"
		if storage 
			args.push "-s"
			args.push storage
		
		if network 
			args.push "-n"
			args.push network
		
		pro= await F.childStream
			cmd: cmd 
			arguments:args 
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("LXD_ERROR")
		
		data= content.stdout.toString 'utf8'
		return 
			content: data
		#return await @processContent data


	getcontent: (pro)->
		task= new core.VW.Task()
		allcontent= []
		allerrcontent= []

		pro.pipe1().on "data", (con)->
			allcontent.push(con)
		
		pro.pipe2().on "data", (con)->
			allerrcontent.push con
		
		pro.on "finish", ()->
			allcontent= Buffer.concat allcontent
			allerrcontent= Buffer.concat allerrcontent
			task.result=
				stdout: allcontent 
				stderr: allerrcontent
			
			task.finish()
		
		pro.on "error", (e)->
			task.exception= e 
			task.finish()
		
		return await task 

	processContent: (data, filter, processor)->
		lines= data.split("\n")
		body= []
		header= null
		for part in lines 
			if part.startsWith("|") or part.startsWith("│")
				#return part
				items= part.split part[0]
				if not header 
					items = items.map (a) -> a.trim()
					items.splice(0,1)
					items.splice(items.length - 1)
					header= items
				else 
					items = items.map (a) -> a.trim()
					items.splice(0,1)
					items.splice(items.length - 1)
					b= {}
					same= false 
					
					for item,y in items 
						if y is 0 and not item 
							b= lastb or b 
							same= b is lastb

						prop= header[y] or y.toString()
						
						if b[prop] 
							if item 
								b[prop] += "\n" + item
						else
							b[prop] = item
					
					lastb = b
					body.push b if not same

		
		for b in body 
			for id of b 
				if id.indexOf("IP") >= 0 and b[id].trim()
					# parse ip 
					b[id]= @parseIp b[id]
				processor? b, id
	
		return body

	parseIp: (ip)->
		ip= ip.trim()
		return if not ip
		ifaces=[]
		iface=undefined
		while yes 
			i= ip.indexOf("(")
			if i >= 0
				iface= {}
				iface.ip= ip.substring(0,i).trim()
				y= ip.indexOf(")",i)
				iface.name= ip.substring(i+1, y).trim()
				ifaces.push iface 
				ip= ip.substring(y+1)
			else 	
				break 
		return ifaces


F= (body)->
	global= F.global 

	F.checkFileExists= (path)->
		return new Promise (resolve,reject)->
			fs.access path, fs.F_OK, (error)->
				resolve not error 

	
	F.childStream_o= global.userFunction("child.stream")
	await F.childStream_o.load()
	F.childStream= F.childStream_o.execute.bind F.childStream_o

	F.yaml= await F.global.UserContext.require("yamljs","0.3.0")

	return new LXD



module.exports= (global)->
	F.global= global 
	F