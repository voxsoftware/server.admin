F= (body)->
	global= F.global
	await global.userFunction("passwd.check").invoke(body)

	# get installed containers 
	LocalDB= await global.userFunction("LocalDB").invoke({})
	containers= await LocalDB.readMany
		gt: 'lxd.containers.'
		lt: 'lxd.containery'
	containers= containers.data
	return containers
	

module.exports= (global)->
	F.global= global 
	F