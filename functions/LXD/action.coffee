
F= (body)->
	global= F.global 
	await global.userFunction("passwd.check").invoke body 


	LXD= await global.userFunction("LXD/class").invoke()
	func= LXD[body.action]
	if not func 
		throw global.Exception.create("Action #{body.action} not valid").putCode("INVALID_ACTION")

	task= func.apply LXD, body.arguments or []
	return await task 


module.exports= (global)->
	F.global= global 
	F