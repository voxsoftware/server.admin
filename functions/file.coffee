import Child from 'child_process'
import fs from 'fs'
Fs= core.System.IO.Fs
import Path from 'path'

F= (body)->
	global= F.global 
	
	# check password ...
	await global.userFunction("passwd.check").invoke(body)
	
	if global.context?.response 
		if body.mode == 'stream' 
			
			if body.mode == 'writable'
				task= new core.VW.Task() 
				st= fs.createWriteStream body.file, body.options
				st.on "error", (er)->
					task.exception= er 
					task.finish() 
				st.on "end", ()->
					task.finish()
				
				global.context.request.pipe st 
				await task 
				
				
			else 
				
			
				
				###
				task= new core.VW.Task() 
				st= fs.createReadStream body.file, body.options
				st.on "error", (er)->
					task.exception= er 
					task.finish() 
				st.on "finish", ()->
					task.finish()
				
				if body.compression is "LZ4"
					LZ4= await global.UserFunction("lz4").invoke()
					st.pipe LZ4 
					st= LZ4
				
				st.pipe global.context.response
				await task 
				###
				
				fileServe= new core.VW.Http.StaticServe.Server()
				fileServe.addPath(Path.dirname(body.file))
				if global.context?.request?.method is "HEAD"
					fileServe.getHead= true
				global.context?.request?.url= "/" + Path.basename(body.file)
				await fileServe.handle(global.context)
				task= new core.VW.Task()
				global.context.response.on("finish" ,task.finish.bind task)
				await task
				
				
		else if body.mode == 'stat'
			f= body.file 
			if not (f instanceof Array )
				f=[f]
				
			response= []
			for file in f
				stat=  await Fs.async.lstat file
				stat.blockDevice= stat.isBlockDevice()
				stat.characterDevice= stat.isCharacterDevice()
				stat.directory= stat.isDirectory()
				stat.FIFO= stat.isFIFO()
				stat.file= stat.isFile()
				stat.socket= stat.isSocket()
				stat.symbolicLink= stat.isSymbolicLink()
				response.push stat 
			
			return response
			
		else if body.mode == 'readdir'
			return await Fs.async.readdir body.file	
		
module.exports= (global)->
	F.global= global 
	F 