import os from 'os'
import fs from 'fs'
Fs= core.System.IO.Fs
import fse from 'fs-extra'
import Path from 'path'
version= '10'

class Fsystem
	constructor: (@opts)->
		@cwd= '/'
		
	version:()->
		return version 
	
	
	currentDirectory: ()->
		@cwd 
	
	renew: ()->
		if not @time or Date.now() - @time > 10000
			await F.global.userFunction("ftp/filesystem").invoke @
			@time= Date.now()
		return @n0 if @n0 
		return @
		
	_normalize:(path, relative)->
		if Path.isAbsolute path 
			path= Path.normalize path 
		else 
			path= Path.normalize Path.join(@currentDirectory(),path)
		
		return path if relative
			
		
		if @opts?.home
			path= path.substring 1 if path.startsWith "/"
			path= Path.join @opts.home, path
		
		return path
		
	get: (f)->
		f= @_normalize f
		await Fs.async.stat f
	
	list: (path)->
		path= @_normalize path
		stats=[]
		#console.info("LIST METHOD")
		try 
			files= await Fs.async.readdir path 
			for file in files 
				fpath= Path.join path, file 
				stat= await Fs.async.stat fpath 
				stat.name= file
				stats.push stat 
		catch e 			
			if e.code isnt "ENOENT"
				throw e
		stats
		


	chdir: (path)->
		#throw F.global.Exception.create "failed getting chdir"
		
		
		
		# determine if can cwd
		f= @_normalize path 
		stat= await Fs.async.stat f


		@cwd= @_normalize path, yes
		@cwd
	
	mkdir: (path)->
		path= @_normalize path
		await Fs.async.mkdir path 
		if @opts?.uid and @opts?.gid
			await Fs.async.chown path, @opts?.uid, @opts?.gid
	
	write: (filename, options)->
		
		filename= @_normalize filename
		if not Fs.sync.exists filename
			await Fs.async.writeFile filename,''
			if @opts?.uid and @opts?.gid
				await Fs.async.chown filename, @opts?.uid, @opts?.gid
		return fs.createWriteStream filename,options
	
	read: (filename, options)->
		filename= @_normalize filename
		return fs.createReadStream filename,options

	delete: (path)->
		path= @_normalize path
		stat= await Fs.async.stat path
		if stat.isDirectory()
			#await Fs.async.rmdir path 
			task= new core.VW.Task()
			fse.remove path, (e)->
				task.exception= e if e 
				task.finish()
			await task 

		else 
			await Fs.async.unlink path 
	
	rename: (from,to)->
		from= @_normalize from
		to= @_normalize to
		await Fs.async.rename from,to
	
	chmod: (path,mode)->
		path= @_normalize path
		await Fs.async.chmod path,mode 
	
	getUniqueName: ()->
		return F.global.uniqid()
	
		

F= (body)->
	global= F.global
	if body.n0?.version 
		if body.n0.version() isnt version
			v= new Fsystem(body.n0.opts)
			v.cwd= body.n0.cwd
			body.n0= v
			return v
		return body.n0
		
	if body.version 
		if body.version() isnt version
			v= new Fsystem(body.opts)
			v.cwd= body.cwd
			body.n0= v
			return v
		return body
		
	return new Fsystem(body)

module.exports= (global)->
	F.global= global
	F