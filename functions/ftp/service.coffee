import os from 'os'
F= (body)->
	global= F.global
	
	
	await global.userFunction("passwd.check")
	
	
	# get ftp configuration 
	
	localDB= await global.userFunction("LocalDB").invoke()
	FtpConfig= await localDB.tryGetString("ftp.service.#{body?.ftp or  'default'}")
	if not FtpConfig
		throw global.Exception.create("Config not found for ftp service").putCode("CONFIG_NOT_FOUND")
	FtpConfig= JSON.parse FtpConfig
	
	
		
	
	
	getIp = ()->
		ip=null
		ifaces = os.networkInterfaces()
		console.log(ifaces)
		Object.keys(ifaces).forEach (ifname)->
			alias= 0
			ifaces[ifname].forEach (iface)->
				if 'IPv4' isnt iface.family or iface.internal isnt no or iface.address.startsWith("10.")
					return
				
				if alias >= 1 
					undefined
				else
					ip = iface.address
				
				++alias
		ip
	
	
	if global.publicContext.ftpSrv
		await global.publicContext.ftpSrv.server.close()
			
		
	
	# "1xCFDG70E5fTNVzNsSYICl2stg4OB0nuXilkvfzfCovI"
	excelCode= ()->
		obj= null
		try 
			args= e.params
			excel= SpreadsheetApp.openById args.spreadSheet
			sheet= excel.getSheets()[0]
			range= sheet.getDataRange()
			count= range.getNumRows()
			for i in [1..count]
				user= range.getCell(i,2).getValue()
				password= range.getCell(i,3).getValue()
				kowiCode= range.getCell(i,1).getValue()
				
				if user is args.username and password.toString() is args.password and kowiCode is args.kowiCode
					
					obj=
						home: sheet.getRange(i,4).getValue()
						uid: sheet.getRange(i,5).getValue()
						gid: sheet.getRange(i,6).getValue()
					break
		catch er 
			obj= 
				message: er.message 
				code: er.code 
				stack: er.stack
		
		return ContentService.createTextOutput( JSON.stringify(obj) ).setMimeType(ContentService.MimeType.JSON)
	
	
	code= (f)->
		#ctx= await F.global.getSiteContext "disk.kodhe.com"
		code1 = f.toString()
		code1 = code1.substring(0, code1.length - 1)
		jscode = '(\n' + code1 + "})"
		return 
			code: jscode
			
			
			
	getSrv= ()->
		ftpSrv = await global.UserContext.require("ftp-srv", "4.0.0")
		Type = await global.UserContext.require("ftp-srv", "4.0.0", "src/commands/registration/type.js")
		Type.handler = ({ command } = {})->
			if /^A[0-9]?$/i.test(command.arg)
				this.transferType = 'binary'
			else if (/^L[0-9]?$/i.test(command.arg) || /^I$/i.test(command.arg)) 
				this.transferType = 'binary'
			else
				return this.reply(501)
			return this.reply(200, "Switch to #{this.transferType} transfer mode.")
	    
	    
		class FileSystem
			currentDirectory: ()->
				fs= await this.Fs.renew()
				return await fs.currentDirectory()
			
			
			get: ()->
				fs= await this.Fs.renew()
				return await fs.get(arguments...)
			
			list: ()->
				fs= await this.Fs.renew()
				#console.info 'here --list'
				return await fs.list(arguments...)
			
			chdir: ()->
				fs= await this.Fs.renew()
				return await fs.chdir(arguments...)
			
			mkdir: ()->
				fs= await this.Fs.renew()
				return await fs.mkdir(arguments...)
			
			write: ()->
				fs= await this.Fs.renew()
				return await fs.write(arguments...)
			
			read: ()->
				fs= await this.Fs.renew()
				return await fs.read(arguments...)
			
			
			delete: ()->
				fs= await this.Fs.renew()
				return await fs.delete(arguments...)
			
			rename: ()->
				fs= await this.Fs.renew()
				return await fs.rename(arguments...)
			
			chmod: ()->
				fs= await this.Fs.renew()
				return await fs.chmod(arguments...)
			
		class Server 
			listen: (port)->
				@server = new ftpSrv 
					url: 'ftp://' + getIp() + ":#{FtpConfig.port or '21'}", 
					greeting: FtpConfig.greeting or ['Welcome to FTP Server. Kodhe Solutions']
					pasv_range: FtpConfig.passivePort or "9222,9223"
				
				@server.on "login", @connect.bind(@)
				@server.on "end", @end.bind(@)
				@server.listen()
			
			close: ()->
				return @server.close()
			
			end: ()->
				console.log "ending..."
	        
			connect: (auth,resolve,reject)->
				try 
					
					if FtpConfig.users
						args= FtpConfig.users.filter (a)->
							a.name is auth.username and a.password is auth.password
						args= args[0]
						good = not not args
						
						
					else if FtpConfig.spreadSheet
						o= await code excelCode
						args= await global.userFunction("remote/drive.eval").invoke 
							account:FtpConfig.account
							code: o.code
							username: auth.username
							password: auth.password
							kowiCode: F.global.kowiCode
						
						good = not not args
				
					if not good 
						throw global.Exception.create("User or password not valid").putCode("ACCESS_DENIED").end()
					
					fs= new FileSystem()
					fs.Fs= await global.userFunction("ftp/filesystem").invoke args
					resolve 
						fs: fs
						
						
				catch e 
					reject e
		
		return Server


	if not global.publicContext.ftpSrv
		Server= await getSrv()
		fServer = new Server()
		fServer.listen()
		global.publicContext.ftpSrv= fServer
	
	return 
		ip:getIp()
	
	
module.exports= (global)->
	F.global= global
	F