import os from 'os'
F= (body)->
	global= F.global
	
	
	await global.userFunction("passwd.check")
	
	
	if not body 
		throw global.Exception.create "Invalid parameters"
	
	# set ftp configuration 
	localDB= await global.userFunction("LocalDB").invoke()
	await localDB.put("ftp.service.#{body.ftp or  'default'}", JSON.stringify(body.config or body))
	
	
	
	
module.exports= (global)->
	F.global= global
	F