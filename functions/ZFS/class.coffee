import fs from 'fs'
class ZFS

	pools: (filter)->
		zfs= await F.childStream
			cmd: "zpool"
			arguments: "list"
		
		content= await @getcontent zfs
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("ZFS_ERROR")
		
		data= content.stdout.toString 'utf8'
		if data.indexOf("no pools available")>=0
			return []

		return await @processContent data, filter

	listSnapshots: (filter)->
		zfs= await F.childStream
			cmd: "zfs"
			arguments: ["list","-t","snapshot"]
		
		content= await @getcontent zfs
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("ZFS_ERROR")
		
		data= content.stdout.toString 'utf8'
		if data.indexOf("no pools available")>=0
			return []

		return await @processContent data, filter

	list: (filter)->
		zfs= await F.childStream
			cmd: "zfs"
			arguments: "list"
		
		content= await @getcontent zfs
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("ZFS_ERROR")
		
		data= content.stdout.toString 'utf8'
		if data.indexOf("no pools available")>=0
			return []

		return await @processContent data, filter

	
	processContent: (data, filter)->
		lines= data.split("\n")
		header= lines.shift().split(" ").filter (a)-> not not a
		pools= []
		for line in lines 
			parts= lines.shift().split(" ").filter (a)-> not not a
			if parts.length > 1
				obj= {}
				for part,i in parts
					obj[header[i]]= part
				pools.push obj
			
		if filter 
			if typeof filter is "function"
				c= pools.filter filter
			else 

				c= pools.filter (a)->
					for id, val of filter 
						if a[id] isnt val 
							return nos
					return yes

			return c
		return pools
	
	snapshot: ({name, snapshot})->
		zfs= await F.childStream
			cmd: "zfs"
			arguments: ["snapshot", "#{name}@#{snapshot}"]	
		
		content= await @getcontent zfs
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("ZFS_ERROR")
		
		return 

	clone: ({src, name})->
		zfs= await F.childStream
			cmd: "zfs"
			arguments: ["clone", "#{src}", name]	
		
		content= await @getcontent zfs
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("ZFS_ERROR")
		
		return 



	destroy: (name)->
		zfs= await F.childStream
			cmd: "zfs"
			arguments: ["destroy", "-R", name]
		
		content= await @getcontent zfs
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("ZFS_ERROR")
		
		return 

	poolDestroy: (name)->
		zfs= await F.childStream
			cmd: "zpool"
			arguments: ["destroy", name]
		
		content= await @getcontent zfs
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("ZFS_ERROR")
		
		return 


	parseSize: (size)->
		realSize=parseInt size
		if typeof size is "string"
			ends= size[size.length-1].toUpperCase()
			if ends is "G"
				realSize*= (1024*1024*1024)
			else if ends is "M"
				realSize*= (1024*1024)
			else if ends is "K"
				realSize*= 1024
			else if not (/\d/.test ends)
				throw F.global.Exception.create("Invalid size #{size} ").putCode("INVALID_ARGUMENT")
		return realSize


	createVolume: ({name, size})->
		size= @parseSize size 
		zfs= await F.childStream
			cmd: "zfs"
			arguments: ["create", "-V", size, name]

		content= await @getcontent zfs
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("ZFS_ERROR")
		
		return

	datasetCreate: (options)->
		return @createDataset options


	rename: (name, newname)->
		zfs= await F.childStream
			cmd: "zfs"
			arguments: ["rename", name, newname]

		content= await @getcontent zfs
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("ZFS_ERROR")

		return 
					
	createDataset: ({name, size})->
		if not name and arguments[0]
			name= arguments[0]
		size= @parseSize size  if size 
		zfs= await F.childStream
			cmd: "zfs"
			arguments: ["create", name]

		content= await @getcontent zfs
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("ZFS_ERROR")
		

		# set quota
		if size 
			await @set 	
				name: name 
				param: "quota"
				value: size
		return

	


	###
	createAutomountVolume: ({name, size, path, format='xfs'})->
		await @createVolume name, size 
		await @formatVolume name, format
	###

	


	formatVolume: ({name, format='xfs'})->
		dev= "/dev/zvol/#{name}"
		args=[]
		if format is 'xfs'
			cmd= "mkfs.xfs"
			args.push "-f"
		else if format is 'ext4'
			cmd= 'mkfs.ext4'
			args.push "-f"
		args.push dev 

		p= await F.childStream
			cmd: cmd
			arguments: args

		content= await @getcontent p
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("ZFS_ERROR")


	disableCompression: (name)->
		return await @enableCompression name?.name or name, "off"

	set:({name, param, value})->
		zfs= await F.childStream
			cmd: "zfs"
			arguments: ["set", "#{param}=#{value}", name]
		
		content= await @getcontent zfs
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("ZFS_ERROR")
		
		return 
	
	get:({name, param})->
		zfs= await F.childStream
			cmd: "zfs"
			arguments: ["get", "#{param}", name]
		
		content= await @getcontent zfs
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("ZFS_ERROR")
		
		return await @processContent content.stdout.toString()

	enableCompression: (options)->
		name= options.name 
		compression= options.compression or "lz4"
		name= options if not name

		return await @set 
			param: "compression"
			value: compression
			name: name

	createRawFile: ({size, blockSize="1M", path})->

		if ( await F.checkFileExists(path))
			throw F.global.Exception.create("File #{path} exists").putCode("FILE_EXISTS")
		
		realBlockSize= @parseSize blockSize 
		realSize= @parseSize size 

		blocks= realSize / realBlockSize
		if blocks isnt parseInt(blocks)
			throw F.global.Exception.create("Invalid arguments. Total size is not divisible in blockSize").putCode("INVALID_ARGUMENT")

		p= await F.childStream
			cmd: "dd"
			arguments: ["if=/dev/null", "of=#{path}", "bs=#{realBlockSize}", "seek=#{blocks}"]

		content= await @getcontent p
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			if err.indexOf("failed") >= 0
				throw F.global.Exception.create(err).putCode("DD_ERROR")

		return 
			output: err
			created: yes

	poolCreate: ({input, force, name, mode})->
		args= ["create"]
		args.push "-f" if force 

		args.push name
		args.push mode if mode 

		if typeof input is "string"
			args.push input 
		else 
			for in0 in input 
				args.push in0


		

		zfs= await F.childStream
			cmd: "zpool"
			arguments: args
		
		content= await @getcontent zfs
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("ZFS_ERROR")
		
		data= content.stdout.toString 'utf8'
		return await @pools
			name: name

	
	getcontent: (zfs)->
		task= new core.VW.Task()
		allcontent= []
		allerrcontent= []

		zfs.pipe1().on "data", (con)->
			allcontent.push(con)
		
		zfs.pipe2().on "data", (con)->
			allerrcontent.push con
		
		zfs.on "finish", ()->
			allcontent= Buffer.concat allcontent
			allerrcontent= Buffer.concat allerrcontent
			task.result=
				stdout: allcontent 
				stderr: allerrcontent
			
			task.finish()
		
		zfs.on "error", (e)->
			task.exception= e 
			task.finish()
		
		return await task 


F= (body)->
	global= F.global 

	F.checkFileExists= (path)->
		return new Promise (resolve,reject)->
			fs.access path, fs.F_OK, (error)->
				resolve not error 

	
	F.childStream_o= global.userFunction("child.stream")
	await F.childStream_o.load()
	F.childStream= F.childStream_o.execute.bind F.childStream_o

	return new ZFS



module.exports= (global)->
	F.global= global 
	F