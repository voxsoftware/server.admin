
F= (body)->
	global= F.global 
	await global.userFunction("passwd.check").invoke body 


	ZFS= await global.userFunction("ZFS/class").invoke()
	func= ZFS[body.action]
	if not func 
		throw global.Exception.create("Action #{body.action} not valid").putCode("INVALID_ACTION")

	task= func.apply ZFS, body.arguments or []
	return await task 


module.exports= (global)->
	F.global= global 
	F