Fs= core.System.IO.Fs

F= (body)->
	global= F.global 
	#passwd= await global.userFunction("passwd.generate").invoke({})
	
	t= global.publicContext.LocalDB
	if not t
		t= await global.userFunction("LocalDB").invoke()
	
	
	user = body.username or "root"
	
	
	#t= global.publicContext.LocalDB.table("user")
	#item= await t.find
	#	name: user
	#item= item[0]
	
	
	item = await t.tryGetString "user.#{user}"
	item = JSON.parse item if item 
	
	if not item and user is "root"
		bcrypt= await global.UserContext.require("bcryptjs","2.4.3") if not bcrypt
		salt= bcrypt.genSaltSync 10 
		hash= bcrypt.hashSync "admin",  salt
		item= 
			password: hash 
			name: 'root'
			created: Date.now()
			
		
		await t.put "user.#{user}", JSON.stringify(item)
		if body.password is "admin"
			return item

	if item 		
		bcrypt= await global.UserContext.require("bcryptjs","2.4.3") if not bcrypt
		ok= bcrypt.compareSync body.password or "", item.password
		
		
	if not ok 
		throw global.Exception.create("Access denied. Invalid password").putCode("ACCESS_DENIED")
	
	return item
	
module.exports= (global)->
	F.global= global 
	F 
	
	