Fs= core.System.IO.Fs
import Path from 'path'

F= (body)->
	global= F.global 
	item= await global.userFunction("passwd.check").invoke body
	
	
	bcrypt= await global.UserContext.require("bcryptjs","2.4.3") if not bcrypt
	salt= bcrypt.genSaltSync 10 
	hash= bcrypt.hashSync body.newpassword,  salt
	
	item.password= hash 
	item.updated= Date.now()
	
	username = body.username or "root"
	t = global.publicContext.LocalDB
	t.put "user.#{username}", JSON.stringify(item)
	
	
	
module.exports= (global)->
	F.global= global 
	F
