import Child from 'child_process'
import fs from 'fs'
import Zlib from 'zlib'

G= (body)->
	global= F.global 
	
	try 
		
			
		
		if not body.type 
			throw global.Exception.create "Specify the pipe type"
		
		
		
		# generate link 
		if body.generatelink
			task= new core.VW.Task()
			buf= Buffer.from JSON.stringify body 
			Zlib.gzip buf, (er,data)->
				task.exception= er if er
				task.result= data 
				task.finish() 
				
			buf= await task 
			part= encodeURIComponent(buf.toString 'base64')
			h1= global.context.request.headers 
			https= "http://"
			if h1["cf-visitor"]?.indexOf("https")
				https= "https://"
			idSite= (await global.getSite()).idSite
			return 
				url:https + h1.host + "/site/" + idSite + "/api/function/c/pipe?g=" + part 
			
		
		
		# pipe.0 
		# pipe.1
		# pipe.2
		
		if typeof body.pipes is "string"
			pipes= body.pipes.split("$$")
			body.pipes= {}
			for pipe in pipes 
				i= pipe.indexOf(">")
				if i > 0
					body.pipes[pipe.substring(0, i)]= pipe.substring(i+1)
			
		else 
			body.pipes= body.pipes or {}
			
		for pipec in ["pipe.0","pipe.1","pipe.2"]
			
			c= body[pipec]
			if typeof c is "string" and c 
				
				if c.startsWith("child.stream:") 
					k= c.split(":")
					c= 
						type: 'child.stream'
						cmd: k[1]
						arguments: k[2]
						"pipe.0": body.pipes[k[3]]
						"pipe.1": body.pipes[k[4]]
						"pipe.2": body.pipes[k[5]]
						pipes: body.pipes 
						
				else if c.startsWith("remote/drive.readstream:")
					k= c.split(":")
					c= 
						type: 'remote/drive.readstream',
						path: k[2]
						account: k[1]
						"pipe.0": body.pipes[k[3]]
						"pipe.1": body.pipes[k[4]]
						"pipe.2": body.pipes[k[5]]
						pipes: body.pipes 
						
					if c.account.startsWith("base64,")
						c.account= JSON.parse(Buffer.from(c.account,'base64').toString())
				
				else if c.startsWith("remote/drive.writestream:")
					k= c.split(":")
					c= 
						type: 'remote/drive.writestream',
						path: k[2]
						account: k[1]
						"pipe.0": body.pipes[k[3]]
						"pipe.1": body.pipes[k[4]]
						"pipe.2": body.pipes[k[5]]
						pipes: body.pipes 
						
					if c.account.startsWith("base64,")
						c.account= JSON.parse(Buffer.from(c.account,'base64').toString())
						
						
				else if c.startsWith("file@write:")
					k= c.split(":")
					c= 
						type: 'file@write'
						path: k[1]
						"pipe.0": body.pipes[k[2]]
						"pipe.1": body.pipes[k[3]]
						"pipe.2": body.pipes[k[4]]
						pipes: body.pipes 
					
				else if c.startsWith("file@read:")
					k= c.split(":")
					c= 
						type: 'file@read'
						path: k[1]
						"pipe.0": body.pipes[k[2]]
						"pipe.1": body.pipes[k[3]]
						"pipe.2": body.pipes[k[4]]
						pipes: body.pipes 
				
				else if c.startsWith("request:")
					k= c.split(":")
					c=
						type: 'request'
						"pipe.0": body.pipes[k[1]]
						"pipe.1": body.pipes[k[2]]
						"pipe.2": body.pipes[k[3]]
						pipes: body.pipes 
					
				else if c.startsWith("response:")
					k= c.split(":")
					c=
						type: 'response'
						"pipe.0": body.pipes[k[1]]
						"pipe.1": body.pipes[k[2]]
						"pipe.2": body.pipes[k[3]]
						pipes: body.pipes 
				else if c.startsWith("response.pretty")
					k= c.split(":")
					c=
						type: k[0]
						channel: k[1]
						encoding: k[2]
						"pipe.0": body.pipes[k[3]]
						"pipe.1": body.pipes[k[4]]
						"pipe.2": body.pipes[k[5]]
						pipes: body.pipes 
					
				else if c.startsWith("response")
					k= c.split(":")
					c=
						type: k[0]
						"pipe.0": body.pipes[k[1]]
						"pipe.1": body.pipes[k[2]]
						"pipe.2": body.pipes[k[3]]
						pipes: body.pipes 
				
				else 
					throw global.Exception.create("Pipe #{c} not supported").putCode("INVALID_PIPE")
					
		
				body[pipec]= c
				
			else if typeof c is "object"
				body[pipec]= c 
			else 
				delete body[pipec]
				
		
					
					
					
		
		if body.type is "request"
			Stream= global.context.request 
		else if body.type is "response"
			Stream= global.context.response 
		
		else if body.type is "file@read"
			Stream= fs.createReadStream(body.path)
		else if body.type is "file@write"
			Stream= fs.createWriteStream(body.path)
		else 
			Stream= await global.userFunction(body.type).invoke body
			
		
		if body.onerror 
			onerror = body.onerror 
		else 
			task= new core.VW.Task()
			onerror= (e)->
				console.error "error in pipe: ", e 
				if task 
					task.exception= e 
					task.finish() 
		
		
		Stream.on "error", onerror 
		Stream.on "i_error", onerror 


		pipe0= body["pipe.0"]
		pipe1= body["pipe.1"]
		pipe2= body["pipe.2"]
	
		if pipe0 
			# create pipe0 
			pipe0.password= body.password
			pipe0.onerror= onerror 
			pipe0.internal= 1
			Pipe0= await G pipe0	
			
			try 
				if not Stream 
					Stream= await global.userFunction(body.type).invoke body
			catch e 
				Pipe0.end?()
				Pipe0.destroy?()
				throw e

			if Stream.pipe0?()
				console.info "Piping to stdin ..."	
				Pipe0.pipe Stream.pipe0()
			else  
				Pipe0.pipe Stream
			Stream.$pipe0= Pipe0
		
		if pipe1 
			#return pipe1
			console.log "PIPE : ", pipe1
			pipe1.password= body.password
			# create pipe0 
			pipe1.onerror= onerror 
			pipe1.internal= 1
			Pipe1= await G pipe1
			
			try 
				if not Stream 
					Stream= await global.userFunction(body.type).invoke body
			catch e 
				Pipe1.end?()
				Pipe1.destroy?()
				throw e
			if Pipe1.pipe0
				Stream.pipe Pipe1.pipe0()
			else 
				if typeof Pipe1.setHeader is "function"
					Stream.once "headers", (headers)->
						Pipe1.status= headers.status if headers.status
						for id,val of headers 
							Pipe1.setHeader(id,val)
						
				Stream.pipe Pipe1 
			Stream.$pipe1= Pipe1 

		if pipe2
			
			
		
		
			pipe2.password= body.password
			
			pipe2.onerror= onerror 
			pipe2.internal= 1
			Pipe2= await G pipe2
			
			try 
				if not Stream 
					Stream= await global.userFunction(body.type).invoke body
				PipeIn2= Stream.pipe2?()
				if not PipeIn2
					throw global.Exception.create "PIPE doesn't expose stderr support"
			catch e 
				Pipe2.end?()
				Pipe2.destroy?()
				throw e 
			
				
			
			if Pipe2.pipe0
				PipeIn2.pipe Pipe2.pipe0()
			else 
				
				if typeof Pipe2.setHeader is "function"
					Stream.once "headers", (headers)->
						Pipe2.status= headers.status if headers.status
						for id,val of headers 
							Pipe2.setHeader(id,val)
		
				PipeIn2.pipe Pipe2
			
			Stream.$pipe2= Pipe2
		
		if not Stream
			Stream= await global.userFunction(body.type).invoke body
			

		if body.stderrAsError
			$pipe= Stream.pipe2?()
			if $pipe 
				$pipe.err= []
				$pipe.on "data", (e)->
					$pipe.err.push e 
				$pipe.end= ()->
					$pipe.err= Buffer.concat($pipe.err)
					Stream.emit "error", F.global.Exception.create($pipe.err.toString()).putCode("STDERR_ERROR")
				Stream.on "finish", $pipe.end
				Stream.on "end", $pipe.end

		
		if body.internal is 1 
			return Stream 
		else 
			
			
			cancel= ()->
				return if cancel.yet
				
				Stream.end?()
				Stream.destroy?() 
			
			global.context.request.once "close", cancel
			global.context.request.socket.once "close", cancel
			
			# finish when finish stream 
			onfinish= ()->
				return if not task 
				task.finish()
				task= null 
				
			Stream.on "finish", onfinish
			Stream.on "end", onfinish
			
			###
			if body.catchStderr 	
				countWatch= 0
				err= []
				pipe= Stream.pipe2?()
				if pipe 
					pipe.on "data", (d)->
						err.push d.toString()
			###		

			await task 
			
			#responseData= global.context.ResponseData
			if body.waitresponse
				#console.log "here 11...."
				task= new core.VW.Task()
				global.context.response.on "finish", task.finish.bind task
				await task 
				console.log "here"
				
			#return if responseData
				

		
	catch e 
		throw e 
	finally 

F= ()-> G
	
		
module.exports= (global)->
	F.global= global 
	F 