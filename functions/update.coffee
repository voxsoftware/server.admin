import Child from 'child_process'
import Path from 'path'

F= (body)->
	global= F.global 
	task= new core.VW.Task() 
	
	p= Child.spawn "git", ["pull"], 
		cwd: Path.join __dirname, ".."
	p.on "exit", ()->
		task.finish()
	await task 
	
module.exports= (global )->
	F.global= global 
	F 
	
	
	
	