import fs from 'fs' 
import Path from 'path'
Fs= core.System.IO.Fs
import Os from 'os'

class KWVM

	#deleteAllRules: ()->


	setAutoUploadFolder: ({path, remotepath, account})->
		


	removeRedirectRule: ({id, ip, port})->
		if not ip and not port and not id and arguments[0]
			id= arguments[0]
		
		if not id 
			id= "#{ip or ''}>#{port}"
		
		str= await F.LocalDB.tryGetString "kowixvm.redirect.rule.#{id}"
		if str 
			await F.LocalDB.del "kowixvm.redirect.rule.#{id}"
			setTimeout @tryGenerateRules.bind(@), 200
		
		return




	redirectRule: ({src, dest})->
		ip= src.ip or ""
		if not src.ip 
			ifaces = Os.networkInterfaces()
			for iname, ifacesi of ifaces 
				for iface in ifacesi 
					if iface.family isnt "IPv4" or iface.internal isnt no 
						continue 
					if iface.address.endsWith ".1"
						continue 

					src.ip= iface.address
					break 
			
		if not dest.machine and not dest.ip
			throw F.global.Exception.create("You should specify machine for port redirection")
		
		if not dest.ip 
			vm= await @getVM(dest.machine)
			if not vm 
				throw F.global.Exception.create("Machine #{dest.machine} not found")
		
			if vm.type isnt "lxd"
				throw F.global.Exception.create("Just now only lxd machines are supported").putCode("NOT_IMPLEMENTED")
			
			lxd= await @lxd()
			list= await lxd.list()
			machine= list.filter (a)-> a.NAME is dest.machine
			if not machine.length 
				throw F.global.Exception.create("Machine #{dest.machine} not found")
			machine= machine[0]

			dest.ip= machine.IPV4?[0]?.ip 
			if not dest.ip 
				throw F.global.Exception.create("Failed getting ip from #{dest.machine}. Is powered on?").putCode("FAILED_GET_IP")
			
		obj= 
			id: "kowixvm.redirect.rule.#{ip}>#{src.port}"
			src: src
			dest: dest 
		str= JSON.stringify obj 
		await F.LocalDB.put obj.id, str	


		setTimeout @tryGenerateRules.bind(@), 200
		return obj


	
	tryGenerateRules: ()->
		try 
			@generateRules()

	generateRules: ()->

		fw = "iptables -I FORWARD -m state -d $MASK --state NEW,RELATED,ESTABLISHED -j ACCEPT -m comment --comment \"$COMMENT\""
		fp1= "iptables -t nat -A POSTROUTING -s $MASK -p tcp -m multiport --dports $DESTPORT -j SNAT --to-source $IPSRC -m comment --comment \"$COMMENT\""
		fp2= "iptables -t nat -I PREROUTING -p tcp -d $IPSRC --dport $PORT -j DNAT --to-destination $IPVM:$DESTPORT -m comment --comment \"$COMMENT\""
		fp3= "iptables -t nat -A OUTPUT -o lo -p tcp -m multiport --dports $PORT -j DNAT --to-destination $IPVM:$DESTPORT -m comment --comment \"$COMMENT\""


		masks= {}

		data= await F.LocalDB.readMany
			gt: "kowixvm.redirect.rule."
			lte:"kowixvm.redirect.rulf."
		rules=[]
		for d in data.data 
			d= JSON.parse d.value 
			rules.push d 
		
		iptrules=[]
		srules= []
		for rule in rules 
			mask= rule.dest.ip.split(".")
			mask[mask.length-1]= "1/24"
			mask= mask.join "."

			if not masks[mask]
				masks[mask]= fw.replace "$MASK", mask 
				masks[mask]= masks[mask].replace "$COMMENT", "kowixvm_generated_forward_for:#{mask}"
			
			srules.push "kowixvm_generated_for_rule:#{rule.id}"
			cont1= fp1.replace "$MASK", mask
			cont1= cont1.replace "$DESTPORT", rule.dest.port 
			cont1= cont1.replace "$IPSRC", rule.src.ip 
			cont1= cont1.replace "$COMMENT", "kowixvm_generated_for_rule:#{rule.id}"
			iptrules.push cont1 


			cont1= fp2.replace "$IPSRC", rule.src.ip 
			cont1= cont1.replace "$DESTPORT", rule.dest.port 
			cont1= cont1.replace "$PORT", rule.src.port 
			cont1= cont1.replace "$IPVM", rule.dest.ip 
			cont1= cont1.replace "$COMMENT", "kowixvm_generated_for_rule:#{rule.id}"
			iptrules.push cont1 


			cont1= fp3.replace "$IPSRC", rule.src.ip 
			cont1= cont1.replace "$DESTPORT", rule.dest.port 
			cont1= cont1.replace "$PORT", rule.src.port 
			cont1= cont1.replace "$IPVM", rule.dest.ip 
			cont1= cont1.replace "$COMMENT", "kowixvm_generated_for_rule:#{rule.id}"
			iptrules.push cont1 


		# descubrir que reglas deben eliminarse primero 
		strru=[]
		iptablesinfonat= await @iptables(yes)
		iptablesinfo= await @iptables()

		process= (iptablesinfo, CHAIN, mask)->

			str= "kowixvm_generated_for_rule:"
			if mask 
				str= "kowixvm_generated_forward_for:#{mask}"
			

			if iptablesinfo[CHAIN].rules
				toDelete=[]
				while true 
					continue_= 0
					for rule,i in iptablesinfo[CHAIN].rules
						if rule.description.indexOf(str) >= 0
							return yes if mask 
							#delete this rule 
							toDelete.push i + 1 
							continue_= 1
							iptablesinfo[CHAIN].rules.splice i, 1
							break 
					
					if continue_ is 0 
						break 
				
				for del in toDelete
					strru.push "iptables -t nat -D #{CHAIN} #{del}"
			
			return no
			
		process iptablesinfonat,"PREROUTING"
		process iptablesinfonat,"POSTROUTING"
		process iptablesinfonat,"OUTPUT"

		for mask,value of masks 
			if not process(iptablesinfo, "FORWARD", mask)
				strru.push value 
		
		for rule in iptrules
			strru.push rule

		
		code= """
			#!/usr/bin/env bash
			#{strru.join("\n")}
			"""

		file= Path.join F.global.UserContext.getHomeDir(), F.global.uniqid()
		await Fs.async.writeFile file, code 

		try 

			pro= await F.childStream
				cmd: "bash"
				arguments: [file]
			
			content= await @getcontent pro
			if content.stderr?.length 
				err= content.stderr.toString 'utf8'
				throw F.global.Exception.create(err).putCode("LXD_ERROR")



		catch e 
			throw  e 
		finally 
			if await F.checkFileExists file 
				await Fs.async.unlink file 
		
		return 
			rules: iptrules
			lines: strru
		




	iptables: (nat)->
		args=["-L"]	
		if nat 	
			args.push "-t"
			args.push "nat"
		pro= await F.childStream
			cmd: "iptables"
			arguments: args
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("LXD_ERROR")

		data= content.stdout.toString 'utf8'
		lines= data.split "\n"
		chains={}
		cchain= undefined 
		for line in lines 
			line= line.trim()
			if line.startsWith "Chain"
				chain= line.split(" ")[1]
				cchain= chains[chain]=chains[chain] or {}
			else if line and chain and not cchain.header				
				cchain.header= line.split(" ").filter (a)-> not not a 
				cchain.header.push "description"
			
			else if line  and chain
				parts= line.split(" ").filter (a)-> not not a
				rule= {}
				cchain.rules= cchain.rules or []
				for i in [0...4]
					rule[cchain.header[i]]= parts[i]
				rule[cchain.header[5]]= parts.slice(5).join(" ")
				cchain.rules.push rule
		#if not nat 
		#	other= await @iptables yes 
		#	chains= Object.assign other, chains
		return chains
				







	list: (filter)->
		pro= await F.childStream
			cmd: "lxc"
			arguments: "list"
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("LXD_ERROR")
		
		data= content.stdout.toString 'utf8'
		return await @processContent data, filter



	zfs: ()->		
		@$zfs= await F.global.userFunction("ZFS/class").invoke() if not @$zfs
		@$zfs

	lxd: ()->		
		@$lxd= await F.global.userFunction("LXD/class").invoke() if not @$lxd
		@$lxd 


	defineDefaultPool: ({name})->	
		if not name and arguments[0]
			name= arguments[0]
		await F.LocalDB.put "kowixvm.zfs.pool", name

	
	poolCreate: (options)->	
		zfs= await @zfs()
		await zfs.poolCreate options
		if options.name 
			defaultPool= await  F.LocalDB.tryGetString("kowixvm.zfs.pool")
			if not defaultPool
				await @defineDefaultPool options.name 
		

	networkList: (options)->
		lxd= await @lxd()
		return await lxd.networkList(options)


	

	replaceMdadmWithZpool: (options)->

		dev= options.raidDevice 
		pro= await F.childStream
			cmd: "mdadm"
			arguments: ["--detail", dev]
		
		
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("KOWIXVM_ERROR")
		
		content= content.stdout.toString()
		lines= content.split("\n")
		devices= []
		for i in [(lines.length -1) ... 0]
			line = lines[i]
			if line.indexOf("active sync") >= 0
				parts= line.split(" ").map (a)-> not not a
				devices.push parts[parts.length-1]
		
		if not devices.length 
			throw F.global.Exception.create("Devices for this mdadm setup not found")
		


		pro= await F.childStream
			cmd: "umount"
			arguments: [dev]	
		content= await @getcontent pro


		pro= await F.childStream
			cmd: "mdadm"
			arguments: ["--stop", dev]	
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			throw F.global.Exception.create(err).putCode("KOWIXVM_ERROR")


		for device in devices
			# zero superblock 
			pro= await F.childStream
				cmd: "mdadm"
				arguments: ["--zero-superblock", device]	
			content= await @getcontent pro	
		
		ops= {}
		ops.name= options.name
		ops.input= devices 
		ops.mode= "mirror"
		ops.force= yes
		await @createPool ops

	createNetwork: ({name, params, ipblock})->

		lxd= await @lxd()


		name= name or "kwvm0"
		if not params 
			num= Math.round(Math.random() * 20)
			params=
				"ipv6.address":"none"
				"ipv4.address": ipblock or "10.0.#{num}.1/24"
				"ipv4.nat": true

		
		await lxd.networkCreate
			name: name 
			params: params 


	get: (options)->
		return @getVM options

	getVM: (name)->
		name= name.name or name
		vm= await F.LocalDB.tryGetString "kowixvm.vms.#{name}"
		if vm 
			vm= JSON.parse vm
			if not vm.snapshots
				vm= await @updateVMSnapshotInfo(name)
			
			return vm

	freeOldSnapshots: (options)->
		return @updateVMSnapshotInfo options

	updateVMSnapshotInfo: (name)->

		name= name.name or name

		# liberar los snapshots viejos 
		# deja siempre solo los dos últimos 
		vm= await F.LocalDB.tryGetString "kowixvm.vms.#{name}"
		updatevm= ()->
			str= core.safeJSON.stringify vm
			await F.LocalDB.put "kowixvm.vms.#{vm.name}", str
			await F.LocalDB.put "kowixvm.vms.#{vm.id}", str
		
		if vm 
			vm= JSON.parse vm
			zfs= await @zfs()

			# list 
			snapshots= await zfs.listSnapshots (a)->
				a.NAME.startsWith "#{vm.dataset}@"
			

			vm.snapshots= vm.snapshots or {}
			if snapshots.length > 2
				#eliminar viejos 
				for i in [0... (snapshots.length - 2)]
					snap= snapshots[i]
					rname= snap.NAME.split("@")[1]
					await zfs.destroy snap.NAME
					delete vm.snapshots[rname]

				snapshots.splice 0, snapshots.length - 2
			
			# save in vm config			
			for snap in snapshots
				rname= snap.NAME.split("@")[1]
				vm.snapshots[rname]= 
					name: rname 
					basename: snap.NAME
					refer: snap.REFER 

			await updatevm()
			
		return vm




	snapshot: ({name, snapshot})->
		vm= await F.LocalDB.tryGetString "kowixvm.vms.#{name}"
		updatevm= ()->
			str= core.safeJSON.stringify vm
			await F.LocalDB.put "kowixvm.vms.#{vm.name}", str
			await F.LocalDB.put "kowixvm.vms.#{vm.id}", str
		if vm 
			vm= JSON.parse vm
			zfs= await @zfs()

			vm.snapshots= vm.snapshots or {}
			# create an snapshot ...
			if not snapshot
				snapshot= core.VW.Moment().format("YYYYMMDD_HHmm") + "-" + F.global.uniqid()
			await zfs.snapshot
				name: vm.dataset 
				snapshot: snapshot 
			vm.snapshots[snapshot]=  
				created: Date.now()
				name: snapshot
			await updatevm()
			return 
				vm: vm 
				snapshot: snapshot
			


	getUuid: ()->
		return
			uuid: F.Uuid()



	export: ({name, compression= "lz4", incremental= no})->
		vm= await F.LocalDB.tryGetString "kowixvm.vms.#{name}"

		updatevm= ()->
			str= core.safeJSON.stringify vm
			await F.LocalDB.put "kowixvm.vms.#{vm.name}", str
			await F.LocalDB.put "kowixvm.vms.#{vm.id}", str
		if vm 
			vm= JSON.parse vm
			zfs= await @zfs()
			

			# backup dataset 
			bdataset = vm.dataset.split "/"
			bdataset[bdataset.length-2] = 'backups'
			bdataset = bdataset.join "/"
			try 
				await zfs.createDataset bdataset


			mountpoint= await zfs.get 
				name: bdataset
				param: "mountpoint"

			mountpoint= mountpoint[0]?.VALUE 
			if not mountpoint
				throw F.global.Exception "#{bdataset} doesn't have a mount point"

			vm.snapshots= vm.snapshots or {}
			# create an snapshot ...
			sname= core.VW.Moment().format("YYYYMMDD_HHmm") + "-" + F.global.uniqid()
			await zfs.snapshot
				name: vm.dataset 
				snapshot: sname 
			vm.snapshots[sname]=  
				created: Date.now()
				name: sname
			
			
			updatevm()
			
			
				

			
			ext= compression
			if ext is "gzip"
				ext= ".gz"
			else if ext is "off"
				ext= ""
			else 
				ext= ".#{ext}"
			bk= "#{mountpoint}/#{sname}.bk#{ext}"


			# send dataset ...
			nname= "#{vm.dataset}@#{sname}"
			try 
				
				pipe1= await F.global.userFunction("pipe.1").invoke()
				if compression and compression isnt "off"
					stream= await pipe1
						type: "child.stream"					
						"cmd": compression
						"pipe.0": 
							"type": "child.stream"
							cmd: "zfs"
							"arguments": ["send", nname]
							
						"internal": 1
				else 
					stream= await pipe1
						"type": "child.stream"
						cmd: "zfs"
						"arguments": ["send", nname]
						"internal": 1

				console.log "Writing to file ...", nname, bk

				# pipe to file 
				err=[]
				filest= fs.createWriteStream bk 
				task= new core.VW.Task() 
				if stream.$pipe0
					stream.$pipe0.pipe2().on "data", (er)->
						err.push er 
				else 
					stream.pipe2().on "data", (er)->
						err.push er 
					

				stream.pipe filest 
				stream.on "finish", task.finish.bind task 
				stream.on "error", (e)->
					task.exception= e 
					task.finish() 
				filest.on "error", (e)->
					task.exception= e 
					task.finish() 
				
				await task 
				if err.length 
					err= Buffer.concat err 
					err= err.toString()
					throw F.global.Exception.create("Export failed: #{err}").putCode("FAILED")


			catch e 
				if filest 
					filest.close()

				if await  F.checkFileExists(bk)
					await Fs.async.unlink bk

				# delete snapshot if error 
				try 
					await zfs.destroy nname
					delete vm.snapshots[sname]
					updatevm()



				throw e

			return 
				file: bk
				snapshot: sname

	getVolumes: ()->
		res= await F.LocalDB.readMany
			gte: "kowixvm.disks."
			lt: "kowixvm.disky"
		data= res.data
		volumes=[]
		for d in data 
			try 
				volume= JSON.parse d.value
				volumes.push volume
		volumes 

	autoInit: ()->
		# automount all disks 
		volumes= await @getVolumes()
		for volume in volumes
			try 
				pro= await F.childStream
					cmd: "mount"
					arguments: ["-o", "discard,noatime", "/dev/zvol/#{volume.name}", volume.mount]
				content= await @getcontent pro
				if content.stderr?.length 
					err= content.stderr.toString 'utf8'
					throw F.global.Exception.create(err).putCode("KOWIXVM_ERROR")
		

		# autostart lxd machines 
		vms= await @getVMs()
		for vm in vms
			try 
				if (vm.lastBoot  and not vm.lastStop) or (vm.lastBoot > vm.lastStop)
					# start machine 
					await @start vm.name


	getVMs: ()->
		res= await F.LocalDB.readMany
			gte: "kowixvm.vms."
			lt: "kowixvm.vmy"	
		data= res.data
		
		vms=[]
		t={}
		for d in data 
			try 
				vm= JSON.parse d.value
				if not t[vm.id]
					t[vm.id]= yes 
					vms.push vm 
		vms

	getOrCreateDefaultNetwork: (name)->
		
		network= await @getDefaultNetwork()
		console.log "NETWORK", network
		if not network     
			network= "kwvm0"  
			await @createNetwork
				name: network 
			await @defineDefaultNetwork network
		return network
	
	defineDefaultNetwork: (name)->	
		await F.LocalDB.put "kowixvm.network.default", name
	
	getDefaultNetwork: ()->
		return F.LocalDB.tryGetString "kowixvm.network.default"


	destroyVm: ({name, id, force})->
		#force quiere decir ignorar errores
		nameOrId= name or id 
		try
			vm= await F.LocalDB.tryGetString "kowixvm.vms.#{nameOrId}"
			if vm 
				zfs= await @zfs()
				vm= JSON.parse vm 
				
				if vm.type is "lxd"
					lxd= await @lxd()
					newname= vm.dataset.split("/")
					newname[newname.length-1]= "deleted.#{F.global.uniqid()}.#{newname[newname.length-1]}"
					newname= newname.join "/"

					type= await zfs.get 
						name: vm.dataset 
						param: "type"
					type= type?[0]?.VALUE

					if type is "volume"
						try
							await lxd.stop
								name: vm.name
								force: yes
						await @destroyDataset vm.dataset
						await lxd.destroy vm.name 
					else 
						try 
							await lxd.stop
								name: vm.name
								force: yes 
						await zfs.rename vm.dataset , newname
						await lxd.destroy vm.name 
						try 
							await zfs.destroy newname 
						catch e 
							vw.log "Failed deleting old dataset: ", newname

					

				else if vm.type is "kvm"
					# TODO 
					undefined 
		catch e 
			throw e  if not force 
			result= 
				warning: 
					message: e.message 
					code: e.code
		if vm 
			await F.LocalDB.del "kowixvm.vms.#{vm.name}"
			await F.LocalDB.del "kowixvm.vms.#{vm.id}"
		return result



	replaceVM: (vm, tempDataset)->
		#await @updateVMSnapshotInfo vm.name
		zfs= await @zfs()
		type= await zfs.get 
			name: tempDataset
			param: "type" 

		type= type?[0]?.VALUE 
		if not type 
			throw F.global.Exception.create("Dataset #{tempDataset} doesn't exists").putCode("NOT_FOUND")
		
		type2= await zfs.get 
			name: vm.dataset
			param: "type" 

		type2= type2?[0]?.VALUE 
		if not type2
			throw F.global.Exception.create("Dataset #{vm.dataset} doesn't exists").putCode("NOT_FOUND")
		
		if type2 is "filesystem"
			oldmountpoint= await zfs.get 
				name: vm.dataset
				param: "mountpoint"			

			oldmountpoint= oldmountpoint?[0]?.VALUE
			if not oldmountpoint
				throw F.global.Exception.create("Failed get mountpoint")

		else if type2 is "volume"
			vname= vm.dataset.split "/"
			uname= vname.pop()
			vname= vname.join "/"

			oldmountpoint= await zfs.get 
				name: vname
				param: "mountpoint"			
			oldmountpoint= oldmountpoint?[0]?.VALUE
			if not oldmountpoint
				throw F.global.Exception.create("Failed get mountpoint")
			
			oldmountpoint= Path.join oldmountpoint, uname


		if type is "filesystem"
			mountpoint= await zfs.get 
				name: tempDataset
				param: "mountpoint" 
			mountpoint= mountpoint[0]?.VALUE 
			if not mountpoint
				throw F.global.Exception.create("Failed determining mountpoint for dataset")
			

		else if type is "volume"
			# la parte díficil. Acá toca hacer un mount
			pro= await F.childStream
				cmd: "blkid"
				arguments: [ "/dev/zvol/#{tempDataset}"]

			content= await @getcontent pro
			if content.stderr?.length 
				err= content.stderr.toString 'utf8'
				if err.indexOf("error") >=0 or err.indexOf("invalid") >= 0
					throw F.global.Exception.create(err).putCode("KOWIXVM_ERROR")
				
			content= content.stdout.toString()
			if content.indexOf('TYPE="xfs"')>=0
				
				# good is xfs
				# generate new uid
				pro= await F.childStream
					cmd: "xfs_admin"
					arguments: [ "-U", @getUuid(), "/dev/zvol/#{tempDataset}"]
				content= @getcontent pro 
				if content.stderr?.length 
					err= content.stderr.toString 'utf8'
					throw F.global.Exception.create(err).putCode("KOWIXVM_ERROR")
				

				# mount to folder 
				mountname= Path.basename vm.dataset
				mountname+= "." + F.global.uniqid()
				volume= await @registerXFSVolume
					name: tempDataset
					mountname: mountname
				
				mountpoint= volume.mount
				
			else 
				throw F.global.Exception.create("Just now only XFS is supported").putCode("NOT_IMPLEMENTED")


		


		filein= Path.join oldmountpoint, "backup.yaml"
		content= await Fs.async.readFile filein

		if type is type2 
			
			# los dos son el mismo tipo 
			await @destroyDataset vm.dataset

			if type is "volume"
				# mount to correct folder 				
				if await F.checkFileExists(oldmountpoint)
					await Fs.async.unlink(oldmountpoint)
				
				# create symLink
				await Fs.async.symlink mountpoint, oldmountpoint
				#mountpoint= oldmountpoint
			

			fileout= Path.join mountpoint, "backup.yaml"
			if await F.checkFileExists fileout 
				await Fs.async.unlink fileout 
			await Fs.async.writeFile fileout, content

			# rename dataset 
			await zfs.rename tempDataset, vm.dataset

		else 

			# si cambia el tipo hay que copiar la carpeta rootfs
			rootfs= Path.join mountpoint, "rootfs"			
			rootfsori= Path.join oldmountpoint, "rootfs"
			task= new core.VW.Task()
			F.fs.remove rootfsori, (er)->
				task.exception= er if er 
				task.finish()
			await task

			cid= F.global.uniqid()
			tar= Path.join(rootfs, "..", "..", cid+".tar")

			try 
				
				pro= await F.childStream
					cmd: "tar"
					arguments: [ "-cf", tar , "."]
					options: 
						cwd: rootfs 
				content= @getcontent pro 
				if content.stderr?.length 
					err= content.stderr.toString 'utf8'
					throw F.global.Exception.create(err).putCode("KOWIXVM_ERROR")
				

				
				await core.VW.Task.sleep 10000
				await Fs.async.mkdir rootfsori
				pro= await F.childStream
					cmd: "tar"
					arguments: [ "xvf", tar , "."]
					options: 
						cwd: rootfsori
				content= @getcontent pro 
				if content.stderr?.length 
					err= content.stderr.toString 'utf8'
					throw F.global.Exception.create(err).putCode("KOWIXVM_ERROR")
				await core.VW.Task.sleep 4000
			catch e 				
				throw e
			finally 
				
				if tar and (await F.checkFileExists tar)
					await core.VW.Task.sleep 2000
					await Fs.async.unlink tar 
				

			# destroy currentDataset 
			await core.VW.Task.sleep 1000
			try 
				await @destroyDataset tempDataset
			catch e 
				await core.VW.Task.sleep 4000
				try 
					await @destroyDataset tempDataset
				
			#await zfs.rename oldDataset, vm.dataset



		await @updateVMSnapshotInfo vm.name
		



	importContainer: (options)->

		# importa uno o más archivos guardados con export 
		# secrea una máquina normal 
		
		autostart= options.autostart

		options.autostart= no
		options.image= 'ubuntu:16.04'
		vm= await @createContainer options 
		await core.VW.Task.sleep 1000
		console.info "creado", vm
		 
		

		# se restaura el ROOTFS
		tempDataset= vm.dataset.split "/"
		tempDataset[tempDataset.length-1]+= ".temp"
		uname= tempDataset[tempDataset.length-1]
		tempDataset= tempDataset.join "/"
		zfs= await @zfs()


		#await zfs.rename vm.dataset, tempDataset	


		# receive file as dataset 
		try 
			
			pipe1= await F.global.userFunction("pipe.1").invoke()
			procesarArchivo= (file)->
				compression= Path.extname file 
				compression= compression.substring(1)
				program= null 
				if compression is "lz4"
					program= "unlz4"
				else if compression is "gz"
					program= "gunzip"
				else if compression isnt "bk"
					throw global.Exception.create("Compression format not valid")


				if program
					stream= await pipe1
						type: "child.stream"					
						"cmd": program
						"pipe.1": 
							"type": "child.stream"
							cmd: "zfs"
							"arguments": ["recv", tempDataset ]
							
						"internal": 1
				else 
					stream= await pipe1
						"type": "child.stream"
						cmd: "zfs"
						"arguments": ["recv", tempDataset]
						"internal": 1


				

				# pipe to file 
				err=[]
				filest= fs.createReadStream file 
				task= new core.VW.Task() 
				if stream.$pipe1
					console.log "Aquí xD"
					stream.$pipe1.pipe2().on "data", (er)->
						err.push er 
					stream.$pipe1.on "finish", task.finish.bind task 
				else
					stream.on "finish", task.finish.bind task 

				stream.pipe2().on "data", (er)->
					err.push er 
					
				filest.pipe stream.pipe0()
				
				
				stream.on "error", (e)->
					task.exception= e 
					task.finish() 
				filest.on "error", (e)->
					task.exception= e 
					task.finish() 
				
				await task 
				if err.length 
					err= Buffer.concat err 
					err= err.toString()
					throw F.global.Exception.create("Import failed: #{err}").putCode("FAILED")

				await core.VW.Task.sleep 1000
			## TODO: Implementar import incremental 
			## Por ahora se usa un solo archivo  origen 
			file= options.files[0]
			await procesarArchivo(file)
			await core.VW.Task.sleep 2000

			# aquí debió haber terminado recv
			#return "test"

			
			await @replaceVM vm, tempDataset			


		catch e 

			#  si sale mal eliminar todo
			await core.VW.Task.sleep 5000
			await @destroyVm
				name: options.name
				force: yes 
			
			try 
				await @destroyDataset tempDataset
			
			#try 
			#	await @destroyDataset vm.dataset
			
			throw e
	
		if autostart
			await @start options.name

	

	stop: ({name,force})->
		if not name and arguments[0]
			name= arguments[0]	

		vm= await @getVM(name)
		if not vm 
			throw F.global.Exception.create("VM #{name} not found").putCode("NOT_FOUND")
		updatevm= ()->
			str= core.safeJSON.stringify vm
			await F.LocalDB.put "kowixvm.vms.#{vm.name}", str
			await F.LocalDB.put "kowixvm.vms.#{vm.id}", str

		if vm.type is "lxd"	
			lxd= await @lxd()
			await lxd.stop
				name: name 
				force: force 
		###		
		if not vm.firstBoot
			vm.firstBoot= Date.now()
		### 
		vm.lastStop= Date.now()
		await updatevm()


	start: (name)->
		name= name.name or name
		vm= await @getVM(name)
		if not vm 
			throw F.global.Exception.create("VM #{name} not found").putCode("NOT_FOUND")
		updatevm= ()->
			str= core.safeJSON.stringify vm
			await F.LocalDB.put "kowixvm.vms.#{vm.name}", str
			await F.LocalDB.put "kowixvm.vms.#{vm.id}", str

		if vm.type is "lxd"	
			lxd= await @lxd()
			await lxd.start name
		if not vm.firstBoot
			vm.firstBoot= Date.now()
		vm.lastBoot= Date.now()
		await updatevm()


	parseSize: (size)->
		realSize=parseInt size
		if typeof size is "string"
			ends= size[size.length-1].toUpperCase()
			if ends is "G"
				realSize*= (1024*1024*1024)
			else if ends is "M"
				realSize*= (1024*1024)
			else if ends is "K"
				realSize*= 1024
			else if not (/\d/.test ends)
				throw F.global.Exception.create("Invalid size #{size} ").putCode("INVALID_ARGUMENT")
		return realSize


	destroyDataset:({name})->
		if not name and arguments[0]
			name= arguments[0]
		
		zfs= await @zfs()
		type= await zfs.get 
			param: "type"
			name: name 
		type= type?[0]?.VALUE
		if type  is "filesystem" or type is "snapshot"
			await zfs.destroy name
		else if type is "volume"
			await @destroyVolume name 
	


	destroyVolume: ({name})->
		if not name and arguments[0]
			name= arguments[0]

		volume= await F.LocalDB.tryGetString "kowixvm.disks.#{name}"
		

		

		volume= JSON.parse volume if volume 
		if volume?.mount 
			pro= await F.childStream
				cmd: "umount"
				arguments: [volume.mount]
			content= await @getcontent pro
			if content.stderr?.length 
				err= content.stderr.toString 'utf8'
				if err.indexOf("error") >= 0 or err.indexOf("busy") >= 0
					throw F.global.Exception.create(err).putCode("KOWIXVM_ERROR")


		pro= await F.childStream
			cmd: "umount"
			arguments: ["/dev/zvol/#{name}"]
		content= await @getcontent pro
		if content.stderr?.length 
			err= content.stderr.toString 'utf8'
			if err.indexOf("error") >= 0 or err.indexOf("busy") >= 0
				throw F.global.Exception.create(err).putCode("KOWIXVM_ERROR")
		
		zfs= await @zfs()
		await zfs.destroy name 
		volume= await F.LocalDB.tryGetString "kowixvm.disks.#{name}"
		if volume 
			await F.LocalDB.del "kowixvm.disks.#{name}"
		
			

	changeVolumeSize: ({name, size})->
		if not size
			throw F.global.Exception.create("You need define volume size").putCode("INVALID_ARGUMENTS")
		size= @parseSize size 
		volume= await F.LocalDB.tryGetString "kowixvm.disks.#{name}"
		updateVolume= ()->
			str= JSON.stringify	volume
			await F.LocalDB.put "kowixvm.disks.#{name}", str 
		zfs= await @zfs()
		if not volume 	
			await zfs.set 
				name: name 
				param: "volsize"
				value: size
		else 
			volume= JSON.parse volume 
			if size < volume.size 
				throw F.global.Exception.create("Size cannot be less").putCode("INVALID_ARGUMENTS")

			await zfs.set 
				name: name 
				param: "volsize"
				value: size

			volume.size= size 
			await updateVolume()

			if volume.filesystem is 'xfs'
				pro= await F.childStream
					cmd: "xfs_growfs"
					arguments: [ volume.mount ]

				content= await @getcontent pro
				if content.stderr?.length 
					err= content.stderr.toString 'utf8'
					if err.indexOf("error") >=0 or err.indexOf("invalid") >= 0
						throw F.global.Exception.create(err).putCode("KOWIXVM_ERROR")



	createXFSVolume: ({name, size, mount})->
		try
			if not size
				throw F.global.Exception.create("You need define volume size").putCode("INVALID_ARGUMENTS")
			size= @parseSize size 

			if size < 1024*1024*1024
				throw F.global.Exception.create("Minimum size is 1G").putCode("INVALID_ARGUMENTS")

			zfs= await @zfs()
			try 
				type= await zfs.get 
					name: name 
					param: "type"
				
			if type?[0]?.VALUE 
				throw F.global.Exception.create("Failed created volume. Dataset #{name} exists").putCode("DATASET_EXISTS")

			# create a volume 
			vset= name.split "/"
			vname= vset[vset.length-1]
			vset[vset.length-1]= ".base.volume"
			vset= vset.join "/"

		


			list= await zfs.list()
			base= list.filter (a)-> a.NAME is vset 
			if not base.length 
				await zfs.createVolume
					name: vset 
					size: '1G'
				await zfs.snapshot 
					name: vset 
					snapshot: '0'
			
			await zfs.clone 
				src: "#{vset}@0"
				name: name 

			#set volume size 
			await zfs.set 
				name: name 
				param: "volsize" 
				value: size 

			return await @registerXFSVolume
				name: name
				mountname: vname
				mount: mount
				size: size
		
		catch e
			try 
				await core.VW.Task.sleep 1000
				await zfs.destroy name
			throw e
		


	registerXFSVolume: ({name, mountname, size, mount})->
		zfs= await @zfs()

		list= await zfs.list()

		mset= name.split "/"		
		mset[mset.length-1]= ".mount"
		mset= mset.join "/"

		base= list.filter (a)-> a.NAME is mset 
		if not base.length 
			await zfs.createDataset
				name: mset

		

		mountFolder= await zfs.get 
			name: mset
			param: "mountpoint"
		
		mountFolder= mountFolder[0]?.VALUE 
		if not mountFolder
			throw F.global.Exception.create("Failed getting folder to mount devices").putCode("MOUNTPREPARE_FAILED")

		

		# save for auto mount 
		tomount= Path.join(mountFolder, mountname)
		if not (await F.checkFileExists(tomount))
			await Fs.async.mkdir tomount 

		obj= 
			name: name 
			size: size 
			filesystem: 'xfs'
			mount: tomount

		

		try 
			# format to xfs 
			pro= await F.childStream
				cmd: "mkfs.xfs"
				arguments: [ "/dev/zvol/#{name}"]

			content= await @getcontent pro
			if content.stderr?.length 
				err= content.stderr.toString 'utf8'
				if err.indexOf("error") >=0 or err.indexOf("invalid") >= 0
					throw F.global.Exception.create(err).putCode("KOWIXVM_ERROR")
			
			str= JSON.stringify obj
			await F.LocalDB.put "kowixvm.disks.#{name}", str

			# mount
			pro= await F.childStream
				cmd: "mount"
				arguments: ["-o", "discard,noatime", "/dev/zvol/#{name}", tomount]
			content= await @getcontent pro
			if content.stderr?.length 
				err= content.stderr.toString 'utf8'
				throw F.global.Exception.create(err).putCode("KOWIXVM_ERROR")
			
			if mount  
				if not (await F.checkFileExists(mount))
					await Fs.async.symlink tomount, mount 
			
			return obj

		catch e 
			throw e




	initPool: ({pool})->
		if not pool and arguments[0]
			pool= arguments[0]
		lxd= await @lxd()
		zfs= await @zfs()

		sdir= "#{pool}/kwvm"
		datasets= await zfs.list()

		if not pool and arguments[0]	
			pool= arguments[0]
		
		try
			await zfs.createDataset
				name:sdir

		# get mountpoint 
		result= await zfs.get 
			name: sdir
			param: "mountpoint"
		if not result[0]?.VALUE
			# create a mountpoint
			await zfs.set
				name: sdir
				param: "mountpoint"
				value: "/#{sdir}"
			mountpoint= "/#{sdir}"
		else
			mountpoint= result[0].VALUE

		namesto= "kwvm-#{pool.replace(/\//ig,'_')}"
		storageList= await lxd.storageList()
		r1= storageList.filter (a)-> a.NAME is namesto 
		if not r1.length 
			# create symlink 
			await Fs.async.symlink mountpoint, "/var/lib/lxd/storage-pools/#{namesto}"
			await lxd.storageCreate
				name: namesto


		r1= datasets.filter (a)-> a.NAME is "#{sdir}/images"
		r2= datasets.filter (a)-> a.NAME is "#{sdir}/containers"
		r3= datasets.filter (a)-> a.NAME is "#{sdir}/backups"

		if not r1.length 
			await zfs.createDataset "#{sdir}/images"
		
		if not r2.length 
			await zfs.createDataset "#{sdir}/containers"
		
		if not r3.length 
			await zfs.createDataset "#{sdir}/backups"
		
		return 
			lxdstorage: namesto 
			mountpoint: mountpoint 
			dataset: sdir


	createContainer: ({name, image, size, params, pool, storageType='filesystem', network, autostart=yes})->

		pool= await F.LocalDB.tryGetString("kowixvm.zfs.pool")
		if not pool 
			throw F.global.Exception.create("Define default storage pool").putCode()

		zfs= await @zfs()
		lxd= await @lxd()


		datasets= await zfs.list()


		#sdir= "#{pool}/kwvm"
		poolInfo= await @initPool pool
		mountpoint= poolInfo.mountpoint
		namesto= poolInfo.lxdstorage
		sdir= poolInfo.dataset





		# create dataset for container 
		dset= "#{sdir}/containers/#{name}"
		if storageType is 'filesystem'
			try 
				r3= await zfs.get 
					name: dset
					param: "type"
				if r3[0]?.VALUE isnt "filesystem"
					throw F.global.Exception.create("Dataset #{dset} is not filesystem")
			catch e 
				await zfs.createDataset 
					name:dset
					size: size
					
		else if storageType is "volume"
			# create a volume 
			await @createXFSVolume
				name:dset
				size: size
				mount: Path.join mountpoint, "containers", name
				
					
		vm= await F.LocalDB.tryGetString "kowixvm.vms.#{name}"
		if vm
			throw F.global.Exception.create("There is another machine with same name #{name}")


		
		#good dataset prepared. now check network 
		if not network 
			network= await @getOrCreateDefaultNetwork()

		params= params or 
			"security.privileged": yes 
			"security.nesting": yes 
		
		

		await lxd.init 
			name: name 
			image: image
			params: params 
			storage: namesto 
			network: network 
			autostart: no
		
		containerObj= 
			type: 'lxd' 	
			name: name 
			id: "lxd-#{name}"
			params: params 
			internal_storage: namesto 
			pool: pool 
			network: network 
			dataset: dset
			storagetype: storageType	
			
		
		str= core.safeJSON.stringify containerObj

		await F.LocalDB.put "kowixvm.vms.#{containerObj.name}", str
		await F.LocalDB.put "kowixvm.vms.#{containerObj.id}", str 

		if autostart
			await @start containerObj.name

		return containerObj







		

	getcontent: (pro)->
		task= new core.VW.Task()
		allcontent= []
		allerrcontent= []

		pro.pipe1().on "data", (con)->
			allcontent.push(con)
		
		pro.pipe2().on "data", (con)->
			allerrcontent.push con
		
		pro.on "finish", ()->
			allcontent= Buffer.concat allcontent
			allerrcontent= Buffer.concat allerrcontent
			task.result=
				stdout: allcontent 
				stderr: allerrcontent
			
			task.finish()
		
		pro.on "error", (e)->
			task.exception= e 
			task.finish()
		
		return await task 




F= (body)->
	global= F.global 

	F.checkFileExists= (path)->
		return new Promise (resolve,reject)->
			fs.access path, fs.F_OK, (error)->
				resolve not error 

	
	F.childStream_o= global.userFunction("child.stream")
	await F.childStream_o.load()
	F.childStream= F.childStream_o.execute.bind F.childStream_o
	F.LocalDB= await global.userFunction("LocalDB").invoke({})

	F.Uuid= await F.global.UserContext.require("uuid","3.3.2","v4")
	F.fs= await F.global.UserContext.require("fs-extra","7.0.1")
	

	return new KWVM



module.exports= (global)->
	F.global= global 
	F
	