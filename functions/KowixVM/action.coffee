
F= (body)->
	global= F.global 
	await global.userFunction("passwd.check").invoke body 


	KowixVM= await global.userFunction("KowixVM/class").invoke()
	func= KowixVM[body.action]
	if not func 
		throw global.Exception.create("Action #{body.action} not valid").putCode("INVALID_ACTION")

	task= func.apply KowixVM, body.arguments or []
	return await task 


module.exports= (global)->
	F.global= global 
	F