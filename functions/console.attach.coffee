import Net from 'net'
F= (body)->
	global= F.global
	
	# execute on master 
	cid= global.uniqid()
	
	try
		kowixCtx= await global.getSiteContext("kowix")
		channel= await kowixCtx.userFunction("master.channel").invoke()
		
		stream_out= global.context?.response
	
		
		close= ()->
			try 
				if not close.yet
					close.yet= yes
					await channel.detachStdout cid
					await channel.detachStderr cid
			catch e 
				console.error "Error closing channel to master: ", e
			
			delete global.context.response
			task.finish()
			
		socket= body.socket
		
			
		data1= (mode, data)->
			#console.log "Getting response", data
			if data and data.length 
				data= data.map (a)->
					if not Buffer.isBuffer(a)
						a= Buffer.from a 					
					return a
									
				data= Buffer.concat data
				
				if socket 
					
					cevent= body[mode]
					if cevent 
						if body.encoding 
							data= data.toString body.encoding 
						try 
							socket.emit cevent, data
				else 
					code= JSON.stringify
						data: data.toString('binary')
						mode: mode
					try 
						stream_out.write(code + "\n")
			if not close.yet
				channel.read( mode, cid).then(data1[mode]).catch(data1.err)
		
		
		data1.stdout= data1.bind null, 'stdout'
		data1.stderr= data1.bind null, 'stderr'
		data1.err= (er)->
			console.error "ERROR:", er
		
		
		stream_out?.setTimeout? 0
		if global.context?.request?.on
			global.context.request.on "close", close
			global.context.request.socket.on "close", close
		if socket 
			socket.on "disconnect", close 
			
			
		task= new core.VW.Task()
		await global.userFunction("passwd.check").invoke(body)
		
	
		
		stream_out?.writeHead? 200, 
			"content-type":'text/plain'
		
		
		await channel.attachStdout cid 
		await channel.attachStderr cid 
		channel.read( 'stdout', cid).then(data1.stdout).catch(data1.err)
		channel.read( 'stderr', cid).then(data1.stderr).catch(data1.err)
		
		if not socket
			await task 
		else
			return 
				connected: yes
	catch e
		throw e
	
	
	

module.exports = (global)->
	F.global= global 
	F