IO= core.System.IO
import Stream from 'stream'
class ResponseStream extends Stream.Writable 
	
	constructor:(channel,@encoding)->
		super()
		@channel= channel
		@context= F.global.context
		
	
	finish: ()->
		console.log "FINISHING"
		
	
	
	_raiseError: (e,code)->
		
		@writing= false 
		@haveError= true
		@stream.close() if @stream 
			
		if @file1
			try 
				await IO.Fs.async.unlink @file1
			catch 
			@file1= null
			
		er= F.global.Exception.create e.message or e.toString()
		if e?.code or code
			er.putCode e?.code or code 
		@emit "error", er
		
		
	writeEnd: ()->
		json= JSON.stringify @context.response.ResponseData, null, '\t'
		@context.response.writeHead 200, 
			"content-type": "application/json"
		
		@context.response.write json 
		@context.response.end()
	
	
	end: (chunk,enc,next)->
		if typeof enc is "function"
			next= enc 
			enc= null	
			
		
		return if @haveError
		if chunk
			@_write chunk, enc 
		return if @haveError
		
		#await @finish()
		
		if @context.response.ResponseData[@channel]
			d= Buffer.concat @context.response.ResponseData[@channel]
			if @encoding 
				d= d.toString @encoding
			@context.response.ResponseData[@channel]=d
		
		
		
		if @context.response.ResponseData.count isnt undefined
			@context.response.ResponseData.count--
			if @context.response.ResponseData.count is  0
				delete @context.response.ResponseData.count
				@writeEnd()
		
		next() if next
		@emit "finish"
	
		
	_write: (chunk, enc, next)->
		#return @context.response.ResponseData.write ...arguments
		
		if typeof enc is "function"
			next= enc 
			enc= null 
		if not Buffer.isBuffer(chunk)
			chunk= Buffer.from chunk, enc or 'utf8'
			
		
		c= @context.response.ResponseData[@channel]=@context.response.ResponseData[@channel] or []
		c.push chunk 
		next?() 
		
		
		
			


F= (body)->
	global= F.global 
	if not global.context.response.ResponseData
		global.context.response.ResponseData= { count: 0 }
	
	
	j= new ResponseStream(body.channel, body.encoding)
	global.context.response.ResponseData.count++ 
	return j
	

module.exports= (global)->
	F.global= global 
	F 