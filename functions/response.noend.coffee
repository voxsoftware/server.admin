IO= core.System.IO
import Stream from 'stream'
class ResponseNoEndStream extends Stream.Writable 
	
	
	finish: ()->
		console.log "FINISHING"
		
	
	
	_raiseError: (e,code)->
		
		@writing= false 
		@haveError= true
		@stream.close() if @stream 
			
		if @file1
			try 
				await IO.Fs.async.unlink @file1
			catch 
			@file1= null
			
		er= F.global.Exception.create e.message or e.toString()
		if e?.code or code
			er.putCode e?.code or code 
		@emit "error", er
		
		
	
	
	end: (chunk,enc,next)->
		if typeof enc is "function"
			next= enc 
			enc= null	
			
		
		return if @haveError
		if chunk
			@_write chunk, enc 
		return if @haveError
		
		await @finish()
		next() if next
		@emit "finish"
		
	
		
	_write: (chunk, enc, next)->
		return F.global.context.response.write ...arguments
		
			


F= (body)->
	global= F.global 
	return new ResponseNoEndStream
	

module.exports= (global)->
	F.global= global 
	F 