import Path from 'path'
Fs= core.System.IO.Fs

F= (body)->
	
	global= F.global 
	body= body or {}
	
	if global.publicContext.LevelDB
		return global.publicContext.LevelDB
	
	LevelUp= await global.UserContext.require "levelup", "4.0.0"
	
	# LevelDown have native dependencies, need npm requiremode
	global.UserContext.setRequireMode "npm"
	LevelDown= await global.UserContext.require "leveldown", "4.0.1"


	dbhome= Path.join global.UserContext.getHomeDir(), "db0"
	if not Fs.sync.exists dbhome
		await Fs.async.mkdir dbhome
	
	dbhome= Path.join dbhome, "Local"
	LevelDB= LevelUp LevelDown dbhome 
	
	
	LevelDB.readMany= (options)->
		rows = []
		result=
			data: rows
		task= new core.VW.Task()
		sr= @createReadStream options 
		sr.on "data", (data)->
			data.key= data.key.toString()
			rows.push data 
		sr.on "error", (e)->
			task.exception= e 
			task.finish()
		sr.on "end", ()->
			task.finish()
		sr.on "stats", (stats)->
			result.stats= stats
		await task
		return result
		
	
	
	LevelDB.tryGet= (key, options, callback)->
		if typeof options is "function"
			callback= options 
			options= null
			
		if typeof callback is "function"
			call= callback
			callback = (err, data)->
				if err.notFound
					err  = null 
					data = null
				call(err, data)
				
		try 
			value= @get key, options, callback
			if not callback
				value= await value 
		catch e 
			#console.log "error: ",e, e.type, e.code, e.notFound
			if not e.notFound 
				throw e 
			else 
				return null 
		return value 

	
	LevelDB.getString= (key, callback)->
		return @get key, {asBuffer: false}, callback
	
	LevelDB.tryGetString= (key, callback)->
		return @tryGet key, {asBuffer: false}, callback
	
	return global.publicContext.LevelDB = LevelDB
	
	


module.exports= (global)->
	F.global = global 
	F