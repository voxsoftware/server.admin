
window.kodhe_css_code= "@charset \"UTF-8\";\na {\n  text-decoration: none;\n  color: inherit;\n}\n* {\n  box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  -webkit-box-sizing: border-box;\n}\nhtml {\n  line-height: 1.3;\n  font-weight: normal;\n  font-family: Roboto, 'Open sans', sans-serif;\n  -ms-text-size-adjust: 100%;\n  -webkit-text-size-adjust: 100%;\n}\narticle,\naside,\ndetails,\nfigcaption,\nfigure,\nfooter,\nheader,\nhgroup,\nmain,\nnav,\nsection,\nsummary {\n  display: block;\n}\naudio,\ncanvas,\nprogress,\nvideo {\n  display: inline-block;\n  vertical-align: baseline;\n}\naudio:not([controls]) {\n  display: none;\n  height: 0;\n}\n[hidden],\ntemplate {\n  display: none;\n}\na {\n  background: transparent;\n}\na:active,\na:hover {\n  outline: 0;\n}\nabbr[title] {\n  border-bottom: 1px dotted;\n}\nbody {\n  margin: 0;\n}\nb,\nstrong {\n  font-weight: bold;\n}\ndfn {\n  font-style: italic;\n}\nmark {\n  background: #ff0;\n  color: #000;\n}\nsmall {\n  font-size: 80%;\n}\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline;\n}\nsup {\n  top: -0.5em;\n}\nsub {\n  bottom: -0.25em;\n}\nimg {\n  border: 0;\n}\nsvg:not(:root) {\n  overflow: hidden;\n}\nfigure {\n  margin: 1em 40px;\n}\nhr {\n  -moz-box-sizing: content-box;\n  box-sizing: content-box;\n  height: 0;\n}\npre {\n  overflow: auto;\n}\ncode,\nkbd,\npre,\nsamp {\n  font-family: monospace, monospace;\n  font-size: 1em;\n}\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  color: inherit;\n  font: inherit;\n  margin: 0;\n}\nbutton {\n  overflow: visible;\n}\nbutton,\nselect {\n  text-transform: none;\n}\nbutton,\nhtml input[type=\"button\"],\ninput[type=\"reset\"],\ninput[type=\"submit\"] {\n  -webkit-appearance: button;\n  cursor: pointer;\n}\nbutton[disabled],\nhtml input[disabled] {\n  cursor: default;\n}\nbutton::-moz-focus-inner,\ninput::-moz-focus-inner {\n  border: 0;\n  padding: 0;\n}\ninput {\n  line-height: normal;\n}\ninput[type=\"checkbox\"],\ninput[type=\"radio\"] {\n  box-sizing: border-box;\n  padding: 0;\n}\ninput[type=\"number\"]::-webkit-inner-spin-button,\ninput[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto;\n}\ninput[type=\"search\"] {\n  -webkit-appearance: textfield;\n  -moz-box-sizing: content-box;\n  -webkit-box-sizing: content-box;\n  box-sizing: content-box;\n}\ninput[type=\"search\"]::-webkit-search-cancel-button,\ninput[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none;\n}\nfieldset {\n  border: 1px solid #c0c0c0;\n  margin: 0 2px;\n  padding: 0.35em 0.625em 0.75em;\n}\nlegend {\n  border: 0;\n  padding: 0;\n}\ntextarea {\n  overflow: auto;\n}\noptgroup {\n  font-weight: bold;\n}\na[disabled],\n[disabled] > a {\n  cursor: default;\n}\ntable {\n  border-collapse: collapse;\n  border-spacing: 0;\n}\ntd,\nth {\n  padding: 0;\n}\n.pd0 {\n  padding: 0;\n}\n.mg0 {\n  padding: 0;\n}\n/*!\n * Font Awesome Free 5.0.13 by @fontawesome - https://fontawesome.com\n * License - https://fontawesome.com/license (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)\n */\n.fa,\n.fas,\n.far,\n.fal,\n.fab {\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-font-smoothing: antialiased;\n  display: inline-block;\n  font-style: normal;\n  font-variant: normal;\n  text-rendering: auto;\n  line-height: 1;\n}\n/* makes the font 33% larger relative to the icon container */\n.fa-lg {\n  font-size: 1.33333333em;\n  line-height: 0.75em;\n  vertical-align: -0.0667em;\n}\n.fa-xs {\n  font-size: .75em;\n}\n.fa-sm {\n  font-size: .875em;\n}\n.fa-1x {\n  font-size: 1em;\n}\n.fa-2x {\n  font-size: 2em;\n}\n.fa-3x {\n  font-size: 3em;\n}\n.fa-4x {\n  font-size: 4em;\n}\n.fa-5x {\n  font-size: 5em;\n}\n.fa-6x {\n  font-size: 6em;\n}\n.fa-7x {\n  font-size: 7em;\n}\n.fa-8x {\n  font-size: 8em;\n}\n.fa-9x {\n  font-size: 9em;\n}\n.fa-10x {\n  font-size: 10em;\n}\n.fa-fw {\n  text-align: center;\n  width: 1.25em;\n}\n.fa-ul {\n  list-style-type: none;\n  margin-left: 2.5em;\n  padding-left: 0;\n}\n.fa-ul > li {\n  position: relative;\n}\n.fa-li {\n  left: -2em;\n  position: absolute;\n  text-align: center;\n  width: 2em;\n  line-height: inherit;\n}\n.fa-border {\n  border-radius: .1em;\n  border: solid 0.08em #eee;\n  padding: .2em .25em .15em;\n}\n.fa-pull-left {\n  float: left;\n}\n.fa-pull-right {\n  float: right;\n}\n.fa.fa-pull-left,\n.fas.fa-pull-left,\n.far.fa-pull-left,\n.fal.fa-pull-left,\n.fab.fa-pull-left {\n  margin-right: .3em;\n}\n.fa.fa-pull-right,\n.fas.fa-pull-right,\n.far.fa-pull-right,\n.fal.fa-pull-right,\n.fab.fa-pull-right {\n  margin-left: .3em;\n}\n.fa-spin {\n  animation: fa-spin 2s infinite linear;\n}\n.fa-pulse {\n  animation: fa-spin 1s infinite steps(8);\n}\n@keyframes fa-spin {\n  0% {\n    transform: rotate(0deg);\n  }\n  100% {\n    transform: rotate(360deg);\n  }\n}\n.fa-rotate-90 {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=1)\";\n  transform: rotate(90deg);\n}\n.fa-rotate-180 {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=2)\";\n  transform: rotate(180deg);\n}\n.fa-rotate-270 {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=3)\";\n  transform: rotate(270deg);\n}\n.fa-flip-horizontal {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=0, mirror=1)\";\n  transform: scale(-1, 1);\n}\n.fa-flip-vertical {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=2, mirror=1)\";\n  transform: scale(1, -1);\n}\n.fa-flip-horizontal.fa-flip-vertical {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=2, mirror=1)\";\n  transform: scale(-1, -1);\n}\n:root .fa-rotate-90,\n:root .fa-rotate-180,\n:root .fa-rotate-270,\n:root .fa-flip-horizontal,\n:root .fa-flip-vertical {\n  filter: none;\n}\n.fa-stack {\n  display: inline-block;\n  height: 2em;\n  line-height: 2em;\n  position: relative;\n  vertical-align: middle;\n  width: 2em;\n}\n.fa-stack-1x,\n.fa-stack-2x {\n  left: 0;\n  position: absolute;\n  text-align: center;\n  width: 100%;\n}\n.fa-stack-1x {\n  line-height: inherit;\n}\n.fa-stack-2x {\n  font-size: 2em;\n}\n.fa-inverse {\n  color: #fff;\n}\n/* Font Awesome uses the Unicode Private Use Area (PUA) to ensure screen\n   readers do not read off random characters that represent icons */\n.fa-500px:before {\n  content: \"\\f26e\";\n}\n.fa-accessible-icon:before {\n  content: \"\\f368\";\n}\n.fa-accusoft:before {\n  content: \"\\f369\";\n}\n.fa-address-book:before {\n  content: \"\\f2b9\";\n}\n.fa-address-card:before {\n  content: \"\\f2bb\";\n}\n.fa-adjust:before {\n  content: \"\\f042\";\n}\n.fa-adn:before {\n  content: \"\\f170\";\n}\n.fa-adversal:before {\n  content: \"\\f36a\";\n}\n.fa-affiliatetheme:before {\n  content: \"\\f36b\";\n}\n.fa-algolia:before {\n  content: \"\\f36c\";\n}\n.fa-align-center:before {\n  content: \"\\f037\";\n}\n.fa-align-justify:before {\n  content: \"\\f039\";\n}\n.fa-align-left:before {\n  content: \"\\f036\";\n}\n.fa-align-right:before {\n  content: \"\\f038\";\n}\n.fa-allergies:before {\n  content: \"\\f461\";\n}\n.fa-amazon:before {\n  content: \"\\f270\";\n}\n.fa-amazon-pay:before {\n  content: \"\\f42c\";\n}\n.fa-ambulance:before {\n  content: \"\\f0f9\";\n}\n.fa-american-sign-language-interpreting:before {\n  content: \"\\f2a3\";\n}\n.fa-amilia:before {\n  content: \"\\f36d\";\n}\n.fa-anchor:before {\n  content: \"\\f13d\";\n}\n.fa-android:before {\n  content: \"\\f17b\";\n}\n.fa-angellist:before {\n  content: \"\\f209\";\n}\n.fa-angle-double-down:before {\n  content: \"\\f103\";\n}\n.fa-angle-double-left:before {\n  content: \"\\f100\";\n}\n.fa-angle-double-right:before {\n  content: \"\\f101\";\n}\n.fa-angle-double-up:before {\n  content: \"\\f102\";\n}\n.fa-angle-down:before {\n  content: \"\\f107\";\n}\n.fa-angle-left:before {\n  content: \"\\f104\";\n}\n.fa-angle-right:before {\n  content: \"\\f105\";\n}\n.fa-angle-up:before {\n  content: \"\\f106\";\n}\n.fa-angrycreative:before {\n  content: \"\\f36e\";\n}\n.fa-angular:before {\n  content: \"\\f420\";\n}\n.fa-app-store:before {\n  content: \"\\f36f\";\n}\n.fa-app-store-ios:before {\n  content: \"\\f370\";\n}\n.fa-apper:before {\n  content: \"\\f371\";\n}\n.fa-apple:before {\n  content: \"\\f179\";\n}\n.fa-apple-pay:before {\n  content: \"\\f415\";\n}\n.fa-archive:before {\n  content: \"\\f187\";\n}\n.fa-arrow-alt-circle-down:before {\n  content: \"\\f358\";\n}\n.fa-arrow-alt-circle-left:before {\n  content: \"\\f359\";\n}\n.fa-arrow-alt-circle-right:before {\n  content: \"\\f35a\";\n}\n.fa-arrow-alt-circle-up:before {\n  content: \"\\f35b\";\n}\n.fa-arrow-circle-down:before {\n  content: \"\\f0ab\";\n}\n.fa-arrow-circle-left:before {\n  content: \"\\f0a8\";\n}\n.fa-arrow-circle-right:before {\n  content: \"\\f0a9\";\n}\n.fa-arrow-circle-up:before {\n  content: \"\\f0aa\";\n}\n.fa-arrow-down:before {\n  content: \"\\f063\";\n}\n.fa-arrow-left:before {\n  content: \"\\f060\";\n}\n.fa-arrow-right:before {\n  content: \"\\f061\";\n}\n.fa-arrow-up:before {\n  content: \"\\f062\";\n}\n.fa-arrows-alt:before {\n  content: \"\\f0b2\";\n}\n.fa-arrows-alt-h:before {\n  content: \"\\f337\";\n}\n.fa-arrows-alt-v:before {\n  content: \"\\f338\";\n}\n.fa-assistive-listening-systems:before {\n  content: \"\\f2a2\";\n}\n.fa-asterisk:before {\n  content: \"\\f069\";\n}\n.fa-asymmetrik:before {\n  content: \"\\f372\";\n}\n.fa-at:before {\n  content: \"\\f1fa\";\n}\n.fa-audible:before {\n  content: \"\\f373\";\n}\n.fa-audio-description:before {\n  content: \"\\f29e\";\n}\n.fa-autoprefixer:before {\n  content: \"\\f41c\";\n}\n.fa-avianex:before {\n  content: \"\\f374\";\n}\n.fa-aviato:before {\n  content: \"\\f421\";\n}\n.fa-aws:before {\n  content: \"\\f375\";\n}\n.fa-backward:before {\n  content: \"\\f04a\";\n}\n.fa-balance-scale:before {\n  content: \"\\f24e\";\n}\n.fa-ban:before {\n  content: \"\\f05e\";\n}\n.fa-band-aid:before {\n  content: \"\\f462\";\n}\n.fa-bandcamp:before {\n  content: \"\\f2d5\";\n}\n.fa-barcode:before {\n  content: \"\\f02a\";\n}\n.fa-bars:before {\n  content: \"\\f0c9\";\n}\n.fa-baseball-ball:before {\n  content: \"\\f433\";\n}\n.fa-basketball-ball:before {\n  content: \"\\f434\";\n}\n.fa-bath:before {\n  content: \"\\f2cd\";\n}\n.fa-battery-empty:before {\n  content: \"\\f244\";\n}\n.fa-battery-full:before {\n  content: \"\\f240\";\n}\n.fa-battery-half:before {\n  content: \"\\f242\";\n}\n.fa-battery-quarter:before {\n  content: \"\\f243\";\n}\n.fa-battery-three-quarters:before {\n  content: \"\\f241\";\n}\n.fa-bed:before {\n  content: \"\\f236\";\n}\n.fa-beer:before {\n  content: \"\\f0fc\";\n}\n.fa-behance:before {\n  content: \"\\f1b4\";\n}\n.fa-behance-square:before {\n  content: \"\\f1b5\";\n}\n.fa-bell:before {\n  content: \"\\f0f3\";\n}\n.fa-bell-slash:before {\n  content: \"\\f1f6\";\n}\n.fa-bicycle:before {\n  content: \"\\f206\";\n}\n.fa-bimobject:before {\n  content: \"\\f378\";\n}\n.fa-binoculars:before {\n  content: \"\\f1e5\";\n}\n.fa-birthday-cake:before {\n  content: \"\\f1fd\";\n}\n.fa-bitbucket:before {\n  content: \"\\f171\";\n}\n.fa-bitcoin:before {\n  content: \"\\f379\";\n}\n.fa-bity:before {\n  content: \"\\f37a\";\n}\n.fa-black-tie:before {\n  content: \"\\f27e\";\n}\n.fa-blackberry:before {\n  content: \"\\f37b\";\n}\n.fa-blender:before {\n  content: \"\\f517\";\n}\n.fa-blind:before {\n  content: \"\\f29d\";\n}\n.fa-blogger:before {\n  content: \"\\f37c\";\n}\n.fa-blogger-b:before {\n  content: \"\\f37d\";\n}\n.fa-bluetooth:before {\n  content: \"\\f293\";\n}\n.fa-bluetooth-b:before {\n  content: \"\\f294\";\n}\n.fa-bold:before {\n  content: \"\\f032\";\n}\n.fa-bolt:before {\n  content: \"\\f0e7\";\n}\n.fa-bomb:before {\n  content: \"\\f1e2\";\n}\n.fa-book:before {\n  content: \"\\f02d\";\n}\n.fa-book-open:before {\n  content: \"\\f518\";\n}\n.fa-bookmark:before {\n  content: \"\\f02e\";\n}\n.fa-bowling-ball:before {\n  content: \"\\f436\";\n}\n.fa-box:before {\n  content: \"\\f466\";\n}\n.fa-box-open:before {\n  content: \"\\f49e\";\n}\n.fa-boxes:before {\n  content: \"\\f468\";\n}\n.fa-braille:before {\n  content: \"\\f2a1\";\n}\n.fa-briefcase:before {\n  content: \"\\f0b1\";\n}\n.fa-briefcase-medical:before {\n  content: \"\\f469\";\n}\n.fa-broadcast-tower:before {\n  content: \"\\f519\";\n}\n.fa-broom:before {\n  content: \"\\f51a\";\n}\n.fa-btc:before {\n  content: \"\\f15a\";\n}\n.fa-bug:before {\n  content: \"\\f188\";\n}\n.fa-building:before {\n  content: \"\\f1ad\";\n}\n.fa-bullhorn:before {\n  content: \"\\f0a1\";\n}\n.fa-bullseye:before {\n  content: \"\\f140\";\n}\n.fa-burn:before {\n  content: \"\\f46a\";\n}\n.fa-buromobelexperte:before {\n  content: \"\\f37f\";\n}\n.fa-bus:before {\n  content: \"\\f207\";\n}\n.fa-buysellads:before {\n  content: \"\\f20d\";\n}\n.fa-calculator:before {\n  content: \"\\f1ec\";\n}\n.fa-calendar:before {\n  content: \"\\f133\";\n}\n.fa-calendar-alt:before {\n  content: \"\\f073\";\n}\n.fa-calendar-check:before {\n  content: \"\\f274\";\n}\n.fa-calendar-minus:before {\n  content: \"\\f272\";\n}\n.fa-calendar-plus:before {\n  content: \"\\f271\";\n}\n.fa-calendar-times:before {\n  content: \"\\f273\";\n}\n.fa-camera:before {\n  content: \"\\f030\";\n}\n.fa-camera-retro:before {\n  content: \"\\f083\";\n}\n.fa-capsules:before {\n  content: \"\\f46b\";\n}\n.fa-car:before {\n  content: \"\\f1b9\";\n}\n.fa-caret-down:before {\n  content: \"\\f0d7\";\n}\n.fa-caret-left:before {\n  content: \"\\f0d9\";\n}\n.fa-caret-right:before {\n  content: \"\\f0da\";\n}\n.fa-caret-square-down:before {\n  content: \"\\f150\";\n}\n.fa-caret-square-left:before {\n  content: \"\\f191\";\n}\n.fa-caret-square-right:before {\n  content: \"\\f152\";\n}\n.fa-caret-square-up:before {\n  content: \"\\f151\";\n}\n.fa-caret-up:before {\n  content: \"\\f0d8\";\n}\n.fa-cart-arrow-down:before {\n  content: \"\\f218\";\n}\n.fa-cart-plus:before {\n  content: \"\\f217\";\n}\n.fa-cc-amazon-pay:before {\n  content: \"\\f42d\";\n}\n.fa-cc-amex:before {\n  content: \"\\f1f3\";\n}\n.fa-cc-apple-pay:before {\n  content: \"\\f416\";\n}\n.fa-cc-diners-club:before {\n  content: \"\\f24c\";\n}\n.fa-cc-discover:before {\n  content: \"\\f1f2\";\n}\n.fa-cc-jcb:before {\n  content: \"\\f24b\";\n}\n.fa-cc-mastercard:before {\n  content: \"\\f1f1\";\n}\n.fa-cc-paypal:before {\n  content: \"\\f1f4\";\n}\n.fa-cc-stripe:before {\n  content: \"\\f1f5\";\n}\n.fa-cc-visa:before {\n  content: \"\\f1f0\";\n}\n.fa-centercode:before {\n  content: \"\\f380\";\n}\n.fa-certificate:before {\n  content: \"\\f0a3\";\n}\n.fa-chalkboard:before {\n  content: \"\\f51b\";\n}\n.fa-chalkboard-teacher:before {\n  content: \"\\f51c\";\n}\n.fa-chart-area:before {\n  content: \"\\f1fe\";\n}\n.fa-chart-bar:before {\n  content: \"\\f080\";\n}\n.fa-chart-line:before {\n  content: \"\\f201\";\n}\n.fa-chart-pie:before {\n  content: \"\\f200\";\n}\n.fa-check:before {\n  content: \"\\f00c\";\n}\n.fa-check-circle:before {\n  content: \"\\f058\";\n}\n.fa-check-square:before {\n  content: \"\\f14a\";\n}\n.fa-chess:before {\n  content: \"\\f439\";\n}\n.fa-chess-bishop:before {\n  content: \"\\f43a\";\n}\n.fa-chess-board:before {\n  content: \"\\f43c\";\n}\n.fa-chess-king:before {\n  content: \"\\f43f\";\n}\n.fa-chess-knight:before {\n  content: \"\\f441\";\n}\n.fa-chess-pawn:before {\n  content: \"\\f443\";\n}\n.fa-chess-queen:before {\n  content: \"\\f445\";\n}\n.fa-chess-rook:before {\n  content: \"\\f447\";\n}\n.fa-chevron-circle-down:before {\n  content: \"\\f13a\";\n}\n.fa-chevron-circle-left:before {\n  content: \"\\f137\";\n}\n.fa-chevron-circle-right:before {\n  content: \"\\f138\";\n}\n.fa-chevron-circle-up:before {\n  content: \"\\f139\";\n}\n.fa-chevron-down:before {\n  content: \"\\f078\";\n}\n.fa-chevron-left:before {\n  content: \"\\f053\";\n}\n.fa-chevron-right:before {\n  content: \"\\f054\";\n}\n.fa-chevron-up:before {\n  content: \"\\f077\";\n}\n.fa-child:before {\n  content: \"\\f1ae\";\n}\n.fa-chrome:before {\n  content: \"\\f268\";\n}\n.fa-church:before {\n  content: \"\\f51d\";\n}\n.fa-circle:before {\n  content: \"\\f111\";\n}\n.fa-circle-notch:before {\n  content: \"\\f1ce\";\n}\n.fa-clipboard:before {\n  content: \"\\f328\";\n}\n.fa-clipboard-check:before {\n  content: \"\\f46c\";\n}\n.fa-clipboard-list:before {\n  content: \"\\f46d\";\n}\n.fa-clock:before {\n  content: \"\\f017\";\n}\n.fa-clone:before {\n  content: \"\\f24d\";\n}\n.fa-closed-captioning:before {\n  content: \"\\f20a\";\n}\n.fa-cloud:before {\n  content: \"\\f0c2\";\n}\n.fa-cloud-download-alt:before {\n  content: \"\\f381\";\n}\n.fa-cloud-upload-alt:before {\n  content: \"\\f382\";\n}\n.fa-cloudscale:before {\n  content: \"\\f383\";\n}\n.fa-cloudsmith:before {\n  content: \"\\f384\";\n}\n.fa-cloudversify:before {\n  content: \"\\f385\";\n}\n.fa-code:before {\n  content: \"\\f121\";\n}\n.fa-code-branch:before {\n  content: \"\\f126\";\n}\n.fa-codepen:before {\n  content: \"\\f1cb\";\n}\n.fa-codiepie:before {\n  content: \"\\f284\";\n}\n.fa-coffee:before {\n  content: \"\\f0f4\";\n}\n.fa-cog:before {\n  content: \"\\f013\";\n}\n.fa-cogs:before {\n  content: \"\\f085\";\n}\n.fa-coins:before {\n  content: \"\\f51e\";\n}\n.fa-columns:before {\n  content: \"\\f0db\";\n}\n.fa-comment:before {\n  content: \"\\f075\";\n}\n.fa-comment-alt:before {\n  content: \"\\f27a\";\n}\n.fa-comment-dots:before {\n  content: \"\\f4ad\";\n}\n.fa-comment-slash:before {\n  content: \"\\f4b3\";\n}\n.fa-comments:before {\n  content: \"\\f086\";\n}\n.fa-compact-disc:before {\n  content: \"\\f51f\";\n}\n.fa-compass:before {\n  content: \"\\f14e\";\n}\n.fa-compress:before {\n  content: \"\\f066\";\n}\n.fa-connectdevelop:before {\n  content: \"\\f20e\";\n}\n.fa-contao:before {\n  content: \"\\f26d\";\n}\n.fa-copy:before {\n  content: \"\\f0c5\";\n}\n.fa-copyright:before {\n  content: \"\\f1f9\";\n}\n.fa-couch:before {\n  content: \"\\f4b8\";\n}\n.fa-cpanel:before {\n  content: \"\\f388\";\n}\n.fa-creative-commons:before {\n  content: \"\\f25e\";\n}\n.fa-creative-commons-by:before {\n  content: \"\\f4e7\";\n}\n.fa-creative-commons-nc:before {\n  content: \"\\f4e8\";\n}\n.fa-creative-commons-nc-eu:before {\n  content: \"\\f4e9\";\n}\n.fa-creative-commons-nc-jp:before {\n  content: \"\\f4ea\";\n}\n.fa-creative-commons-nd:before {\n  content: \"\\f4eb\";\n}\n.fa-creative-commons-pd:before {\n  content: \"\\f4ec\";\n}\n.fa-creative-commons-pd-alt:before {\n  content: \"\\f4ed\";\n}\n.fa-creative-commons-remix:before {\n  content: \"\\f4ee\";\n}\n.fa-creative-commons-sa:before {\n  content: \"\\f4ef\";\n}\n.fa-creative-commons-sampling:before {\n  content: \"\\f4f0\";\n}\n.fa-creative-commons-sampling-plus:before {\n  content: \"\\f4f1\";\n}\n.fa-creative-commons-share:before {\n  content: \"\\f4f2\";\n}\n.fa-credit-card:before {\n  content: \"\\f09d\";\n}\n.fa-crop:before {\n  content: \"\\f125\";\n}\n.fa-crosshairs:before {\n  content: \"\\f05b\";\n}\n.fa-crow:before {\n  content: \"\\f520\";\n}\n.fa-crown:before {\n  content: \"\\f521\";\n}\n.fa-css3:before {\n  content: \"\\f13c\";\n}\n.fa-css3-alt:before {\n  content: \"\\f38b\";\n}\n.fa-cube:before {\n  content: \"\\f1b2\";\n}\n.fa-cubes:before {\n  content: \"\\f1b3\";\n}\n.fa-cut:before {\n  content: \"\\f0c4\";\n}\n.fa-cuttlefish:before {\n  content: \"\\f38c\";\n}\n.fa-d-and-d:before {\n  content: \"\\f38d\";\n}\n.fa-dashcube:before {\n  content: \"\\f210\";\n}\n.fa-database:before {\n  content: \"\\f1c0\";\n}\n.fa-deaf:before {\n  content: \"\\f2a4\";\n}\n.fa-delicious:before {\n  content: \"\\f1a5\";\n}\n.fa-deploydog:before {\n  content: \"\\f38e\";\n}\n.fa-deskpro:before {\n  content: \"\\f38f\";\n}\n.fa-desktop:before {\n  content: \"\\f108\";\n}\n.fa-deviantart:before {\n  content: \"\\f1bd\";\n}\n.fa-diagnoses:before {\n  content: \"\\f470\";\n}\n.fa-dice:before {\n  content: \"\\f522\";\n}\n.fa-dice-five:before {\n  content: \"\\f523\";\n}\n.fa-dice-four:before {\n  content: \"\\f524\";\n}\n.fa-dice-one:before {\n  content: \"\\f525\";\n}\n.fa-dice-six:before {\n  content: \"\\f526\";\n}\n.fa-dice-three:before {\n  content: \"\\f527\";\n}\n.fa-dice-two:before {\n  content: \"\\f528\";\n}\n.fa-digg:before {\n  content: \"\\f1a6\";\n}\n.fa-digital-ocean:before {\n  content: \"\\f391\";\n}\n.fa-discord:before {\n  content: \"\\f392\";\n}\n.fa-discourse:before {\n  content: \"\\f393\";\n}\n.fa-divide:before {\n  content: \"\\f529\";\n}\n.fa-dna:before {\n  content: \"\\f471\";\n}\n.fa-dochub:before {\n  content: \"\\f394\";\n}\n.fa-docker:before {\n  content: \"\\f395\";\n}\n.fa-dollar-sign:before {\n  content: \"\\f155\";\n}\n.fa-dolly:before {\n  content: \"\\f472\";\n}\n.fa-dolly-flatbed:before {\n  content: \"\\f474\";\n}\n.fa-donate:before {\n  content: \"\\f4b9\";\n}\n.fa-door-closed:before {\n  content: \"\\f52a\";\n}\n.fa-door-open:before {\n  content: \"\\f52b\";\n}\n.fa-dot-circle:before {\n  content: \"\\f192\";\n}\n.fa-dove:before {\n  content: \"\\f4ba\";\n}\n.fa-download:before {\n  content: \"\\f019\";\n}\n.fa-draft2digital:before {\n  content: \"\\f396\";\n}\n.fa-dribbble:before {\n  content: \"\\f17d\";\n}\n.fa-dribbble-square:before {\n  content: \"\\f397\";\n}\n.fa-dropbox:before {\n  content: \"\\f16b\";\n}\n.fa-drupal:before {\n  content: \"\\f1a9\";\n}\n.fa-dumbbell:before {\n  content: \"\\f44b\";\n}\n.fa-dyalog:before {\n  content: \"\\f399\";\n}\n.fa-earlybirds:before {\n  content: \"\\f39a\";\n}\n.fa-ebay:before {\n  content: \"\\f4f4\";\n}\n.fa-edge:before {\n  content: \"\\f282\";\n}\n.fa-edit:before {\n  content: \"\\f044\";\n}\n.fa-eject:before {\n  content: \"\\f052\";\n}\n.fa-elementor:before {\n  content: \"\\f430\";\n}\n.fa-ellipsis-h:before {\n  content: \"\\f141\";\n}\n.fa-ellipsis-v:before {\n  content: \"\\f142\";\n}\n.fa-ember:before {\n  content: \"\\f423\";\n}\n.fa-empire:before {\n  content: \"\\f1d1\";\n}\n.fa-envelope:before {\n  content: \"\\f0e0\";\n}\n.fa-envelope-open:before {\n  content: \"\\f2b6\";\n}\n.fa-envelope-square:before {\n  content: \"\\f199\";\n}\n.fa-envira:before {\n  content: \"\\f299\";\n}\n.fa-equals:before {\n  content: \"\\f52c\";\n}\n.fa-eraser:before {\n  content: \"\\f12d\";\n}\n.fa-erlang:before {\n  content: \"\\f39d\";\n}\n.fa-ethereum:before {\n  content: \"\\f42e\";\n}\n.fa-etsy:before {\n  content: \"\\f2d7\";\n}\n.fa-euro-sign:before {\n  content: \"\\f153\";\n}\n.fa-exchange-alt:before {\n  content: \"\\f362\";\n}\n.fa-exclamation:before {\n  content: \"\\f12a\";\n}\n.fa-exclamation-circle:before {\n  content: \"\\f06a\";\n}\n.fa-exclamation-triangle:before {\n  content: \"\\f071\";\n}\n.fa-expand:before {\n  content: \"\\f065\";\n}\n.fa-expand-arrows-alt:before {\n  content: \"\\f31e\";\n}\n.fa-expeditedssl:before {\n  content: \"\\f23e\";\n}\n.fa-external-link-alt:before {\n  content: \"\\f35d\";\n}\n.fa-external-link-square-alt:before {\n  content: \"\\f360\";\n}\n.fa-eye:before {\n  content: \"\\f06e\";\n}\n.fa-eye-dropper:before {\n  content: \"\\f1fb\";\n}\n.fa-eye-slash:before {\n  content: \"\\f070\";\n}\n.fa-facebook:before {\n  content: \"\\f09a\";\n}\n.fa-facebook-f:before {\n  content: \"\\f39e\";\n}\n.fa-facebook-messenger:before {\n  content: \"\\f39f\";\n}\n.fa-facebook-square:before {\n  content: \"\\f082\";\n}\n.fa-fast-backward:before {\n  content: \"\\f049\";\n}\n.fa-fast-forward:before {\n  content: \"\\f050\";\n}\n.fa-fax:before {\n  content: \"\\f1ac\";\n}\n.fa-feather:before {\n  content: \"\\f52d\";\n}\n.fa-female:before {\n  content: \"\\f182\";\n}\n.fa-fighter-jet:before {\n  content: \"\\f0fb\";\n}\n.fa-file:before {\n  content: \"\\f15b\";\n}\n.fa-file-alt:before {\n  content: \"\\f15c\";\n}\n.fa-file-archive:before {\n  content: \"\\f1c6\";\n}\n.fa-file-audio:before {\n  content: \"\\f1c7\";\n}\n.fa-file-code:before {\n  content: \"\\f1c9\";\n}\n.fa-file-excel:before {\n  content: \"\\f1c3\";\n}\n.fa-file-image:before {\n  content: \"\\f1c5\";\n}\n.fa-file-medical:before {\n  content: \"\\f477\";\n}\n.fa-file-medical-alt:before {\n  content: \"\\f478\";\n}\n.fa-file-pdf:before {\n  content: \"\\f1c1\";\n}\n.fa-file-powerpoint:before {\n  content: \"\\f1c4\";\n}\n.fa-file-video:before {\n  content: \"\\f1c8\";\n}\n.fa-file-word:before {\n  content: \"\\f1c2\";\n}\n.fa-film:before {\n  content: \"\\f008\";\n}\n.fa-filter:before {\n  content: \"\\f0b0\";\n}\n.fa-fire:before {\n  content: \"\\f06d\";\n}\n.fa-fire-extinguisher:before {\n  content: \"\\f134\";\n}\n.fa-firefox:before {\n  content: \"\\f269\";\n}\n.fa-first-aid:before {\n  content: \"\\f479\";\n}\n.fa-first-order:before {\n  content: \"\\f2b0\";\n}\n.fa-first-order-alt:before {\n  content: \"\\f50a\";\n}\n.fa-firstdraft:before {\n  content: \"\\f3a1\";\n}\n.fa-flag:before {\n  content: \"\\f024\";\n}\n.fa-flag-checkered:before {\n  content: \"\\f11e\";\n}\n.fa-flask:before {\n  content: \"\\f0c3\";\n}\n.fa-flickr:before {\n  content: \"\\f16e\";\n}\n.fa-flipboard:before {\n  content: \"\\f44d\";\n}\n.fa-fly:before {\n  content: \"\\f417\";\n}\n.fa-folder:before {\n  content: \"\\f07b\";\n}\n.fa-folder-open:before {\n  content: \"\\f07c\";\n}\n.fa-font:before {\n  content: \"\\f031\";\n}\n.fa-font-awesome:before {\n  content: \"\\f2b4\";\n}\n.fa-font-awesome-alt:before {\n  content: \"\\f35c\";\n}\n.fa-font-awesome-flag:before {\n  content: \"\\f425\";\n}\n.fa-font-awesome-logo-full:before {\n  content: \"\\f4e6\";\n}\n.fa-fonticons:before {\n  content: \"\\f280\";\n}\n.fa-fonticons-fi:before {\n  content: \"\\f3a2\";\n}\n.fa-football-ball:before {\n  content: \"\\f44e\";\n}\n.fa-fort-awesome:before {\n  content: \"\\f286\";\n}\n.fa-fort-awesome-alt:before {\n  content: \"\\f3a3\";\n}\n.fa-forumbee:before {\n  content: \"\\f211\";\n}\n.fa-forward:before {\n  content: \"\\f04e\";\n}\n.fa-foursquare:before {\n  content: \"\\f180\";\n}\n.fa-free-code-camp:before {\n  content: \"\\f2c5\";\n}\n.fa-freebsd:before {\n  content: \"\\f3a4\";\n}\n.fa-frog:before {\n  content: \"\\f52e\";\n}\n.fa-frown:before {\n  content: \"\\f119\";\n}\n.fa-fulcrum:before {\n  content: \"\\f50b\";\n}\n.fa-futbol:before {\n  content: \"\\f1e3\";\n}\n.fa-galactic-republic:before {\n  content: \"\\f50c\";\n}\n.fa-galactic-senate:before {\n  content: \"\\f50d\";\n}\n.fa-gamepad:before {\n  content: \"\\f11b\";\n}\n.fa-gas-pump:before {\n  content: \"\\f52f\";\n}\n.fa-gavel:before {\n  content: \"\\f0e3\";\n}\n.fa-gem:before {\n  content: \"\\f3a5\";\n}\n.fa-genderless:before {\n  content: \"\\f22d\";\n}\n.fa-get-pocket:before {\n  content: \"\\f265\";\n}\n.fa-gg:before {\n  content: \"\\f260\";\n}\n.fa-gg-circle:before {\n  content: \"\\f261\";\n}\n.fa-gift:before {\n  content: \"\\f06b\";\n}\n.fa-git:before {\n  content: \"\\f1d3\";\n}\n.fa-git-square:before {\n  content: \"\\f1d2\";\n}\n.fa-github:before {\n  content: \"\\f09b\";\n}\n.fa-github-alt:before {\n  content: \"\\f113\";\n}\n.fa-github-square:before {\n  content: \"\\f092\";\n}\n.fa-gitkraken:before {\n  content: \"\\f3a6\";\n}\n.fa-gitlab:before {\n  content: \"\\f296\";\n}\n.fa-gitter:before {\n  content: \"\\f426\";\n}\n.fa-glass-martini:before {\n  content: \"\\f000\";\n}\n.fa-glasses:before {\n  content: \"\\f530\";\n}\n.fa-glide:before {\n  content: \"\\f2a5\";\n}\n.fa-glide-g:before {\n  content: \"\\f2a6\";\n}\n.fa-globe:before {\n  content: \"\\f0ac\";\n}\n.fa-gofore:before {\n  content: \"\\f3a7\";\n}\n.fa-golf-ball:before {\n  content: \"\\f450\";\n}\n.fa-goodreads:before {\n  content: \"\\f3a8\";\n}\n.fa-goodreads-g:before {\n  content: \"\\f3a9\";\n}\n.fa-google:before {\n  content: \"\\f1a0\";\n}\n.fa-google-drive:before {\n  content: \"\\f3aa\";\n}\n.fa-google-play:before {\n  content: \"\\f3ab\";\n}\n.fa-google-plus:before {\n  content: \"\\f2b3\";\n}\n.fa-google-plus-g:before {\n  content: \"\\f0d5\";\n}\n.fa-google-plus-square:before {\n  content: \"\\f0d4\";\n}\n.fa-google-wallet:before {\n  content: \"\\f1ee\";\n}\n.fa-graduation-cap:before {\n  content: \"\\f19d\";\n}\n.fa-gratipay:before {\n  content: \"\\f184\";\n}\n.fa-grav:before {\n  content: \"\\f2d6\";\n}\n.fa-greater-than:before {\n  content: \"\\f531\";\n}\n.fa-greater-than-equal:before {\n  content: \"\\f532\";\n}\n.fa-gripfire:before {\n  content: \"\\f3ac\";\n}\n.fa-grunt:before {\n  content: \"\\f3ad\";\n}\n.fa-gulp:before {\n  content: \"\\f3ae\";\n}\n.fa-h-square:before {\n  content: \"\\f0fd\";\n}\n.fa-hacker-news:before {\n  content: \"\\f1d4\";\n}\n.fa-hacker-news-square:before {\n  content: \"\\f3af\";\n}\n.fa-hand-holding:before {\n  content: \"\\f4bd\";\n}\n.fa-hand-holding-heart:before {\n  content: \"\\f4be\";\n}\n.fa-hand-holding-usd:before {\n  content: \"\\f4c0\";\n}\n.fa-hand-lizard:before {\n  content: \"\\f258\";\n}\n.fa-hand-paper:before {\n  content: \"\\f256\";\n}\n.fa-hand-peace:before {\n  content: \"\\f25b\";\n}\n.fa-hand-point-down:before {\n  content: \"\\f0a7\";\n}\n.fa-hand-point-left:before {\n  content: \"\\f0a5\";\n}\n.fa-hand-point-right:before {\n  content: \"\\f0a4\";\n}\n.fa-hand-point-up:before {\n  content: \"\\f0a6\";\n}\n.fa-hand-pointer:before {\n  content: \"\\f25a\";\n}\n.fa-hand-rock:before {\n  content: \"\\f255\";\n}\n.fa-hand-scissors:before {\n  content: \"\\f257\";\n}\n.fa-hand-spock:before {\n  content: \"\\f259\";\n}\n.fa-hands:before {\n  content: \"\\f4c2\";\n}\n.fa-hands-helping:before {\n  content: \"\\f4c4\";\n}\n.fa-handshake:before {\n  content: \"\\f2b5\";\n}\n.fa-hashtag:before {\n  content: \"\\f292\";\n}\n.fa-hdd:before {\n  content: \"\\f0a0\";\n}\n.fa-heading:before {\n  content: \"\\f1dc\";\n}\n.fa-headphones:before {\n  content: \"\\f025\";\n}\n.fa-heart:before {\n  content: \"\\f004\";\n}\n.fa-heartbeat:before {\n  content: \"\\f21e\";\n}\n.fa-helicopter:before {\n  content: \"\\f533\";\n}\n.fa-hips:before {\n  content: \"\\f452\";\n}\n.fa-hire-a-helper:before {\n  content: \"\\f3b0\";\n}\n.fa-history:before {\n  content: \"\\f1da\";\n}\n.fa-hockey-puck:before {\n  content: \"\\f453\";\n}\n.fa-home:before {\n  content: \"\\f015\";\n}\n.fa-hooli:before {\n  content: \"\\f427\";\n}\n.fa-hospital:before {\n  content: \"\\f0f8\";\n}\n.fa-hospital-alt:before {\n  content: \"\\f47d\";\n}\n.fa-hospital-symbol:before {\n  content: \"\\f47e\";\n}\n.fa-hotjar:before {\n  content: \"\\f3b1\";\n}\n.fa-hourglass:before {\n  content: \"\\f254\";\n}\n.fa-hourglass-end:before {\n  content: \"\\f253\";\n}\n.fa-hourglass-half:before {\n  content: \"\\f252\";\n}\n.fa-hourglass-start:before {\n  content: \"\\f251\";\n}\n.fa-houzz:before {\n  content: \"\\f27c\";\n}\n.fa-html5:before {\n  content: \"\\f13b\";\n}\n.fa-hubspot:before {\n  content: \"\\f3b2\";\n}\n.fa-i-cursor:before {\n  content: \"\\f246\";\n}\n.fa-id-badge:before {\n  content: \"\\f2c1\";\n}\n.fa-id-card:before {\n  content: \"\\f2c2\";\n}\n.fa-id-card-alt:before {\n  content: \"\\f47f\";\n}\n.fa-image:before {\n  content: \"\\f03e\";\n}\n.fa-images:before {\n  content: \"\\f302\";\n}\n.fa-imdb:before {\n  content: \"\\f2d8\";\n}\n.fa-inbox:before {\n  content: \"\\f01c\";\n}\n.fa-indent:before {\n  content: \"\\f03c\";\n}\n.fa-industry:before {\n  content: \"\\f275\";\n}\n.fa-infinity:before {\n  content: \"\\f534\";\n}\n.fa-info:before {\n  content: \"\\f129\";\n}\n.fa-info-circle:before {\n  content: \"\\f05a\";\n}\n.fa-instagram:before {\n  content: \"\\f16d\";\n}\n.fa-internet-explorer:before {\n  content: \"\\f26b\";\n}\n.fa-ioxhost:before {\n  content: \"\\f208\";\n}\n.fa-italic:before {\n  content: \"\\f033\";\n}\n.fa-itunes:before {\n  content: \"\\f3b4\";\n}\n.fa-itunes-note:before {\n  content: \"\\f3b5\";\n}\n.fa-java:before {\n  content: \"\\f4e4\";\n}\n.fa-jedi-order:before {\n  content: \"\\f50e\";\n}\n.fa-jenkins:before {\n  content: \"\\f3b6\";\n}\n.fa-joget:before {\n  content: \"\\f3b7\";\n}\n.fa-joomla:before {\n  content: \"\\f1aa\";\n}\n.fa-js:before {\n  content: \"\\f3b8\";\n}\n.fa-js-square:before {\n  content: \"\\f3b9\";\n}\n.fa-jsfiddle:before {\n  content: \"\\f1cc\";\n}\n.fa-key:before {\n  content: \"\\f084\";\n}\n.fa-keybase:before {\n  content: \"\\f4f5\";\n}\n.fa-keyboard:before {\n  content: \"\\f11c\";\n}\n.fa-keycdn:before {\n  content: \"\\f3ba\";\n}\n.fa-kickstarter:before {\n  content: \"\\f3bb\";\n}\n.fa-kickstarter-k:before {\n  content: \"\\f3bc\";\n}\n.fa-kiwi-bird:before {\n  content: \"\\f535\";\n}\n.fa-korvue:before {\n  content: \"\\f42f\";\n}\n.fa-language:before {\n  content: \"\\f1ab\";\n}\n.fa-laptop:before {\n  content: \"\\f109\";\n}\n.fa-laravel:before {\n  content: \"\\f3bd\";\n}\n.fa-lastfm:before {\n  content: \"\\f202\";\n}\n.fa-lastfm-square:before {\n  content: \"\\f203\";\n}\n.fa-leaf:before {\n  content: \"\\f06c\";\n}\n.fa-leanpub:before {\n  content: \"\\f212\";\n}\n.fa-lemon:before {\n  content: \"\\f094\";\n}\n.fa-less:before {\n  content: \"\\f41d\";\n}\n.fa-less-than:before {\n  content: \"\\f536\";\n}\n.fa-less-than-equal:before {\n  content: \"\\f537\";\n}\n.fa-level-down-alt:before {\n  content: \"\\f3be\";\n}\n.fa-level-up-alt:before {\n  content: \"\\f3bf\";\n}\n.fa-life-ring:before {\n  content: \"\\f1cd\";\n}\n.fa-lightbulb:before {\n  content: \"\\f0eb\";\n}\n.fa-line:before {\n  content: \"\\f3c0\";\n}\n.fa-link:before {\n  content: \"\\f0c1\";\n}\n.fa-linkedin:before {\n  content: \"\\f08c\";\n}\n.fa-linkedin-in:before {\n  content: \"\\f0e1\";\n}\n.fa-linode:before {\n  content: \"\\f2b8\";\n}\n.fa-linux:before {\n  content: \"\\f17c\";\n}\n.fa-lira-sign:before {\n  content: \"\\f195\";\n}\n.fa-list:before {\n  content: \"\\f03a\";\n}\n.fa-list-alt:before {\n  content: \"\\f022\";\n}\n.fa-list-ol:before {\n  content: \"\\f0cb\";\n}\n.fa-list-ul:before {\n  content: \"\\f0ca\";\n}\n.fa-location-arrow:before {\n  content: \"\\f124\";\n}\n.fa-lock:before {\n  content: \"\\f023\";\n}\n.fa-lock-open:before {\n  content: \"\\f3c1\";\n}\n.fa-long-arrow-alt-down:before {\n  content: \"\\f309\";\n}\n.fa-long-arrow-alt-left:before {\n  content: \"\\f30a\";\n}\n.fa-long-arrow-alt-right:before {\n  content: \"\\f30b\";\n}\n.fa-long-arrow-alt-up:before {\n  content: \"\\f30c\";\n}\n.fa-low-vision:before {\n  content: \"\\f2a8\";\n}\n.fa-lyft:before {\n  content: \"\\f3c3\";\n}\n.fa-magento:before {\n  content: \"\\f3c4\";\n}\n.fa-magic:before {\n  content: \"\\f0d0\";\n}\n.fa-magnet:before {\n  content: \"\\f076\";\n}\n.fa-male:before {\n  content: \"\\f183\";\n}\n.fa-mandalorian:before {\n  content: \"\\f50f\";\n}\n.fa-map:before {\n  content: \"\\f279\";\n}\n.fa-map-marker:before {\n  content: \"\\f041\";\n}\n.fa-map-marker-alt:before {\n  content: \"\\f3c5\";\n}\n.fa-map-pin:before {\n  content: \"\\f276\";\n}\n.fa-map-signs:before {\n  content: \"\\f277\";\n}\n.fa-mars:before {\n  content: \"\\f222\";\n}\n.fa-mars-double:before {\n  content: \"\\f227\";\n}\n.fa-mars-stroke:before {\n  content: \"\\f229\";\n}\n.fa-mars-stroke-h:before {\n  content: \"\\f22b\";\n}\n.fa-mars-stroke-v:before {\n  content: \"\\f22a\";\n}\n.fa-mastodon:before {\n  content: \"\\f4f6\";\n}\n.fa-maxcdn:before {\n  content: \"\\f136\";\n}\n.fa-medapps:before {\n  content: \"\\f3c6\";\n}\n.fa-medium:before {\n  content: \"\\f23a\";\n}\n.fa-medium-m:before {\n  content: \"\\f3c7\";\n}\n.fa-medkit:before {\n  content: \"\\f0fa\";\n}\n.fa-medrt:before {\n  content: \"\\f3c8\";\n}\n.fa-meetup:before {\n  content: \"\\f2e0\";\n}\n.fa-meh:before {\n  content: \"\\f11a\";\n}\n.fa-memory:before {\n  content: \"\\f538\";\n}\n.fa-mercury:before {\n  content: \"\\f223\";\n}\n.fa-microchip:before {\n  content: \"\\f2db\";\n}\n.fa-microphone:before {\n  content: \"\\f130\";\n}\n.fa-microphone-alt:before {\n  content: \"\\f3c9\";\n}\n.fa-microphone-alt-slash:before {\n  content: \"\\f539\";\n}\n.fa-microphone-slash:before {\n  content: \"\\f131\";\n}\n.fa-microsoft:before {\n  content: \"\\f3ca\";\n}\n.fa-minus:before {\n  content: \"\\f068\";\n}\n.fa-minus-circle:before {\n  content: \"\\f056\";\n}\n.fa-minus-square:before {\n  content: \"\\f146\";\n}\n.fa-mix:before {\n  content: \"\\f3cb\";\n}\n.fa-mixcloud:before {\n  content: \"\\f289\";\n}\n.fa-mizuni:before {\n  content: \"\\f3cc\";\n}\n.fa-mobile:before {\n  content: \"\\f10b\";\n}\n.fa-mobile-alt:before {\n  content: \"\\f3cd\";\n}\n.fa-modx:before {\n  content: \"\\f285\";\n}\n.fa-monero:before {\n  content: \"\\f3d0\";\n}\n.fa-money-bill:before {\n  content: \"\\f0d6\";\n}\n.fa-money-bill-alt:before {\n  content: \"\\f3d1\";\n}\n.fa-money-bill-wave:before {\n  content: \"\\f53a\";\n}\n.fa-money-bill-wave-alt:before {\n  content: \"\\f53b\";\n}\n.fa-money-check:before {\n  content: \"\\f53c\";\n}\n.fa-money-check-alt:before {\n  content: \"\\f53d\";\n}\n.fa-moon:before {\n  content: \"\\f186\";\n}\n.fa-motorcycle:before {\n  content: \"\\f21c\";\n}\n.fa-mouse-pointer:before {\n  content: \"\\f245\";\n}\n.fa-music:before {\n  content: \"\\f001\";\n}\n.fa-napster:before {\n  content: \"\\f3d2\";\n}\n.fa-neuter:before {\n  content: \"\\f22c\";\n}\n.fa-newspaper:before {\n  content: \"\\f1ea\";\n}\n.fa-nintendo-switch:before {\n  content: \"\\f418\";\n}\n.fa-node:before {\n  content: \"\\f419\";\n}\n.fa-node-js:before {\n  content: \"\\f3d3\";\n}\n.fa-not-equal:before {\n  content: \"\\f53e\";\n}\n.fa-notes-medical:before {\n  content: \"\\f481\";\n}\n.fa-npm:before {\n  content: \"\\f3d4\";\n}\n.fa-ns8:before {\n  content: \"\\f3d5\";\n}\n.fa-nutritionix:before {\n  content: \"\\f3d6\";\n}\n.fa-object-group:before {\n  content: \"\\f247\";\n}\n.fa-object-ungroup:before {\n  content: \"\\f248\";\n}\n.fa-odnoklassniki:before {\n  content: \"\\f263\";\n}\n.fa-odnoklassniki-square:before {\n  content: \"\\f264\";\n}\n.fa-old-republic:before {\n  content: \"\\f510\";\n}\n.fa-opencart:before {\n  content: \"\\f23d\";\n}\n.fa-openid:before {\n  content: \"\\f19b\";\n}\n.fa-opera:before {\n  content: \"\\f26a\";\n}\n.fa-optin-monster:before {\n  content: \"\\f23c\";\n}\n.fa-osi:before {\n  content: \"\\f41a\";\n}\n.fa-outdent:before {\n  content: \"\\f03b\";\n}\n.fa-page4:before {\n  content: \"\\f3d7\";\n}\n.fa-pagelines:before {\n  content: \"\\f18c\";\n}\n.fa-paint-brush:before {\n  content: \"\\f1fc\";\n}\n.fa-palette:before {\n  content: \"\\f53f\";\n}\n.fa-palfed:before {\n  content: \"\\f3d8\";\n}\n.fa-pallet:before {\n  content: \"\\f482\";\n}\n.fa-paper-plane:before {\n  content: \"\\f1d8\";\n}\n.fa-paperclip:before {\n  content: \"\\f0c6\";\n}\n.fa-parachute-box:before {\n  content: \"\\f4cd\";\n}\n.fa-paragraph:before {\n  content: \"\\f1dd\";\n}\n.fa-parking:before {\n  content: \"\\f540\";\n}\n.fa-paste:before {\n  content: \"\\f0ea\";\n}\n.fa-patreon:before {\n  content: \"\\f3d9\";\n}\n.fa-pause:before {\n  content: \"\\f04c\";\n}\n.fa-pause-circle:before {\n  content: \"\\f28b\";\n}\n.fa-paw:before {\n  content: \"\\f1b0\";\n}\n.fa-paypal:before {\n  content: \"\\f1ed\";\n}\n.fa-pen-square:before {\n  content: \"\\f14b\";\n}\n.fa-pencil-alt:before {\n  content: \"\\f303\";\n}\n.fa-people-carry:before {\n  content: \"\\f4ce\";\n}\n.fa-percent:before {\n  content: \"\\f295\";\n}\n.fa-percentage:before {\n  content: \"\\f541\";\n}\n.fa-periscope:before {\n  content: \"\\f3da\";\n}\n.fa-phabricator:before {\n  content: \"\\f3db\";\n}\n.fa-phoenix-framework:before {\n  content: \"\\f3dc\";\n}\n.fa-phoenix-squadron:before {\n  content: \"\\f511\";\n}\n.fa-phone:before {\n  content: \"\\f095\";\n}\n.fa-phone-slash:before {\n  content: \"\\f3dd\";\n}\n.fa-phone-square:before {\n  content: \"\\f098\";\n}\n.fa-phone-volume:before {\n  content: \"\\f2a0\";\n}\n.fa-php:before {\n  content: \"\\f457\";\n}\n.fa-pied-piper:before {\n  content: \"\\f2ae\";\n}\n.fa-pied-piper-alt:before {\n  content: \"\\f1a8\";\n}\n.fa-pied-piper-hat:before {\n  content: \"\\f4e5\";\n}\n.fa-pied-piper-pp:before {\n  content: \"\\f1a7\";\n}\n.fa-piggy-bank:before {\n  content: \"\\f4d3\";\n}\n.fa-pills:before {\n  content: \"\\f484\";\n}\n.fa-pinterest:before {\n  content: \"\\f0d2\";\n}\n.fa-pinterest-p:before {\n  content: \"\\f231\";\n}\n.fa-pinterest-square:before {\n  content: \"\\f0d3\";\n}\n.fa-plane:before {\n  content: \"\\f072\";\n}\n.fa-play:before {\n  content: \"\\f04b\";\n}\n.fa-play-circle:before {\n  content: \"\\f144\";\n}\n.fa-playstation:before {\n  content: \"\\f3df\";\n}\n.fa-plug:before {\n  content: \"\\f1e6\";\n}\n.fa-plus:before {\n  content: \"\\f067\";\n}\n.fa-plus-circle:before {\n  content: \"\\f055\";\n}\n.fa-plus-square:before {\n  content: \"\\f0fe\";\n}\n.fa-podcast:before {\n  content: \"\\f2ce\";\n}\n.fa-poo:before {\n  content: \"\\f2fe\";\n}\n.fa-portrait:before {\n  content: \"\\f3e0\";\n}\n.fa-pound-sign:before {\n  content: \"\\f154\";\n}\n.fa-power-off:before {\n  content: \"\\f011\";\n}\n.fa-prescription-bottle:before {\n  content: \"\\f485\";\n}\n.fa-prescription-bottle-alt:before {\n  content: \"\\f486\";\n}\n.fa-print:before {\n  content: \"\\f02f\";\n}\n.fa-procedures:before {\n  content: \"\\f487\";\n}\n.fa-product-hunt:before {\n  content: \"\\f288\";\n}\n.fa-project-diagram:before {\n  content: \"\\f542\";\n}\n.fa-pushed:before {\n  content: \"\\f3e1\";\n}\n.fa-puzzle-piece:before {\n  content: \"\\f12e\";\n}\n.fa-python:before {\n  content: \"\\f3e2\";\n}\n.fa-qq:before {\n  content: \"\\f1d6\";\n}\n.fa-qrcode:before {\n  content: \"\\f029\";\n}\n.fa-question:before {\n  content: \"\\f128\";\n}\n.fa-question-circle:before {\n  content: \"\\f059\";\n}\n.fa-quidditch:before {\n  content: \"\\f458\";\n}\n.fa-quinscape:before {\n  content: \"\\f459\";\n}\n.fa-quora:before {\n  content: \"\\f2c4\";\n}\n.fa-quote-left:before {\n  content: \"\\f10d\";\n}\n.fa-quote-right:before {\n  content: \"\\f10e\";\n}\n.fa-r-project:before {\n  content: \"\\f4f7\";\n}\n.fa-random:before {\n  content: \"\\f074\";\n}\n.fa-ravelry:before {\n  content: \"\\f2d9\";\n}\n.fa-react:before {\n  content: \"\\f41b\";\n}\n.fa-readme:before {\n  content: \"\\f4d5\";\n}\n.fa-rebel:before {\n  content: \"\\f1d0\";\n}\n.fa-receipt:before {\n  content: \"\\f543\";\n}\n.fa-recycle:before {\n  content: \"\\f1b8\";\n}\n.fa-red-river:before {\n  content: \"\\f3e3\";\n}\n.fa-reddit:before {\n  content: \"\\f1a1\";\n}\n.fa-reddit-alien:before {\n  content: \"\\f281\";\n}\n.fa-reddit-square:before {\n  content: \"\\f1a2\";\n}\n.fa-redo:before {\n  content: \"\\f01e\";\n}\n.fa-redo-alt:before {\n  content: \"\\f2f9\";\n}\n.fa-registered:before {\n  content: \"\\f25d\";\n}\n.fa-rendact:before {\n  content: \"\\f3e4\";\n}\n.fa-renren:before {\n  content: \"\\f18b\";\n}\n.fa-reply:before {\n  content: \"\\f3e5\";\n}\n.fa-reply-all:before {\n  content: \"\\f122\";\n}\n.fa-replyd:before {\n  content: \"\\f3e6\";\n}\n.fa-researchgate:before {\n  content: \"\\f4f8\";\n}\n.fa-resolving:before {\n  content: \"\\f3e7\";\n}\n.fa-retweet:before {\n  content: \"\\f079\";\n}\n.fa-ribbon:before {\n  content: \"\\f4d6\";\n}\n.fa-road:before {\n  content: \"\\f018\";\n}\n.fa-robot:before {\n  content: \"\\f544\";\n}\n.fa-rocket:before {\n  content: \"\\f135\";\n}\n.fa-rocketchat:before {\n  content: \"\\f3e8\";\n}\n.fa-rockrms:before {\n  content: \"\\f3e9\";\n}\n.fa-rss:before {\n  content: \"\\f09e\";\n}\n.fa-rss-square:before {\n  content: \"\\f143\";\n}\n.fa-ruble-sign:before {\n  content: \"\\f158\";\n}\n.fa-ruler:before {\n  content: \"\\f545\";\n}\n.fa-ruler-combined:before {\n  content: \"\\f546\";\n}\n.fa-ruler-horizontal:before {\n  content: \"\\f547\";\n}\n.fa-ruler-vertical:before {\n  content: \"\\f548\";\n}\n.fa-rupee-sign:before {\n  content: \"\\f156\";\n}\n.fa-safari:before {\n  content: \"\\f267\";\n}\n.fa-sass:before {\n  content: \"\\f41e\";\n}\n.fa-save:before {\n  content: \"\\f0c7\";\n}\n.fa-schlix:before {\n  content: \"\\f3ea\";\n}\n.fa-school:before {\n  content: \"\\f549\";\n}\n.fa-screwdriver:before {\n  content: \"\\f54a\";\n}\n.fa-scribd:before {\n  content: \"\\f28a\";\n}\n.fa-search:before {\n  content: \"\\f002\";\n}\n.fa-search-minus:before {\n  content: \"\\f010\";\n}\n.fa-search-plus:before {\n  content: \"\\f00e\";\n}\n.fa-searchengin:before {\n  content: \"\\f3eb\";\n}\n.fa-seedling:before {\n  content: \"\\f4d8\";\n}\n.fa-sellcast:before {\n  content: \"\\f2da\";\n}\n.fa-sellsy:before {\n  content: \"\\f213\";\n}\n.fa-server:before {\n  content: \"\\f233\";\n}\n.fa-servicestack:before {\n  content: \"\\f3ec\";\n}\n.fa-share:before {\n  content: \"\\f064\";\n}\n.fa-share-alt:before {\n  content: \"\\f1e0\";\n}\n.fa-share-alt-square:before {\n  content: \"\\f1e1\";\n}\n.fa-share-square:before {\n  content: \"\\f14d\";\n}\n.fa-shekel-sign:before {\n  content: \"\\f20b\";\n}\n.fa-shield-alt:before {\n  content: \"\\f3ed\";\n}\n.fa-ship:before {\n  content: \"\\f21a\";\n}\n.fa-shipping-fast:before {\n  content: \"\\f48b\";\n}\n.fa-shirtsinbulk:before {\n  content: \"\\f214\";\n}\n.fa-shoe-prints:before {\n  content: \"\\f54b\";\n}\n.fa-shopping-bag:before {\n  content: \"\\f290\";\n}\n.fa-shopping-basket:before {\n  content: \"\\f291\";\n}\n.fa-shopping-cart:before {\n  content: \"\\f07a\";\n}\n.fa-shower:before {\n  content: \"\\f2cc\";\n}\n.fa-sign:before {\n  content: \"\\f4d9\";\n}\n.fa-sign-in-alt:before {\n  content: \"\\f2f6\";\n}\n.fa-sign-language:before {\n  content: \"\\f2a7\";\n}\n.fa-sign-out-alt:before {\n  content: \"\\f2f5\";\n}\n.fa-signal:before {\n  content: \"\\f012\";\n}\n.fa-simplybuilt:before {\n  content: \"\\f215\";\n}\n.fa-sistrix:before {\n  content: \"\\f3ee\";\n}\n.fa-sitemap:before {\n  content: \"\\f0e8\";\n}\n.fa-sith:before {\n  content: \"\\f512\";\n}\n.fa-skull:before {\n  content: \"\\f54c\";\n}\n.fa-skyatlas:before {\n  content: \"\\f216\";\n}\n.fa-skype:before {\n  content: \"\\f17e\";\n}\n.fa-slack:before {\n  content: \"\\f198\";\n}\n.fa-slack-hash:before {\n  content: \"\\f3ef\";\n}\n.fa-sliders-h:before {\n  content: \"\\f1de\";\n}\n.fa-slideshare:before {\n  content: \"\\f1e7\";\n}\n.fa-smile:before {\n  content: \"\\f118\";\n}\n.fa-smoking:before {\n  content: \"\\f48d\";\n}\n.fa-smoking-ban:before {\n  content: \"\\f54d\";\n}\n.fa-snapchat:before {\n  content: \"\\f2ab\";\n}\n.fa-snapchat-ghost:before {\n  content: \"\\f2ac\";\n}\n.fa-snapchat-square:before {\n  content: \"\\f2ad\";\n}\n.fa-snowflake:before {\n  content: \"\\f2dc\";\n}\n.fa-sort:before {\n  content: \"\\f0dc\";\n}\n.fa-sort-alpha-down:before {\n  content: \"\\f15d\";\n}\n.fa-sort-alpha-up:before {\n  content: \"\\f15e\";\n}\n.fa-sort-amount-down:before {\n  content: \"\\f160\";\n}\n.fa-sort-amount-up:before {\n  content: \"\\f161\";\n}\n.fa-sort-down:before {\n  content: \"\\f0dd\";\n}\n.fa-sort-numeric-down:before {\n  content: \"\\f162\";\n}\n.fa-sort-numeric-up:before {\n  content: \"\\f163\";\n}\n.fa-sort-up:before {\n  content: \"\\f0de\";\n}\n.fa-soundcloud:before {\n  content: \"\\f1be\";\n}\n.fa-space-shuttle:before {\n  content: \"\\f197\";\n}\n.fa-speakap:before {\n  content: \"\\f3f3\";\n}\n.fa-spinner:before {\n  content: \"\\f110\";\n}\n.fa-spotify:before {\n  content: \"\\f1bc\";\n}\n.fa-square:before {\n  content: \"\\f0c8\";\n}\n.fa-square-full:before {\n  content: \"\\f45c\";\n}\n.fa-stack-exchange:before {\n  content: \"\\f18d\";\n}\n.fa-stack-overflow:before {\n  content: \"\\f16c\";\n}\n.fa-star:before {\n  content: \"\\f005\";\n}\n.fa-star-half:before {\n  content: \"\\f089\";\n}\n.fa-staylinked:before {\n  content: \"\\f3f5\";\n}\n.fa-steam:before {\n  content: \"\\f1b6\";\n}\n.fa-steam-square:before {\n  content: \"\\f1b7\";\n}\n.fa-steam-symbol:before {\n  content: \"\\f3f6\";\n}\n.fa-step-backward:before {\n  content: \"\\f048\";\n}\n.fa-step-forward:before {\n  content: \"\\f051\";\n}\n.fa-stethoscope:before {\n  content: \"\\f0f1\";\n}\n.fa-sticker-mule:before {\n  content: \"\\f3f7\";\n}\n.fa-sticky-note:before {\n  content: \"\\f249\";\n}\n.fa-stop:before {\n  content: \"\\f04d\";\n}\n.fa-stop-circle:before {\n  content: \"\\f28d\";\n}\n.fa-stopwatch:before {\n  content: \"\\f2f2\";\n}\n.fa-store:before {\n  content: \"\\f54e\";\n}\n.fa-store-alt:before {\n  content: \"\\f54f\";\n}\n.fa-strava:before {\n  content: \"\\f428\";\n}\n.fa-stream:before {\n  content: \"\\f550\";\n}\n.fa-street-view:before {\n  content: \"\\f21d\";\n}\n.fa-strikethrough:before {\n  content: \"\\f0cc\";\n}\n.fa-stripe:before {\n  content: \"\\f429\";\n}\n.fa-stripe-s:before {\n  content: \"\\f42a\";\n}\n.fa-stroopwafel:before {\n  content: \"\\f551\";\n}\n.fa-studiovinari:before {\n  content: \"\\f3f8\";\n}\n.fa-stumbleupon:before {\n  content: \"\\f1a4\";\n}\n.fa-stumbleupon-circle:before {\n  content: \"\\f1a3\";\n}\n.fa-subscript:before {\n  content: \"\\f12c\";\n}\n.fa-subway:before {\n  content: \"\\f239\";\n}\n.fa-suitcase:before {\n  content: \"\\f0f2\";\n}\n.fa-sun:before {\n  content: \"\\f185\";\n}\n.fa-superpowers:before {\n  content: \"\\f2dd\";\n}\n.fa-superscript:before {\n  content: \"\\f12b\";\n}\n.fa-supple:before {\n  content: \"\\f3f9\";\n}\n.fa-sync:before {\n  content: \"\\f021\";\n}\n.fa-sync-alt:before {\n  content: \"\\f2f1\";\n}\n.fa-syringe:before {\n  content: \"\\f48e\";\n}\n.fa-table:before {\n  content: \"\\f0ce\";\n}\n.fa-table-tennis:before {\n  content: \"\\f45d\";\n}\n.fa-tablet:before {\n  content: \"\\f10a\";\n}\n.fa-tablet-alt:before {\n  content: \"\\f3fa\";\n}\n.fa-tablets:before {\n  content: \"\\f490\";\n}\n.fa-tachometer-alt:before {\n  content: \"\\f3fd\";\n}\n.fa-tag:before {\n  content: \"\\f02b\";\n}\n.fa-tags:before {\n  content: \"\\f02c\";\n}\n.fa-tape:before {\n  content: \"\\f4db\";\n}\n.fa-tasks:before {\n  content: \"\\f0ae\";\n}\n.fa-taxi:before {\n  content: \"\\f1ba\";\n}\n.fa-teamspeak:before {\n  content: \"\\f4f9\";\n}\n.fa-telegram:before {\n  content: \"\\f2c6\";\n}\n.fa-telegram-plane:before {\n  content: \"\\f3fe\";\n}\n.fa-tencent-weibo:before {\n  content: \"\\f1d5\";\n}\n.fa-terminal:before {\n  content: \"\\f120\";\n}\n.fa-text-height:before {\n  content: \"\\f034\";\n}\n.fa-text-width:before {\n  content: \"\\f035\";\n}\n.fa-th:before {\n  content: \"\\f00a\";\n}\n.fa-th-large:before {\n  content: \"\\f009\";\n}\n.fa-th-list:before {\n  content: \"\\f00b\";\n}\n.fa-themeisle:before {\n  content: \"\\f2b2\";\n}\n.fa-thermometer:before {\n  content: \"\\f491\";\n}\n.fa-thermometer-empty:before {\n  content: \"\\f2cb\";\n}\n.fa-thermometer-full:before {\n  content: \"\\f2c7\";\n}\n.fa-thermometer-half:before {\n  content: \"\\f2c9\";\n}\n.fa-thermometer-quarter:before {\n  content: \"\\f2ca\";\n}\n.fa-thermometer-three-quarters:before {\n  content: \"\\f2c8\";\n}\n.fa-thumbs-down:before {\n  content: \"\\f165\";\n}\n.fa-thumbs-up:before {\n  content: \"\\f164\";\n}\n.fa-thumbtack:before {\n  content: \"\\f08d\";\n}\n.fa-ticket-alt:before {\n  content: \"\\f3ff\";\n}\n.fa-times:before {\n  content: \"\\f00d\";\n}\n.fa-times-circle:before {\n  content: \"\\f057\";\n}\n.fa-tint:before {\n  content: \"\\f043\";\n}\n.fa-toggle-off:before {\n  content: \"\\f204\";\n}\n.fa-toggle-on:before {\n  content: \"\\f205\";\n}\n.fa-toolbox:before {\n  content: \"\\f552\";\n}\n.fa-trade-federation:before {\n  content: \"\\f513\";\n}\n.fa-trademark:before {\n  content: \"\\f25c\";\n}\n.fa-train:before {\n  content: \"\\f238\";\n}\n.fa-transgender:before {\n  content: \"\\f224\";\n}\n.fa-transgender-alt:before {\n  content: \"\\f225\";\n}\n.fa-trash:before {\n  content: \"\\f1f8\";\n}\n.fa-trash-alt:before {\n  content: \"\\f2ed\";\n}\n.fa-tree:before {\n  content: \"\\f1bb\";\n}\n.fa-trello:before {\n  content: \"\\f181\";\n}\n.fa-tripadvisor:before {\n  content: \"\\f262\";\n}\n.fa-trophy:before {\n  content: \"\\f091\";\n}\n.fa-truck:before {\n  content: \"\\f0d1\";\n}\n.fa-truck-loading:before {\n  content: \"\\f4de\";\n}\n.fa-truck-moving:before {\n  content: \"\\f4df\";\n}\n.fa-tshirt:before {\n  content: \"\\f553\";\n}\n.fa-tty:before {\n  content: \"\\f1e4\";\n}\n.fa-tumblr:before {\n  content: \"\\f173\";\n}\n.fa-tumblr-square:before {\n  content: \"\\f174\";\n}\n.fa-tv:before {\n  content: \"\\f26c\";\n}\n.fa-twitch:before {\n  content: \"\\f1e8\";\n}\n.fa-twitter:before {\n  content: \"\\f099\";\n}\n.fa-twitter-square:before {\n  content: \"\\f081\";\n}\n.fa-typo3:before {\n  content: \"\\f42b\";\n}\n.fa-uber:before {\n  content: \"\\f402\";\n}\n.fa-uikit:before {\n  content: \"\\f403\";\n}\n.fa-umbrella:before {\n  content: \"\\f0e9\";\n}\n.fa-underline:before {\n  content: \"\\f0cd\";\n}\n.fa-undo:before {\n  content: \"\\f0e2\";\n}\n.fa-undo-alt:before {\n  content: \"\\f2ea\";\n}\n.fa-uniregistry:before {\n  content: \"\\f404\";\n}\n.fa-universal-access:before {\n  content: \"\\f29a\";\n}\n.fa-university:before {\n  content: \"\\f19c\";\n}\n.fa-unlink:before {\n  content: \"\\f127\";\n}\n.fa-unlock:before {\n  content: \"\\f09c\";\n}\n.fa-unlock-alt:before {\n  content: \"\\f13e\";\n}\n.fa-untappd:before {\n  content: \"\\f405\";\n}\n.fa-upload:before {\n  content: \"\\f093\";\n}\n.fa-usb:before {\n  content: \"\\f287\";\n}\n.fa-user:before {\n  content: \"\\f007\";\n}\n.fa-user-alt:before {\n  content: \"\\f406\";\n}\n.fa-user-alt-slash:before {\n  content: \"\\f4fa\";\n}\n.fa-user-astronaut:before {\n  content: \"\\f4fb\";\n}\n.fa-user-check:before {\n  content: \"\\f4fc\";\n}\n.fa-user-circle:before {\n  content: \"\\f2bd\";\n}\n.fa-user-clock:before {\n  content: \"\\f4fd\";\n}\n.fa-user-cog:before {\n  content: \"\\f4fe\";\n}\n.fa-user-edit:before {\n  content: \"\\f4ff\";\n}\n.fa-user-friends:before {\n  content: \"\\f500\";\n}\n.fa-user-graduate:before {\n  content: \"\\f501\";\n}\n.fa-user-lock:before {\n  content: \"\\f502\";\n}\n.fa-user-md:before {\n  content: \"\\f0f0\";\n}\n.fa-user-minus:before {\n  content: \"\\f503\";\n}\n.fa-user-ninja:before {\n  content: \"\\f504\";\n}\n.fa-user-plus:before {\n  content: \"\\f234\";\n}\n.fa-user-secret:before {\n  content: \"\\f21b\";\n}\n.fa-user-shield:before {\n  content: \"\\f505\";\n}\n.fa-user-slash:before {\n  content: \"\\f506\";\n}\n.fa-user-tag:before {\n  content: \"\\f507\";\n}\n.fa-user-tie:before {\n  content: \"\\f508\";\n}\n.fa-user-times:before {\n  content: \"\\f235\";\n}\n.fa-users:before {\n  content: \"\\f0c0\";\n}\n.fa-users-cog:before {\n  content: \"\\f509\";\n}\n.fa-ussunnah:before {\n  content: \"\\f407\";\n}\n.fa-utensil-spoon:before {\n  content: \"\\f2e5\";\n}\n.fa-utensils:before {\n  content: \"\\f2e7\";\n}\n.fa-vaadin:before {\n  content: \"\\f408\";\n}\n.fa-venus:before {\n  content: \"\\f221\";\n}\n.fa-venus-double:before {\n  content: \"\\f226\";\n}\n.fa-venus-mars:before {\n  content: \"\\f228\";\n}\n.fa-viacoin:before {\n  content: \"\\f237\";\n}\n.fa-viadeo:before {\n  content: \"\\f2a9\";\n}\n.fa-viadeo-square:before {\n  content: \"\\f2aa\";\n}\n.fa-vial:before {\n  content: \"\\f492\";\n}\n.fa-vials:before {\n  content: \"\\f493\";\n}\n.fa-viber:before {\n  content: \"\\f409\";\n}\n.fa-video:before {\n  content: \"\\f03d\";\n}\n.fa-video-slash:before {\n  content: \"\\f4e2\";\n}\n.fa-vimeo:before {\n  content: \"\\f40a\";\n}\n.fa-vimeo-square:before {\n  content: \"\\f194\";\n}\n.fa-vimeo-v:before {\n  content: \"\\f27d\";\n}\n.fa-vine:before {\n  content: \"\\f1ca\";\n}\n.fa-vk:before {\n  content: \"\\f189\";\n}\n.fa-vnv:before {\n  content: \"\\f40b\";\n}\n.fa-volleyball-ball:before {\n  content: \"\\f45f\";\n}\n.fa-volume-down:before {\n  content: \"\\f027\";\n}\n.fa-volume-off:before {\n  content: \"\\f026\";\n}\n.fa-volume-up:before {\n  content: \"\\f028\";\n}\n.fa-vuejs:before {\n  content: \"\\f41f\";\n}\n.fa-walking:before {\n  content: \"\\f554\";\n}\n.fa-wallet:before {\n  content: \"\\f555\";\n}\n.fa-warehouse:before {\n  content: \"\\f494\";\n}\n.fa-weibo:before {\n  content: \"\\f18a\";\n}\n.fa-weight:before {\n  content: \"\\f496\";\n}\n.fa-weixin:before {\n  content: \"\\f1d7\";\n}\n.fa-whatsapp:before {\n  content: \"\\f232\";\n}\n.fa-whatsapp-square:before {\n  content: \"\\f40c\";\n}\n.fa-wheelchair:before {\n  content: \"\\f193\";\n}\n.fa-whmcs:before {\n  content: \"\\f40d\";\n}\n.fa-wifi:before {\n  content: \"\\f1eb\";\n}\n.fa-wikipedia-w:before {\n  content: \"\\f266\";\n}\n.fa-window-close:before {\n  content: \"\\f410\";\n}\n.fa-window-maximize:before {\n  content: \"\\f2d0\";\n}\n.fa-window-minimize:before {\n  content: \"\\f2d1\";\n}\n.fa-window-restore:before {\n  content: \"\\f2d2\";\n}\n.fa-windows:before {\n  content: \"\\f17a\";\n}\n.fa-wine-glass:before {\n  content: \"\\f4e3\";\n}\n.fa-wolf-pack-battalion:before {\n  content: \"\\f514\";\n}\n.fa-won-sign:before {\n  content: \"\\f159\";\n}\n.fa-wordpress:before {\n  content: \"\\f19a\";\n}\n.fa-wordpress-simple:before {\n  content: \"\\f411\";\n}\n.fa-wpbeginner:before {\n  content: \"\\f297\";\n}\n.fa-wpexplorer:before {\n  content: \"\\f2de\";\n}\n.fa-wpforms:before {\n  content: \"\\f298\";\n}\n.fa-wrench:before {\n  content: \"\\f0ad\";\n}\n.fa-x-ray:before {\n  content: \"\\f497\";\n}\n.fa-xbox:before {\n  content: \"\\f412\";\n}\n.fa-xing:before {\n  content: \"\\f168\";\n}\n.fa-xing-square:before {\n  content: \"\\f169\";\n}\n.fa-y-combinator:before {\n  content: \"\\f23b\";\n}\n.fa-yahoo:before {\n  content: \"\\f19e\";\n}\n.fa-yandex:before {\n  content: \"\\f413\";\n}\n.fa-yandex-international:before {\n  content: \"\\f414\";\n}\n.fa-yelp:before {\n  content: \"\\f1e9\";\n}\n.fa-yen-sign:before {\n  content: \"\\f157\";\n}\n.fa-yoast:before {\n  content: \"\\f2b1\";\n}\n.fa-youtube:before {\n  content: \"\\f167\";\n}\n.fa-youtube-square:before {\n  content: \"\\f431\";\n}\n.sr-only {\n  border: 0;\n  clip: rect(0, 0, 0, 0);\n  height: 1px;\n  margin: -1px;\n  overflow: hidden;\n  padding: 0;\n  position: absolute;\n  width: 1px;\n}\n.sr-only-focusable:active,\n.sr-only-focusable:focus {\n  clip: auto;\n  height: auto;\n  margin: 0;\n  overflow: visible;\n  position: static;\n  width: auto;\n}\n/*!\n * Font Awesome Free 5.0.13 by @fontawesome - https://fontawesome.com\n * License - https://fontawesome.com/license (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)\n */\n@font-face {\n  font-family: 'Font Awesome 5 Brands';\n  font-style: normal;\n  font-weight: normal;\n  src: url('../webfonts/fa-brands-400.eot');\n  src: url('../webfonts/fa-brands-400.eot?#iefix') format('embedded-opentype'), url('../webfonts/fa-brands-400.woff2') format('woff2'), url('../webfonts/fa-brands-400.woff') format('woff'), url('../webfonts/fa-brands-400.ttf') format('truetype'), url('../webfonts/fa-brands-400.svg#fontawesome') format('svg');\n}\n.fab {\n  font-family: 'Font Awesome 5 Brands';\n}\n/*!\n * Font Awesome Free 5.0.13 by @fontawesome - https://fontawesome.com\n * License - https://fontawesome.com/license (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)\n */\n@font-face {\n  font-family: 'Font Awesome 5 Free';\n  font-style: normal;\n  font-weight: 400;\n  src: url('../webfonts/fa-regular-400.eot');\n  src: url('../webfonts/fa-regular-400.eot?#iefix') format('embedded-opentype'), url('../webfonts/fa-regular-400.woff2') format('woff2'), url('../webfonts/fa-regular-400.woff') format('woff'), url('../webfonts/fa-regular-400.ttf') format('truetype'), url('../webfonts/fa-regular-400.svg#fontawesome') format('svg');\n}\n.far {\n  font-family: 'Font Awesome 5 Free';\n  font-weight: 400;\n}\n/*!\n * Font Awesome Free 5.0.13 by @fontawesome - https://fontawesome.com\n * License - https://fontawesome.com/license (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)\n */\n@font-face {\n  font-family: 'Font Awesome 5 Free';\n  font-style: normal;\n  font-weight: 900;\n  src: url('../webfonts/fa-solid-900.eot');\n  src: url('../webfonts/fa-solid-900.eot?#iefix') format('embedded-opentype'), url('../webfonts/fa-solid-900.woff2') format('woff2'), url('../webfonts/fa-solid-900.woff') format('woff'), url('../webfonts/fa-solid-900.ttf') format('truetype'), url('../webfonts/fa-solid-900.svg#fontawesome') format('svg');\n}\n.fa,\n.fas {\n  font-family: 'Font Awesome 5 Free';\n  font-weight: 900;\n}\n@font-face {\n  font-family: \"Material-Design-Icons\";\n  src: url(\"../font/Material-Design-Icons.eot?#iefix\") format(\"embedded-opentype\"), url(\"../font/Material-Design-Icons.woff2\") format(\"woff2\"), url(\"../font/Material-Design-Icons.woff\") format(\"woff\"), url(\"../font/Material-Design-Icons.ttf\") format(\"truetype\"), url(\"../font/Material-Design-Icons.svg#Material-Design-Icons\") format(\"svg\");\n  font-weight: normal;\n  font-style: normal;\n}\n@font-face {\n  font-family: \"Material Icons\";\n  src: url(\"../font/MaterialIcons-Regular.eot?#iefix\") format(\"embedded-opentype\"), url(\"../font/MaterialIcons-Regular.woff2\") format(\"woff2\"), url(\"../font/MaterialIcons-Regular.woff\") format(\"woff\"), url(\"../font/MaterialIcons-Regular.ttf\") format(\"truetype\") /*,\n        url(\"../font/Material-Design-Icons.svg#Material-Design-Icons\") format(\"svg\")*/;\n  font-weight: normal;\n  font-style: normal;\n}\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  line-height: inherit;\n  letter-spacing: normal;\n  text-transform: none;\n  display: inline-block;\n  word-wrap: normal;\n  -webkit-font-feature-settings: 'liga';\n  vertical-align: middle;\n  -webkit-font-smoothing: antialiased;\n}\n[class^=\"mdi-\"],\n[class*=\"mdi-\"] {\n  speak: none;\n  display: inline-block;\n  font-family: \"Material-Design-Icons\";\n  font-style: normal;\n  font-weight: normal;\n  font-variant: normal;\n  text-rendering: auto;\n  /* Better Font Rendering =========== */\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  transform: translate(0, 0);\n}\n[class^=\"mdi-\"]:before,\n[class*=\"mdi-\"]:before {\n  display: inline-block;\n  speak: none;\n  text-decoration: inherit;\n}\n[class^=\"mdi-\"].pull-left,\n[class*=\"mdi-\"].pull-left {\n  margin-right: .3em;\n}\n[class^=\"mdi-\"].pull-right,\n[class*=\"mdi-\"].pull-right {\n  margin-left: .3em;\n}\n[class^=\"mdi-\"].mdi-lg:before,\n[class*=\"mdi-\"].mdi-lg:before,\n[class^=\"mdi-\"].mdi-lg:after,\n[class*=\"mdi-\"].mdi-lg:after {\n  font-size: 1.33333333em;\n  line-height: 0.75em;\n  vertical-align: -15%;\n}\n[class^=\"mdi-\"].mdi-2x:before,\n[class*=\"mdi-\"].mdi-2x:before,\n[class^=\"mdi-\"].mdi-2x:after,\n[class*=\"mdi-\"].mdi-2x:after {\n  font-size: 2em;\n}\n[class^=\"mdi-\"].mdi-3x:before,\n[class*=\"mdi-\"].mdi-3x:before,\n[class^=\"mdi-\"].mdi-3x:after,\n[class*=\"mdi-\"].mdi-3x:after {\n  font-size: 3em;\n}\n[class^=\"mdi-\"].mdi-4x:before,\n[class*=\"mdi-\"].mdi-4x:before,\n[class^=\"mdi-\"].mdi-4x:after,\n[class*=\"mdi-\"].mdi-4x:after {\n  font-size: 4em;\n}\n[class^=\"mdi-\"].mdi-5x:before,\n[class*=\"mdi-\"].mdi-5x:before,\n[class^=\"mdi-\"].mdi-5x:after,\n[class*=\"mdi-\"].mdi-5x:after {\n  font-size: 5em;\n}\n[class^=\"mdi-device-signal-cellular-\"]:after,\n[class^=\"mdi-device-battery-\"]:after,\n[class^=\"mdi-device-battery-charging-\"]:after,\n[class^=\"mdi-device-signal-cellular-connected-no-internet-\"]:after,\n[class^=\"mdi-device-signal-wifi-\"]:after,\n[class^=\"mdi-device-signal-wifi-statusbar-not-connected\"]:after,\n.mdi-device-network-wifi:after {\n  opacity: .3;\n  position: absolute;\n  left: 0;\n  top: 0;\n  z-index: 1;\n  display: inline-block;\n  speak: none;\n  text-decoration: inherit;\n}\n[class^=\"mdi-device-signal-cellular-\"]:after {\n  content: \"\\e758\";\n}\n[class^=\"mdi-device-battery-\"]:after {\n  content: \"\\e735\";\n}\n[class^=\"mdi-device-battery-charging-\"]:after {\n  content: \"\\e733\";\n}\n[class^=\"mdi-device-signal-cellular-connected-no-internet-\"]:after {\n  content: \"\\e75d\";\n}\n[class^=\"mdi-device-signal-wifi-\"]:after,\n.mdi-device-network-wifi:after {\n  content: \"\\e765\";\n}\n[class^=\"mdi-device-signal-wifi-statusbasr-not-connected\"]:after {\n  content: \"\\e8f7\";\n}\n.mdi-device-signal-cellular-off:after,\n.mdi-device-signal-cellular-null:after,\n.mdi-device-signal-cellular-no-sim:after,\n.mdi-device-signal-wifi-off:after,\n.mdi-device-signal-wifi-4-bar:after,\n.mdi-device-signal-cellular-4-bar:after,\n.mdi-device-battery-alert:after,\n.mdi-device-signal-cellular-connected-no-internet-4-bar:after,\n.mdi-device-battery-std:after,\n.mdi-device-battery-full .mdi-device-battery-unknown:after {\n  content: \"\";\n}\n.mdi-fw {\n  width: 1.28571429em;\n  text-align: center;\n}\n.mdi-ul {\n  padding-left: 0;\n  margin-left: 2.14285714em;\n  list-style-type: none;\n}\n.mdi-ul > li {\n  position: relative;\n}\n.mdi-li {\n  position: absolute;\n  left: -2.14285714em;\n  width: 2.14285714em;\n  top: 0.14285714em;\n  text-align: center;\n}\n.mdi-li.mdi-lg {\n  left: -1.85714286em;\n}\n.mdi-border {\n  padding: .2em .25em .15em;\n  border: solid 0.08em #eeeeee;\n  border-radius: .1em;\n}\n.mdi-spin {\n  -webkit-animation: mdi-spin 2s infinite linear;\n  animation: mdi-spin 2s infinite linear;\n  -webkit-transform-origin: 50% 50%;\n  -moz-transform-origin: 50% 50%;\n  -o-transform-origin: 50% 50%;\n  transform-origin: 50% 50%;\n}\n.mdi-pulse {\n  -webkit-animation: mdi-spin 1s steps(8) infinite;\n  animation: mdi-spin 1s steps(8) infinite;\n  -webkit-transform-origin: 50% 50%;\n  -moz-transform-origin: 50% 50%;\n  -o-transform-origin: 50% 50%;\n  transform-origin: 50% 50%;\n}\n@-webkit-keyframes mdi-spin {\n  0% {\n    -webkit-transform: rotate(0deg);\n    transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(359deg);\n    transform: rotate(359deg);\n  }\n}\n@keyframes mdi-spin {\n  0% {\n    -webkit-transform: rotate(0deg);\n    transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(359deg);\n    transform: rotate(359deg);\n  }\n}\n.mdi-rotate-90 {\n  filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=1);\n  -webkit-transform: rotate(90deg);\n  -ms-transform: rotate(90deg);\n  transform: rotate(90deg);\n}\n.mdi-rotate-180 {\n  filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=2);\n  -webkit-transform: rotate(180deg);\n  -ms-transform: rotate(180deg);\n  transform: rotate(180deg);\n}\n.mdi-rotate-270 {\n  filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);\n  -webkit-transform: rotate(270deg);\n  -ms-transform: rotate(270deg);\n  transform: rotate(270deg);\n}\n.mdi-flip-horizontal {\n  filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=0, mirror=1);\n  -webkit-transform: scale(-1, 1);\n  -ms-transform: scale(-1, 1);\n  transform: scale(-1, 1);\n}\n.mdi-flip-vertical {\n  filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=2, mirror=1);\n  -webkit-transform: scale(1, -1);\n  -ms-transform: scale(1, -1);\n  transform: scale(1, -1);\n}\n:root .mdi-rotate-90,\n:root .mdi-rotate-180,\n:root .mdi-rotate-270,\n:root .mdi-flip-horizontal,\n:root .mdi-flip-vertical {\n  filter: none;\n}\n.mdi-stack {\n  position: relative;\n  display: inline-block;\n  width: 2em;\n  height: 2em;\n  line-height: 2em;\n  vertical-align: middle;\n}\n.mdi-stack-1x,\n.mdi-stack-2x {\n  position: absolute;\n  left: 0;\n  width: 100%;\n  text-align: center;\n}\n.mdi-stack-1x {\n  line-height: inherit;\n}\n.mdi-stack-2x {\n  font-size: 2em;\n}\n.mdi-inverse {\n  color: #ffffff;\n}\n/* Start Icons */\n.mdi-action-3d-rotation:before {\n  content: \"\\e600\";\n}\n.mdi-action-accessibility:before {\n  content: \"\\e601\";\n}\n.mdi-action-account-balance-wallet:before {\n  content: \"\\e602\";\n}\n.mdi-action-account-balance:before {\n  content: \"\\e603\";\n}\n.mdi-action-account-box:before {\n  content: \"\\e604\";\n}\n.mdi-action-account-child:before {\n  content: \"\\e605\";\n}\n.mdi-action-account-circle:before {\n  content: \"\\e606\";\n}\n.mdi-action-add-shopping-cart:before {\n  content: \"\\e607\";\n}\n.mdi-action-alarm-add:before {\n  content: \"\\e608\";\n}\n.mdi-action-alarm-off:before {\n  content: \"\\e609\";\n}\n.mdi-action-alarm-on:before {\n  content: \"\\e60a\";\n}\n.mdi-action-alarm:before {\n  content: \"\\e60b\";\n}\n.mdi-action-android:before {\n  content: \"\\e60c\";\n}\n.mdi-action-announcement:before {\n  content: \"\\e60d\";\n}\n.mdi-action-aspect-ratio:before {\n  content: \"\\e60e\";\n}\n.mdi-action-assessment:before {\n  content: \"\\e60f\";\n}\n.mdi-action-assignment-ind:before {\n  content: \"\\e610\";\n}\n.mdi-action-assignment-late:before {\n  content: \"\\e611\";\n}\n.mdi-action-assignment-return:before {\n  content: \"\\e612\";\n}\n.mdi-action-assignment-returned:before {\n  content: \"\\e613\";\n}\n.mdi-action-assignment-turned-in:before {\n  content: \"\\e614\";\n}\n.mdi-action-assignment:before {\n  content: \"\\e615\";\n}\n.mdi-action-autorenew:before {\n  content: \"\\e616\";\n}\n.mdi-action-backup:before {\n  content: \"\\e617\";\n}\n.mdi-action-book:before {\n  content: \"\\e618\";\n}\n.mdi-action-bookmark-outline:before {\n  content: \"\\e619\";\n}\n.mdi-action-bookmark:before {\n  content: \"\\e61a\";\n}\n.mdi-action-bug-report:before {\n  content: \"\\e61b\";\n}\n.mdi-action-cached:before {\n  content: \"\\e61c\";\n}\n.mdi-action-check-circle:before {\n  content: \"\\e61d\";\n}\n.mdi-action-class:before {\n  content: \"\\e61e\";\n}\n.mdi-action-credit-card:before {\n  content: \"\\e61f\";\n}\n.mdi-action-dashboard:before {\n  content: \"\\e620\";\n}\n.mdi-action-delete:before {\n  content: \"\\e621\";\n}\n.mdi-action-description:before {\n  content: \"\\e622\";\n}\n.mdi-action-dns:before {\n  content: \"\\e623\";\n}\n.mdi-action-done-all:before {\n  content: \"\\e624\";\n}\n.mdi-action-done:before {\n  content: \"\\e625\";\n}\n.mdi-action-event:before {\n  content: \"\\e626\";\n}\n.mdi-action-exit-to-app:before {\n  content: \"\\e627\";\n}\n.mdi-action-explore:before {\n  content: \"\\e628\";\n}\n.mdi-action-extension:before {\n  content: \"\\e629\";\n}\n.mdi-action-face-unlock:before {\n  content: \"\\e62a\";\n}\n.mdi-action-favorite-outline:before {\n  content: \"\\e62b\";\n}\n.mdi-action-favorite:before {\n  content: \"\\e62c\";\n}\n.mdi-action-find-in-page:before {\n  content: \"\\e62d\";\n}\n.mdi-action-find-replace:before {\n  content: \"\\e62e\";\n}\n.mdi-action-flip-to-back:before {\n  content: \"\\e62f\";\n}\n.mdi-action-flip-to-front:before {\n  content: \"\\e630\";\n}\n.mdi-action-get-app:before {\n  content: \"\\e631\";\n}\n.mdi-action-grade:before {\n  content: \"\\e632\";\n}\n.mdi-action-group-work:before {\n  content: \"\\e633\";\n}\n.mdi-action-help:before {\n  content: \"\\e634\";\n}\n.mdi-action-highlight-remove:before {\n  content: \"\\e635\";\n}\n.mdi-action-history:before {\n  content: \"\\e636\";\n}\n.mdi-action-home:before {\n  content: \"\\e637\";\n}\n.mdi-action-https:before {\n  content: \"\\e638\";\n}\n.mdi-action-info-outline:before {\n  content: \"\\e639\";\n}\n.mdi-action-info:before {\n  content: \"\\e63a\";\n}\n.mdi-action-input:before {\n  content: \"\\e63b\";\n}\n.mdi-action-invert-colors:before {\n  content: \"\\e63c\";\n}\n.mdi-action-label-outline:before {\n  content: \"\\e63d\";\n}\n.mdi-action-label:before {\n  content: \"\\e63e\";\n}\n.mdi-action-language:before {\n  content: \"\\e63f\";\n}\n.mdi-action-launch:before {\n  content: \"\\e640\";\n}\n.mdi-action-list:before {\n  content: \"\\e641\";\n}\n.mdi-action-lock-open:before {\n  content: \"\\e642\";\n}\n.mdi-action-lock-outline:before {\n  content: \"\\e643\";\n}\n.mdi-action-lock:before {\n  content: \"\\e644\";\n}\n.mdi-action-loyalty:before {\n  content: \"\\e645\";\n}\n.mdi-action-markunread-mailbox:before {\n  content: \"\\e646\";\n}\n.mdi-action-note-add:before {\n  content: \"\\e647\";\n}\n.mdi-action-open-in-browser:before {\n  content: \"\\e648\";\n}\n.mdi-action-open-in-new:before {\n  content: \"\\e649\";\n}\n.mdi-action-open-with:before {\n  content: \"\\e64a\";\n}\n.mdi-action-pageview:before {\n  content: \"\\e64b\";\n}\n.mdi-action-payment:before {\n  content: \"\\e64c\";\n}\n.mdi-action-perm-camera-mic:before {\n  content: \"\\e64d\";\n}\n.mdi-action-perm-contact-cal:before {\n  content: \"\\e64e\";\n}\n.mdi-action-perm-data-setting:before {\n  content: \"\\e64f\";\n}\n.mdi-action-perm-device-info:before {\n  content: \"\\e650\";\n}\n.mdi-action-perm-identity:before {\n  content: \"\\e651\";\n}\n.mdi-action-perm-media:before {\n  content: \"\\e652\";\n}\n.mdi-action-perm-phone-msg:before {\n  content: \"\\e653\";\n}\n.mdi-action-perm-scan-wifi:before {\n  content: \"\\e654\";\n}\n.mdi-action-picture-in-picture:before {\n  content: \"\\e655\";\n}\n.mdi-action-polymer:before {\n  content: \"\\e656\";\n}\n.mdi-action-print:before {\n  content: \"\\e657\";\n}\n.mdi-action-query-builder:before {\n  content: \"\\e658\";\n}\n.mdi-action-question-answer:before {\n  content: \"\\e659\";\n}\n.mdi-action-receipt:before {\n  content: \"\\e65a\";\n}\n.mdi-action-redeem:before {\n  content: \"\\e65b\";\n}\n.mdi-action-reorder:before {\n  content: \"\\e65c\";\n}\n.mdi-action-report-problem:before {\n  content: \"\\e65d\";\n}\n.mdi-action-restore:before {\n  content: \"\\e65e\";\n}\n.mdi-action-room:before {\n  content: \"\\e65f\";\n}\n.mdi-action-schedule:before {\n  content: \"\\e660\";\n}\n.mdi-action-search:before {\n  content: \"\\e661\";\n}\n.mdi-action-settings-applications:before {\n  content: \"\\e662\";\n}\n.mdi-action-settings-backup-restore:before {\n  content: \"\\e663\";\n}\n.mdi-action-settings-bluetooth:before {\n  content: \"\\e664\";\n}\n.mdi-action-settings-cell:before {\n  content: \"\\e665\";\n}\n.mdi-action-settings-display:before {\n  content: \"\\e666\";\n}\n.mdi-action-settings-ethernet:before {\n  content: \"\\e667\";\n}\n.mdi-action-settings-input-antenna:before {\n  content: \"\\e668\";\n}\n.mdi-action-settings-input-component:before {\n  content: \"\\e669\";\n}\n.mdi-action-settings-input-composite:before {\n  content: \"\\e66a\";\n}\n.mdi-action-settings-input-hdmi:before {\n  content: \"\\e66b\";\n}\n.mdi-action-settings-input-svideo:before {\n  content: \"\\e66c\";\n}\n.mdi-action-settings-overscan:before {\n  content: \"\\e66d\";\n}\n.mdi-action-settings-phone:before {\n  content: \"\\e66e\";\n}\n.mdi-action-settings-power:before {\n  content: \"\\e66f\";\n}\n.mdi-action-settings-remote:before {\n  content: \"\\e670\";\n}\n.mdi-action-settings-voice:before {\n  content: \"\\e671\";\n}\n.mdi-action-settings:before {\n  content: \"\\e672\";\n}\n.mdi-action-shop-two:before {\n  content: \"\\e673\";\n}\n.mdi-action-shop:before {\n  content: \"\\e674\";\n}\n.mdi-action-shopping-basket:before {\n  content: \"\\e675\";\n}\n.mdi-action-shopping-cart:before {\n  content: \"\\e676\";\n}\n.mdi-action-speaker-notes:before {\n  content: \"\\e677\";\n}\n.mdi-action-spellcheck:before {\n  content: \"\\e678\";\n}\n.mdi-action-star-rate:before {\n  content: \"\\e679\";\n}\n.mdi-action-stars:before {\n  content: \"\\e67a\";\n}\n.mdi-action-store:before {\n  content: \"\\e67b\";\n}\n.mdi-action-subject:before {\n  content: \"\\e67c\";\n}\n.mdi-action-supervisor-account:before {\n  content: \"\\e67d\";\n}\n.mdi-action-swap-horiz:before {\n  content: \"\\e67e\";\n}\n.mdi-action-swap-vert-circle:before {\n  content: \"\\e67f\";\n}\n.mdi-action-swap-vert:before {\n  content: \"\\e680\";\n}\n.mdi-action-system-update-tv:before {\n  content: \"\\e681\";\n}\n.mdi-action-tab-unselected:before {\n  content: \"\\e682\";\n}\n.mdi-action-tab:before {\n  content: \"\\e683\";\n}\n.mdi-action-theaters:before {\n  content: \"\\e684\";\n}\n.mdi-action-thumb-down:before {\n  content: \"\\e685\";\n}\n.mdi-action-thumb-up:before {\n  content: \"\\e686\";\n}\n.mdi-action-thumbs-up-down:before {\n  content: \"\\e687\";\n}\n.mdi-action-toc:before {\n  content: \"\\e688\";\n}\n.mdi-action-today:before {\n  content: \"\\e689\";\n}\n.mdi-action-track-changes:before {\n  content: \"\\e68a\";\n}\n.mdi-action-translate:before {\n  content: \"\\e68b\";\n}\n.mdi-action-trending-down:before {\n  content: \"\\e68c\";\n}\n.mdi-action-trending-neutral:before {\n  content: \"\\e68d\";\n}\n.mdi-action-trending-up:before {\n  content: \"\\e68e\";\n}\n.mdi-action-turned-in-not:before {\n  content: \"\\e68f\";\n}\n.mdi-action-turned-in:before {\n  content: \"\\e690\";\n}\n.mdi-action-verified-user:before {\n  content: \"\\e691\";\n}\n.mdi-action-view-agenda:before {\n  content: \"\\e692\";\n}\n.mdi-action-view-array:before {\n  content: \"\\e693\";\n}\n.mdi-action-view-carousel:before {\n  content: \"\\e694\";\n}\n.mdi-action-view-column:before {\n  content: \"\\e695\";\n}\n.mdi-action-view-day:before {\n  content: \"\\e696\";\n}\n.mdi-action-view-headline:before {\n  content: \"\\e697\";\n}\n.mdi-action-view-list:before {\n  content: \"\\e698\";\n}\n.mdi-action-view-module:before {\n  content: \"\\e699\";\n}\n.mdi-action-view-quilt:before {\n  content: \"\\e69a\";\n}\n.mdi-action-view-stream:before {\n  content: \"\\e69b\";\n}\n.mdi-action-view-week:before {\n  content: \"\\e69c\";\n}\n.mdi-action-visibility-off:before {\n  content: \"\\e69d\";\n}\n.mdi-action-visibility:before {\n  content: \"\\e69e\";\n}\n.mdi-action-wallet-giftcard:before {\n  content: \"\\e69f\";\n}\n.mdi-action-wallet-membership:before {\n  content: \"\\e6a0\";\n}\n.mdi-action-wallet-travel:before {\n  content: \"\\e6a1\";\n}\n.mdi-action-work:before {\n  content: \"\\e6a2\";\n}\n.mdi-alert-error:before {\n  content: \"\\e6a3\";\n}\n.mdi-alert-warning:before {\n  content: \"\\e6a4\";\n}\n.mdi-av-album:before {\n  content: \"\\e6a5\";\n}\n.mdi-av-closed-caption:before {\n  content: \"\\e6a6\";\n}\n.mdi-av-equalizer:before {\n  content: \"\\e6a7\";\n}\n.mdi-av-explicit:before {\n  content: \"\\e6a8\";\n}\n.mdi-av-fast-forward:before {\n  content: \"\\e6a9\";\n}\n.mdi-av-fast-rewind:before {\n  content: \"\\e6aa\";\n}\n.mdi-av-games:before {\n  content: \"\\e6ab\";\n}\n.mdi-av-hearing:before {\n  content: \"\\e6ac\";\n}\n.mdi-av-high-quality:before {\n  content: \"\\e6ad\";\n}\n.mdi-av-loop:before {\n  content: \"\\e6ae\";\n}\n.mdi-av-mic-none:before {\n  content: \"\\e6af\";\n}\n.mdi-av-mic-off:before {\n  content: \"\\e6b0\";\n}\n.mdi-av-mic:before {\n  content: \"\\e6b1\";\n}\n.mdi-av-movie:before {\n  content: \"\\e6b2\";\n}\n.mdi-av-my-library-add:before {\n  content: \"\\e6b3\";\n}\n.mdi-av-my-library-books:before {\n  content: \"\\e6b4\";\n}\n.mdi-av-my-library-music:before {\n  content: \"\\e6b5\";\n}\n.mdi-av-new-releases:before {\n  content: \"\\e6b6\";\n}\n.mdi-av-not-interested:before {\n  content: \"\\e6b7\";\n}\n.mdi-av-pause-circle-fill:before {\n  content: \"\\e6b8\";\n}\n.mdi-av-pause-circle-outline:before {\n  content: \"\\e6b9\";\n}\n.mdi-av-pause:before {\n  content: \"\\e6ba\";\n}\n.mdi-av-play-arrow:before {\n  content: \"\\e6bb\";\n}\n.mdi-av-play-circle-fill:before {\n  content: \"\\e6bc\";\n}\n.mdi-av-play-circle-outline:before {\n  content: \"\\e6bd\";\n}\n.mdi-av-play-shopping-bag:before {\n  content: \"\\e6be\";\n}\n.mdi-av-playlist-add:before {\n  content: \"\\e6bf\";\n}\n.mdi-av-queue-music:before {\n  content: \"\\e6c0\";\n}\n.mdi-av-queue:before {\n  content: \"\\e6c1\";\n}\n.mdi-av-radio:before {\n  content: \"\\e6c2\";\n}\n.mdi-av-recent-actors:before {\n  content: \"\\e6c3\";\n}\n.mdi-av-repeat-one:before {\n  content: \"\\e6c4\";\n}\n.mdi-av-repeat:before {\n  content: \"\\e6c5\";\n}\n.mdi-av-replay:before {\n  content: \"\\e6c6\";\n}\n.mdi-av-shuffle:before {\n  content: \"\\e6c7\";\n}\n.mdi-av-skip-next:before {\n  content: \"\\e6c8\";\n}\n.mdi-av-skip-previous:before {\n  content: \"\\e6c9\";\n}\n.mdi-av-snooze:before {\n  content: \"\\e6ca\";\n}\n.mdi-av-stop:before {\n  content: \"\\e6cb\";\n}\n.mdi-av-subtitles:before {\n  content: \"\\e6cc\";\n}\n.mdi-av-surround-sound:before {\n  content: \"\\e6cd\";\n}\n.mdi-av-timer:before {\n  content: \"\\e6ce\";\n}\n.mdi-av-video-collection:before {\n  content: \"\\e6cf\";\n}\n.mdi-av-videocam-off:before {\n  content: \"\\e6d0\";\n}\n.mdi-av-videocam:before {\n  content: \"\\e6d1\";\n}\n.mdi-av-volume-down:before {\n  content: \"\\e6d2\";\n}\n.mdi-av-volume-mute:before {\n  content: \"\\e6d3\";\n}\n.mdi-av-volume-off:before {\n  content: \"\\e6d4\";\n}\n.mdi-av-volume-up:before {\n  content: \"\\e6d5\";\n}\n.mdi-av-web:before {\n  content: \"\\e6d6\";\n}\n.mdi-communication-business:before {\n  content: \"\\e6d7\";\n}\n.mdi-communication-call-end:before {\n  content: \"\\e6d8\";\n}\n.mdi-communication-call-made:before {\n  content: \"\\e6d9\";\n}\n.mdi-communication-call-merge:before {\n  content: \"\\e6da\";\n}\n.mdi-communication-call-missed:before {\n  content: \"\\e6db\";\n}\n.mdi-communication-call-received:before {\n  content: \"\\e6dc\";\n}\n.mdi-communication-call-split:before {\n  content: \"\\e6dd\";\n}\n.mdi-communication-call:before {\n  content: \"\\e6de\";\n}\n.mdi-communication-chat:before {\n  content: \"\\e6df\";\n}\n.mdi-communication-clear-all:before {\n  content: \"\\e6e0\";\n}\n.mdi-communication-comment:before {\n  content: \"\\e6e1\";\n}\n.mdi-communication-contacts:before {\n  content: \"\\e6e2\";\n}\n.mdi-communication-dialer-sip:before {\n  content: \"\\e6e3\";\n}\n.mdi-communication-dialpad:before {\n  content: \"\\e6e4\";\n}\n.mdi-communication-dnd-on:before {\n  content: \"\\e6e5\";\n}\n.mdi-communication-email:before {\n  content: \"\\e6e6\";\n}\n.mdi-communication-forum:before {\n  content: \"\\e6e7\";\n}\n.mdi-communication-import-export:before {\n  content: \"\\e6e8\";\n}\n.mdi-communication-invert-colors-off:before {\n  content: \"\\e6e9\";\n}\n.mdi-communication-invert-colors-on:before {\n  content: \"\\e6ea\";\n}\n.mdi-communication-live-help:before {\n  content: \"\\e6eb\";\n}\n.mdi-communication-location-off:before {\n  content: \"\\e6ec\";\n}\n.mdi-communication-location-on:before {\n  content: \"\\e6ed\";\n}\n.mdi-communication-message:before {\n  content: \"\\e6ee\";\n}\n.mdi-communication-messenger:before {\n  content: \"\\e6ef\";\n}\n.mdi-communication-no-sim:before {\n  content: \"\\e6f0\";\n}\n.mdi-communication-phone:before {\n  content: \"\\e6f1\";\n}\n.mdi-communication-portable-wifi-off:before {\n  content: \"\\e6f2\";\n}\n.mdi-communication-quick-contacts-dialer:before {\n  content: \"\\e6f3\";\n}\n.mdi-communication-quick-contacts-mail:before {\n  content: \"\\e6f4\";\n}\n.mdi-communication-ring-volume:before {\n  content: \"\\e6f5\";\n}\n.mdi-communication-stay-current-landscape:before {\n  content: \"\\e6f6\";\n}\n.mdi-communication-stay-current-portrait:before {\n  content: \"\\e6f7\";\n}\n.mdi-communication-stay-primary-landscape:before {\n  content: \"\\e6f8\";\n}\n.mdi-communication-stay-primary-portrait:before {\n  content: \"\\e6f9\";\n}\n.mdi-communication-swap-calls:before {\n  content: \"\\e6fa\";\n}\n.mdi-communication-textsms:before {\n  content: \"\\e6fb\";\n}\n.mdi-communication-voicemail:before {\n  content: \"\\e6fc\";\n}\n.mdi-communication-vpn-key:before {\n  content: \"\\e6fd\";\n}\n.mdi-content-add-box:before {\n  content: \"\\e6fe\";\n}\n.mdi-content-add-circle-outline:before {\n  content: \"\\e6ff\";\n}\n.mdi-content-add-circle:before {\n  content: \"\\e700\";\n}\n.mdi-content-add:before {\n  content: \"\\e701\";\n}\n.mdi-content-archive:before {\n  content: \"\\e702\";\n}\n.mdi-content-backspace:before {\n  content: \"\\e703\";\n}\n.mdi-content-block:before {\n  content: \"\\e704\";\n}\n.mdi-content-clear:before {\n  content: \"\\e705\";\n}\n.mdi-content-content-copy:before {\n  content: \"\\e706\";\n}\n.mdi-content-content-cut:before {\n  content: \"\\e707\";\n}\n.mdi-content-content-paste:before {\n  content: \"\\e708\";\n}\n.mdi-content-create:before {\n  content: \"\\e709\";\n}\n.mdi-content-drafts:before {\n  content: \"\\e70a\";\n}\n.mdi-content-filter-list:before {\n  content: \"\\e70b\";\n}\n.mdi-content-flag:before {\n  content: \"\\e70c\";\n}\n.mdi-content-forward:before {\n  content: \"\\e70d\";\n}\n.mdi-content-gesture:before {\n  content: \"\\e70e\";\n}\n.mdi-content-inbox:before {\n  content: \"\\e70f\";\n}\n.mdi-content-link:before {\n  content: \"\\e710\";\n}\n.mdi-content-mail:before {\n  content: \"\\e711\";\n}\n.mdi-content-markunread:before {\n  content: \"\\e712\";\n}\n.mdi-content-redo:before {\n  content: \"\\e713\";\n}\n.mdi-content-remove-circle-outline:before {\n  content: \"\\e714\";\n}\n.mdi-content-remove-circle:before {\n  content: \"\\e715\";\n}\n.mdi-content-remove:before {\n  content: \"\\e716\";\n}\n.mdi-content-reply-all:before {\n  content: \"\\e717\";\n}\n.mdi-content-reply:before {\n  content: \"\\e718\";\n}\n.mdi-content-report:before {\n  content: \"\\e719\";\n}\n.mdi-content-save:before {\n  content: \"\\e71a\";\n}\n.mdi-content-select-all:before {\n  content: \"\\e71b\";\n}\n.mdi-content-send:before {\n  content: \"\\e71c\";\n}\n.mdi-content-sort:before {\n  content: \"\\e71d\";\n}\n.mdi-content-text-format:before {\n  content: \"\\e71e\";\n}\n.mdi-content-undo:before {\n  content: \"\\e71f\";\n}\n.mdi-editor-attach-file:before {\n  content: \"\\e776\";\n}\n.mdi-editor-attach-money:before {\n  content: \"\\e777\";\n}\n.mdi-editor-border-all:before {\n  content: \"\\e778\";\n}\n.mdi-editor-border-bottom:before {\n  content: \"\\e779\";\n}\n.mdi-editor-border-clear:before {\n  content: \"\\e77a\";\n}\n.mdi-editor-border-color:before {\n  content: \"\\e77b\";\n}\n.mdi-editor-border-horizontal:before {\n  content: \"\\e77c\";\n}\n.mdi-editor-border-inner:before {\n  content: \"\\e77d\";\n}\n.mdi-editor-border-left:before {\n  content: \"\\e77e\";\n}\n.mdi-editor-border-outer:before {\n  content: \"\\e77f\";\n}\n.mdi-editor-border-right:before {\n  content: \"\\e780\";\n}\n.mdi-editor-border-style:before {\n  content: \"\\e781\";\n}\n.mdi-editor-border-top:before {\n  content: \"\\e782\";\n}\n.mdi-editor-border-vertical:before {\n  content: \"\\e783\";\n}\n.mdi-editor-format-align-center:before {\n  content: \"\\e784\";\n}\n.mdi-editor-format-align-justify:before {\n  content: \"\\e785\";\n}\n.mdi-editor-format-align-left:before {\n  content: \"\\e786\";\n}\n.mdi-editor-format-align-right:before {\n  content: \"\\e787\";\n}\n.mdi-editor-format-bold:before {\n  content: \"\\e788\";\n}\n.mdi-editor-format-clear:before {\n  content: \"\\e789\";\n}\n.mdi-editor-format-color-fill:before {\n  content: \"\\e78a\";\n}\n.mdi-editor-format-color-reset:before {\n  content: \"\\e78b\";\n}\n.mdi-editor-format-color-text:before {\n  content: \"\\e78c\";\n}\n.mdi-editor-format-indent-decrease:before {\n  content: \"\\e78d\";\n}\n.mdi-editor-format-indent-increase:before {\n  content: \"\\e78e\";\n}\n.mdi-editor-format-italic:before {\n  content: \"\\e78f\";\n}\n.mdi-editor-format-line-spacing:before {\n  content: \"\\e790\";\n}\n.mdi-editor-format-list-bulleted:before {\n  content: \"\\e791\";\n}\n.mdi-editor-format-list-numbered:before {\n  content: \"\\e792\";\n}\n.mdi-editor-format-paint:before {\n  content: \"\\e793\";\n}\n.mdi-editor-format-quote:before {\n  content: \"\\e794\";\n}\n.mdi-editor-format-size:before {\n  content: \"\\e795\";\n}\n.mdi-editor-format-strikethrough:before {\n  content: \"\\e796\";\n}\n.mdi-editor-format-textdirection-l-to-r:before {\n  content: \"\\e797\";\n}\n.mdi-editor-format-textdirection-r-to-l:before {\n  content: \"\\e798\";\n}\n.mdi-editor-format-underline:before {\n  content: \"\\e799\";\n}\n.mdi-editor-functions:before {\n  content: \"\\e79a\";\n}\n.mdi-editor-insert-chart:before {\n  content: \"\\e79b\";\n}\n.mdi-editor-insert-comment:before {\n  content: \"\\e79c\";\n}\n.mdi-editor-insert-drive-file:before {\n  content: \"\\e79d\";\n}\n.mdi-editor-insert-emoticon:before {\n  content: \"\\e79e\";\n}\n.mdi-editor-insert-invitation:before {\n  content: \"\\e79f\";\n}\n.mdi-editor-insert-link:before {\n  content: \"\\e7a0\";\n}\n.mdi-editor-insert-photo:before {\n  content: \"\\e7a1\";\n}\n.mdi-editor-merge-type:before {\n  content: \"\\e7a2\";\n}\n.mdi-editor-mode-comment:before {\n  content: \"\\e7a3\";\n}\n.mdi-editor-mode-edit:before {\n  content: \"\\e7a4\";\n}\n.mdi-editor-publish:before {\n  content: \"\\e7a5\";\n}\n.mdi-editor-vertical-align-bottom:before {\n  content: \"\\e7a6\";\n}\n.mdi-editor-vertical-align-center:before {\n  content: \"\\e7a7\";\n}\n.mdi-editor-vertical-align-top:before {\n  content: \"\\e7a8\";\n}\n.mdi-editor-wrap-text:before {\n  content: \"\\e7a9\";\n}\n.mdi-file-attachment:before {\n  content: \"\\e7aa\";\n}\n.mdi-file-cloud-circle:before {\n  content: \"\\e7ab\";\n}\n.mdi-file-cloud-done:before {\n  content: \"\\e7ac\";\n}\n.mdi-file-cloud-download:before {\n  content: \"\\e7ad\";\n}\n.mdi-file-cloud-off:before {\n  content: \"\\e7ae\";\n}\n.mdi-file-cloud-queue:before {\n  content: \"\\e7af\";\n}\n.mdi-file-cloud-upload:before {\n  content: \"\\e7b0\";\n}\n.mdi-file-cloud:before {\n  content: \"\\e7b1\";\n}\n.mdi-file-file-download:before {\n  content: \"\\e7b2\";\n}\n.mdi-file-file-upload:before {\n  content: \"\\e7b3\";\n}\n.mdi-file-folder-open:before {\n  content: \"\\e7b4\";\n}\n.mdi-file-folder-shared:before {\n  content: \"\\e7b5\";\n}\n.mdi-file-folder:before {\n  content: \"\\e7b6\";\n}\n.mdi-device-access-alarm:before {\n  content: \"\\e720\";\n}\n.mdi-device-access-alarms:before {\n  content: \"\\e721\";\n}\n.mdi-device-access-time:before {\n  content: \"\\e722\";\n}\n.mdi-device-add-alarm:before {\n  content: \"\\e723\";\n}\n.mdi-device-airplanemode-off:before {\n  content: \"\\e724\";\n}\n.mdi-device-airplanemode-on:before {\n  content: \"\\e725\";\n}\n.mdi-device-battery-20:before {\n  content: \"\\e726\";\n}\n.mdi-device-battery-30:before {\n  content: \"\\e727\";\n}\n.mdi-device-battery-50:before {\n  content: \"\\e728\";\n}\n.mdi-device-battery-60:before {\n  content: \"\\e729\";\n}\n.mdi-device-battery-80:before {\n  content: \"\\e72a\";\n}\n.mdi-device-battery-90:before {\n  content: \"\\e72b\";\n}\n.mdi-device-battery-alert:before {\n  content: \"\\e72c\";\n}\n.mdi-device-battery-charging-20:before {\n  content: \"\\e72d\";\n}\n.mdi-device-battery-charging-30:before {\n  content: \"\\e72e\";\n}\n.mdi-device-battery-charging-50:before {\n  content: \"\\e72f\";\n}\n.mdi-device-battery-charging-60:before {\n  content: \"\\e730\";\n}\n.mdi-device-battery-charging-80:before {\n  content: \"\\e731\";\n}\n.mdi-device-battery-charging-90:before {\n  content: \"\\e732\";\n}\n.mdi-device-battery-charging-full:before {\n  content: \"\\e733\";\n}\n.mdi-device-battery-full:before {\n  content: \"\\e734\";\n}\n.mdi-device-battery-std:before {\n  content: \"\\e735\";\n}\n.mdi-device-battery-unknown:before {\n  content: \"\\e736\";\n}\n.mdi-device-bluetooth-connected:before {\n  content: \"\\e737\";\n}\n.mdi-device-bluetooth-disabled:before {\n  content: \"\\e738\";\n}\n.mdi-device-bluetooth-searching:before {\n  content: \"\\e739\";\n}\n.mdi-device-bluetooth:before {\n  content: \"\\e73a\";\n}\n.mdi-device-brightness-auto:before {\n  content: \"\\e73b\";\n}\n.mdi-device-brightness-high:before {\n  content: \"\\e73c\";\n}\n.mdi-device-brightness-low:before {\n  content: \"\\e73d\";\n}\n.mdi-device-brightness-medium:before {\n  content: \"\\e73e\";\n}\n.mdi-device-data-usage:before {\n  content: \"\\e73f\";\n}\n.mdi-device-developer-mode:before {\n  content: \"\\e740\";\n}\n.mdi-device-devices:before {\n  content: \"\\e741\";\n}\n.mdi-device-dvr:before {\n  content: \"\\e742\";\n}\n.mdi-device-gps-fixed:before {\n  content: \"\\e743\";\n}\n.mdi-device-gps-not-fixed:before {\n  content: \"\\e744\";\n}\n.mdi-device-gps-off:before {\n  content: \"\\e745\";\n}\n.mdi-device-location-disabled:before {\n  content: \"\\e746\";\n}\n.mdi-device-location-searching:before {\n  content: \"\\e747\";\n}\n.mdi-device-multitrack-audio:before {\n  content: \"\\e748\";\n}\n.mdi-device-network-cell:before {\n  content: \"\\e749\";\n}\n.mdi-device-network-wifi:before {\n  content: \"\\e74a\";\n}\n.mdi-device-nfc:before {\n  content: \"\\e74b\";\n}\n.mdi-device-now-wallpaper:before {\n  content: \"\\e74c\";\n}\n.mdi-device-now-widgets:before {\n  content: \"\\e74d\";\n}\n.mdi-device-screen-lock-landscape:before {\n  content: \"\\e74e\";\n}\n.mdi-device-screen-lock-portrait:before {\n  content: \"\\e74f\";\n}\n.mdi-device-screen-lock-rotation:before {\n  content: \"\\e750\";\n}\n.mdi-device-screen-rotation:before {\n  content: \"\\e751\";\n}\n.mdi-device-sd-storage:before {\n  content: \"\\e752\";\n}\n.mdi-device-settings-system-daydream:before {\n  content: \"\\e753\";\n}\n.mdi-device-signal-cellular-0-bar:before {\n  content: \"\\e754\";\n}\n.mdi-device-signal-cellular-1-bar:before {\n  content: \"\\e755\";\n}\n.mdi-device-signal-cellular-2-bar:before {\n  content: \"\\e756\";\n}\n.mdi-device-signal-cellular-3-bar:before {\n  content: \"\\e757\";\n}\n.mdi-device-signal-cellular-4-bar:before {\n  content: \"\\e758\";\n}\n.mdi-signal-wifi-statusbar-connected-no-internet-after:before {\n  content: \"\\e8f6\";\n}\n.mdi-device-signal-cellular-connected-no-internet-0-bar:before {\n  content: \"\\e759\";\n}\n.mdi-device-signal-cellular-connected-no-internet-1-bar:before {\n  content: \"\\e75a\";\n}\n.mdi-device-signal-cellular-connected-no-internet-2-bar:before {\n  content: \"\\e75b\";\n}\n.mdi-device-signal-cellular-connected-no-internet-3-bar:before {\n  content: \"\\e75c\";\n}\n.mdi-device-signal-cellular-connected-no-internet-4-bar:before {\n  content: \"\\e75d\";\n}\n.mdi-device-signal-cellular-no-sim:before {\n  content: \"\\e75e\";\n}\n.mdi-device-signal-cellular-null:before {\n  content: \"\\e75f\";\n}\n.mdi-device-signal-cellular-off:before {\n  content: \"\\e760\";\n}\n.mdi-device-signal-wifi-0-bar:before {\n  content: \"\\e761\";\n}\n.mdi-device-signal-wifi-1-bar:before {\n  content: \"\\e762\";\n}\n.mdi-device-signal-wifi-2-bar:before {\n  content: \"\\e763\";\n}\n.mdi-device-signal-wifi-3-bar:before {\n  content: \"\\e764\";\n}\n.mdi-device-signal-wifi-4-bar:before {\n  content: \"\\e765\";\n}\n.mdi-device-signal-wifi-off:before {\n  content: \"\\e766\";\n}\n.mdi-device-signal-wifi-statusbar-1-bar:before {\n  content: \"\\e767\";\n}\n.mdi-device-signal-wifi-statusbar-2-bar:before {\n  content: \"\\e768\";\n}\n.mdi-device-signal-wifi-statusbar-3-bar:before {\n  content: \"\\e769\";\n}\n.mdi-device-signal-wifi-statusbar-4-bar:before {\n  content: \"\\e76a\";\n}\n.mdi-device-signal-wifi-statusbar-connected-no-internet-:before {\n  content: \"\\e76b\";\n}\n.mdi-device-signal-wifi-statusbar-connected-no-internet:before {\n  content: \"\\e76f\";\n}\n.mdi-device-signal-wifi-statusbar-connected-no-internet-2:before {\n  content: \"\\e76c\";\n}\n.mdi-device-signal-wifi-statusbar-connected-no-internet-3:before {\n  content: \"\\e76d\";\n}\n.mdi-device-signal-wifi-statusbar-connected-no-internet-4:before {\n  content: \"\\e76e\";\n}\n.mdi-signal-wifi-statusbar-not-connected-after:before {\n  content: \"\\e8f7\";\n}\n.mdi-device-signal-wifi-statusbar-not-connected:before {\n  content: \"\\e770\";\n}\n.mdi-device-signal-wifi-statusbar-null:before {\n  content: \"\\e771\";\n}\n.mdi-device-storage:before {\n  content: \"\\e772\";\n}\n.mdi-device-usb:before {\n  content: \"\\e773\";\n}\n.mdi-device-wifi-lock:before {\n  content: \"\\e774\";\n}\n.mdi-device-wifi-tethering:before {\n  content: \"\\e775\";\n}\n.mdi-hardware-cast-connected:before {\n  content: \"\\e7b7\";\n}\n.mdi-hardware-cast:before {\n  content: \"\\e7b8\";\n}\n.mdi-hardware-computer:before {\n  content: \"\\e7b9\";\n}\n.mdi-hardware-desktop-mac:before {\n  content: \"\\e7ba\";\n}\n.mdi-hardware-desktop-windows:before {\n  content: \"\\e7bb\";\n}\n.mdi-hardware-dock:before {\n  content: \"\\e7bc\";\n}\n.mdi-hardware-gamepad:before {\n  content: \"\\e7bd\";\n}\n.mdi-hardware-headset-mic:before {\n  content: \"\\e7be\";\n}\n.mdi-hardware-headset:before {\n  content: \"\\e7bf\";\n}\n.mdi-hardware-keyboard-alt:before {\n  content: \"\\e7c0\";\n}\n.mdi-hardware-keyboard-arrow-down:before {\n  content: \"\\e7c1\";\n}\n.mdi-hardware-keyboard-arrow-left:before {\n  content: \"\\e7c2\";\n}\n.mdi-hardware-keyboard-arrow-right:before {\n  content: \"\\e7c3\";\n}\n.mdi-hardware-keyboard-arrow-up:before {\n  content: \"\\e7c4\";\n}\n.mdi-hardware-keyboard-backspace:before {\n  content: \"\\e7c5\";\n}\n.mdi-hardware-keyboard-capslock:before {\n  content: \"\\e7c6\";\n}\n.mdi-hardware-keyboard-control:before {\n  content: \"\\e7c7\";\n}\n.mdi-hardware-keyboard-hide:before {\n  content: \"\\e7c8\";\n}\n.mdi-hardware-keyboard-return:before {\n  content: \"\\e7c9\";\n}\n.mdi-hardware-keyboard-tab:before {\n  content: \"\\e7ca\";\n}\n.mdi-hardware-keyboard-voice:before {\n  content: \"\\e7cb\";\n}\n.mdi-hardware-keyboard:before {\n  content: \"\\e7cc\";\n}\n.mdi-hardware-laptop-chromebook:before {\n  content: \"\\e7cd\";\n}\n.mdi-hardware-laptop-mac:before {\n  content: \"\\e7ce\";\n}\n.mdi-hardware-laptop-windows:before {\n  content: \"\\e7cf\";\n}\n.mdi-hardware-laptop:before {\n  content: \"\\e7d0\";\n}\n.mdi-hardware-memory:before {\n  content: \"\\e7d1\";\n}\n.mdi-hardware-mouse:before {\n  content: \"\\e7d2\";\n}\n.mdi-hardware-phone-android:before {\n  content: \"\\e7d3\";\n}\n.mdi-hardware-phone-iphone:before {\n  content: \"\\e7d4\";\n}\n.mdi-hardware-phonelink-off:before {\n  content: \"\\e7d5\";\n}\n.mdi-hardware-phonelink:before {\n  content: \"\\e7d6\";\n}\n.mdi-hardware-security:before {\n  content: \"\\e7d7\";\n}\n.mdi-hardware-sim-card:before {\n  content: \"\\e7d8\";\n}\n.mdi-hardware-smartphone:before {\n  content: \"\\e7d9\";\n}\n.mdi-hardware-speaker:before {\n  content: \"\\e7da\";\n}\n.mdi-hardware-tablet-android:before {\n  content: \"\\e7db\";\n}\n.mdi-hardware-tablet-mac:before {\n  content: \"\\e7dc\";\n}\n.mdi-hardware-tablet:before {\n  content: \"\\e7dd\";\n}\n.mdi-hardware-tv:before {\n  content: \"\\e7de\";\n}\n.mdi-hardware-watch:before {\n  content: \"\\e7df\";\n}\n.mdi-image-add-to-photos:before {\n  content: \"\\e7e0\";\n}\n.mdi-image-adjust:before {\n  content: \"\\e7e1\";\n}\n.mdi-image-assistant-photo:before {\n  content: \"\\e7e2\";\n}\n.mdi-image-audiotrack:before {\n  content: \"\\e7e3\";\n}\n.mdi-image-blur-circular:before {\n  content: \"\\e7e4\";\n}\n.mdi-image-blur-linear:before {\n  content: \"\\e7e5\";\n}\n.mdi-image-blur-off:before {\n  content: \"\\e7e6\";\n}\n.mdi-image-blur-on:before {\n  content: \"\\e7e7\";\n}\n.mdi-image-brightness-1:before {\n  content: \"\\e7e8\";\n}\n.mdi-image-brightness-2:before {\n  content: \"\\e7e9\";\n}\n.mdi-image-brightness-3:before {\n  content: \"\\e7ea\";\n}\n.mdi-image-brightness-4:before {\n  content: \"\\e7eb\";\n}\n.mdi-image-brightness-5:before {\n  content: \"\\e7ec\";\n}\n.mdi-image-brightness-6:before {\n  content: \"\\e7ed\";\n}\n.mdi-image-brightness-7:before {\n  content: \"\\e7ee\";\n}\n.mdi-image-brush:before {\n  content: \"\\e7ef\";\n}\n.mdi-image-camera-alt:before {\n  content: \"\\e7f0\";\n}\n.mdi-image-camera-front:before {\n  content: \"\\e7f1\";\n}\n.mdi-image-camera-rear:before {\n  content: \"\\e7f2\";\n}\n.mdi-image-camera-roll:before {\n  content: \"\\e7f3\";\n}\n.mdi-image-camera:before {\n  content: \"\\e7f4\";\n}\n.mdi-image-center-focus-strong:before {\n  content: \"\\e7f5\";\n}\n.mdi-image-center-focus-weak:before {\n  content: \"\\e7f6\";\n}\n.mdi-image-collections:before {\n  content: \"\\e7f7\";\n}\n.mdi-image-color-lens:before {\n  content: \"\\e7f8\";\n}\n.mdi-image-colorize:before {\n  content: \"\\e7f9\";\n}\n.mdi-image-compare:before {\n  content: \"\\e7fa\";\n}\n.mdi-image-control-point-duplicate:before {\n  content: \"\\e7fb\";\n}\n.mdi-image-control-point:before {\n  content: \"\\e7fc\";\n}\n.mdi-image-crop-3-2:before {\n  content: \"\\e7fd\";\n}\n.mdi-image-crop-5-4:before {\n  content: \"\\e7fe\";\n}\n.mdi-image-crop-7-5:before {\n  content: \"\\e7ff\";\n}\n.mdi-image-crop-16-9:before {\n  content: \"\\e800\";\n}\n.mdi-image-crop-din:before {\n  content: \"\\e801\";\n}\n.mdi-image-crop-free:before {\n  content: \"\\e802\";\n}\n.mdi-image-crop-landscape:before {\n  content: \"\\e803\";\n}\n.mdi-image-crop-original:before {\n  content: \"\\e804\";\n}\n.mdi-image-crop-portrait:before {\n  content: \"\\e805\";\n}\n.mdi-image-crop-square:before {\n  content: \"\\e806\";\n}\n.mdi-image-crop:before {\n  content: \"\\e807\";\n}\n.mdi-image-dehaze:before {\n  content: \"\\e808\";\n}\n.mdi-image-details:before {\n  content: \"\\e809\";\n}\n.mdi-image-edit:before {\n  content: \"\\e80a\";\n}\n.mdi-image-exposure-minus-1:before {\n  content: \"\\e80b\";\n}\n.mdi-image-exposure-minus-2:before {\n  content: \"\\e80c\";\n}\n.mdi-image-exposure-plus-1:before {\n  content: \"\\e80d\";\n}\n.mdi-image-exposure-plus-2:before {\n  content: \"\\e80e\";\n}\n.mdi-image-exposure-zero:before {\n  content: \"\\e80f\";\n}\n.mdi-image-exposure:before {\n  content: \"\\e810\";\n}\n.mdi-image-filter-1:before {\n  content: \"\\e811\";\n}\n.mdi-image-filter-2:before {\n  content: \"\\e812\";\n}\n.mdi-image-filter-3:before {\n  content: \"\\e813\";\n}\n.mdi-image-filter-4:before {\n  content: \"\\e814\";\n}\n.mdi-image-filter-5:before {\n  content: \"\\e815\";\n}\n.mdi-image-filter-6:before {\n  content: \"\\e816\";\n}\n.mdi-image-filter-7:before {\n  content: \"\\e817\";\n}\n.mdi-image-filter-8:before {\n  content: \"\\e818\";\n}\n.mdi-image-filter-9-plus:before {\n  content: \"\\e819\";\n}\n.mdi-image-filter-9:before {\n  content: \"\\e81a\";\n}\n.mdi-image-filter-b-and-w:before {\n  content: \"\\e81b\";\n}\n.mdi-image-filter-center-focus:before {\n  content: \"\\e81c\";\n}\n.mdi-image-filter-drama:before {\n  content: \"\\e81d\";\n}\n.mdi-image-filter-frames:before {\n  content: \"\\e81e\";\n}\n.mdi-image-filter-hdr:before {\n  content: \"\\e81f\";\n}\n.mdi-image-filter-none:before {\n  content: \"\\e820\";\n}\n.mdi-image-filter-tilt-shift:before {\n  content: \"\\e821\";\n}\n.mdi-image-filter-vintage:before {\n  content: \"\\e822\";\n}\n.mdi-image-filter:before {\n  content: \"\\e823\";\n}\n.mdi-image-flare:before {\n  content: \"\\e824\";\n}\n.mdi-image-flash-auto:before {\n  content: \"\\e825\";\n}\n.mdi-image-flash-off:before {\n  content: \"\\e826\";\n}\n.mdi-image-flash-on:before {\n  content: \"\\e827\";\n}\n.mdi-image-flip:before {\n  content: \"\\e828\";\n}\n.mdi-image-gradient:before {\n  content: \"\\e829\";\n}\n.mdi-image-grain:before {\n  content: \"\\e82a\";\n}\n.mdi-image-grid-off:before {\n  content: \"\\e82b\";\n}\n.mdi-image-grid-on:before {\n  content: \"\\e82c\";\n}\n.mdi-image-hdr-off:before {\n  content: \"\\e82d\";\n}\n.mdi-image-hdr-on:before {\n  content: \"\\e82e\";\n}\n.mdi-image-hdr-strong:before {\n  content: \"\\e82f\";\n}\n.mdi-image-hdr-weak:before {\n  content: \"\\e830\";\n}\n.mdi-image-healing:before {\n  content: \"\\e831\";\n}\n.mdi-image-image-aspect-ratio:before {\n  content: \"\\e832\";\n}\n.mdi-image-image:before {\n  content: \"\\e833\";\n}\n.mdi-image-iso:before {\n  content: \"\\e834\";\n}\n.mdi-image-landscape:before {\n  content: \"\\e835\";\n}\n.mdi-image-leak-add:before {\n  content: \"\\e836\";\n}\n.mdi-image-leak-remove:before {\n  content: \"\\e837\";\n}\n.mdi-image-lens:before {\n  content: \"\\e838\";\n}\n.mdi-image-looks-3:before {\n  content: \"\\e839\";\n}\n.mdi-image-looks-4:before {\n  content: \"\\e83a\";\n}\n.mdi-image-looks-5:before {\n  content: \"\\e83b\";\n}\n.mdi-image-looks-6:before {\n  content: \"\\e83c\";\n}\n.mdi-image-looks-one:before {\n  content: \"\\e83d\";\n}\n.mdi-image-looks-two:before {\n  content: \"\\e83e\";\n}\n.mdi-image-looks:before {\n  content: \"\\e83f\";\n}\n.mdi-image-loupe:before {\n  content: \"\\e840\";\n}\n.mdi-image-movie-creation:before {\n  content: \"\\e841\";\n}\n.mdi-image-nature-people:before {\n  content: \"\\e842\";\n}\n.mdi-image-nature:before {\n  content: \"\\e843\";\n}\n.mdi-image-navigate-before:before {\n  content: \"\\e844\";\n}\n.mdi-image-navigate-next:before {\n  content: \"\\e845\";\n}\n.mdi-image-palette:before {\n  content: \"\\e846\";\n}\n.mdi-image-panorama-fisheye:before {\n  content: \"\\e847\";\n}\n.mdi-image-panorama-horizontal:before {\n  content: \"\\e848\";\n}\n.mdi-image-panorama-vertical:before {\n  content: \"\\e849\";\n}\n.mdi-image-panorama-wide-angle:before {\n  content: \"\\e84a\";\n}\n.mdi-image-panorama:before {\n  content: \"\\e84b\";\n}\n.mdi-image-photo-album:before {\n  content: \"\\e84c\";\n}\n.mdi-image-photo-camera:before {\n  content: \"\\e84d\";\n}\n.mdi-image-photo-library:before {\n  content: \"\\e84e\";\n}\n.mdi-image-photo:before {\n  content: \"\\e84f\";\n}\n.mdi-image-portrait:before {\n  content: \"\\e850\";\n}\n.mdi-image-remove-red-eye:before {\n  content: \"\\e851\";\n}\n.mdi-image-rotate-left:before {\n  content: \"\\e852\";\n}\n.mdi-image-rotate-right:before {\n  content: \"\\e853\";\n}\n.mdi-image-slideshow:before {\n  content: \"\\e854\";\n}\n.mdi-image-straighten:before {\n  content: \"\\e855\";\n}\n.mdi-image-style:before {\n  content: \"\\e856\";\n}\n.mdi-image-switch-camera:before {\n  content: \"\\e857\";\n}\n.mdi-image-switch-video:before {\n  content: \"\\e858\";\n}\n.mdi-image-tag-faces:before {\n  content: \"\\e859\";\n}\n.mdi-image-texture:before {\n  content: \"\\e85a\";\n}\n.mdi-image-timelapse:before {\n  content: \"\\e85b\";\n}\n.mdi-image-timer-3:before {\n  content: \"\\e85c\";\n}\n.mdi-image-timer-10:before {\n  content: \"\\e85d\";\n}\n.mdi-image-timer-auto:before {\n  content: \"\\e85e\";\n}\n.mdi-image-timer-off:before {\n  content: \"\\e85f\";\n}\n.mdi-image-timer:before {\n  content: \"\\e860\";\n}\n.mdi-image-tonality:before {\n  content: \"\\e861\";\n}\n.mdi-image-transform:before {\n  content: \"\\e862\";\n}\n.mdi-image-tune:before {\n  content: \"\\e863\";\n}\n.mdi-image-wb-auto:before {\n  content: \"\\e864\";\n}\n.mdi-image-wb-cloudy:before {\n  content: \"\\e865\";\n}\n.mdi-image-wb-incandescent:before {\n  content: \"\\e866\";\n}\n.mdi-image-wb-irradescent:before {\n  content: \"\\e867\";\n}\n.mdi-image-wb-sunny:before {\n  content: \"\\e868\";\n}\n.mdi-maps-beenhere:before {\n  content: \"\\e869\";\n}\n.mdi-maps-directions-bike:before {\n  content: \"\\e86a\";\n}\n.mdi-maps-directions-bus:before {\n  content: \"\\e86b\";\n}\n.mdi-maps-directions-car:before {\n  content: \"\\e86c\";\n}\n.mdi-maps-directions-ferry:before {\n  content: \"\\e86d\";\n}\n.mdi-maps-directions-subway:before {\n  content: \"\\e86e\";\n}\n.mdi-maps-directions-train:before {\n  content: \"\\e86f\";\n}\n.mdi-maps-directions-transit:before {\n  content: \"\\e870\";\n}\n.mdi-maps-directions-walk:before {\n  content: \"\\e871\";\n}\n.mdi-maps-directions:before {\n  content: \"\\e872\";\n}\n.mdi-maps-flight:before {\n  content: \"\\e873\";\n}\n.mdi-maps-hotel:before {\n  content: \"\\e874\";\n}\n.mdi-maps-layers-clear:before {\n  content: \"\\e875\";\n}\n.mdi-maps-layers:before {\n  content: \"\\e876\";\n}\n.mdi-maps-local-airport:before {\n  content: \"\\e877\";\n}\n.mdi-maps-local-atm:before {\n  content: \"\\e878\";\n}\n.mdi-maps-local-attraction:before {\n  content: \"\\e879\";\n}\n.mdi-maps-local-bar:before {\n  content: \"\\e87a\";\n}\n.mdi-maps-local-cafe:before {\n  content: \"\\e87b\";\n}\n.mdi-maps-local-car-wash:before {\n  content: \"\\e87c\";\n}\n.mdi-maps-local-convenience-store:before {\n  content: \"\\e87d\";\n}\n.mdi-maps-local-drink:before {\n  content: \"\\e87e\";\n}\n.mdi-maps-local-florist:before {\n  content: \"\\e87f\";\n}\n.mdi-maps-local-gas-station:before {\n  content: \"\\e880\";\n}\n.mdi-maps-local-grocery-store:before {\n  content: \"\\e881\";\n}\n.mdi-maps-local-hospital:before {\n  content: \"\\e882\";\n}\n.mdi-maps-local-hotel:before {\n  content: \"\\e883\";\n}\n.mdi-maps-local-laundry-service:before {\n  content: \"\\e884\";\n}\n.mdi-maps-local-library:before {\n  content: \"\\e885\";\n}\n.mdi-maps-local-mall:before {\n  content: \"\\e886\";\n}\n.mdi-maps-local-movies:before {\n  content: \"\\e887\";\n}\n.mdi-maps-local-offer:before {\n  content: \"\\e888\";\n}\n.mdi-maps-local-parking:before {\n  content: \"\\e889\";\n}\n.mdi-maps-local-pharmacy:before {\n  content: \"\\e88a\";\n}\n.mdi-maps-local-phone:before {\n  content: \"\\e88b\";\n}\n.mdi-maps-local-pizza:before {\n  content: \"\\e88c\";\n}\n.mdi-maps-local-play:before {\n  content: \"\\e88d\";\n}\n.mdi-maps-local-post-office:before {\n  content: \"\\e88e\";\n}\n.mdi-maps-local-print-shop:before {\n  content: \"\\e88f\";\n}\n.mdi-maps-local-restaurant:before {\n  content: \"\\e890\";\n}\n.mdi-maps-local-see:before {\n  content: \"\\e891\";\n}\n.mdi-maps-local-shipping:before {\n  content: \"\\e892\";\n}\n.mdi-maps-local-taxi:before {\n  content: \"\\e893\";\n}\n.mdi-maps-location-history:before {\n  content: \"\\e894\";\n}\n.mdi-maps-map:before {\n  content: \"\\e895\";\n}\n.mdi-maps-my-location:before {\n  content: \"\\e896\";\n}\n.mdi-maps-navigation:before {\n  content: \"\\e897\";\n}\n.mdi-maps-pin-drop:before {\n  content: \"\\e898\";\n}\n.mdi-maps-place:before {\n  content: \"\\e899\";\n}\n.mdi-maps-rate-review:before {\n  content: \"\\e89a\";\n}\n.mdi-maps-restaurant-menu:before {\n  content: \"\\e89b\";\n}\n.mdi-maps-satellite:before {\n  content: \"\\e89c\";\n}\n.mdi-maps-store-mall-directory:before {\n  content: \"\\e89d\";\n}\n.mdi-maps-terrain:before {\n  content: \"\\e89e\";\n}\n.mdi-maps-traffic:before {\n  content: \"\\e89f\";\n}\n.mdi-navigation-apps:before {\n  content: \"\\e8a0\";\n}\n.mdi-navigation-arrow-back:before {\n  content: \"\\e8a1\";\n}\n.mdi-navigation-arrow-drop-down-circle:before {\n  content: \"\\e8a2\";\n}\n.mdi-navigation-arrow-drop-down:before {\n  content: \"\\e8a3\";\n}\n.mdi-navigation-arrow-drop-up:before {\n  content: \"\\e8a4\";\n}\n.mdi-navigation-arrow-forward:before {\n  content: \"\\e8a5\";\n}\n.mdi-navigation-cancel:before {\n  content: \"\\e8a6\";\n}\n.mdi-navigation-check:before {\n  content: \"\\e8a7\";\n}\n.mdi-navigation-chevron-left:before {\n  content: \"\\e8a8\";\n}\n.mdi-navigation-chevron-right:before {\n  content: \"\\e8a9\";\n}\n.mdi-navigation-close:before {\n  content: \"\\e8aa\";\n}\n.mdi-navigation-expand-less:before {\n  content: \"\\e8ab\";\n}\n.mdi-navigation-expand-more:before {\n  content: \"\\e8ac\";\n}\n.mdi-navigation-fullscreen-exit:before {\n  content: \"\\e8ad\";\n}\n.mdi-navigation-fullscreen:before {\n  content: \"\\e8ae\";\n}\n.mdi-navigation-menu:before {\n  content: \"\\e8af\";\n}\n.mdi-navigation-more-horiz:before {\n  content: \"\\e8b0\";\n}\n.mdi-navigation-more-vert:before {\n  content: \"\\e8b1\";\n}\n.mdi-navigation-refresh:before {\n  content: \"\\e8b2\";\n}\n.mdi-navigation-unfold-less:before {\n  content: \"\\e8b3\";\n}\n.mdi-navigation-unfold-more:before {\n  content: \"\\e8b4\";\n}\n.mdi-notification-adb:before {\n  content: \"\\e8b5\";\n}\n.mdi-notification-bluetooth-audio:before {\n  content: \"\\e8b6\";\n}\n.mdi-notification-disc-full:before {\n  content: \"\\e8b7\";\n}\n.mdi-notification-dnd-forwardslash:before {\n  content: \"\\e8b8\";\n}\n.mdi-notification-do-not-disturb:before {\n  content: \"\\e8b9\";\n}\n.mdi-notification-drive-eta:before {\n  content: \"\\e8ba\";\n}\n.mdi-notification-event-available:before {\n  content: \"\\e8bb\";\n}\n.mdi-notification-event-busy:before {\n  content: \"\\e8bc\";\n}\n.mdi-notification-event-note:before {\n  content: \"\\e8bd\";\n}\n.mdi-notification-folder-special:before {\n  content: \"\\e8be\";\n}\n.mdi-notification-mms:before {\n  content: \"\\e8bf\";\n}\n.mdi-notification-more:before {\n  content: \"\\e8c0\";\n}\n.mdi-notification-network-locked:before {\n  content: \"\\e8c1\";\n}\n.mdi-notification-phone-bluetooth-speaker:before {\n  content: \"\\e8c2\";\n}\n.mdi-notification-phone-forwarded:before {\n  content: \"\\e8c3\";\n}\n.mdi-notification-phone-in-talk:before {\n  content: \"\\e8c4\";\n}\n.mdi-notification-phone-locked:before {\n  content: \"\\e8c5\";\n}\n.mdi-notification-phone-missed:before {\n  content: \"\\e8c6\";\n}\n.mdi-notification-phone-paused:before {\n  content: \"\\e8c7\";\n}\n.mdi-notification-play-download:before {\n  content: \"\\e8c8\";\n}\n.mdi-notification-play-install:before {\n  content: \"\\e8c9\";\n}\n.mdi-notification-sd-card:before {\n  content: \"\\e8ca\";\n}\n.mdi-notification-sim-card-alert:before {\n  content: \"\\e8cb\";\n}\n.mdi-notification-sms-failed:before {\n  content: \"\\e8cc\";\n}\n.mdi-notification-sms:before {\n  content: \"\\e8cd\";\n}\n.mdi-notification-sync-disabled:before {\n  content: \"\\e8ce\";\n}\n.mdi-notification-sync-problem:before {\n  content: \"\\e8cf\";\n}\n.mdi-notification-sync:before {\n  content: \"\\e8d0\";\n}\n.mdi-notification-system-update:before {\n  content: \"\\e8d1\";\n}\n.mdi-notification-tap-and-play:before {\n  content: \"\\e8d2\";\n}\n.mdi-notification-time-to-leave:before {\n  content: \"\\e8d3\";\n}\n.mdi-notification-vibration:before {\n  content: \"\\e8d4\";\n}\n.mdi-notification-voice-chat:before {\n  content: \"\\e8d5\";\n}\n.mdi-notification-vpn-lock:before {\n  content: \"\\e8d6\";\n}\n.mdi-social-cake:before {\n  content: \"\\e8d7\";\n}\n.mdi-social-domain:before {\n  content: \"\\e8d8\";\n}\n.mdi-social-group-add:before {\n  content: \"\\e8d9\";\n}\n.mdi-social-group:before {\n  content: \"\\e8da\";\n}\n.mdi-social-location-city:before {\n  content: \"\\e8db\";\n}\n.mdi-social-mood:before {\n  content: \"\\e8dc\";\n}\n.mdi-social-notifications-none:before {\n  content: \"\\e8dd\";\n}\n.mdi-social-notifications-off:before {\n  content: \"\\e8de\";\n}\n.mdi-social-notifications-on:before {\n  content: \"\\e8df\";\n}\n.mdi-social-notifications-paused:before {\n  content: \"\\e8e0\";\n}\n.mdi-social-notifications:before {\n  content: \"\\e8e1\";\n}\n.mdi-social-pages:before {\n  content: \"\\e8e2\";\n}\n.mdi-social-party-mode:before {\n  content: \"\\e8e3\";\n}\n.mdi-social-people-outline:before {\n  content: \"\\e8e4\";\n}\n.mdi-social-people:before {\n  content: \"\\e8e5\";\n}\n.mdi-social-person-add:before {\n  content: \"\\e8e6\";\n}\n.mdi-social-person-outline:before {\n  content: \"\\e8e7\";\n}\n.mdi-social-person:before {\n  content: \"\\e8e8\";\n}\n.mdi-social-plus-one:before {\n  content: \"\\e8e9\";\n}\n.mdi-social-poll:before {\n  content: \"\\e8ea\";\n}\n.mdi-social-public:before {\n  content: \"\\e8eb\";\n}\n.mdi-social-school:before {\n  content: \"\\e8ec\";\n}\n.mdi-social-share:before {\n  content: \"\\e8ed\";\n}\n.mdi-social-whatshot:before {\n  content: \"\\e8ee\";\n}\n.mdi-toggle-check-box-outline-blank:before {\n  content: \"\\e8ef\";\n}\n.mdi-toggle-check-box:before {\n  content: \"\\e8f0\";\n}\n.mdi-toggle-radio-button-off:before {\n  content: \"\\e8f1\";\n}\n.mdi-toggle-radio-button-on:before {\n  content: \"\\e8f2\";\n}\n.mdi-toggle-star-half:before {\n  content: \"\\e8f3\";\n}\n.mdi-toggle-star-outline:before {\n  content: \"\\e8f4\";\n}\n.mdi-toggle-star:before {\n  content: \"\\e8f5\";\n}\n/* @xl : 1200px; */\n.cover {\n  background-position: center center;\n  background-size: cover;\n  width: 100%;\n}\n.all-height {\n  height: 100%;\n}\n.container {\n  margin: auto;\n  width: 85%;\n}\n.row {\n  width: 100%;\n  margin-bottom: 20px;\n}\n.row:after,\n.row:before {\n  content: \"\";\n  display: table;\n  clear: both;\n}\n.row .col {\n  /*float:left;*/\n  display: inline-block;\n  vertical-align: top;\n  padding: 0 0.7em;\n}\n.row.row-padding {\n  padding: 0 0.7em;\n}\n.row.row-padding .col {\n  padding: 0;\n}\n.col {\n  width: 100%;\n}\n.s1 {\n  width: 8.33333333%;\n}\n.s2 {\n  width: 16.66666667%;\n}\n.s3 {\n  width: 25%;\n}\n.s4 {\n  width: 33.33333333%;\n}\n.s5 {\n  width: 41.66666667%;\n}\n.s6 {\n  width: 50%;\n}\n.s7 {\n  width: 58.33333333%;\n}\n.s8 {\n  width: 66.66666667%;\n}\n.s9 {\n  width: 75%;\n}\n.s10 {\n  width: 83.33333333%;\n}\n.s11 {\n  width: 91.66666667%;\n}\n.s12 {\n  width: 100%;\n}\n@media (min-width: 600px) {\n  .container {\n    width: 84%;\n  }\n  .sl1 {\n    width: 8.33333333%;\n  }\n  .sl2 {\n    width: 16.66666667%;\n  }\n  .sl3 {\n    width: 25%;\n  }\n  .sl4 {\n    width: 33.33333333%;\n  }\n  .sl5 {\n    width: 41.66666667%;\n  }\n  .sl6 {\n    width: 50%;\n  }\n  .sl7 {\n    width: 58.33333333%;\n  }\n  .sl8 {\n    width: 66.66666667%;\n  }\n  .sl9 {\n    width: 75%;\n  }\n  .sl10 {\n    width: 83.33333333%;\n  }\n  .sl11 {\n    width: 91.66666667%;\n  }\n  .sl12 {\n    width: 100%;\n  }\n}\n@media (min-width: 768px) {\n  .container {\n    width: 83%;\n  }\n  .m1 {\n    width: 8.33333333%;\n  }\n  .m2 {\n    width: 16.66666667%;\n  }\n  .m3 {\n    width: 25%;\n  }\n  .m4 {\n    width: 33.33333333%;\n  }\n  .m5 {\n    width: 41.66666667%;\n  }\n  .m6 {\n    width: 50%;\n  }\n  .m7 {\n    width: 58.33333333%;\n  }\n  .m8 {\n    width: 66.66666667%;\n  }\n  .m9 {\n    width: 75%;\n  }\n  .m10 {\n    width: 83.33333333%;\n  }\n  .m11 {\n    width: 91.66666667%;\n  }\n  .m12 {\n    width: 100%;\n  }\n}\n@media (min-width: 1000px) {\n  .container {\n    width: 82%;\n  }\n  .ml1 {\n    width: 8.33333333%;\n  }\n  .ml2 {\n    width: 16.66666667%;\n  }\n  .ml3 {\n    width: 25%;\n  }\n  .ml4 {\n    width: 33.33333333%;\n  }\n  .ml5 {\n    width: 41.66666667%;\n  }\n  .ml6 {\n    width: 50%;\n  }\n  .ml7 {\n    width: 58.33333333%;\n  }\n  .ml8 {\n    width: 66.66666667%;\n  }\n  .ml9 {\n    width: 75%;\n  }\n  .ml10 {\n    width: 83.33333333%;\n  }\n  .ml11 {\n    width: 91.66666667%;\n  }\n  .ml12 {\n    width: 100%;\n  }\n}\n@media (min-width: 1200px) {\n  .container {\n    width: 80%;\n  }\n  .l1 {\n    width: 8.33333333%;\n  }\n  .l2 {\n    width: 16.66666667%;\n  }\n  .l3 {\n    width: 25%;\n  }\n  .l4 {\n    width: 33.33333333%;\n  }\n  .l5 {\n    width: 41.66666667%;\n  }\n  .l6 {\n    width: 50%;\n  }\n  .l7 {\n    width: 58.33333333%;\n  }\n  .l8 {\n    width: 66.66666667%;\n  }\n  .l9 {\n    width: 75%;\n  }\n  .l10 {\n    width: 83.33333333%;\n  }\n  .l11 {\n    width: 91.66666667%;\n  }\n  .l12 {\n    width: 100%;\n  }\n}\n.container.fluid {\n  width: 100%;\n}\n.toast-container {\n  position: fixed;\n  top: 20px;\n  min-width: 8%;\n  right: 7%;\n  display: block;\n  z-index: 2100;\n  max-width: 86%;\n}\n.toast-container .toast {\n  margin-bottom: 1.4em;\n  display: none;\n  clear: both;\n  position: relative;\n  padding: 0.8em 1em;\n}\n.toast-container .toast.default {\n  background-color: rgba(0, 0, 0, 0.8);\n  color: white;\n}\n.pull-left {\n  float: left;\n}\n.pull-right {\n  float: right;\n}\n.special-invisible {\n  display: none !important;\n}\n.relative {\n  position: relative;\n}\n.show-at-start {\n  display: block !important;\n}\n.hide-at-start {\n  display: none !important;\n}\n.inline-block {\n  display: inline-block;\n}\n.flex {\n  display: flex;\n}\n.text-center {\n  text-align: center;\n}\n.text-left {\n  text-align: left;\n}\n.text-right {\n  text-align: right;\n}\n.text-justify {\n  text-align: justify;\n}\n/*\n.size-xl .xl-hide, .size-xl .l-hide, .size-xl .ml-hide, .size-xl .m-hide, .size-xl .sl-hide, .size-xl .s-hide, .size-xl .xl-hide-only{\n    display:none;\n}\n*/\n.clearfix:after,\n.clearfix:before {\n  content: \" \";\n  display: table;\n  clear: both;\n}\n.valign-wrapper {\n  display: -webkit-box;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: -webkit-flex;\n  display: flex;\n  -webkit-flex-align: center;\n  -ms-flex-align: center;\n  -webkit-align-items: center;\n  align-items: center;\n}\n.valign {\n  vertical-align: middle;\n  display: block;\n}\n.hoverable:hover {\n  transition: box-shadow 0.2s;\n  box-shadow: 0 0 20px rgba(0, 0, 0, 0.4);\n}\n.small {\n  font-size: 70%;\n}\n.large {\n  font-size: 120%;\n}\n.verylarge {\n  font-size: 150%;\n}\n.f0-5em {\n  font-size: 0.5em;\n}\n.f1em {\n  font-size: 1em;\n}\n.f1-5em {\n  font-size: 1.5em;\n}\n.f2em {\n  font-size: 2em;\n}\n.f2-5em {\n  font-size: 2.5em;\n}\n.f3em {\n  font-size: 3em;\n}\n.f3-5em {\n  font-size: 3.5em;\n}\n.f4em {\n  font-size: 4em;\n}\n.f4-5em {\n  font-size: 4.5em;\n}\n/*\n.f1em,.f1-5em, .f2em, f2-5em, .f3em, .f3-5em, f4em, .f4-5em{\n    line-height: 1.3em;\n}\n*/\n.flow-height {\n  height: 80%;\n  position: absolute;\n}\n@media all {\n  /* CONTENIDOS OCULTOS DE ACUERDO AL DISPOSITIVO */\n  .hide {\n    display: none ;\n  }\n  .size-sl .s-show-lt {\n    display: none ;\n  }\n  .size-m .s-show-lt,\n  .size-m .sl-show-lt {\n    display: none ;\n  }\n  .size-ml .s-show-lt,\n  .size-ml .sl-show-lt,\n  .size-ml .m-show-lt {\n    display: none ;\n  }\n  .size-l .s-show-lt,\n  .size-l .sl-show-lt,\n  .size-l .m-show-lt,\n  .size-l .ml-show-lt {\n    display: none ;\n  }\n  .size-s .sl-show,\n  .size-s .m-show,\n  .size-s .ml-show,\n  .size-s .l-show {\n    display: none ;\n  }\n  .size-sl .m-show,\n  .size-sl .ml-show,\n  .size-sl .l-show {\n    display: none ;\n  }\n  .size-m .ml-show,\n  .size-m .l-show {\n    display: none ;\n  }\n  .size-ml .l-show {\n    display: none ;\n  }\n  .size-s .s-hide,\n  .size-s .sl-hide,\n  .size-s .m-hide,\n  .size-s .ml-hide,\n  .size-s .l-hide,\n  .size-s .s-hide-only {\n    display: none ;\n  }\n  .size-sl .sl-hide,\n  .size-sl .m-hide,\n  .size-sl .ml-hide,\n  .size-sl .l-hide,\n  .size-sl .sl-hide-only {\n    display: none ;\n  }\n  .size-m .m-hide,\n  .size-m .ml-hide,\n  .size-m .l-hide,\n  .size-m .m-hide-only {\n    display: none ;\n  }\n  .size-ml .ml-hide,\n  .size-ml .l-hide,\n  .size-ml .ml-hide-only {\n    display: none ;\n  }\n  .size-l .l-hide,\n  .size-l .l-hide-only {\n    display: none ;\n  }\n}\n@media (min-width: 600px) {\n  .flow-height {\n    height: 90%;\n  }\n}\n@media (min-width: 768px) {\n  .flow-height {\n    height: 100%;\n  }\n}\n@media (min-width: 1000px) {\n  .flow-height {\n    height: 110%;\n  }\n}\n@media (min-width: 1200px) {\n  .flow-height {\n    height: 120%;\n  }\n}\n@font-face {\n  font-family: \"Roboto\";\n  src: url(\"../font/Roboto-Thin.woff2\") format(\"woff2\"), url(\"../font/Roboto-Thin.woff\") format(\"woff\"), url(\"../font/Roboto-Thin.ttf\") format(\"truetype\");\n  font-weight: 200;\n}\n@font-face {\n  font-family: \"Roboto\";\n  src: url(\"../font/Roboto-Light.woff2\") format(\"woff2\"), url(\"../font/Roboto-Light.woff\") format(\"woff\"), url(\"../font/Roboto-Light.ttf\") format(\"truetype\");\n  font-weight: 300;\n}\n@font-face {\n  font-family: \"Roboto\";\n  src: url(\"../font/Roboto-Regular.woff2\") format(\"woff2\"), url(\"../font/Roboto-Regular.woff\") format(\"woff\"), url(\"../font/Roboto-Regular.ttf\") format(\"truetype\");\n  font-weight: 400;\n}\n@font-face {\n  font-family: \"Roboto\";\n  src: url(\"../font/Roboto-Medium.woff2\") format(\"woff2\"), url(\"../font/Roboto-Medium.woff\") format(\"woff\"), url(\"../font/Roboto-Medium.ttf\") format(\"truetype\");\n  font-weight: 500;\n}\n@font-face {\n  font-family: \"Roboto\";\n  src: url(\"../font/Roboto-Bold.woff2\") format(\"woff2\"), url(\"../font/Roboto-Bold.woff\") format(\"woff\"), url(\"../font/Roboto-Bold.ttf\") format(\"truetype\");\n  font-weight: 700;\n}\n.flow-text {\n  font-size: 0.9em;\n}\nh1,\nh2,\nh3,\nh4,\nh5,\nh6 {\n  font-weight: 300;\n  line-height: initial;\n}\nh1 {\n  font-size: 2.1em;\n  margin: 0.5em 0;\n}\nh2 {\n  font-size: 1.8em;\n  margin: 0.4em 0;\n}\nh3 {\n  font-size: 1.4em;\n  margin: 0.3em 0;\n}\nh4 {\n  font-size: 1.1em;\n  margin: 0.25em 0;\n}\nh5 {\n  font-size: 0.9em;\n  margin: 0.2em 0;\n}\nh6 {\n  font-size: 0.7em;\n  margin: 0.2em 0;\n}\n@media (min-width: 600px) {\n  .flow-text {\n    font-size: 0.93em;\n  }\n  /*\n    h1{\n        font-size:2.8em;\n    }\n    h2{\n        font-size:2.4em;\n    }\n    h3{\n        font-size:2.2em;\n    }\n    h4{\n        font-size:2em;\n    }\n    h5{\n        font-size:1.55em;\n    }\n    h6{\n        font-size:0.9em;\n    }\n    */\n}\n@media (min-width: 768px) {\n  .flow-text {\n    font-size: 0.98em;\n  }\n  /*\n    h1{\n        font-size:2.9em;\n    }\n    h2{\n        font-size:2.45em;\n    }\n    h3{\n        font-size:2.25em;\n    }\n    h4{\n        font-size:2.05em;\n    }\n    h5{\n        font-size:1.6em;\n    }\n    h6{\n        font-size:1em;\n    }\n    */\n}\n@media (min-width: 1000px) {\n  .flow-text {\n    font-size: 0.995em;\n  }\n  /*\n    h1{\n        font-size:3em;\n    }\n\n    h2{\n        font-size:2.5em;\n    }\n    h3{\n        font-size:2.3em;\n    }\n    h4{\n        font-size:2.1em;\n    }\n    h5{\n        font-size:1.65em;\n    }\n    h6{\n        font-size:1.1em;\n    }\n    */\n}\n@media (min-width: 1200px) {\n  .flow-text {\n    font-size: 1.02em;\n  }\n  /*\n    h1{\n        font-size:3.1em;\n    }\n    h2{\n        font-size:2.55em;\n    }\n    h3{\n        font-size:2.35em;\n    }\n    h4{\n        font-size:2.15em;\n    }\n    h5{\n        font-size:1.7em;\n    }\n    h6{\n        font-size:1.15em;\n    }\n    */\n}\n.truncate {\n  display: block;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n.responsive-img {\n  max-width: 100%;\n  height: auto;\n}\n.responsive-img.fixed {\n  max-height: 100%;\n}\ndiv.responsive-img {\n  background-position: center center;\n  width: 100%;\n  background-size: cover !important;\n}\ndiv.responsive-img.top {\n  background-position-y: 0%;\n}\ndiv.responsive-img.bottom {\n  background-position-y: 100%;\n}\ndiv.responsive-img.left {\n  background-position-x: 0%;\n}\ndiv.responsive-img.right {\n  background-position-x: 100%;\n}\n.circle {\n  border-radius: 50% !important;\n}\n.z-depth-1 {\n  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);\n}\n.z-depth-2 {\n  box-shadow: 0 4px 10px 0 rgba(0, 0, 0, 0.2), 0 4px 20px 0 rgba(0, 0, 0, 0.22);\n}\n.z-depth-3 {\n  box-shadow: 0 8px 20px 0 rgba(0, 0, 0, 0.26), 0 8px 40px 0 rgba(0, 0, 0, 0.22);\n}\n.z-depth-4 {\n  box-shadow: 0 10px 30px 0 rgba(0, 0, 0, 0.26), 0 10px 60px 0 rgba(0, 0, 0, 0.22);\n}\n.z-depth-5 {\n  box-shadow: 0 12px 40px 0 rgba(0, 0, 0, 0.26), 0 12px 80px 0 rgba(0, 0, 0, 0.22);\n}\n.shadow-1 {\n  box-shadow: 0 0 3px rgba(0, 0, 0, 0.6);\n}\n.shadow-2 {\n  box-shadow: 0 0 6px rgba(0, 0, 0, 0.6);\n}\n.shadow-3 {\n  box-shadow: 0 0 9px rgba(0, 0, 0, 0.6);\n}\n.shadow-4 {\n  box-shadow: 0 0 12px rgba(0, 0, 0, 0.6);\n}\n.shadow-5 {\n  box-shadow: 0 0 15px rgba(0, 0, 0, 0.6);\n}\n.button {\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  cursor: pointer;\n  box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);\n  box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  -webkit-box-sizing: border-box;\n  outline: none;\n  border-radius: 2px;\n  padding: 0.5em 1.3em;\n  display: inline-block;\n  text-align: center;\n  border: 0;\n  -webkit-transform: translateZ(0);\n  -moz-transform: translateZ(0);\n  -ms-transform: translateZ(0);\n  -o-transform: translateZ(0);\n  transform: translateZ(0);\n}\n.button:hover {\n  box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.2);\n}\n.button[disabled] {\n  opacity: 0.65;\n  cursor: default !important;\n}\n.transitioned {\n  transition-property: all;\n  transition-duration: 1s;\n}\n.transitioned.short {\n  -webkit-transition-duration: 0.5s;\n  transition-duration: 0.5s;\n}\n.transitioned.veryshort {\n  -webkit-transition-duration: 0.25s;\n  transition-duration: 0.25s;\n}\n.transitioned.ease {\n  transition-timing-function: ease;\n}\n.transitioned.easeIn {\n  transition-timing-function: ease-in;\n}\n.transitioned.easeOut {\n  transition-timing-function: ease-out;\n}\n.transitioned.linear {\n  transition-timing-function: linear;\n}\n.transitioned.easeInOut {\n  transition-timing-function: ease-in-out;\n}\n/*!\nAnimate.css - http://daneden.me/animate\nLicensed under the MIT license - http://opensource.org/licenses/MIT\n\nCopyright (c) 2015 Daniel Eden\n*/\n.animated.short {\n  -webkit-animation-duration: 0.5s;\n  animation-duration: 0.5s;\n}\n.animated {\n  -webkit-animation-duration: 1s;\n  animation-duration: 1s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n}\n.animated.infinite {\n  -webkit-animation-iteration-count: infinite;\n  animation-iteration-count: infinite;\n}\n.animated.hinge {\n  -webkit-animation-duration: 2s;\n  animation-duration: 2s;\n}\n.animated.bounceIn,\n.animated.bounceOut {\n  -webkit-animation-duration: .75s;\n  animation-duration: .75s;\n}\n.animated.flipOutX,\n.animated.flipOutY {\n  -webkit-animation-duration: .75s;\n  animation-duration: .75s;\n}\n@-webkit-keyframes bounce {\n  0%,\n  20%,\n  53%,\n  80%,\n  100% {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n  40%,\n  43% {\n    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    -webkit-transform: translate3d(0, -30px, 0);\n    transform: translate3d(0, -30px, 0);\n  }\n  70% {\n    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    -webkit-transform: translate3d(0, -15px, 0);\n    transform: translate3d(0, -15px, 0);\n  }\n  90% {\n    -webkit-transform: translate3d(0, -4px, 0);\n    transform: translate3d(0, -4px, 0);\n  }\n}\n@keyframes bounce {\n  0%,\n  20%,\n  53%,\n  80%,\n  100% {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n  40%,\n  43% {\n    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    -webkit-transform: translate3d(0, -30px, 0);\n    transform: translate3d(0, -30px, 0);\n  }\n  70% {\n    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    -webkit-transform: translate3d(0, -15px, 0);\n    transform: translate3d(0, -15px, 0);\n  }\n  90% {\n    -webkit-transform: translate3d(0, -4px, 0);\n    transform: translate3d(0, -4px, 0);\n  }\n}\n.bounce {\n  -webkit-animation-name: bounce;\n  animation-name: bounce;\n  -webkit-transform-origin: center bottom;\n  transform-origin: center bottom;\n}\n@-webkit-keyframes flash {\n  0%,\n  50%,\n  100% {\n    opacity: 1;\n  }\n  25%,\n  75% {\n    opacity: 0;\n  }\n}\n@keyframes flash {\n  0%,\n  50%,\n  100% {\n    opacity: 1;\n  }\n  25%,\n  75% {\n    opacity: 0;\n  }\n}\n.flash {\n  -webkit-animation-name: flash;\n  animation-name: flash;\n}\n/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */\n@-webkit-keyframes pulse {\n  0% {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n  50% {\n    -webkit-transform: scale3d(1.05, 1.05, 1.05);\n    transform: scale3d(1.05, 1.05, 1.05);\n  }\n  100% {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n}\n@keyframes pulse {\n  0% {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n  50% {\n    -webkit-transform: scale3d(1.05, 1.05, 1.05);\n    transform: scale3d(1.05, 1.05, 1.05);\n  }\n  100% {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n}\n.pulse {\n  -webkit-animation-name: pulse;\n  animation-name: pulse;\n}\n@-webkit-keyframes rubberBand {\n  0% {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n  30% {\n    -webkit-transform: scale3d(1.25, 0.75, 1);\n    transform: scale3d(1.25, 0.75, 1);\n  }\n  40% {\n    -webkit-transform: scale3d(0.75, 1.25, 1);\n    transform: scale3d(0.75, 1.25, 1);\n  }\n  50% {\n    -webkit-transform: scale3d(1.15, 0.85, 1);\n    transform: scale3d(1.15, 0.85, 1);\n  }\n  65% {\n    -webkit-transform: scale3d(0.95, 1.05, 1);\n    transform: scale3d(0.95, 1.05, 1);\n  }\n  75% {\n    -webkit-transform: scale3d(1.05, 0.95, 1);\n    transform: scale3d(1.05, 0.95, 1);\n  }\n  100% {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n}\n@keyframes rubberBand {\n  0% {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n  30% {\n    -webkit-transform: scale3d(1.25, 0.75, 1);\n    transform: scale3d(1.25, 0.75, 1);\n  }\n  40% {\n    -webkit-transform: scale3d(0.75, 1.25, 1);\n    transform: scale3d(0.75, 1.25, 1);\n  }\n  50% {\n    -webkit-transform: scale3d(1.15, 0.85, 1);\n    transform: scale3d(1.15, 0.85, 1);\n  }\n  65% {\n    -webkit-transform: scale3d(0.95, 1.05, 1);\n    transform: scale3d(0.95, 1.05, 1);\n  }\n  75% {\n    -webkit-transform: scale3d(1.05, 0.95, 1);\n    transform: scale3d(1.05, 0.95, 1);\n  }\n  100% {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n}\n.rubberBand {\n  -webkit-animation-name: rubberBand;\n  animation-name: rubberBand;\n}\n@-webkit-keyframes shake {\n  0%,\n  100% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n  10%,\n  30%,\n  50%,\n  70%,\n  90% {\n    -webkit-transform: translate3d(-10px, 0, 0);\n    transform: translate3d(-10px, 0, 0);\n  }\n  20%,\n  40%,\n  60%,\n  80% {\n    -webkit-transform: translate3d(10px, 0, 0);\n    transform: translate3d(10px, 0, 0);\n  }\n}\n@keyframes shake {\n  0%,\n  100% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n  10%,\n  30%,\n  50%,\n  70%,\n  90% {\n    -webkit-transform: translate3d(-10px, 0, 0);\n    transform: translate3d(-10px, 0, 0);\n  }\n  20%,\n  40%,\n  60%,\n  80% {\n    -webkit-transform: translate3d(10px, 0, 0);\n    transform: translate3d(10px, 0, 0);\n  }\n}\n.shake {\n  -webkit-animation-name: shake;\n  animation-name: shake;\n}\n@-webkit-keyframes swing {\n  20% {\n    -webkit-transform: rotate3d(0, 0, 1, 15deg);\n    transform: rotate3d(0, 0, 1, 15deg);\n  }\n  40% {\n    -webkit-transform: rotate3d(0, 0, 1, -10deg);\n    transform: rotate3d(0, 0, 1, -10deg);\n  }\n  60% {\n    -webkit-transform: rotate3d(0, 0, 1, 5deg);\n    transform: rotate3d(0, 0, 1, 5deg);\n  }\n  80% {\n    -webkit-transform: rotate3d(0, 0, 1, -5deg);\n    transform: rotate3d(0, 0, 1, -5deg);\n  }\n  100% {\n    -webkit-transform: rotate3d(0, 0, 1, 0deg);\n    transform: rotate3d(0, 0, 1, 0deg);\n  }\n}\n@keyframes swing {\n  20% {\n    -webkit-transform: rotate3d(0, 0, 1, 15deg);\n    transform: rotate3d(0, 0, 1, 15deg);\n  }\n  40% {\n    -webkit-transform: rotate3d(0, 0, 1, -10deg);\n    transform: rotate3d(0, 0, 1, -10deg);\n  }\n  60% {\n    -webkit-transform: rotate3d(0, 0, 1, 5deg);\n    transform: rotate3d(0, 0, 1, 5deg);\n  }\n  80% {\n    -webkit-transform: rotate3d(0, 0, 1, -5deg);\n    transform: rotate3d(0, 0, 1, -5deg);\n  }\n  100% {\n    -webkit-transform: rotate3d(0, 0, 1, 0deg);\n    transform: rotate3d(0, 0, 1, 0deg);\n  }\n}\n.swing {\n  -webkit-transform-origin: top center;\n  transform-origin: top center;\n  -webkit-animation-name: swing;\n  animation-name: swing;\n}\n@-webkit-keyframes tada {\n  0% {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n  10%,\n  20% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);\n    transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);\n  }\n  30%,\n  50%,\n  70%,\n  90% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);\n    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);\n  }\n  40%,\n  60%,\n  80% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);\n    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);\n  }\n  100% {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n}\n@keyframes tada {\n  0% {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n  10%,\n  20% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);\n    transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);\n  }\n  30%,\n  50%,\n  70%,\n  90% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);\n    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);\n  }\n  40%,\n  60%,\n  80% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);\n    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);\n  }\n  100% {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n}\n.tada {\n  -webkit-animation-name: tada;\n  animation-name: tada;\n}\n/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */\n@-webkit-keyframes wobble {\n  0% {\n    -webkit-transform: none;\n    transform: none;\n  }\n  15% {\n    -webkit-transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg);\n    transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg);\n  }\n  30% {\n    -webkit-transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg);\n    transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg);\n  }\n  45% {\n    -webkit-transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg);\n    transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg);\n  }\n  60% {\n    -webkit-transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg);\n    transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg);\n  }\n  75% {\n    -webkit-transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg);\n    transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg);\n  }\n  100% {\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n@keyframes wobble {\n  0% {\n    -webkit-transform: none;\n    transform: none;\n  }\n  15% {\n    -webkit-transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg);\n    transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg);\n  }\n  30% {\n    -webkit-transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg);\n    transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg);\n  }\n  45% {\n    -webkit-transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg);\n    transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg);\n  }\n  60% {\n    -webkit-transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg);\n    transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg);\n  }\n  75% {\n    -webkit-transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg);\n    transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg);\n  }\n  100% {\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n.wobble {\n  -webkit-animation-name: wobble;\n  animation-name: wobble;\n}\n@-webkit-keyframes jello {\n  11.1% {\n    -webkit-transform: none;\n    transform: none;\n  }\n  22.2% {\n    -webkit-transform: skewX(-12.5deg) skewY(-12.5deg);\n    transform: skewX(-12.5deg) skewY(-12.5deg);\n  }\n  33.3% {\n    -webkit-transform: skewX(6.25deg) skewY(6.25deg);\n    transform: skewX(6.25deg) skewY(6.25deg);\n  }\n  44.4% {\n    -webkit-transform: skewX(-3.125deg) skewY(-3.125deg);\n    transform: skewX(-3.125deg) skewY(-3.125deg);\n  }\n  55.5% {\n    -webkit-transform: skewX(1.5625deg) skewY(1.5625deg);\n    transform: skewX(1.5625deg) skewY(1.5625deg);\n  }\n  66.6% {\n    -webkit-transform: skewX(-0.78125deg) skewY(-0.78125deg);\n    transform: skewX(-0.78125deg) skewY(-0.78125deg);\n  }\n  77.7% {\n    -webkit-transform: skewX(0.390625deg) skewY(0.390625deg);\n    transform: skewX(0.390625deg) skewY(0.390625deg);\n  }\n  88.8% {\n    -webkit-transform: skewX(-0.1953125deg) skewY(-0.1953125deg);\n    transform: skewX(-0.1953125deg) skewY(-0.1953125deg);\n  }\n  100% {\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n@keyframes jello {\n  11.1% {\n    -webkit-transform: none;\n    transform: none;\n  }\n  22.2% {\n    -webkit-transform: skewX(-12.5deg) skewY(-12.5deg);\n    transform: skewX(-12.5deg) skewY(-12.5deg);\n  }\n  33.3% {\n    -webkit-transform: skewX(6.25deg) skewY(6.25deg);\n    transform: skewX(6.25deg) skewY(6.25deg);\n  }\n  44.4% {\n    -webkit-transform: skewX(-3.125deg) skewY(-3.125deg);\n    transform: skewX(-3.125deg) skewY(-3.125deg);\n  }\n  55.5% {\n    -webkit-transform: skewX(1.5625deg) skewY(1.5625deg);\n    transform: skewX(1.5625deg) skewY(1.5625deg);\n  }\n  66.6% {\n    -webkit-transform: skewX(-0.78125deg) skewY(-0.78125deg);\n    transform: skewX(-0.78125deg) skewY(-0.78125deg);\n  }\n  77.7% {\n    -webkit-transform: skewX(0.390625deg) skewY(0.390625deg);\n    transform: skewX(0.390625deg) skewY(0.390625deg);\n  }\n  88.8% {\n    -webkit-transform: skewX(-0.1953125deg) skewY(-0.1953125deg);\n    transform: skewX(-0.1953125deg) skewY(-0.1953125deg);\n  }\n  100% {\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n.jello {\n  -webkit-animation-name: jello;\n  animation-name: jello;\n  -webkit-transform-origin: center;\n  transform-origin: center;\n}\n@-webkit-keyframes bounceIn {\n  0%,\n  20%,\n  40%,\n  60%,\n  80%,\n  100% {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3);\n  }\n  20% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\n    transform: scale3d(1.1, 1.1, 1.1);\n  }\n  40% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\n    transform: scale3d(0.9, 0.9, 0.9);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(1.03, 1.03, 1.03);\n    transform: scale3d(1.03, 1.03, 1.03);\n  }\n  80% {\n    -webkit-transform: scale3d(0.97, 0.97, 0.97);\n    transform: scale3d(0.97, 0.97, 0.97);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n}\n@keyframes bounceIn {\n  0%,\n  20%,\n  40%,\n  60%,\n  80%,\n  100% {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3);\n  }\n  20% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\n    transform: scale3d(1.1, 1.1, 1.1);\n  }\n  40% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\n    transform: scale3d(0.9, 0.9, 0.9);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(1.03, 1.03, 1.03);\n    transform: scale3d(1.03, 1.03, 1.03);\n  }\n  80% {\n    -webkit-transform: scale3d(0.97, 0.97, 0.97);\n    transform: scale3d(0.97, 0.97, 0.97);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n}\n.bounceIn {\n  -webkit-animation-name: bounceIn;\n  animation-name: bounceIn;\n}\n@-webkit-keyframes bounceInDown {\n  0%,\n  60%,\n  75%,\n  90%,\n  100% {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -3000px, 0);\n    transform: translate3d(0, -3000px, 0);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, 25px, 0);\n    transform: translate3d(0, 25px, 0);\n  }\n  75% {\n    -webkit-transform: translate3d(0, -10px, 0);\n    transform: translate3d(0, -10px, 0);\n  }\n  90% {\n    -webkit-transform: translate3d(0, 5px, 0);\n    transform: translate3d(0, 5px, 0);\n  }\n  100% {\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n@keyframes bounceInDown {\n  0%,\n  60%,\n  75%,\n  90%,\n  100% {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -3000px, 0);\n    transform: translate3d(0, -3000px, 0);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, 25px, 0);\n    transform: translate3d(0, 25px, 0);\n  }\n  75% {\n    -webkit-transform: translate3d(0, -10px, 0);\n    transform: translate3d(0, -10px, 0);\n  }\n  90% {\n    -webkit-transform: translate3d(0, 5px, 0);\n    transform: translate3d(0, 5px, 0);\n  }\n  100% {\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n.bounceInDown {\n  -webkit-animation-name: bounceInDown;\n  animation-name: bounceInDown;\n}\n@-webkit-keyframes bounceInLeft {\n  0%,\n  60%,\n  75%,\n  90%,\n  100% {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(-3000px, 0, 0);\n    transform: translate3d(-3000px, 0, 0);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(25px, 0, 0);\n    transform: translate3d(25px, 0, 0);\n  }\n  75% {\n    -webkit-transform: translate3d(-10px, 0, 0);\n    transform: translate3d(-10px, 0, 0);\n  }\n  90% {\n    -webkit-transform: translate3d(5px, 0, 0);\n    transform: translate3d(5px, 0, 0);\n  }\n  100% {\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n@keyframes bounceInLeft {\n  0%,\n  60%,\n  75%,\n  90%,\n  100% {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(-3000px, 0, 0);\n    transform: translate3d(-3000px, 0, 0);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(25px, 0, 0);\n    transform: translate3d(25px, 0, 0);\n  }\n  75% {\n    -webkit-transform: translate3d(-10px, 0, 0);\n    transform: translate3d(-10px, 0, 0);\n  }\n  90% {\n    -webkit-transform: translate3d(5px, 0, 0);\n    transform: translate3d(5px, 0, 0);\n  }\n  100% {\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n.bounceInLeft {\n  -webkit-animation-name: bounceInLeft;\n  animation-name: bounceInLeft;\n}\n@-webkit-keyframes bounceInRight {\n  0%,\n  60%,\n  75%,\n  90%,\n  100% {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(3000px, 0, 0);\n    transform: translate3d(3000px, 0, 0);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(-25px, 0, 0);\n    transform: translate3d(-25px, 0, 0);\n  }\n  75% {\n    -webkit-transform: translate3d(10px, 0, 0);\n    transform: translate3d(10px, 0, 0);\n  }\n  90% {\n    -webkit-transform: translate3d(-5px, 0, 0);\n    transform: translate3d(-5px, 0, 0);\n  }\n  100% {\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n@keyframes bounceInRight {\n  0%,\n  60%,\n  75%,\n  90%,\n  100% {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(3000px, 0, 0);\n    transform: translate3d(3000px, 0, 0);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(-25px, 0, 0);\n    transform: translate3d(-25px, 0, 0);\n  }\n  75% {\n    -webkit-transform: translate3d(10px, 0, 0);\n    transform: translate3d(10px, 0, 0);\n  }\n  90% {\n    -webkit-transform: translate3d(-5px, 0, 0);\n    transform: translate3d(-5px, 0, 0);\n  }\n  100% {\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n.bounceInRight {\n  -webkit-animation-name: bounceInRight;\n  animation-name: bounceInRight;\n}\n@-webkit-keyframes bounceInUp {\n  0%,\n  60%,\n  75%,\n  90%,\n  100% {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 3000px, 0);\n    transform: translate3d(0, 3000px, 0);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, -20px, 0);\n    transform: translate3d(0, -20px, 0);\n  }\n  75% {\n    -webkit-transform: translate3d(0, 10px, 0);\n    transform: translate3d(0, 10px, 0);\n  }\n  90% {\n    -webkit-transform: translate3d(0, -5px, 0);\n    transform: translate3d(0, -5px, 0);\n  }\n  100% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n}\n@keyframes bounceInUp {\n  0%,\n  60%,\n  75%,\n  90%,\n  100% {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 3000px, 0);\n    transform: translate3d(0, 3000px, 0);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, -20px, 0);\n    transform: translate3d(0, -20px, 0);\n  }\n  75% {\n    -webkit-transform: translate3d(0, 10px, 0);\n    transform: translate3d(0, 10px, 0);\n  }\n  90% {\n    -webkit-transform: translate3d(0, -5px, 0);\n    transform: translate3d(0, -5px, 0);\n  }\n  100% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n}\n.bounceInUp {\n  -webkit-animation-name: bounceInUp;\n  animation-name: bounceInUp;\n}\n@-webkit-keyframes bounceOut {\n  20% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\n    transform: scale3d(0.9, 0.9, 0.9);\n  }\n  50%,\n  55% {\n    opacity: 1;\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\n    transform: scale3d(1.1, 1.1, 1.1);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3);\n  }\n}\n@keyframes bounceOut {\n  20% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\n    transform: scale3d(0.9, 0.9, 0.9);\n  }\n  50%,\n  55% {\n    opacity: 1;\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\n    transform: scale3d(1.1, 1.1, 1.1);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3);\n  }\n}\n.bounceOut {\n  -webkit-animation-name: bounceOut;\n  animation-name: bounceOut;\n}\n@-webkit-keyframes bounceOutDown {\n  20% {\n    -webkit-transform: translate3d(0, 10px, 0);\n    transform: translate3d(0, 10px, 0);\n  }\n  40%,\n  45% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, -20px, 0);\n    transform: translate3d(0, -20px, 0);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 2000px, 0);\n    transform: translate3d(0, 2000px, 0);\n  }\n}\n@keyframes bounceOutDown {\n  20% {\n    -webkit-transform: translate3d(0, 10px, 0);\n    transform: translate3d(0, 10px, 0);\n  }\n  40%,\n  45% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, -20px, 0);\n    transform: translate3d(0, -20px, 0);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 2000px, 0);\n    transform: translate3d(0, 2000px, 0);\n  }\n}\n.bounceOutDown {\n  -webkit-animation-name: bounceOutDown;\n  animation-name: bounceOutDown;\n}\n@-webkit-keyframes bounceOutLeft {\n  20% {\n    opacity: 1;\n    -webkit-transform: translate3d(20px, 0, 0);\n    transform: translate3d(20px, 0, 0);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(-2000px, 0, 0);\n    transform: translate3d(-2000px, 0, 0);\n  }\n}\n@keyframes bounceOutLeft {\n  20% {\n    opacity: 1;\n    -webkit-transform: translate3d(20px, 0, 0);\n    transform: translate3d(20px, 0, 0);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(-2000px, 0, 0);\n    transform: translate3d(-2000px, 0, 0);\n  }\n}\n.bounceOutLeft {\n  -webkit-animation-name: bounceOutLeft;\n  animation-name: bounceOutLeft;\n}\n@-webkit-keyframes bounceOutRight {\n  20% {\n    opacity: 1;\n    -webkit-transform: translate3d(-20px, 0, 0);\n    transform: translate3d(-20px, 0, 0);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(2000px, 0, 0);\n    transform: translate3d(2000px, 0, 0);\n  }\n}\n@keyframes bounceOutRight {\n  20% {\n    opacity: 1;\n    -webkit-transform: translate3d(-20px, 0, 0);\n    transform: translate3d(-20px, 0, 0);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(2000px, 0, 0);\n    transform: translate3d(2000px, 0, 0);\n  }\n}\n.bounceOutRight {\n  -webkit-animation-name: bounceOutRight;\n  animation-name: bounceOutRight;\n}\n@-webkit-keyframes bounceOutUp {\n  20% {\n    -webkit-transform: translate3d(0, -10px, 0);\n    transform: translate3d(0, -10px, 0);\n  }\n  40%,\n  45% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, 20px, 0);\n    transform: translate3d(0, 20px, 0);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -2000px, 0);\n    transform: translate3d(0, -2000px, 0);\n  }\n}\n@keyframes bounceOutUp {\n  20% {\n    -webkit-transform: translate3d(0, -10px, 0);\n    transform: translate3d(0, -10px, 0);\n  }\n  40%,\n  45% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, 20px, 0);\n    transform: translate3d(0, 20px, 0);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -2000px, 0);\n    transform: translate3d(0, -2000px, 0);\n  }\n}\n.bounceOutUp {\n  -webkit-animation-name: bounceOutUp;\n  animation-name: bounceOutUp;\n}\n@-webkit-keyframes fadeIn {\n  0% {\n    opacity: 0;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n@keyframes fadeIn {\n  0% {\n    opacity: 0;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n.fadeIn {\n  -webkit-animation-name: fadeIn;\n  animation-name: fadeIn;\n}\n@-webkit-keyframes fadeInDown {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n@keyframes fadeInDown {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n.fadeInDown {\n  -webkit-animation-name: fadeInDown;\n  animation-name: fadeInDown;\n}\n@-webkit-keyframes fadeInDownBig {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -2000px, 0);\n    transform: translate3d(0, -2000px, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n@keyframes fadeInDownBig {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -2000px, 0);\n    transform: translate3d(0, -2000px, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n.fadeInDownBig {\n  -webkit-animation-name: fadeInDownBig;\n  animation-name: fadeInDownBig;\n}\n@-webkit-keyframes fadeInLeft {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n@keyframes fadeInLeft {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n.fadeInLeft {\n  -webkit-animation-name: fadeInLeft;\n  animation-name: fadeInLeft;\n}\n@-webkit-keyframes fadeInLeftBig {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(-2000px, 0, 0);\n    transform: translate3d(-2000px, 0, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n@keyframes fadeInLeftBig {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(-2000px, 0, 0);\n    transform: translate3d(-2000px, 0, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n.fadeInLeftBig {\n  -webkit-animation-name: fadeInLeftBig;\n  animation-name: fadeInLeftBig;\n}\n@-webkit-keyframes fadeInRight {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n@keyframes fadeInRight {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n.fadeInRight {\n  -webkit-animation-name: fadeInRight;\n  animation-name: fadeInRight;\n}\n@-webkit-keyframes fadeInRightBig {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(2000px, 0, 0);\n    transform: translate3d(2000px, 0, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n@keyframes fadeInRightBig {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(2000px, 0, 0);\n    transform: translate3d(2000px, 0, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n.fadeInRightBig {\n  -webkit-animation-name: fadeInRightBig;\n  animation-name: fadeInRightBig;\n}\n@-webkit-keyframes fadeInUp {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n@keyframes fadeInUp {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n.fadeInUp {\n  -webkit-animation-name: fadeInUp;\n  animation-name: fadeInUp;\n}\n@-webkit-keyframes fadeInUpBig {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 2000px, 0);\n    transform: translate3d(0, 2000px, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n@keyframes fadeInUpBig {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 2000px, 0);\n    transform: translate3d(0, 2000px, 0);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n.fadeInUpBig {\n  -webkit-animation-name: fadeInUpBig;\n  animation-name: fadeInUpBig;\n}\n@-webkit-keyframes fadeOut {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n  }\n}\n@keyframes fadeOut {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n  }\n}\n.fadeOut {\n  -webkit-animation-name: fadeOut;\n  animation-name: fadeOut;\n}\n@-webkit-keyframes fadeOutDown {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0);\n  }\n}\n@keyframes fadeOutDown {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0);\n  }\n}\n.fadeOutDown {\n  -webkit-animation-name: fadeOutDown;\n  animation-name: fadeOutDown;\n}\n@-webkit-keyframes fadeOutDownBig {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 2000px, 0);\n    transform: translate3d(0, 2000px, 0);\n  }\n}\n@keyframes fadeOutDownBig {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 2000px, 0);\n    transform: translate3d(0, 2000px, 0);\n  }\n}\n.fadeOutDownBig {\n  -webkit-animation-name: fadeOutDownBig;\n  animation-name: fadeOutDownBig;\n}\n@-webkit-keyframes fadeOutLeft {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0);\n  }\n}\n@keyframes fadeOutLeft {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0);\n  }\n}\n.fadeOutLeft {\n  -webkit-animation-name: fadeOutLeft;\n  animation-name: fadeOutLeft;\n}\n@-webkit-keyframes fadeOutLeftBig {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(-2000px, 0, 0);\n    transform: translate3d(-2000px, 0, 0);\n  }\n}\n@keyframes fadeOutLeftBig {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(-2000px, 0, 0);\n    transform: translate3d(-2000px, 0, 0);\n  }\n}\n.fadeOutLeftBig {\n  -webkit-animation-name: fadeOutLeftBig;\n  animation-name: fadeOutLeftBig;\n}\n@-webkit-keyframes fadeOutRight {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0);\n  }\n}\n@keyframes fadeOutRight {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0);\n  }\n}\n.fadeOutRight {\n  -webkit-animation-name: fadeOutRight;\n  animation-name: fadeOutRight;\n}\n@-webkit-keyframes fadeOutRightBig {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(2000px, 0, 0);\n    transform: translate3d(2000px, 0, 0);\n  }\n}\n@keyframes fadeOutRightBig {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(2000px, 0, 0);\n    transform: translate3d(2000px, 0, 0);\n  }\n}\n.fadeOutRightBig {\n  -webkit-animation-name: fadeOutRightBig;\n  animation-name: fadeOutRightBig;\n}\n@-webkit-keyframes fadeOutUp {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0);\n  }\n}\n@keyframes fadeOutUp {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0);\n  }\n}\n.fadeOutUp {\n  -webkit-animation-name: fadeOutUp;\n  animation-name: fadeOutUp;\n}\n@-webkit-keyframes fadeOutUpBig {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -2000px, 0);\n    transform: translate3d(0, -2000px, 0);\n  }\n}\n@keyframes fadeOutUpBig {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -2000px, 0);\n    transform: translate3d(0, -2000px, 0);\n  }\n}\n.fadeOutUpBig {\n  -webkit-animation-name: fadeOutUpBig;\n  animation-name: fadeOutUpBig;\n}\n@-webkit-keyframes flip {\n  0% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -360deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -360deg);\n    -webkit-animation-timing-function: ease-out;\n    animation-timing-function: ease-out;\n  }\n  40% {\n    -webkit-transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -190deg);\n    transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -190deg);\n    -webkit-animation-timing-function: ease-out;\n    animation-timing-function: ease-out;\n  }\n  50% {\n    -webkit-transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -170deg);\n    transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -170deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n  }\n  80% {\n    -webkit-transform: perspective(400px) scale3d(0.95, 0.95, 0.95);\n    transform: perspective(400px) scale3d(0.95, 0.95, 0.95);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n  }\n  100% {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n  }\n}\n@keyframes flip {\n  0% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -360deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -360deg);\n    -webkit-animation-timing-function: ease-out;\n    animation-timing-function: ease-out;\n  }\n  40% {\n    -webkit-transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -190deg);\n    transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -190deg);\n    -webkit-animation-timing-function: ease-out;\n    animation-timing-function: ease-out;\n  }\n  50% {\n    -webkit-transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -170deg);\n    transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -170deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n  }\n  80% {\n    -webkit-transform: perspective(400px) scale3d(0.95, 0.95, 0.95);\n    transform: perspective(400px) scale3d(0.95, 0.95, 0.95);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n  }\n  100% {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n  }\n}\n.animated.flip {\n  -webkit-backface-visibility: visible;\n  backface-visibility: visible;\n  -webkit-animation-name: flip;\n  animation-name: flip;\n}\n@-webkit-keyframes flipInX {\n  0% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n    opacity: 0;\n  }\n  40% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n  }\n  60% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 10deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, 10deg);\n    opacity: 1;\n  }\n  80% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -5deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, -5deg);\n  }\n  100% {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px);\n  }\n}\n@keyframes flipInX {\n  0% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n    opacity: 0;\n  }\n  40% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n  }\n  60% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 10deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, 10deg);\n    opacity: 1;\n  }\n  80% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -5deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, -5deg);\n  }\n  100% {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px);\n  }\n}\n.flipInX {\n  -webkit-backface-visibility: visible !important;\n  backface-visibility: visible !important;\n  -webkit-animation-name: flipInX;\n  animation-name: flipInX;\n}\n@-webkit-keyframes flipInY {\n  0% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n    opacity: 0;\n  }\n  40% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n  }\n  60% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\n    opacity: 1;\n  }\n  80% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -5deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -5deg);\n  }\n  100% {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px);\n  }\n}\n@keyframes flipInY {\n  0% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n    opacity: 0;\n  }\n  40% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n  }\n  60% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\n    opacity: 1;\n  }\n  80% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -5deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -5deg);\n  }\n  100% {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px);\n  }\n}\n.flipInY {\n  -webkit-backface-visibility: visible !important;\n  backface-visibility: visible !important;\n  -webkit-animation-name: flipInY;\n  animation-name: flipInY;\n}\n@-webkit-keyframes flipOutX {\n  0% {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px);\n  }\n  30% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    opacity: 0;\n  }\n}\n@keyframes flipOutX {\n  0% {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px);\n  }\n  30% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    opacity: 0;\n  }\n}\n.flipOutX {\n  -webkit-animation-name: flipOutX;\n  animation-name: flipOutX;\n  -webkit-backface-visibility: visible !important;\n  backface-visibility: visible !important;\n}\n@-webkit-keyframes flipOutY {\n  0% {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px);\n  }\n  30% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -15deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -15deg);\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    opacity: 0;\n  }\n}\n@keyframes flipOutY {\n  0% {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px);\n  }\n  30% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -15deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -15deg);\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    opacity: 0;\n  }\n}\n.flipOutY {\n  -webkit-backface-visibility: visible !important;\n  backface-visibility: visible !important;\n  -webkit-animation-name: flipOutY;\n  animation-name: flipOutY;\n}\n@-webkit-keyframes lightSpeedIn {\n  0% {\n    -webkit-transform: translate3d(100%, 0, 0) skewX(-30deg);\n    transform: translate3d(100%, 0, 0) skewX(-30deg);\n    opacity: 0;\n  }\n  60% {\n    -webkit-transform: skewX(20deg);\n    transform: skewX(20deg);\n    opacity: 1;\n  }\n  80% {\n    -webkit-transform: skewX(-5deg);\n    transform: skewX(-5deg);\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1;\n  }\n}\n@keyframes lightSpeedIn {\n  0% {\n    -webkit-transform: translate3d(100%, 0, 0) skewX(-30deg);\n    transform: translate3d(100%, 0, 0) skewX(-30deg);\n    opacity: 0;\n  }\n  60% {\n    -webkit-transform: skewX(20deg);\n    transform: skewX(20deg);\n    opacity: 1;\n  }\n  80% {\n    -webkit-transform: skewX(-5deg);\n    transform: skewX(-5deg);\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1;\n  }\n}\n.lightSpeedIn {\n  -webkit-animation-name: lightSpeedIn;\n  animation-name: lightSpeedIn;\n  -webkit-animation-timing-function: ease-out;\n  animation-timing-function: ease-out;\n}\n@-webkit-keyframes lightSpeedOut {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform: translate3d(100%, 0, 0) skewX(30deg);\n    transform: translate3d(100%, 0, 0) skewX(30deg);\n    opacity: 0;\n  }\n}\n@keyframes lightSpeedOut {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform: translate3d(100%, 0, 0) skewX(30deg);\n    transform: translate3d(100%, 0, 0) skewX(30deg);\n    opacity: 0;\n  }\n}\n.lightSpeedOut {\n  -webkit-animation-name: lightSpeedOut;\n  animation-name: lightSpeedOut;\n  -webkit-animation-timing-function: ease-in;\n  animation-timing-function: ease-in;\n}\n@-webkit-keyframes rotateIn {\n  0% {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    -webkit-transform: rotate3d(0, 0, 1, -200deg);\n    transform: rotate3d(0, 0, 1, -200deg);\n    opacity: 0;\n  }\n  100% {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1;\n  }\n}\n@keyframes rotateIn {\n  0% {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    -webkit-transform: rotate3d(0, 0, 1, -200deg);\n    transform: rotate3d(0, 0, 1, -200deg);\n    opacity: 0;\n  }\n  100% {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1;\n  }\n}\n.rotateIn {\n  -webkit-animation-name: rotateIn;\n  animation-name: rotateIn;\n}\n@-webkit-keyframes rotateInDownLeft {\n  0% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\n    transform: rotate3d(0, 0, 1, -45deg);\n    opacity: 0;\n  }\n  100% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1;\n  }\n}\n@keyframes rotateInDownLeft {\n  0% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\n    transform: rotate3d(0, 0, 1, -45deg);\n    opacity: 0;\n  }\n  100% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1;\n  }\n}\n.rotateInDownLeft {\n  -webkit-animation-name: rotateInDownLeft;\n  animation-name: rotateInDownLeft;\n}\n@-webkit-keyframes rotateInDownRight {\n  0% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\n    transform: rotate3d(0, 0, 1, 45deg);\n    opacity: 0;\n  }\n  100% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1;\n  }\n}\n@keyframes rotateInDownRight {\n  0% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\n    transform: rotate3d(0, 0, 1, 45deg);\n    opacity: 0;\n  }\n  100% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1;\n  }\n}\n.rotateInDownRight {\n  -webkit-animation-name: rotateInDownRight;\n  animation-name: rotateInDownRight;\n}\n@-webkit-keyframes rotateInUpLeft {\n  0% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\n    transform: rotate3d(0, 0, 1, 45deg);\n    opacity: 0;\n  }\n  100% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1;\n  }\n}\n@keyframes rotateInUpLeft {\n  0% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\n    transform: rotate3d(0, 0, 1, 45deg);\n    opacity: 0;\n  }\n  100% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1;\n  }\n}\n.rotateInUpLeft {\n  -webkit-animation-name: rotateInUpLeft;\n  animation-name: rotateInUpLeft;\n}\n@-webkit-keyframes rotateInUpRight {\n  0% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -90deg);\n    transform: rotate3d(0, 0, 1, -90deg);\n    opacity: 0;\n  }\n  100% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1;\n  }\n}\n@keyframes rotateInUpRight {\n  0% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -90deg);\n    transform: rotate3d(0, 0, 1, -90deg);\n    opacity: 0;\n  }\n  100% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1;\n  }\n}\n.rotateInUpRight {\n  -webkit-animation-name: rotateInUpRight;\n  animation-name: rotateInUpRight;\n}\n@-webkit-keyframes rotateOut {\n  0% {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    -webkit-transform: rotate3d(0, 0, 1, 200deg);\n    transform: rotate3d(0, 0, 1, 200deg);\n    opacity: 0;\n  }\n}\n@keyframes rotateOut {\n  0% {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    -webkit-transform: rotate3d(0, 0, 1, 200deg);\n    transform: rotate3d(0, 0, 1, 200deg);\n    opacity: 0;\n  }\n}\n.rotateOut {\n  -webkit-animation-name: rotateOut;\n  animation-name: rotateOut;\n}\n@-webkit-keyframes rotateOutDownLeft {\n  0% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\n    transform: rotate3d(0, 0, 1, 45deg);\n    opacity: 0;\n  }\n}\n@keyframes rotateOutDownLeft {\n  0% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\n    transform: rotate3d(0, 0, 1, 45deg);\n    opacity: 0;\n  }\n}\n.rotateOutDownLeft {\n  -webkit-animation-name: rotateOutDownLeft;\n  animation-name: rotateOutDownLeft;\n}\n@-webkit-keyframes rotateOutDownRight {\n  0% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\n    transform: rotate3d(0, 0, 1, -45deg);\n    opacity: 0;\n  }\n}\n@keyframes rotateOutDownRight {\n  0% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\n    transform: rotate3d(0, 0, 1, -45deg);\n    opacity: 0;\n  }\n}\n.rotateOutDownRight {\n  -webkit-animation-name: rotateOutDownRight;\n  animation-name: rotateOutDownRight;\n}\n@-webkit-keyframes rotateOutUpLeft {\n  0% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\n    transform: rotate3d(0, 0, 1, -45deg);\n    opacity: 0;\n  }\n}\n@keyframes rotateOutUpLeft {\n  0% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\n    transform: rotate3d(0, 0, 1, -45deg);\n    opacity: 0;\n  }\n}\n.rotateOutUpLeft {\n  -webkit-animation-name: rotateOutUpLeft;\n  animation-name: rotateOutUpLeft;\n}\n@-webkit-keyframes rotateOutUpRight {\n  0% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 90deg);\n    transform: rotate3d(0, 0, 1, 90deg);\n    opacity: 0;\n  }\n}\n@keyframes rotateOutUpRight {\n  0% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 90deg);\n    transform: rotate3d(0, 0, 1, 90deg);\n    opacity: 0;\n  }\n}\n.rotateOutUpRight {\n  -webkit-animation-name: rotateOutUpRight;\n  animation-name: rotateOutUpRight;\n}\n@-webkit-keyframes hinge {\n  0% {\n    -webkit-transform-origin: top left;\n    transform-origin: top left;\n    -webkit-animation-timing-function: ease-in-out;\n    animation-timing-function: ease-in-out;\n  }\n  20%,\n  60% {\n    -webkit-transform: rotate3d(0, 0, 1, 80deg);\n    transform: rotate3d(0, 0, 1, 80deg);\n    -webkit-transform-origin: top left;\n    transform-origin: top left;\n    -webkit-animation-timing-function: ease-in-out;\n    animation-timing-function: ease-in-out;\n  }\n  40%,\n  80% {\n    -webkit-transform: rotate3d(0, 0, 1, 60deg);\n    transform: rotate3d(0, 0, 1, 60deg);\n    -webkit-transform-origin: top left;\n    transform-origin: top left;\n    -webkit-animation-timing-function: ease-in-out;\n    animation-timing-function: ease-in-out;\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform: translate3d(0, 700px, 0);\n    transform: translate3d(0, 700px, 0);\n    opacity: 0;\n  }\n}\n@keyframes hinge {\n  0% {\n    -webkit-transform-origin: top left;\n    transform-origin: top left;\n    -webkit-animation-timing-function: ease-in-out;\n    animation-timing-function: ease-in-out;\n  }\n  20%,\n  60% {\n    -webkit-transform: rotate3d(0, 0, 1, 80deg);\n    transform: rotate3d(0, 0, 1, 80deg);\n    -webkit-transform-origin: top left;\n    transform-origin: top left;\n    -webkit-animation-timing-function: ease-in-out;\n    animation-timing-function: ease-in-out;\n  }\n  40%,\n  80% {\n    -webkit-transform: rotate3d(0, 0, 1, 60deg);\n    transform: rotate3d(0, 0, 1, 60deg);\n    -webkit-transform-origin: top left;\n    transform-origin: top left;\n    -webkit-animation-timing-function: ease-in-out;\n    animation-timing-function: ease-in-out;\n    opacity: 1;\n  }\n  100% {\n    -webkit-transform: translate3d(0, 700px, 0);\n    transform: translate3d(0, 700px, 0);\n    opacity: 0;\n  }\n}\n.hinge {\n  -webkit-animation-name: hinge;\n  animation-name: hinge;\n}\n/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */\n@-webkit-keyframes rollIn {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);\n    transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n@keyframes rollIn {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);\n    transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);\n  }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none;\n  }\n}\n.rollIn {\n  -webkit-animation-name: rollIn;\n  animation-name: rollIn;\n}\n/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */\n@-webkit-keyframes rollOut {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);\n    transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);\n  }\n}\n@keyframes rollOut {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);\n    transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);\n  }\n}\n.rollOut {\n  -webkit-animation-name: rollOut;\n  animation-name: rollOut;\n}\n@-webkit-keyframes zoomIn {\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3);\n  }\n  50% {\n    opacity: 1;\n  }\n}\n@keyframes zoomIn {\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3);\n  }\n  50% {\n    opacity: 1;\n  }\n}\n.zoomIn {\n  -webkit-animation-name: zoomIn;\n  animation-name: zoomIn;\n}\n@-webkit-keyframes zoomInDown {\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n  }\n}\n@keyframes zoomInDown {\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n  }\n}\n.zoomInDown {\n  -webkit-animation-name: zoomInDown;\n  animation-name: zoomInDown;\n}\n@-webkit-keyframes zoomInLeft {\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n  }\n}\n@keyframes zoomInLeft {\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n  }\n}\n.zoomInLeft {\n  -webkit-animation-name: zoomInLeft;\n  animation-name: zoomInLeft;\n}\n@-webkit-keyframes zoomInRight {\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n  }\n}\n@keyframes zoomInRight {\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n  }\n}\n.zoomInRight {\n  -webkit-animation-name: zoomInRight;\n  animation-name: zoomInRight;\n}\n@-webkit-keyframes zoomInUp {\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n  }\n}\n@keyframes zoomInUp {\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n  }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n  }\n}\n.zoomInUp {\n  -webkit-animation-name: zoomInUp;\n  animation-name: zoomInUp;\n}\n@-webkit-keyframes zoomOut {\n  0% {\n    opacity: 1;\n  }\n  50% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3);\n  }\n  100% {\n    opacity: 0;\n  }\n}\n@keyframes zoomOut {\n  0% {\n    opacity: 1;\n  }\n  50% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3);\n  }\n  100% {\n    opacity: 0;\n  }\n}\n.zoomOut {\n  -webkit-animation-name: zoomOut;\n  animation-name: zoomOut;\n}\n@-webkit-keyframes zoomOutDown {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);\n    -webkit-transform-origin: center bottom;\n    transform-origin: center bottom;\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n  }\n}\n@keyframes zoomOutDown {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);\n    -webkit-transform-origin: center bottom;\n    transform-origin: center bottom;\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n  }\n}\n.zoomOutDown {\n  -webkit-animation-name: zoomOutDown;\n  animation-name: zoomOutDown;\n}\n@-webkit-keyframes zoomOutLeft {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: scale(0.1) translate3d(-2000px, 0, 0);\n    transform: scale(0.1) translate3d(-2000px, 0, 0);\n    -webkit-transform-origin: left center;\n    transform-origin: left center;\n  }\n}\n@keyframes zoomOutLeft {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: scale(0.1) translate3d(-2000px, 0, 0);\n    transform: scale(0.1) translate3d(-2000px, 0, 0);\n    -webkit-transform-origin: left center;\n    transform-origin: left center;\n  }\n}\n.zoomOutLeft {\n  -webkit-animation-name: zoomOutLeft;\n  animation-name: zoomOutLeft;\n}\n@-webkit-keyframes zoomOutRight {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: scale(0.1) translate3d(2000px, 0, 0);\n    transform: scale(0.1) translate3d(2000px, 0, 0);\n    -webkit-transform-origin: right center;\n    transform-origin: right center;\n  }\n}\n@keyframes zoomOutRight {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: scale(0.1) translate3d(2000px, 0, 0);\n    transform: scale(0.1) translate3d(2000px, 0, 0);\n    -webkit-transform-origin: right center;\n    transform-origin: right center;\n  }\n}\n.zoomOutRight {\n  -webkit-animation-name: zoomOutRight;\n  animation-name: zoomOutRight;\n}\n@-webkit-keyframes zoomOutUp {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);\n    -webkit-transform-origin: center bottom;\n    transform-origin: center bottom;\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n  }\n}\n@keyframes zoomOutUp {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n  }\n  100% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);\n    -webkit-transform-origin: center bottom;\n    transform-origin: center bottom;\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n  }\n}\n.zoomOutUp {\n  -webkit-animation-name: zoomOutUp;\n  animation-name: zoomOutUp;\n}\n@-webkit-keyframes slideInDown {\n  0% {\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0);\n    visibility: visible;\n  }\n  100% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n}\n@keyframes slideInDown {\n  0% {\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0);\n    visibility: visible;\n  }\n  100% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n}\n.slideInDown {\n  -webkit-animation-name: slideInDown;\n  animation-name: slideInDown;\n}\n@-webkit-keyframes slideInLeft {\n  0% {\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0);\n    visibility: visible;\n  }\n  100% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n}\n@keyframes slideInLeft {\n  0% {\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0);\n    visibility: visible;\n  }\n  100% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n}\n.slideInLeft {\n  -webkit-animation-name: slideInLeft;\n  animation-name: slideInLeft;\n}\n@-webkit-keyframes slideInRight {\n  0% {\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0);\n    visibility: visible;\n  }\n  100% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n}\n@keyframes slideInRight {\n  0% {\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0);\n    visibility: visible;\n  }\n  100% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n}\n.slideInRight {\n  -webkit-animation-name: slideInRight;\n  animation-name: slideInRight;\n}\n@-webkit-keyframes slideInUp {\n  0% {\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0);\n    visibility: visible;\n  }\n  100% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n}\n@keyframes slideInUp {\n  0% {\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0);\n    visibility: visible;\n  }\n  100% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n}\n.slideInUp {\n  -webkit-animation-name: slideInUp;\n  animation-name: slideInUp;\n}\n@-webkit-keyframes slideOutDown {\n  0% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n  100% {\n    visibility: hidden;\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0);\n  }\n}\n@keyframes slideOutDown {\n  0% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n  100% {\n    visibility: hidden;\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0);\n  }\n}\n.slideOutDown {\n  -webkit-animation-name: slideOutDown;\n  animation-name: slideOutDown;\n}\n@-webkit-keyframes slideOutLeft {\n  0% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n  100% {\n    visibility: hidden;\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0);\n  }\n}\n@keyframes slideOutLeft {\n  0% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n  100% {\n    visibility: hidden;\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0);\n  }\n}\n.slideOutLeft {\n  -webkit-animation-name: slideOutLeft;\n  animation-name: slideOutLeft;\n}\n@-webkit-keyframes slideOutRight {\n  0% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n  100% {\n    visibility: hidden;\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0);\n  }\n}\n@keyframes slideOutRight {\n  0% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n  100% {\n    visibility: hidden;\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0);\n  }\n}\n.slideOutRight {\n  -webkit-animation-name: slideOutRight;\n  animation-name: slideOutRight;\n}\n@-webkit-keyframes slideOutUp {\n  0% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n  100% {\n    visibility: hidden;\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0);\n  }\n}\n@keyframes slideOutUp {\n  0% {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0);\n  }\n  100% {\n    visibility: hidden;\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0);\n  }\n}\n.slideOutUp {\n  -webkit-animation-name: slideOutUp;\n  animation-name: slideOutUp;\n}\n/*!\n * Waves v0.7.2\n * http://fian.my.id/Waves \n * \n * Copyright 2014 Alfiana E. Sibuea and other contributors \n * Released under the MIT license \n * https://github.com/fians/Waves/blob/master/LICENSE \n */\n.waves-effect {\n  position: relative;\n  cursor: pointer;\n  display: inline-block;\n  overflow: hidden;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  -webkit-tap-highlight-color: transparent;\n}\n.waves-effect .waves-ripple {\n  position: absolute;\n  border-radius: 50%;\n  width: 100px;\n  height: 100px;\n  margin-top: -50px;\n  margin-left: -50px;\n  opacity: 0;\n  background: rgba(0, 0, 0, 0.2);\n  background: -webkit-radial-gradient(rgba(0, 0, 0, 0.2) 0, rgba(0, 0, 0, 0.3) 40%, rgba(0, 0, 0, 0.4) 50%, rgba(0, 0, 0, 0.5) 60%, rgba(255, 255, 255, 0) 70%);\n  background: -o-radial-gradient(rgba(0, 0, 0, 0.2) 0, rgba(0, 0, 0, 0.3) 40%, rgba(0, 0, 0, 0.4) 50%, rgba(0, 0, 0, 0.5) 60%, rgba(255, 255, 255, 0) 70%);\n  background: -moz-radial-gradient(rgba(0, 0, 0, 0.2) 0, rgba(0, 0, 0, 0.3) 40%, rgba(0, 0, 0, 0.4) 50%, rgba(0, 0, 0, 0.5) 60%, rgba(255, 255, 255, 0) 70%);\n  background: radial-gradient(rgba(0, 0, 0, 0.2) 0, rgba(0, 0, 0, 0.3) 40%, rgba(0, 0, 0, 0.4) 50%, rgba(0, 0, 0, 0.5) 60%, rgba(255, 255, 255, 0) 70%);\n  -webkit-transition: all 0.5s ease-out;\n  -moz-transition: all 0.5s ease-out;\n  -o-transition: all 0.5s ease-out;\n  transition: all 0.5s ease-out;\n  -webkit-transition-property: -webkit-transform, opacity;\n  -moz-transition-property: -moz-transform, opacity;\n  -o-transition-property: -o-transform, opacity;\n  transition-property: transform, opacity;\n  -webkit-transform: scale(0) translate(0, 0);\n  -moz-transform: scale(0) translate(0, 0);\n  -ms-transform: scale(0) translate(0, 0);\n  -o-transform: scale(0) translate(0, 0);\n  transform: scale(0) translate(0, 0);\n  pointer-events: none;\n}\n.waves-effect.waves-light .waves-ripple {\n  background: rgba(255, 255, 255, 0.4);\n  background: -webkit-radial-gradient(rgba(255, 255, 255, 0.2) 0, rgba(255, 255, 255, 0.3) 40%, rgba(255, 255, 255, 0.4) 50%, rgba(255, 255, 255, 0.5) 60%, rgba(255, 255, 255, 0) 70%);\n  background: -o-radial-gradient(rgba(255, 255, 255, 0.2) 0, rgba(255, 255, 255, 0.3) 40%, rgba(255, 255, 255, 0.4) 50%, rgba(255, 255, 255, 0.5) 60%, rgba(255, 255, 255, 0) 70%);\n  background: -moz-radial-gradient(rgba(255, 255, 255, 0.2) 0, rgba(255, 255, 255, 0.3) 40%, rgba(255, 255, 255, 0.4) 50%, rgba(255, 255, 255, 0.5) 60%, rgba(255, 255, 255, 0) 70%);\n  background: radial-gradient(rgba(255, 255, 255, 0.2) 0, rgba(255, 255, 255, 0.3) 40%, rgba(255, 255, 255, 0.4) 50%, rgba(255, 255, 255, 0.5) 60%, rgba(255, 255, 255, 0) 70%);\n}\n.waves-effect.waves-classic .waves-ripple {\n  background: rgba(0, 0, 0, 0.2);\n}\n.waves-effect.waves-classic.waves-light .waves-ripple {\n  background: rgba(255, 255, 255, 0.4);\n}\n.waves-notransition {\n  -webkit-transition: none !important;\n  -moz-transition: none !important;\n  -o-transition: none !important;\n  transition: none !important;\n}\n.waves-button,\n.waves-circle {\n  -webkit-transform: translateZ(0);\n  -moz-transform: translateZ(0);\n  -ms-transform: translateZ(0);\n  -o-transform: translateZ(0);\n  transform: translateZ(0);\n  -webkit-mask-image: -webkit-radial-gradient(circle, white 100%, black 100%);\n}\n.waves-button,\n.waves-button:hover,\n.waves-button:visited,\n.waves-button-input {\n  white-space: nowrap;\n  vertical-align: middle;\n  cursor: pointer;\n  border: none;\n  outline: none;\n  color: inherit;\n  background-color: rgba(0, 0, 0, 0);\n  font-size: 1em;\n  line-height: 1em;\n  text-align: center;\n  text-decoration: none;\n  z-index: 1;\n}\n.waves-button {\n  padding: 0.85em 1.1em;\n  border-radius: 0.2em;\n}\n.waves-button-input {\n  margin: 0;\n  padding: 0.85em 1.1em;\n}\n.waves-input-wrapper {\n  border-radius: 0.2em;\n  vertical-align: bottom;\n}\n.waves-input-wrapper.waves-button {\n  padding: 0;\n}\n.waves-input-wrapper .waves-button-input {\n  position: relative;\n  top: 0;\n  left: 0;\n  z-index: 1;\n}\n.waves-circle {\n  text-align: center;\n  width: 2.5em;\n  height: 2.5em;\n  line-height: 2.5em;\n  border-radius: 50%;\n}\n.waves-float {\n  -webkit-mask-image: none;\n  -webkit-box-shadow: 0px 1px 1.5px 1px rgba(0, 0, 0, 0.12);\n  box-shadow: 0px 1px 1.5px 1px rgba(0, 0, 0, 0.12);\n  -webkit-transition: all 300ms;\n  -moz-transition: all 300ms;\n  -o-transition: all 300ms;\n  transition: all 300ms;\n}\n.waves-float:active {\n  -webkit-box-shadow: 0px 8px 20px 1px rgba(0, 0, 0, 0.3);\n  box-shadow: 0px 8px 20px 1px rgba(0, 0, 0, 0.3);\n}\n.waves-block {\n  display: block;\n}\n/* Firefox Bug: link not triggered */\na.waves-effect .waves-ripple {\n  z-index: -1;\n}\n.badge {\n  display: inline-block;\n  min-width: 10px;\n  padding: 3px 7px;\n  font-size: 70%;\n  font-weight: 300;\n  line-height: 1;\n  text-align: center;\n  white-space: nowrap;\n  vertical-align: baseline;\n  border-radius: 10px;\n}\n.badge:empty {\n  display: none;\n}\n.badge * {\n  vertical-align: middle;\n}\n.tooltip {\n  position: absolute;\n  min-width: 50px;\n  max-width: 100%;\n  display: block;\n  z-index: 21010;\n  padding: 0.6em 1em;\n  border-radius: 2px;\n  display: none;\n}\n.tooltip.default {\n  background-color: rgba(0, 0, 0, 0.95);\n  color: white;\n}\n.tooltip.nopad {\n  padding: 0;\n}\n.tooltip.fixed {\n  position: fixed;\n}\n.tooltip.activating {\n  position: fixed;\n  top: 0;\n  opacity: 0;\n  display: block;\n}\n.modal-back {\n  z-index: 1999;\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  opacity: 0.8;\n  display: none;\n}\n.modal-back.default {\n  background: black;\n}\n.modal-container {\n  overflow-y: auto;\n  z-index: 2000;\n  position: fixed;\n  top: 8%;\n  height: 84%;\n  left: 0;\n  padding: 10px 0;\n  width: 100%;\n  display: none;\n}\n.modal-container .modal {\n  display: none;\n  margin: auto;\n  background-color: white;\n  border-radius: 2px;\n}\n.modal-container .modal .modal-content {\n  padding: 1.3em;\n  margin: auto;\n}\n.modal-container .modal .modal-footer {\n  padding: 0.4em 1.3em;\n  min-height: 56px;\n  width: 100%;\n  border-top: solid 1px #eaeaea;\n}\nbody.modal-opened {\n  overflow: hidden;\n}\nbody.modal-opened .modal-container {\n  /*display:block;*/\n}\n.parallax-container {\n  position: relative;\n  overflow: hidden;\n  height: 500px;\n}\n.parallax-container .parallax {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  z-index: -1;\n  height: 100%;\n}\n.parallax-container .parallax .img {\n  /*display: none;*/\n  position: relative;\n  background-position: center center;\n  background-size: cover;\n  width: 100%;\n  height: 170%;\n  top: -70%;\n  min-height: 640px;\n}\ninput {\n  outline: none;\n}\ninput:-ms-input-placeholder {\n  color: #808080;\n}\ninput:-moz-placeholder,\ninput::-webkit-input-placeholder {\n  color: #808080;\n}\ninput[type=checkbox],\ninput[type=radio] {\n  margin: 2px 0 0 ;\n  cursor: pointer;\n}\n.checkbox-inline label,\n.radio-inline label,\n.checkbox label,\n.radio label {\n  cursor: pointer;\n}\n.checkbox-inline,\n.radio-inline {\n  display: inline-block;\n}\n.input {\n  outline: none;\n  border: solid 1px #dcdcdc;\n  border: solid 1px rgba(128, 128, 128, 0.2);\n  border-radius: 2px;\n  padding: 0.34em 0.1em;\n  background-color: white;\n  width: 100%;\n  position: relative;\n  z-index: 2;\n}\n.input:disabled {\n  background-color: #F8F8F8;\n}\n.input::-ms-clear,\n.input::-ms-reveal {\n  display: none;\n}\n.input.sxl-control {\n  display: table-cell;\n}\n.input-group {\n  width: 100%;\n  display: table;\n  box-sizing: border-box;\n  border-spacing: border-collapse;\n}\n.input-group .control,\n.input-group .addon,\n.input-group .input-label,\n.input-group .has-feedback,\n.input-group .input-addon-button {\n  display: table-cell;\n  border-radius: 0;\n  vertical-align: middle;\n}\n.input-field {\n  position: relative;\n  margin-top: 1.5em;\n}\n.input-field .prefix {\n  text-align: center;\n  min-width: 1px;\n}\n.input-field .select-down {\n  position: absolute;\n  right: 4px;\n  top: 0.8em;\n  cursor: pointer;\n}\n.input-field .line {\n  position: absolute;\n  height: 2px;\n  width: 100%;\n  bottom: -2px;\n}\n.input-field:not(.active) .line {\n  background-color: #9e9e9e;\n  height: 1px;\n}\n.input-field textarea.vox-textarea {\n  padding-top: 0.5em;\n  min-height: 2.3em;\n  resize: none;\n}\n.input-field input[type=text],\n.input-field input[type=password],\n.input-field input[type=email],\n.input-field input[type=url],\n.input-field input[type=time],\n.input-field input[type=date],\n.input-field input[type=datetime-local],\n.input-field input[type=tel],\n.input-field input[type=number],\n.input-field input[type=search],\n.input-field textarea.vox-textarea,\n.input-field select {\n  position: relative;\n  z-index: 1;\n  background-color: transparent;\n  border: none;\n  border-bottom: 1px solid #9e9e9e;\n  border-bottom-style: none;\n  border-radius: 0;\n  outline: none;\n  height: 2.3em;\n  padding-left: 0;\n  width: 100%;\n  /* font-size: 1rem; */\n  /* padding: 0; */\n  box-shadow: none;\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box;\n  transition: all .3s;\n}\n.input-field input[type=text] ~ .action,\n.input-field input[type=password] ~ .action,\n.input-field input[type=email] ~ .action,\n.input-field input[type=url] ~ .action,\n.input-field input[type=time] ~ .action,\n.input-field input[type=date] ~ .action,\n.input-field input[type=datetime-local] ~ .action,\n.input-field input[type=tel] ~ .action,\n.input-field input[type=number] ~ .action,\n.input-field input[type=search] ~ .action,\n.input-field textarea.vox-textarea ~ .action,\n.input-field select ~ .action {\n  position: absolute;\n  top: 0;\n  right: 0.2em;\n  color: transparent;\n  cursor: pointer;\n  font-size: 2em;\n  transition: .3s color;\n}\n.input-field.prefixed input[type=text],\n.input-field.prefixed input[type=password],\n.input-field.prefixed input[type=email],\n.input-field.prefixed input[type=url],\n.input-field.prefixed input[type=time],\n.input-field.prefixed input[type=date],\n.input-field.prefixed input[type=datetime-local],\n.input-field.prefixed input[type=tel],\n.input-field.prefixed input[type=number],\n.input-field.prefixed input[type=search],\n.input-field.prefixed textarea.vox-textarea,\n.input-field.prefixed select {\n  padding-left: 0.6em;\n}\n.input-field label {\n  font-weight: 400;\n  position: absolute;\n  top: 0.5em;\n  left: 0;\n  z-index: 0;\n  cursor: text;\n  -webkit-transition: 0.2s ease-out;\n  -moz-transition: 0.2s ease-out;\n  -o-transition: 0.2s ease-out;\n  -ms-transition: 0.2s ease-out;\n  transition: 0.2s ease-out;\n  text-overflow: ellipsis;\n  overflow: hidden;\n  white-space: nowrap;\n  max-width: 100%;\n}\n.input-field label.normal {\n  color: #9e9e9e;\n}\n.input-field label.active {\n  top: -0.9em;\n  font-size: 0.73em;\n  z-index: 2;\n}\n.input-field.prefixed label:not(.active) {\n  left: 2.5em;\n}\n.input-field:focus .line,\n.input-field.active .line,\n.input-field[disabled] .line {\n  display: block;\n}\n.input-field:focus input[type=text],\n.input-field.active input[type=text],\n.input-field[disabled] input[type=text],\n.input-field:focus input[type=password],\n.input-field.active input[type=password],\n.input-field[disabled] input[type=password],\n.input-field:focus input[type=email],\n.input-field.active input[type=email],\n.input-field[disabled] input[type=email],\n.input-field:focus input[type=url],\n.input-field.active input[type=url],\n.input-field[disabled] input[type=url],\n.input-field:focus input[type=time],\n.input-field.active input[type=time],\n.input-field[disabled] input[type=time],\n.input-field:focus input[type=date],\n.input-field.active input[type=date],\n.input-field[disabled] input[type=date],\n.input-field:focus input[type=datetime-local],\n.input-field.active input[type=datetime-local],\n.input-field[disabled] input[type=datetime-local],\n.input-field:focus input[type=tel],\n.input-field.active input[type=tel],\n.input-field[disabled] input[type=tel],\n.input-field:focus input[type=number],\n.input-field.active input[type=number],\n.input-field[disabled] input[type=number],\n.input-field:focus input[type=search],\n.input-field.active input[type=search],\n.input-field[disabled] input[type=search],\n.input-field:focus textarea.vox-textarea,\n.input-field.active textarea.vox-textarea,\n.input-field[disabled] textarea.vox-textarea,\n.input-field:focus select,\n.input-field.active select,\n.input-field[disabled] select {\n  /*border-bottom:none;*/\n}\n.input-field:focus input[type=text] ~ .action,\n.input-field.active input[type=text] ~ .action,\n.input-field[disabled] input[type=text] ~ .action,\n.input-field:focus input[type=password] ~ .action,\n.input-field.active input[type=password] ~ .action,\n.input-field[disabled] input[type=password] ~ .action,\n.input-field:focus input[type=email] ~ .action,\n.input-field.active input[type=email] ~ .action,\n.input-field[disabled] input[type=email] ~ .action,\n.input-field:focus input[type=url] ~ .action,\n.input-field.active input[type=url] ~ .action,\n.input-field[disabled] input[type=url] ~ .action,\n.input-field:focus input[type=time] ~ .action,\n.input-field.active input[type=time] ~ .action,\n.input-field[disabled] input[type=time] ~ .action,\n.input-field:focus input[type=date] ~ .action,\n.input-field.active input[type=date] ~ .action,\n.input-field[disabled] input[type=date] ~ .action,\n.input-field:focus input[type=datetime-local] ~ .action,\n.input-field.active input[type=datetime-local] ~ .action,\n.input-field[disabled] input[type=datetime-local] ~ .action,\n.input-field:focus input[type=tel] ~ .action,\n.input-field.active input[type=tel] ~ .action,\n.input-field[disabled] input[type=tel] ~ .action,\n.input-field:focus input[type=number] ~ .action,\n.input-field.active input[type=number] ~ .action,\n.input-field[disabled] input[type=number] ~ .action,\n.input-field:focus input[type=search] ~ .action,\n.input-field.active input[type=search] ~ .action,\n.input-field[disabled] input[type=search] ~ .action,\n.input-field:focus textarea.vox-textarea ~ .action,\n.input-field.active textarea.vox-textarea ~ .action,\n.input-field[disabled] textarea.vox-textarea ~ .action,\n.input-field:focus select ~ .action,\n.input-field.active select ~ .action,\n.input-field[disabled] select ~ .action {\n  color: inherit;\n}\n.input-field.select input,\n.input-field.select label {\n  cursor: pointer;\n}\n.input-field.select select {\n  display: none;\n}\n.input-field.select > input[type=text] {\n  position: absolute;\n}\n.input-field .select-wrapper .options-wrapper {\n  min-width: 100%;\n  max-height: 400px;\n  overflow-y: auto;\n  z-index: 999;\n  will-change: width,height;\n  margin: 0;\n  list-style: none;\n  padding: 0;\n  background-color: white;\n  box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);\n  position: absolute;\n  top: 0.8em;\n}\n.input-field .select-wrapper .options-wrapper li {\n  clear: both;\n  cursor: pointer;\n  width: 100%;\n  text-transform: none;\n}\n.input-field .select-wrapper .options-wrapper li a {\n  padding: 0.8em 1em;\n  display: block;\n}\n.input-field .select-wrapper .options-wrapper li[disabled] {\n  cursor: default;\n}\n.input-field .select-wrapper .options-wrapper li.default[selected] a {\n  background-color: #e6e6e6;\n}\n.input-field[disabled] label {\n  color: rgba(0, 0, 0, 0.3);\n}\n.input-field[disabled] .line {\n  display: none;\n}\n.input-field[disabled] input {\n  border-bottom-style: dotted;\n  color: rgba(0, 0, 0, 0.3);\n}\n.progress-bar {\n  width: 100%;\n  height: 4px;\n  position: relative;\n  overflow: hidden;\n}\n.progress-bar.default {\n  background-color: #e6e6e6;\n}\n.progress-bar .progress {\n  height: 100%;\n  position: relative;\n}\n.progress-bar .progress.indeterminate {\n  height: 0;\n  will-change: left, right;\n}\n.progress-bar .progress.indeterminate:before,\n.progress-bar .progress.indeterminate:after {\n  width: 100%;\n  height: 4px;\n  content: '';\n  position: absolute;\n  background-color: inherit;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  will-change: left, right;\n  -webkit-animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;\n  -moz-animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;\n  -ms-animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;\n  -o-animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;\n  animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;\n}\n.progress-bar .progress.indeterminate:after {\n  -webkit-animation-delay: 1.15s;\n  -moz-animation-delay: 1.15s;\n  -ms-animation-delay: 1.15s;\n  -o-animation-delay: 1.15s;\n  animation-delay: 1.15s;\n  -webkit-animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n  -moz-animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n  -ms-animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n  -o-animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n  animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n}\n.progress-bar.small {\n  height: 2px;\n}\n.progress-bar.small .progress.indeterminate:after,\n.progress-bar.small .progress.indeterminate:before {\n  height: 2px;\n}\n@-webkit-keyframes indeterminate {\n  0% {\n    left: -35%;\n    right: 100%;\n  }\n  60% {\n    left: 100%;\n    right: -90%;\n  }\n  100% {\n    left: 100%;\n    right: -90%;\n  }\n}\n@-moz-keyframes indeterminate {\n  0% {\n    left: -35%;\n    right: 100%;\n  }\n  60% {\n    left: 100%;\n    right: -90%;\n  }\n  100% {\n    left: 100%;\n    right: -90%;\n  }\n}\n@keyframes indeterminate {\n  0% {\n    left: -35%;\n    right: 100%;\n  }\n  60% {\n    left: 100%;\n    right: -90%;\n  }\n  100% {\n    left: 100%;\n    right: -90%;\n  }\n}\n@-webkit-keyframes indeterminate-short {\n  0% {\n    left: -200%;\n    right: 100%;\n  }\n  60% {\n    left: 107%;\n    right: -8%;\n  }\n  100% {\n    left: 107%;\n    right: -8%;\n  }\n}\n@-moz-keyframes indeterminate-short {\n  0% {\n    left: -200%;\n    right: 100%;\n  }\n  60% {\n    left: 107%;\n    right: -8%;\n  }\n  100% {\n    left: 107%;\n    right: -8%;\n  }\n}\n@keyframes indeterminate-short {\n  0% {\n    left: -200%;\n    right: 100%;\n  }\n  60% {\n    left: 107%;\n    right: -8%;\n  }\n  100% {\n    left: 107%;\n    right: -8%;\n  }\n}\n.tabs {\n  margin: 0;\n  padding: 0;\n  display: block;\n  display: flex;\n  width: 100%;\n  list-style: none;\n  position: relative;\n}\n.tabs.default {\n  color: #E91E63;\n}\n.tabs.default .indicator {\n  background-color: #E91E63;\n}\n.tabs .indicator {\n  height: 2px;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  width: 100%;\n}\n.tabs .tab {\n  padding: 0;\n  margin: 0;\n  display: inline-block;\n  position: relative;\n}\n.tabs .tab > a {\n  display: block;\n  padding: 0.8em 0.5em;\n  text-align: center;\n}\n.card-panel {\n  transition: box-shadow 0.25s;\n  padding: 2em;\n  margin: 20px 0;\n  border-radius: 2px;\n}\n.card-panel.default {\n  background-color: #009688;\n  color: white;\n}\n.card {\n  position: relative;\n  overflow: hidden;\n  margin: 20px 0;\n  transition: box-shadow 0.25s;\n  border-radius: 2px;\n}\n.card.default .card-title {\n  color: white;\n}\n.card .card-title {\n  font-size: 1.53em;\n  font-weight: 300;\n  padding: 0.2em 0.6em;\n}\n.card .card-title.bordered {\n  border-bottom: solid 1px #f0f0f0;\n}\n.card .card-title.activator {\n  cursor: pointer;\n}\n.card.small,\n.card.medium,\n.card.large {\n  position: relative;\n}\n.card.small .card-image,\n.card.medium .card-image,\n.card.large .card-image {\n  overflow: hidden;\n}\n.card.small .card-content,\n.card.medium .card-content,\n.card.large .card-content {\n  overflow: hidden;\n}\n.card.small .card-action,\n.card.medium .card-action,\n.card.large .card-action {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.card.small {\n  height: 300px;\n}\n.card.small .card-image {\n  height: 150px;\n}\n.card.small .card-content {\n  height: 150px;\n}\n.card.medium {\n  height: 400px;\n}\n.card.medium .card-image {\n  height: 250px;\n}\n.card.medium .card-content {\n  height: 150px;\n}\n.card.large {\n  height: 500px;\n}\n.card.large .card-image {\n  height: 330px;\n}\n.card.large .card-content {\n  height: 170px;\n}\n.card .card-image {\n  position: relative;\n}\n.card .card-image img {\n  border-radius: 2px 2px 0 0;\n  position: relative;\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  width: 100%;\n}\n.card .card-image .card-title {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  padding: 2em;\n}\n.card .card-content {\n  padding: 1.5em;\n  border-radius: 0 0 2px 2px;\n}\n.card .card-content p {\n  margin: 0;\n  color: inherit;\n}\n.card .card-content .card-title {\n  line-height: 48px;\n}\n.card .card-action {\n  border-top: 1px solid rgba(160, 160, 160, 0.2);\n}\n.card .card-action a {\n  display: block;\n  padding: 0.7em 1em;\n  text-transform: uppercase;\n}\n.card .card-reveal {\n  padding: 2em;\n  position: absolute;\n  width: 100%;\n  overflow-y: auto;\n  top: 100%;\n  height: 100%;\n  z-index: 1;\n  /*display: none;*/\n}\n.card .card-reveal .card-title {\n  cursor: pointer;\n  display: block;\n}\n.chip {\n  display: inline-block;\n  font-size: 0.80em;\n  font-weight: 500;\n  line-height: 2.5em;\n  height: 2.5em;\n  padding: 0 0.8em;\n  border-radius: 1.25em;\n}\n.chip.default {\n  color: rgba(0, 0, 0, 0.6);\n}\n.chip img {\n  float: left;\n  margin: 0 8px 0 -12px;\n  height: 100%;\n  width: 2.5em;\n  border-radius: 50%;\n}\n.chip .action {\n  cursor: pointer;\n  float: right;\n  font-size: 1.15em;\n  line-height: 2.225em;\n  vertical-align: middle;\n  padding-left: 8px;\n}\n.collection {\n  margin: 0.5em 0 1em 0;\n  border: 1px solid #e0e0e0;\n  border-radius: 2px;\n  overflow: hidden;\n  position: relative;\n  list-style: none;\n  padding: 0;\n}\n.collection.default .collection-item {\n  background-color: #fff;\n}\n.collection.default a.collection-item {\n  color: #26a69a;\n}\n.collection.default a.collection-item:not(.active):hover {\n  background-color: #e0e0e0;\n}\n.collection.default a.collection-item.active {\n  background-color: #009688;\n  color: white;\n}\n.collection.default .collection-header {\n  background-color: #fff;\n}\n.collection .secondary-content {\n  float: right;\n}\n.collection .collection-header {\n  border-bottom: 1px solid #e0e0e0;\n  padding: 10px 20px;\n}\n.collection .collection-item {\n  line-height: 1.5em;\n  padding: 10px 20px;\n  margin: 0;\n  border-bottom: 1px solid #e0e0e0;\n  position: relative;\n}\n.collection .collection-item.avatar {\n  padding-left: 4.8em;\n}\n.collection .collection-item.avatar p {\n  margin: 0;\n}\n.collection .collection-item.avatar .secondary-content {\n  position: absolute;\n  top: 16px;\n  right: 16px;\n}\n.collection .collection-item.avatar .circle {\n  position: absolute;\n  width: 2em;\n  height: 2em;\n  overflow: hidden;\n  left: 15px;\n  display: inline-block;\n  vertical-align: middle;\n  text-align: center;\n  line-height: 2em;\n  font-size: 1.5em;\n}\n.collection a.collection-item {\n  display: block;\n  -webkit-transition: 0.25s;\n  -moz-transition: 0.25s;\n  -o-transition: 0.25s;\n  -ms-transition: 0.25s;\n  /* transition: all .3s ease-out; */\n}\n.pagination {\n  list-style: none;\n  padding: 0;\n}\n.pagination li {\n  display: inline-block;\n  vertical-align: middle;\n  border-radius: 2px;\n}\n.pagination li[disabled] {\n  color: #9e9e9e;\n}\n.pagination li > a {\n  padding: 0.3em 10px;\n  font-size: 1.15em;\n  text-align: center;\n  display: block;\n}\n.pagination li > a[disabled] {\n  color: #9e9e9e;\n}\n.nav {\n  width: 100%;\n  height: 3.2em;\n  line-height: 3.2em;\n}\n.nav .nav-wrapper {\n  position: relative;\n  height: 100%;\n}\n.nav .button-collapse {\n  float: left;\n  position: relative;\n  z-index: 1;\n  height: 100%;\n  display: none;\n}\n.nav .button-collapse i {\n  font-size: 2.5em;\n  height: 100%;\n}\n.nav .brand-logo {\n  position: absolute;\n  display: inline-block;\n  font-size: 2em;\n  padding: 0;\n  white-space: nowrap;\n  /*\n    @media #{$medium-and-down} {\n      left: 50%;\n      transform:translateX(-50%);\n    }\n    */\n}\n.nav .brand-logo.center {\n  left: 50%;\n  transform: translateX(-50%);\n}\n.nav .brand-logo.right {\n  right: 0.5em;\n  padding: 0;\n}\n.nav ul {\n  padding: 0;\n  list-style: none;\n  margin: 0;\n}\n.nav ul li {\n  -webkit-transition: background-color 0.3s;\n  -moz-transition: background-color 0.3s;\n  -o-transition: background-color 0.3s;\n  -ms-transition: background-color 0.3s;\n  /* transition: all .3s ease-out; */\n  float: left;\n  padding: 0;\n}\n.nav ul li:hover,\n.nav ul li.active {\n  background-color: rgba(0, 0, 0, 0.1);\n}\n.nav ul a {\n  font-size: 1em;\n  display: block;\n  padding: 0 15px;\n}\n.nav ul a i {\n  display: inline-block;\n}\n.nav ul.left {\n  float: left;\n}\n.nav .input-field {\n  margin: 0;\n}\n.nav .input-field input {\n  height: 100%;\n  font-size: 1.15em;\n  border: none;\n  padding-left: 2.1em;\n}\n.nav .input-field input:focus,\n.nav .input-field input[type=text]:valid,\n.nav .input-field input[type=password]:valid,\n.nav .input-field input[type=email]:valid,\n.nav .input-field input[type=url]:valid,\n.nav .input-field input[type=date]:valid {\n  border: none;\n  box-shadow: none;\n}\n.nav .input-field input[type=search] {\n  padding-left: 2.6em;\n}\n.nav .input-field label {\n  top: 0;\n  left: 0;\n}\n.nav .input-field label i {\n  color: rgba(255, 255, 255, 0.7);\n  -webkit-transition: color 0.3s;\n  -moz-transition: color 0.3s;\n  -o-transition: color 0.3s;\n  -ms-transition: color 0.3s;\n  /* transition: all .3s ease-out; */\n}\n.nav .input-field label.active {\n  transform: translateY(0);\n}\n.navbar-fixed {\n  position: relative;\n  z-index: 998;\n}\n.navbar-fixed .nav {\n  position: fixed;\n}\n/*\n@media #{$medium-and-up} {\n  nav, nav .nav-wrapper i, nav a.button-collapse, nav a.button-collapse i {\n    height: $navbar-height;\n    line-height: $navbar-height;\n  }\n  .navbar-fixed {\n    height: $navbar-height;\n  }\n}\n*/\n.dropdown-menu {\n  display: none;\n  position: absolute;\n  z-index: 4;\n  min-width: 160px;\n  font-weight: 400;\n  padding: 0 0;\n  margin: 2px 0 0;\n  list-style: none;\n  font-size: 15px;\n  text-align: left;\n  background-color: white;\n  color: #00796b;\n  left: 0;\n  top: 100%;\n  clear: both;\n  line-height: 1.3em;\n}\n.dropdown-menu.to-left {\n  right: 10px;\n  left: initial;\n}\n.dropdown-menu.to-top {\n  bottom: 104%;\n  top: initial;\n}\n.dropdown-menu > li > a {\n  color: inherit;\n}\n.dropdown-menu.default > li > a:hover {\n  background-color: #eceff1;\n}\n.dropdown {\n  position: relative;\n}\n.dropdown.open > .dropdown-menu {\n  display: block;\n}\n.dropdown-menu > li,\n.nav .dropdown-menu > li {\n  float: none;\n}\n.dropdown-menu > li a,\n.nav .dropdown-menu > li a {\n  display: block;\n  padding: 0.7em 0.8em;\n  width: 100%;\n  clear: both;\n}\n.navbar.hidden .dropdown-menu {\n  top: 10px;\n  left: 10px;\n}\n.navbar.hidden .dropdown-menu.to-top {\n  bottom: 10px;\n  top: initial;\n}\n.navbar.hidden .dropdown-menu.to-left {\n  right: 10px;\n  left: initial;\n}\n.navbar.hidden .dropdown-menu li {\n  height: auto;\n}\n.navbar.hidden .dropdown-menu li a {\n  color: #8c8c8c;\n}\n.dropdown-menu li.divider {\n  border-top: solid 1px #e6e6e6;\n}\n.dropdown-menu li.divider:hover {\n  background-color: none;\n}\n.dropdown-menu li.header {\n  font-size: 13px;\n  color: #bebebe;\n  padding: 3px 20px;\n  clear: both;\n  display: block;\n}\n.dropdown-menu li.header:hover {\n  background-color: none;\n  color: #bebebe;\n}\n.dropdown-menu li[disabled],\n.dropdown-menu li[disabled]:hover,\n.dropdown-menu li[disabled] a,\n.dropdown-menu li[disabled] a:hover {\n  background-color: none;\n  color: #d2d2d2;\n  cursor: default;\n}\n.dropdown-menu .dropdown-menu {\n  left: 105%;\n  top: 0;\n}\n.dropdown-menu .dropdown-menu.to-right {\n  right: 105%;\n  left: initial;\n}\n.dropdown-menu .dropdown-menu.to-top {\n  bottom: 0;\n  top: initial;\n}\n.side-nav {\n  position: fixed;\n  width: 240px;\n  left: 0;\n  top: 0;\n  margin: 0;\n  height: 100%;\n  padding: 0;\n  padding-bottom: 60px;\n  list-style: none;\n  z-index: 999;\n  overflow-y: auto;\n  overflow: hidden;\n  will-change: left;\n}\n.side-nav:hover {\n  overflow-y: auto;\n}\n.side-nav.right {\n  will-change: right;\n  right: 0;\n  left: auto;\n}\n.side-nav .collapsible {\n  margin: 0;\n}\n.side-nav ul {\n  margin: 0;\n  padding: 0;\n}\n.side-nav ul > li {\n  padding-left: 1em;\n}\n.side-nav li {\n  list-style: none;\n  float: none;\n}\n.side-nav a {\n  display: block;\n  font-size: 1em;\n  font-weight: 300;\n  padding: 0.8em 1em;\n}\n.side-nav.default {\n  background-color: white;\n}\n.side-nav.default li:hover,\n.side-nav.default li.active {\n  background-color: #ddd;\n}\n.drag-target {\n  height: 100%;\n  width: 10px;\n  position: fixed;\n  top: 0;\n  z-index: 9998;\n}\n#sidenav-overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  height: 120vh;\n  background-color: rgba(0, 0, 0, 0.5);\n  z-index: 997;\n  will-change: opacity;\n  display: block;\n}\n.size-s .s-hide.side-nav,\n.size-s .sl-hide.side-nav,\n.size-s .m-hide.side-nav,\n.size-s .ml-hide.side-nav,\n.size-s .l-hide.side-nav,\n.size-s .s-hide-only.side-nav {\n  display: block;\n  left: -105%;\n}\n.size-s .s-hide.side-nav.right,\n.size-s .sl-hide.side-nav.right,\n.size-s .m-hide.side-nav.right,\n.size-s .ml-hide.side-nav.right,\n.size-s .l-hide.side-nav.right,\n.size-s .s-hide-only.side-nav.right {\n  left: auto;\n  right: 105%;\n}\n.size-sl .sl-hide.side-nav,\n.size-sl .m-hide.side-nav,\n.size-sl .ml-hide.side-nav,\n.size-sl .l-hide.side-nav,\n.size-sl .sl-hide-only.side-nav {\n  display: block;\n  left: -105%;\n}\n.size-sl .sl-hide.side-nav.right,\n.size-sl .m-hide.side-nav.right,\n.size-sl .ml-hide.side-nav.right,\n.size-sl .l-hide.side-nav.right,\n.size-sl .sl-hide-only.side-nav.right {\n  left: auto;\n  right: 105%;\n}\n.size-m .m-hide.side-nav,\n.size-m .ml-hide.side-nav,\n.size-m .l-hide.side-nav,\n.size-m .m-hide-only.side-nav {\n  display: block;\n  left: -105%;\n}\n.size-m .m-hide.side-nav.right,\n.size-m .ml-hide.side-nav.right,\n.size-m .l-hide.side-nav.right,\n.size-m .m-hide-only.side-nav.right {\n  left: auto;\n  right: 105%;\n}\n.size-ml .ml-hide.side-nav,\n.size-ml .l-hide.side-nav,\n.size-ml .ml-hide-only.side-nav {\n  display: block;\n  left: -105%;\n}\n.size-ml .ml-hide.side-nav.right,\n.size-ml .l-hide.side-nav.right,\n.size-ml .ml-hide-only.side-nav.right {\n  left: auto;\n  right: 105%;\n}\n.size-l .l-hide.side-nav,\n.size-l .l-hide-only.side-nav {\n  display: block;\n  left: -105%;\n}\n.size-l .l-hide.side-nav.right,\n.size-l .l-hide-only.side-nav.right {\n  left: auto;\n  right: 105%;\n}\n.side-nav.opened {\n  left: 0 !important;\n}\n.side-nav.opened.right {\n  left: auto !important;\n  right: 0 !important;\n}\n.slider {\n  position: relative;\n  padding: 1.5em;\n  margin-top: 3em;\n  display: inline-block;\n  width: 100%;\n  /*\n    .ball.first:after{\n      content: \"\";\n      top: 1em;\n      border-radius: 100%;\n      position: absolute;\n      margin-left: -1.08em;\n\n      border-left: 1.12em solid transparent;\n      border-right: 1.12em solid transparent;\n      border-top-width: 1.25em;\n      border-top-style: solid;\n      opacity: 0;\n      //-webkit-transform: translate3d(0, -8px, 0) scale(0);\n      .transform( translate3d(0, -8px, 0) scale(0));\n      -webkit-transition: all 0.2s cubic-bezier(0.35, 0, 0.25, 1);\n      transition: all 0.2s cubic-bezier(0.35, 0, 0.25, 1);\n    }\n\n    &.vertical{\n      .ball.first:after{\n        content: \"\";\n        left: 1em;\n        border-radius: 100%;\n        position: absolute;\n        margin-bottom: -1.08em;\n\n        border:none;\n        border-top: 1.12em solid transparent;\n        border-bottom: 1.12em solid transparent;\n        border-left-width: 1.25em;\n        border-left-style: solid;\n        opacity: 0;\n\n        .transform(translate3d(0, -8px, 0) scale(0));\n        -webkit-transition: all 0.2s cubic-bezier(0.35, 0, 0.25, 1);\n        transition: all 0.2s cubic-bezier(0.35, 0, 0.25, 1);\n      }\n    }*/\n}\n.slider.vertical {\n  margin-top: 0;\n  margin-left: 3em;\n}\n.slider .step {\n  width: 3px;\n  height: 3px;\n  background-color: black;\n  background-color: rgba(0, 0, 0, 0.4);\n  position: absolute;\n  margin: -1.5px;\n  top: 0;\n  left: 0;\n  display: none;\n}\n.slider .ball.first {\n  border-radius: 50%;\n  padding: 0.45em;\n  position: absolute;\n  top: 0;\n  margin: -0.45em;\n  outline: none;\n  cursor: default;\n  transition-duration: 0.2s;\n}\n.slider .ball.first span {\n  transition-duration: 0.2s;\n  display: none;\n  opacity: 0;\n  position: absolute;\n  width: 100%;\n  top: 0;\n  left: 0;\n  text-align: center;\n  line-height: 2.5em;\n  font-size: 0.76em;\n  z-index: 10;\n  font-weight: bold;\n}\n.slider .ball.first.highlight {\n  opacity: 0.2;\n  padding: 1.05em;\n  margin: -1.05em;\n  display: none;\n}\n.slider.discrete.focus .ball.highlight,\n.slider.discrete[readonly] .ball.highlight {\n  display: none;\n}\n.slider.discrete.focus .ball.first,\n.slider.discrete[readonly] .ball.first {\n  padding: 0.9em;\n  margin-top: -2.5em;\n  margin-left: -0.92em;\n  margin-right: -0.92em;\n  border-radius: 50% 50% 50% 0;\n  -webkit-transform: rotate(-45deg);\n  -moz-transform: rotate(-45deg);\n  -o-transform: rotate(-45deg);\n  -ms-transform: rotate(-45deg);\n  transform: rotate(-45deg);\n  /*\n      &:after{\n        opacity: 1;\n        //-webkit-transform: translate3d(0, 0, 0) scale(1);\n        .transform( translate3d(0, 0, 0) scale(1));\n      }*/\n}\n.slider.discrete.focus .ball.first span,\n.slider.discrete[readonly] .ball.first span {\n  display: block;\n  opacity: 1;\n  -webkit-transform: rotate(45deg);\n  -moz-transform: rotate(45deg);\n  -o-transform: rotate(45deg);\n  -ms-transform: rotate(45deg);\n  transform: rotate(45deg);\n}\n.slider.discrete.focus.vertical.discrete.focus .ball.first,\n.slider.discrete[readonly].vertical.discrete.focus .ball.first {\n  -webkit-transform: rotate(-135deg);\n  -moz-transform: rotate(-135deg);\n  -o-transform: rotate(-135deg);\n  -ms-transform: rotate(-135deg);\n  transform: rotate(-135deg);\n}\n.slider.discrete.focus.vertical.discrete.focus .ball.first span,\n.slider.discrete[readonly].vertical.discrete.focus .ball.first span {\n  -webkit-transform: rotate(135deg);\n  -moz-transform: rotate(135deg);\n  -o-transform: rotate(135deg);\n  -ms-transform: rotate(135deg);\n  transform: rotate(135deg);\n}\n.slider.discrete.focus.vertical .ball.first,\n.slider.discrete[readonly].vertical .ball.first {\n  margin: 0;\n  margin-left: -2.5em;\n  margin-top: -0.92em;\n  margin-bottom: -0.92em;\n}\n.slider.focus .step {\n  display: block;\n}\n.slider.focus .ball.highlight {\n  display: block;\n}\n.slider .line {\n  transition-duration: 0.2s;\n  height: 3px;\n  width: 100%;\n  position: absolute;\n  left: 0;\n  top: 0;\n  margin-top: -1.5px;\n}\n.slider .line.progress {\n  width: 0;\n}\n.slider .line.default {\n  background-color: #a8a8a8;\n}\n.slider.vertical {\n  height: 100%;\n  width: auto;\n}\n.slider.vertical .line {\n  width: 3px;\n  height: 100%;\n  margin-left: -1.5px;\n}\n.slider.vertical .line.progress {\n  height: 0;\n}\n.slider.vertical .step {\n  top: initial;\n}\n.slider.vertical .ball {\n  top: initial;\n  left: 0;\n}\n.slider[disabled],\n.slider[readonly] {\n  opacity: 0.8;\n}\n.slider[disabled] .ball,\n.slider[readonly] .ball,\n.slider[disabled] .line,\n.slider[readonly] .line {\n  background-color: #787878;\n  color: #787878;\n}\n.slider[disabled].discrete .ball,\n.slider[readonly].discrete .ball {\n  background-color: #aaaaaa;\n  color: #aaaaaa;\n}\n.slider[disabled] .ball {\n  -webkit-transform: scale(0.7);\n  -moz-transform: scale(0.7);\n  -o-transform: scale(0.7);\n  -ms-transform: scale(0.7);\n  transform: scale(0.7);\n}\n.slider.clic .ball {\n  padding: 0.6em;\n  margin: -0.6em;\n}\n.slider.clic .ball.highlight {\n  display: none;\n}\n.black,\n.black-hover:not([disabled]):hover,\n.black-hover[hover-active]:not([disabled]),\n.black-active.active {\n  background-color: #000;\n}\n.white,\n.white-hover:not([disabled]):hover,\n.white-hover[hover-active]:not([disabled]),\n.white-active.active {\n  background-color: #fff;\n}\n.text-black,\n.text-black-hover:not([disabled]):hover,\n.text-black-hover[hover-active]:not([disabled]),\n.text-black-active.active {\n  color: #000;\n}\n.text-white,\n.text-white-hover:not([disabled]):hover,\n.text-white-hover[hover-active]:not([disabled]),\n.text-white-active.active {\n  color: #fff;\n}\n.brown {\n  background-color: #795548;\n}\n.brown.lighten-1,\n.brown.hover:not([disabled]):hover {\n  background-color: #8d6e63;\n}\n.brown.lighten-2 {\n  background-color: #a1887f;\n}\n.brown.lighten-3 {\n  background-color: #bcaaa4;\n}\n.brown.lighten-4 {\n  background-color: #d7ccc8;\n}\n.brown.lighten-5 {\n  background-color: #efebe9;\n}\n.brown.darken-1 {\n  background-color: #6d4c41;\n}\n.brown.darken-2 {\n  background-color: #5d4037;\n}\n.brown.darken-3 {\n  background-color: #4e342e;\n}\n.brown.darken-4 {\n  background-color: #3e2723;\n}\n*:not(.active).red,\n*:not(.active).red-hover:not([disabled]):hover,\n*:not(.active).red-hover[hover-active]:not([disabled]) {\n  background-color: #F44336;\n}\n*:not(.active).red.lighten-1,\n*:not(.active).red-hover:not([disabled]):hover.lighten-1,\n*:not(.active).red-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).red.hover:not([disabled]):hover,\n*:not(.active).red-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).red-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #ef5350;\n}\n*:not(.active).red.lighten-2,\n*:not(.active).red-hover:not([disabled]):hover.lighten-2,\n*:not(.active).red-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #e57373;\n}\n*:not(.active).red.lighten-3,\n*:not(.active).red-hover:not([disabled]):hover.lighten-3,\n*:not(.active).red-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #ef9a9a;\n}\n*:not(.active).red.lighten-4,\n*:not(.active).red-hover:not([disabled]):hover.lighten-4,\n*:not(.active).red-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #ffcdd2;\n}\n*:not(.active).red.lighten-5,\n*:not(.active).red-hover:not([disabled]):hover.lighten-5,\n*:not(.active).red-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #ffebee;\n}\n*:not(.active).red.darken-1,\n*:not(.active).red-hover:not([disabled]):hover.darken-1,\n*:not(.active).red-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #e53935;\n}\n*:not(.active).red.darken-2,\n*:not(.active).red-hover:not([disabled]):hover.darken-2,\n*:not(.active).red-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #d32f2f;\n}\n*:not(.active).red.darken-3,\n*:not(.active).red-hover:not([disabled]):hover.darken-3,\n*:not(.active).red-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #c62828;\n}\n*:not(.active).red.darken-4,\n*:not(.active).red-hover:not([disabled]):hover.darken-4,\n*:not(.active).red-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #b71c1c;\n}\n*:not(.active).red.accent-1,\n*:not(.active).red-hover:not([disabled]):hover.accent-1,\n*:not(.active).red-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #ff8a80;\n}\n*:not(.active).red.accent-2,\n*:not(.active).red-hover:not([disabled]):hover.accent-2,\n*:not(.active).red-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #ff5252;\n}\n*:not(.active).red.accent-3,\n*:not(.active).red-hover:not([disabled]):hover.accent-3,\n*:not(.active).red-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #ff1744;\n}\n*:not(.active).red.accent-4,\n*:not(.active).red-hover:not([disabled]):hover.accent-4,\n*:not(.active).red-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #d50000;\n}\n*:not(.active).pink,\n*:not(.active).pink-hover:not([disabled]):hover,\n*:not(.active).pink-hover[hover-active]:not([disabled]) {\n  background-color: #E91E63;\n}\n*:not(.active).pink.lighten-1,\n*:not(.active).pink-hover:not([disabled]):hover.lighten-1,\n*:not(.active).pink-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).pink.hover:not([disabled]):hover,\n*:not(.active).pink-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).pink-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #ec407a;\n}\n*:not(.active).pink.lighten-2,\n*:not(.active).pink-hover:not([disabled]):hover.lighten-2,\n*:not(.active).pink-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #f06292;\n}\n*:not(.active).pink.lighten-3,\n*:not(.active).pink-hover:not([disabled]):hover.lighten-3,\n*:not(.active).pink-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #f48fb1;\n}\n*:not(.active).pink.lighten-4,\n*:not(.active).pink-hover:not([disabled]):hover.lighten-4,\n*:not(.active).pink-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #f8bbd0;\n}\n*:not(.active).pink.lighten-5,\n*:not(.active).pink-hover:not([disabled]):hover.lighten-5,\n*:not(.active).pink-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #fce4ec;\n}\n*:not(.active).pink.darken-1,\n*:not(.active).pink-hover:not([disabled]):hover.darken-1,\n*:not(.active).pink-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #d81b60;\n}\n*:not(.active).pink.darken-2,\n*:not(.active).pink-hover:not([disabled]):hover.darken-2,\n*:not(.active).pink-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #c2185b;\n}\n*:not(.active).pink.darken-3,\n*:not(.active).pink-hover:not([disabled]):hover.darken-3,\n*:not(.active).pink-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #ad1457;\n}\n*:not(.active).pink.darken-4,\n*:not(.active).pink-hover:not([disabled]):hover.darken-4,\n*:not(.active).pink-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #880e4f;\n}\n*:not(.active).pink.accent-1,\n*:not(.active).pink-hover:not([disabled]):hover.accent-1,\n*:not(.active).pink-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #ff80ab;\n}\n*:not(.active).pink.accent-2,\n*:not(.active).pink-hover:not([disabled]):hover.accent-2,\n*:not(.active).pink-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #ff4081;\n}\n*:not(.active).pink.accent-3,\n*:not(.active).pink-hover:not([disabled]):hover.accent-3,\n*:not(.active).pink-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #f50057;\n}\n*:not(.active).pink.accent-4,\n*:not(.active).pink-hover:not([disabled]):hover.accent-4,\n*:not(.active).pink-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #c51162;\n}\n*:not(.active).purple,\n*:not(.active).purple-hover:not([disabled]):hover,\n*:not(.active).purple-hover[hover-active]:not([disabled]) {\n  background-color: #9c27b0;\n}\n*:not(.active).purple.lighten-1,\n*:not(.active).purple-hover:not([disabled]):hover.lighten-1,\n*:not(.active).purple-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).purple.hover:not([disabled]):hover,\n*:not(.active).purple-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).purple-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #ab47bc;\n}\n*:not(.active).purple.lighten-2,\n*:not(.active).purple-hover:not([disabled]):hover.lighten-2,\n*:not(.active).purple-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #ba68c8;\n}\n*:not(.active).purple.lighten-3,\n*:not(.active).purple-hover:not([disabled]):hover.lighten-3,\n*:not(.active).purple-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #ce93d8;\n}\n*:not(.active).purple.lighten-4,\n*:not(.active).purple-hover:not([disabled]):hover.lighten-4,\n*:not(.active).purple-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #e1bee7;\n}\n*:not(.active).purple.lighten-5,\n*:not(.active).purple-hover:not([disabled]):hover.lighten-5,\n*:not(.active).purple-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #f3e5f5;\n}\n*:not(.active).purple.darken-1,\n*:not(.active).purple-hover:not([disabled]):hover.darken-1,\n*:not(.active).purple-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #8e24aa;\n}\n*:not(.active).purple.darken-2,\n*:not(.active).purple-hover:not([disabled]):hover.darken-2,\n*:not(.active).purple-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #7b1fa2;\n}\n*:not(.active).purple.darken-3,\n*:not(.active).purple-hover:not([disabled]):hover.darken-3,\n*:not(.active).purple-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #6a1b9a;\n}\n*:not(.active).purple.darken-4,\n*:not(.active).purple-hover:not([disabled]):hover.darken-4,\n*:not(.active).purple-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #4a148c;\n}\n*:not(.active).purple.accent-1,\n*:not(.active).purple-hover:not([disabled]):hover.accent-1,\n*:not(.active).purple-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #ea80fc;\n}\n*:not(.active).purple.accent-2,\n*:not(.active).purple-hover:not([disabled]):hover.accent-2,\n*:not(.active).purple-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #e040fb;\n}\n*:not(.active).purple.accent-3,\n*:not(.active).purple-hover:not([disabled]):hover.accent-3,\n*:not(.active).purple-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #d500f9;\n}\n*:not(.active).purple.accent-4,\n*:not(.active).purple-hover:not([disabled]):hover.accent-4,\n*:not(.active).purple-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #aa00ff;\n}\n*:not(.active).deep-purple,\n*:not(.active).deep-purple-hover:not([disabled]):hover,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]) {\n  background-color: #673ab7;\n}\n*:not(.active).deep-purple.lighten-1,\n*:not(.active).deep-purple-hover:not([disabled]):hover.lighten-1,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).deep-purple.hover:not([disabled]):hover,\n*:not(.active).deep-purple-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #7e57c2;\n}\n*:not(.active).deep-purple.lighten-2,\n*:not(.active).deep-purple-hover:not([disabled]):hover.lighten-2,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #9575cd;\n}\n*:not(.active).deep-purple.lighten-3,\n*:not(.active).deep-purple-hover:not([disabled]):hover.lighten-3,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #b39ddb;\n}\n*:not(.active).deep-purple.lighten-4,\n*:not(.active).deep-purple-hover:not([disabled]):hover.lighten-4,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #d1c4e9;\n}\n*:not(.active).deep-purple.lighten-5,\n*:not(.active).deep-purple-hover:not([disabled]):hover.lighten-5,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #ede7f6;\n}\n*:not(.active).deep-purple.darken-1,\n*:not(.active).deep-purple-hover:not([disabled]):hover.darken-1,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #5e35b1;\n}\n*:not(.active).deep-purple.darken-2,\n*:not(.active).deep-purple-hover:not([disabled]):hover.darken-2,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #512da8;\n}\n*:not(.active).deep-purple.darken-3,\n*:not(.active).deep-purple-hover:not([disabled]):hover.darken-3,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #4527a0;\n}\n*:not(.active).deep-purple.darken-4,\n*:not(.active).deep-purple-hover:not([disabled]):hover.darken-4,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #311b92;\n}\n*:not(.active).deep-purple.accent-1,\n*:not(.active).deep-purple-hover:not([disabled]):hover.accent-1,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #b388ff;\n}\n*:not(.active).deep-purple.accent-2,\n*:not(.active).deep-purple-hover:not([disabled]):hover.accent-2,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #7c4dff;\n}\n*:not(.active).deep-purple.accent-3,\n*:not(.active).deep-purple-hover:not([disabled]):hover.accent-3,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #651fff;\n}\n*:not(.active).deep-purple.accent-4,\n*:not(.active).deep-purple-hover:not([disabled]):hover.accent-4,\n*:not(.active).deep-purple-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #6200ea;\n}\n*:not(.active).indigo,\n*:not(.active).indigo-hover:not([disabled]):hover,\n*:not(.active).indigo-hover[hover-active]:not([disabled]) {\n  background-color: #3F51b5;\n}\n*:not(.active).indigo.lighten-1,\n*:not(.active).indigo-hover:not([disabled]):hover.lighten-1,\n*:not(.active).indigo-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).indigo.hover:not([disabled]):hover,\n*:not(.active).indigo-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).indigo-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #5c6bc0;\n}\n*:not(.active).indigo.lighten-2,\n*:not(.active).indigo-hover:not([disabled]):hover.lighten-2,\n*:not(.active).indigo-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #7986cb;\n}\n*:not(.active).indigo.lighten-3,\n*:not(.active).indigo-hover:not([disabled]):hover.lighten-3,\n*:not(.active).indigo-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #9fa8da;\n}\n*:not(.active).indigo.lighten-4,\n*:not(.active).indigo-hover:not([disabled]):hover.lighten-4,\n*:not(.active).indigo-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #c5cae9;\n}\n*:not(.active).indigo.lighten-5,\n*:not(.active).indigo-hover:not([disabled]):hover.lighten-5,\n*:not(.active).indigo-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #e8eaf6;\n}\n*:not(.active).indigo.darken-1,\n*:not(.active).indigo-hover:not([disabled]):hover.darken-1,\n*:not(.active).indigo-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #3949ab;\n}\n*:not(.active).indigo.darken-2,\n*:not(.active).indigo-hover:not([disabled]):hover.darken-2,\n*:not(.active).indigo-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #303f9f;\n}\n*:not(.active).indigo.darken-3,\n*:not(.active).indigo-hover:not([disabled]):hover.darken-3,\n*:not(.active).indigo-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #283593;\n}\n*:not(.active).indigo.darken-4,\n*:not(.active).indigo-hover:not([disabled]):hover.darken-4,\n*:not(.active).indigo-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #1a237e;\n}\n*:not(.active).indigo.accent-1,\n*:not(.active).indigo-hover:not([disabled]):hover.accent-1,\n*:not(.active).indigo-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #8c9eff;\n}\n*:not(.active).indigo.accent-2,\n*:not(.active).indigo-hover:not([disabled]):hover.accent-2,\n*:not(.active).indigo-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #536dfe;\n}\n*:not(.active).indigo.accent-3,\n*:not(.active).indigo-hover:not([disabled]):hover.accent-3,\n*:not(.active).indigo-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #3d5afe;\n}\n*:not(.active).indigo.accent-4,\n*:not(.active).indigo-hover:not([disabled]):hover.accent-4,\n*:not(.active).indigo-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #304ffe;\n}\n*:not(.active).blue,\n*:not(.active).blue-hover:not([disabled]):hover,\n*:not(.active).blue-hover[hover-active]:not([disabled]) {\n  background-color: #2196F3;\n}\n*:not(.active).blue.lighten-1,\n*:not(.active).blue-hover:not([disabled]):hover.lighten-1,\n*:not(.active).blue-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).blue.hover:not([disabled]):hover,\n*:not(.active).blue-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).blue-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #42a5f5;\n}\n*:not(.active).blue.lighten-2,\n*:not(.active).blue-hover:not([disabled]):hover.lighten-2,\n*:not(.active).blue-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #64b5f6;\n}\n*:not(.active).blue.lighten-3,\n*:not(.active).blue-hover:not([disabled]):hover.lighten-3,\n*:not(.active).blue-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #90caf9;\n}\n*:not(.active).blue.lighten-4,\n*:not(.active).blue-hover:not([disabled]):hover.lighten-4,\n*:not(.active).blue-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #bbdefb;\n}\n*:not(.active).blue.lighten-5,\n*:not(.active).blue-hover:not([disabled]):hover.lighten-5,\n*:not(.active).blue-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #e3f2fd;\n}\n*:not(.active).blue.darken-1,\n*:not(.active).blue-hover:not([disabled]):hover.darken-1,\n*:not(.active).blue-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #1e88e5;\n}\n*:not(.active).blue.darken-2,\n*:not(.active).blue-hover:not([disabled]):hover.darken-2,\n*:not(.active).blue-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #1976d2;\n}\n*:not(.active).blue.darken-3,\n*:not(.active).blue-hover:not([disabled]):hover.darken-3,\n*:not(.active).blue-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #1565c0;\n}\n*:not(.active).blue.darken-4,\n*:not(.active).blue-hover:not([disabled]):hover.darken-4,\n*:not(.active).blue-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #0d47a1;\n}\n*:not(.active).blue.accent-1,\n*:not(.active).blue-hover:not([disabled]):hover.accent-1,\n*:not(.active).blue-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #82b1ff;\n}\n*:not(.active).blue.accent-2,\n*:not(.active).blue-hover:not([disabled]):hover.accent-2,\n*:not(.active).blue-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #448aff;\n}\n*:not(.active).blue.accent-3,\n*:not(.active).blue-hover:not([disabled]):hover.accent-3,\n*:not(.active).blue-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #2979ff;\n}\n*:not(.active).blue.accent-4,\n*:not(.active).blue-hover:not([disabled]):hover.accent-4,\n*:not(.active).blue-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #2962ff;\n}\n*:not(.active).light-blue,\n*:not(.active).light-blue-hover:not([disabled]):hover,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]) {\n  background-color: #03A9F4;\n}\n*:not(.active).light-blue.lighten-1,\n*:not(.active).light-blue-hover:not([disabled]):hover.lighten-1,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).light-blue.hover:not([disabled]):hover,\n*:not(.active).light-blue-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #29b6f6;\n}\n*:not(.active).light-blue.lighten-2,\n*:not(.active).light-blue-hover:not([disabled]):hover.lighten-2,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #4fc3f7;\n}\n*:not(.active).light-blue.lighten-3,\n*:not(.active).light-blue-hover:not([disabled]):hover.lighten-3,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #81d4fa;\n}\n*:not(.active).light-blue.lighten-4,\n*:not(.active).light-blue-hover:not([disabled]):hover.lighten-4,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #b3e5fc;\n}\n*:not(.active).light-blue.lighten-5,\n*:not(.active).light-blue-hover:not([disabled]):hover.lighten-5,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #e1f5fe;\n}\n*:not(.active).light-blue.darken-1,\n*:not(.active).light-blue-hover:not([disabled]):hover.darken-1,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #039be5;\n}\n*:not(.active).light-blue.darken-2,\n*:not(.active).light-blue-hover:not([disabled]):hover.darken-2,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #0288d1;\n}\n*:not(.active).light-blue.darken-3,\n*:not(.active).light-blue-hover:not([disabled]):hover.darken-3,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #0277bd;\n}\n*:not(.active).light-blue.darken-4,\n*:not(.active).light-blue-hover:not([disabled]):hover.darken-4,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #01579b;\n}\n*:not(.active).light-blue.accent-1,\n*:not(.active).light-blue-hover:not([disabled]):hover.accent-1,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #80d8ff;\n}\n*:not(.active).light-blue.accent-2,\n*:not(.active).light-blue-hover:not([disabled]):hover.accent-2,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #40c4ff;\n}\n*:not(.active).light-blue.accent-3,\n*:not(.active).light-blue-hover:not([disabled]):hover.accent-3,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #00b0ff;\n}\n*:not(.active).light-blue.accent-4,\n*:not(.active).light-blue-hover:not([disabled]):hover.accent-4,\n*:not(.active).light-blue-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #0091ea;\n}\n*:not(.active).cyan,\n*:not(.active).cyan-hover:not([disabled]):hover,\n*:not(.active).cyan-hover[hover-active]:not([disabled]) {\n  background-color: #00BCD4;\n}\n*:not(.active).cyan.lighten-1,\n*:not(.active).cyan-hover:not([disabled]):hover.lighten-1,\n*:not(.active).cyan-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).cyan.hover:not([disabled]):hover,\n*:not(.active).cyan-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).cyan-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #26c6da;\n}\n*:not(.active).cyan.lighten-2,\n*:not(.active).cyan-hover:not([disabled]):hover.lighten-2,\n*:not(.active).cyan-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #4dd0e1;\n}\n*:not(.active).cyan.lighten-3,\n*:not(.active).cyan-hover:not([disabled]):hover.lighten-3,\n*:not(.active).cyan-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #80deea;\n}\n*:not(.active).cyan.lighten-4,\n*:not(.active).cyan-hover:not([disabled]):hover.lighten-4,\n*:not(.active).cyan-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #b2ebf2;\n}\n*:not(.active).cyan.lighten-5,\n*:not(.active).cyan-hover:not([disabled]):hover.lighten-5,\n*:not(.active).cyan-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #e0f7fa;\n}\n*:not(.active).cyan.darken-1,\n*:not(.active).cyan-hover:not([disabled]):hover.darken-1,\n*:not(.active).cyan-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #00acc1;\n}\n*:not(.active).cyan.darken-2,\n*:not(.active).cyan-hover:not([disabled]):hover.darken-2,\n*:not(.active).cyan-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #0097a7;\n}\n*:not(.active).cyan.darken-3,\n*:not(.active).cyan-hover:not([disabled]):hover.darken-3,\n*:not(.active).cyan-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #00838f;\n}\n*:not(.active).cyan.darken-4,\n*:not(.active).cyan-hover:not([disabled]):hover.darken-4,\n*:not(.active).cyan-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #006064;\n}\n*:not(.active).cyan.accent-1,\n*:not(.active).cyan-hover:not([disabled]):hover.accent-1,\n*:not(.active).cyan-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #84ffff;\n}\n*:not(.active).cyan.accent-2,\n*:not(.active).cyan-hover:not([disabled]):hover.accent-2,\n*:not(.active).cyan-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #18ffff;\n}\n*:not(.active).cyan.accent-3,\n*:not(.active).cyan-hover:not([disabled]):hover.accent-3,\n*:not(.active).cyan-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #00e5ff;\n}\n*:not(.active).cyan.accent-4,\n*:not(.active).cyan-hover:not([disabled]):hover.accent-4,\n*:not(.active).cyan-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #00b8d4;\n}\n*:not(.active).teal,\n*:not(.active).teal-hover:not([disabled]):hover,\n*:not(.active).teal-hover[hover-active]:not([disabled]) {\n  background-color: #009688;\n}\n*:not(.active).teal.lighten-1,\n*:not(.active).teal-hover:not([disabled]):hover.lighten-1,\n*:not(.active).teal-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).teal.hover:not([disabled]):hover,\n*:not(.active).teal-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).teal-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #26a69a;\n}\n*:not(.active).teal.lighten-2,\n*:not(.active).teal-hover:not([disabled]):hover.lighten-2,\n*:not(.active).teal-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #4db6ac;\n}\n*:not(.active).teal.lighten-3,\n*:not(.active).teal-hover:not([disabled]):hover.lighten-3,\n*:not(.active).teal-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #80cbc4;\n}\n*:not(.active).teal.lighten-4,\n*:not(.active).teal-hover:not([disabled]):hover.lighten-4,\n*:not(.active).teal-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #b2dfdb;\n}\n*:not(.active).teal.lighten-5,\n*:not(.active).teal-hover:not([disabled]):hover.lighten-5,\n*:not(.active).teal-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #e0f2f1;\n}\n*:not(.active).teal.darken-1,\n*:not(.active).teal-hover:not([disabled]):hover.darken-1,\n*:not(.active).teal-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #00897b;\n}\n*:not(.active).teal.darken-2,\n*:not(.active).teal-hover:not([disabled]):hover.darken-2,\n*:not(.active).teal-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #00796b;\n}\n*:not(.active).teal.darken-3,\n*:not(.active).teal-hover:not([disabled]):hover.darken-3,\n*:not(.active).teal-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #00695c;\n}\n*:not(.active).teal.darken-4,\n*:not(.active).teal-hover:not([disabled]):hover.darken-4,\n*:not(.active).teal-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #004d40;\n}\n*:not(.active).teal.accent-1,\n*:not(.active).teal-hover:not([disabled]):hover.accent-1,\n*:not(.active).teal-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #a7ffeb;\n}\n*:not(.active).teal.accent-2,\n*:not(.active).teal-hover:not([disabled]):hover.accent-2,\n*:not(.active).teal-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #64ffda;\n}\n*:not(.active).teal.accent-3,\n*:not(.active).teal-hover:not([disabled]):hover.accent-3,\n*:not(.active).teal-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #1de9b6;\n}\n*:not(.active).teal.accent-4,\n*:not(.active).teal-hover:not([disabled]):hover.accent-4,\n*:not(.active).teal-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #00bfa5;\n}\n*:not(.active).green,\n*:not(.active).green-hover:not([disabled]):hover,\n*:not(.active).green-hover[hover-active]:not([disabled]) {\n  background-color: #4caf50;\n}\n*:not(.active).green.lighten-1,\n*:not(.active).green-hover:not([disabled]):hover.lighten-1,\n*:not(.active).green-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).green.hover:not([disabled]):hover,\n*:not(.active).green-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).green-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #66bb6a;\n}\n*:not(.active).green.lighten-2,\n*:not(.active).green-hover:not([disabled]):hover.lighten-2,\n*:not(.active).green-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #81c784;\n}\n*:not(.active).green.lighten-3,\n*:not(.active).green-hover:not([disabled]):hover.lighten-3,\n*:not(.active).green-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #a5d6a7;\n}\n*:not(.active).green.lighten-4,\n*:not(.active).green-hover:not([disabled]):hover.lighten-4,\n*:not(.active).green-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #c8e6c9;\n}\n*:not(.active).green.lighten-5,\n*:not(.active).green-hover:not([disabled]):hover.lighten-5,\n*:not(.active).green-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #e8f5e9;\n}\n*:not(.active).green.darken-1,\n*:not(.active).green-hover:not([disabled]):hover.darken-1,\n*:not(.active).green-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #43a047;\n}\n*:not(.active).green.darken-2,\n*:not(.active).green-hover:not([disabled]):hover.darken-2,\n*:not(.active).green-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #388e3c;\n}\n*:not(.active).green.darken-3,\n*:not(.active).green-hover:not([disabled]):hover.darken-3,\n*:not(.active).green-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #2e7d32;\n}\n*:not(.active).green.darken-4,\n*:not(.active).green-hover:not([disabled]):hover.darken-4,\n*:not(.active).green-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #1b5e20;\n}\n*:not(.active).green.accent-1,\n*:not(.active).green-hover:not([disabled]):hover.accent-1,\n*:not(.active).green-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #b9f6ca;\n}\n*:not(.active).green.accent-2,\n*:not(.active).green-hover:not([disabled]):hover.accent-2,\n*:not(.active).green-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #69f0ae;\n}\n*:not(.active).green.accent-3,\n*:not(.active).green-hover:not([disabled]):hover.accent-3,\n*:not(.active).green-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #00e676;\n}\n*:not(.active).green.accent-4,\n*:not(.active).green-hover:not([disabled]):hover.accent-4,\n*:not(.active).green-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #00c853;\n}\n*:not(.active).light-green,\n*:not(.active).light-green-hover:not([disabled]):hover,\n*:not(.active).light-green-hover[hover-active]:not([disabled]) {\n  background-color: #8bc34a;\n}\n*:not(.active).light-green.lighten-1,\n*:not(.active).light-green-hover:not([disabled]):hover.lighten-1,\n*:not(.active).light-green-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).light-green.hover:not([disabled]):hover,\n*:not(.active).light-green-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).light-green-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #9ccc65;\n}\n*:not(.active).light-green.lighten-2,\n*:not(.active).light-green-hover:not([disabled]):hover.lighten-2,\n*:not(.active).light-green-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #aed581;\n}\n*:not(.active).light-green.lighten-3,\n*:not(.active).light-green-hover:not([disabled]):hover.lighten-3,\n*:not(.active).light-green-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #c5e1a5;\n}\n*:not(.active).light-green.lighten-4,\n*:not(.active).light-green-hover:not([disabled]):hover.lighten-4,\n*:not(.active).light-green-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #dcedc8;\n}\n*:not(.active).light-green.lighten-5,\n*:not(.active).light-green-hover:not([disabled]):hover.lighten-5,\n*:not(.active).light-green-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #f1f8e9;\n}\n*:not(.active).light-green.darken-1,\n*:not(.active).light-green-hover:not([disabled]):hover.darken-1,\n*:not(.active).light-green-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #7cb342;\n}\n*:not(.active).light-green.darken-2,\n*:not(.active).light-green-hover:not([disabled]):hover.darken-2,\n*:not(.active).light-green-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #689f38;\n}\n*:not(.active).light-green.darken-3,\n*:not(.active).light-green-hover:not([disabled]):hover.darken-3,\n*:not(.active).light-green-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #558b2f;\n}\n*:not(.active).light-green.darken-4,\n*:not(.active).light-green-hover:not([disabled]):hover.darken-4,\n*:not(.active).light-green-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #33691e;\n}\n*:not(.active).light-green.accent-1,\n*:not(.active).light-green-hover:not([disabled]):hover.accent-1,\n*:not(.active).light-green-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #ccff90;\n}\n*:not(.active).light-green.accent-2,\n*:not(.active).light-green-hover:not([disabled]):hover.accent-2,\n*:not(.active).light-green-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #b2ff59;\n}\n*:not(.active).light-green.accent-3,\n*:not(.active).light-green-hover:not([disabled]):hover.accent-3,\n*:not(.active).light-green-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #76ff03;\n}\n*:not(.active).light-green.accent-4,\n*:not(.active).light-green-hover:not([disabled]):hover.accent-4,\n*:not(.active).light-green-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #64dd17;\n}\n*:not(.active) .lime,\n*:not(.active).lime-hover:not([disabled]):hover,\n*:not(.active) .lime-hover[hover-active]:not([disabled]) {\n  background-color: #cddc39;\n}\n*:not(.active) .lime.lighten-1,\n*:not(.active).lime-hover:not([disabled]):hover.lighten-1,\n*:not(.active) .lime-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active) .lime.hover:not([disabled]):hover,\n*:not(.active).lime-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active) .lime-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #d4e157;\n}\n*:not(.active) .lime.lighten-2,\n*:not(.active).lime-hover:not([disabled]):hover.lighten-2,\n*:not(.active) .lime-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #dce775;\n}\n*:not(.active) .lime.lighten-3,\n*:not(.active).lime-hover:not([disabled]):hover.lighten-3,\n*:not(.active) .lime-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #e6ee9c;\n}\n*:not(.active) .lime.lighten-4,\n*:not(.active).lime-hover:not([disabled]):hover.lighten-4,\n*:not(.active) .lime-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #f0f4c3;\n}\n*:not(.active) .lime.lighten-5,\n*:not(.active).lime-hover:not([disabled]):hover.lighten-5,\n*:not(.active) .lime-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #f9fbe7;\n}\n*:not(.active) .lime.darken-1,\n*:not(.active).lime-hover:not([disabled]):hover.darken-1,\n*:not(.active) .lime-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #c0ca33;\n}\n*:not(.active) .lime.darken-2,\n*:not(.active).lime-hover:not([disabled]):hover.darken-2,\n*:not(.active) .lime-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #afb42b;\n}\n*:not(.active) .lime.darken-3,\n*:not(.active).lime-hover:not([disabled]):hover.darken-3,\n*:not(.active) .lime-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #9e9d24;\n}\n*:not(.active) .lime.darken-4,\n*:not(.active).lime-hover:not([disabled]):hover.darken-4,\n*:not(.active) .lime-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #827717;\n}\n*:not(.active) .lime.accent-1,\n*:not(.active).lime-hover:not([disabled]):hover.accent-1,\n*:not(.active) .lime-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #f4ff81;\n}\n*:not(.active) .lime.accent-2,\n*:not(.active).lime-hover:not([disabled]):hover.accent-2,\n*:not(.active) .lime-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #eeff41;\n}\n*:not(.active) .lime.accent-3,\n*:not(.active).lime-hover:not([disabled]):hover.accent-3,\n*:not(.active) .lime-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #c6ff00;\n}\n*:not(.active) .lime.accent-4,\n*:not(.active).lime-hover:not([disabled]):hover.accent-4,\n*:not(.active) .lime-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #aeea00;\n}\n*:not(.active).yellow,\n*:not(.active).yellow-hover:not([disabled]):hover,\n*:not(.active).yellow-hover[hover-active]:not([disabled]) {\n  background-color: #ffeb3b;\n}\n*:not(.active).yellow.lighten-1,\n*:not(.active).yellow-hover:not([disabled]):hover.lighten-1,\n*:not(.active).yellow-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).yellow.hover:not([disabled]):hover,\n*:not(.active).yellow-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).yellow-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #ffee58;\n}\n*:not(.active).yellow.lighten-2,\n*:not(.active).yellow-hover:not([disabled]):hover.lighten-2,\n*:not(.active).yellow-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #fff176;\n}\n*:not(.active).yellow.lighten-3,\n*:not(.active).yellow-hover:not([disabled]):hover.lighten-3,\n*:not(.active).yellow-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #fff59d;\n}\n*:not(.active).yellow.lighten-4,\n*:not(.active).yellow-hover:not([disabled]):hover.lighten-4,\n*:not(.active).yellow-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #fff9c4;\n}\n*:not(.active).yellow.lighten-5,\n*:not(.active).yellow-hover:not([disabled]):hover.lighten-5,\n*:not(.active).yellow-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #fffde7;\n}\n*:not(.active).yellow.darken-1,\n*:not(.active).yellow-hover:not([disabled]):hover.darken-1,\n*:not(.active).yellow-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #fdd835;\n}\n*:not(.active).yellow.darken-2,\n*:not(.active).yellow-hover:not([disabled]):hover.darken-2,\n*:not(.active).yellow-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #fbc02d;\n}\n*:not(.active).yellow.darken-3,\n*:not(.active).yellow-hover:not([disabled]):hover.darken-3,\n*:not(.active).yellow-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #f9a825;\n}\n*:not(.active).yellow.darken-4,\n*:not(.active).yellow-hover:not([disabled]):hover.darken-4,\n*:not(.active).yellow-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #f57f17;\n}\n*:not(.active).yellow.accent-1,\n*:not(.active).yellow-hover:not([disabled]):hover.accent-1,\n*:not(.active).yellow-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #ffff8d;\n}\n*:not(.active).yellow.accent-2,\n*:not(.active).yellow-hover:not([disabled]):hover.accent-2,\n*:not(.active).yellow-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #ffff00;\n}\n*:not(.active).yellow.accent-3,\n*:not(.active).yellow-hover:not([disabled]):hover.accent-3,\n*:not(.active).yellow-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #ffea00;\n}\n*:not(.active).yellow.accent-4,\n*:not(.active).yellow-hover:not([disabled]):hover.accent-4,\n*:not(.active).yellow-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #ffd600;\n}\n*:not(.active).amber,\n*:not(.active).amber-hover:not([disabled]):hover,\n*:not(.active).amber-hover[hover-active]:not([disabled]) {\n  background-color: #FFC107;\n}\n*:not(.active).amber.lighten-1,\n*:not(.active).amber-hover:not([disabled]):hover.lighten-1,\n*:not(.active).amber-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).amber.hover:not([disabled]):hover,\n*:not(.active).amber-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).amber-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #ffca28;\n}\n*:not(.active).amber.lighten-2,\n*:not(.active).amber-hover:not([disabled]):hover.lighten-2,\n*:not(.active).amber-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #ffd54f;\n}\n*:not(.active).amber.lighten-3,\n*:not(.active).amber-hover:not([disabled]):hover.lighten-3,\n*:not(.active).amber-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #ffe082;\n}\n*:not(.active).amber.lighten-4,\n*:not(.active).amber-hover:not([disabled]):hover.lighten-4,\n*:not(.active).amber-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #ffecb3;\n}\n*:not(.active).amber.lighten-5,\n*:not(.active).amber-hover:not([disabled]):hover.lighten-5,\n*:not(.active).amber-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #fff8e1;\n}\n*:not(.active).amber.darken-1,\n*:not(.active).amber-hover:not([disabled]):hover.darken-1,\n*:not(.active).amber-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #ffb300;\n}\n*:not(.active).amber.darken-2,\n*:not(.active).amber-hover:not([disabled]):hover.darken-2,\n*:not(.active).amber-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #ffa000;\n}\n*:not(.active).amber.darken-3,\n*:not(.active).amber-hover:not([disabled]):hover.darken-3,\n*:not(.active).amber-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #ff8f00;\n}\n*:not(.active).amber.darken-4,\n*:not(.active).amber-hover:not([disabled]):hover.darken-4,\n*:not(.active).amber-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #ff6f00;\n}\n*:not(.active).amber.accent-1,\n*:not(.active).amber-hover:not([disabled]):hover.accent-1,\n*:not(.active).amber-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #ffe57f;\n}\n*:not(.active).amber.accent-2,\n*:not(.active).amber-hover:not([disabled]):hover.accent-2,\n*:not(.active).amber-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #ffd740;\n}\n*:not(.active).amber.accent-3,\n*:not(.active).amber-hover:not([disabled]):hover.accent-3,\n*:not(.active).amber-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #ffc400;\n}\n*:not(.active).amber.accent-4,\n*:not(.active).amber-hover:not([disabled]):hover.accent-4,\n*:not(.active).amber-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #ffab00;\n}\n*:not(.active).orange,\n*:not(.active).orange-hover:not([disabled]):hover,\n*:not(.active).orange-hover[hover-active]:not([disabled]) {\n  background-color: #FF9800;\n}\n*:not(.active).orange.lighten-1,\n*:not(.active).orange-hover:not([disabled]):hover.lighten-1,\n*:not(.active).orange-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).orange.hover:not([disabled]):hover,\n*:not(.active).orange-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).orange-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #ffa726;\n}\n*:not(.active).orange.lighten-2,\n*:not(.active).orange-hover:not([disabled]):hover.lighten-2,\n*:not(.active).orange-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #ffb74d;\n}\n*:not(.active).orange.lighten-3,\n*:not(.active).orange-hover:not([disabled]):hover.lighten-3,\n*:not(.active).orange-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #ffcc80;\n}\n*:not(.active).orange.lighten-4,\n*:not(.active).orange-hover:not([disabled]):hover.lighten-4,\n*:not(.active).orange-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #ffe0b2;\n}\n*:not(.active).orange.lighten-5,\n*:not(.active).orange-hover:not([disabled]):hover.lighten-5,\n*:not(.active).orange-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #fff3e0;\n}\n*:not(.active).orange.darken-1,\n*:not(.active).orange-hover:not([disabled]):hover.darken-1,\n*:not(.active).orange-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #fb8c00;\n}\n*:not(.active).orange.darken-2,\n*:not(.active).orange-hover:not([disabled]):hover.darken-2,\n*:not(.active).orange-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #f57c00;\n}\n*:not(.active).orange.darken-3,\n*:not(.active).orange-hover:not([disabled]):hover.darken-3,\n*:not(.active).orange-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #ef6c00;\n}\n*:not(.active).orange.darken-4,\n*:not(.active).orange-hover:not([disabled]):hover.darken-4,\n*:not(.active).orange-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #e65100;\n}\n*:not(.active).orange.accent-1,\n*:not(.active).orange-hover:not([disabled]):hover.accent-1,\n*:not(.active).orange-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #ffd180;\n}\n*:not(.active).orange.accent-2,\n*:not(.active).orange-hover:not([disabled]):hover.accent-2,\n*:not(.active).orange-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #ffab40;\n}\n*:not(.active).orange.accent-3,\n*:not(.active).orange-hover:not([disabled]):hover.accent-3,\n*:not(.active).orange-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #ff9100;\n}\n*:not(.active).orange.accent-4,\n*:not(.active).orange-hover:not([disabled]):hover.accent-4,\n*:not(.active).orange-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #ff6d00;\n}\n*:not(.active).deep-orange,\n*:not(.active).deep-orange-hover:not([disabled]):hover,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]) {\n  background-color: #ff5722;\n}\n*:not(.active).deep-orange.lighten-1,\n*:not(.active).deep-orange-hover:not([disabled]):hover.lighten-1,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).deep-orange.hover:not([disabled]):hover,\n*:not(.active).deep-orange-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #ff7043;\n}\n*:not(.active).deep-orange.lighten-2,\n*:not(.active).deep-orange-hover:not([disabled]):hover.lighten-2,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #ff8a65;\n}\n*:not(.active).deep-orange.lighten-3,\n*:not(.active).deep-orange-hover:not([disabled]):hover.lighten-3,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #ffab91;\n}\n*:not(.active).deep-orange.lighten-4,\n*:not(.active).deep-orange-hover:not([disabled]):hover.lighten-4,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #ffccbc;\n}\n*:not(.active).deep-orange.lighten-5,\n*:not(.active).deep-orange-hover:not([disabled]):hover.lighten-5,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #fbe9e7;\n}\n*:not(.active).deep-orange.darken-1,\n*:not(.active).deep-orange-hover:not([disabled]):hover.darken-1,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #f4511e;\n}\n*:not(.active).deep-orange.darken-2,\n*:not(.active).deep-orange-hover:not([disabled]):hover.darken-2,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #e64a19;\n}\n*:not(.active).deep-orange.darken-3,\n*:not(.active).deep-orange-hover:not([disabled]):hover.darken-3,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #d84315;\n}\n*:not(.active).deep-orange.darken-4,\n*:not(.active).deep-orange-hover:not([disabled]):hover.darken-4,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #bf360c;\n}\n*:not(.active).deep-orange.accent-1,\n*:not(.active).deep-orange-hover:not([disabled]):hover.accent-1,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #ff9e80;\n}\n*:not(.active).deep-orange.accent-2,\n*:not(.active).deep-orange-hover:not([disabled]):hover.accent-2,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #ff6e40;\n}\n*:not(.active).deep-orange.accent-3,\n*:not(.active).deep-orange-hover:not([disabled]):hover.accent-3,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #ff3d00;\n}\n*:not(.active).deep-orange.accent-4,\n*:not(.active).deep-orange-hover:not([disabled]):hover.accent-4,\n*:not(.active).deep-orange-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #dd2c00;\n}\n*:not(.active).brown,\n*:not(.active).brown-hover:not([disabled]):hover,\n*:not(.active).brown-hover[hover-active]:not([disabled]) {\n  background-color: #795548;\n}\n*:not(.active).brown.lighten-1,\n*:not(.active).brown-hover:not([disabled]):hover.lighten-1,\n*:not(.active).brown-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).brown.hover:not([disabled]):hover,\n*:not(.active).brown-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).brown-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #8d6e63;\n}\n*:not(.active).brown.lighten-2,\n*:not(.active).brown-hover:not([disabled]):hover.lighten-2,\n*:not(.active).brown-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #a1887f;\n}\n*:not(.active).brown.lighten-3,\n*:not(.active).brown-hover:not([disabled]):hover.lighten-3,\n*:not(.active).brown-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #bcaaa4;\n}\n*:not(.active).brown.lighten-4,\n*:not(.active).brown-hover:not([disabled]):hover.lighten-4,\n*:not(.active).brown-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #d7ccc8;\n}\n*:not(.active).brown.lighten-5,\n*:not(.active).brown-hover:not([disabled]):hover.lighten-5,\n*:not(.active).brown-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #efebe9;\n}\n*:not(.active).brown.darken-1,\n*:not(.active).brown-hover:not([disabled]):hover.darken-1,\n*:not(.active).brown-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #6d4c41;\n}\n*:not(.active).brown.darken-2,\n*:not(.active).brown-hover:not([disabled]):hover.darken-2,\n*:not(.active).brown-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #5d4037;\n}\n*:not(.active).brown.darken-3,\n*:not(.active).brown-hover:not([disabled]):hover.darken-3,\n*:not(.active).brown-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #4e342e;\n}\n*:not(.active).brown.darken-4,\n*:not(.active).brown-hover:not([disabled]):hover.darken-4,\n*:not(.active).brown-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #3e2723;\n}\n*:not(.active).gray,\n*:not(.active).gray-hover:not([disabled]):hover,\n*:not(.active).gray-hover[hover-active]:not([disabled]) {\n  background-color: #9e9e9e;\n}\n*:not(.active).gray.lighten-1,\n*:not(.active).gray-hover:not([disabled]):hover.lighten-1,\n*:not(.active).gray-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).gray.hover:not([disabled]):hover,\n*:not(.active).gray-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).gray-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #bdbdbd;\n}\n*:not(.active).gray.lighten-2,\n*:not(.active).gray-hover:not([disabled]):hover.lighten-2,\n*:not(.active).gray-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #e0e0e0;\n}\n*:not(.active).gray.lighten-3,\n*:not(.active).gray-hover:not([disabled]):hover.lighten-3,\n*:not(.active).gray-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #eeeeee;\n}\n*:not(.active).gray.lighten-4,\n*:not(.active).gray-hover:not([disabled]):hover.lighten-4,\n*:not(.active).gray-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #f5f5f5;\n}\n*:not(.active).gray.lighten-5,\n*:not(.active).gray-hover:not([disabled]):hover.lighten-5,\n*:not(.active).gray-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #fafafa;\n}\n*:not(.active).gray.darken-1,\n*:not(.active).gray-hover:not([disabled]):hover.darken-1,\n*:not(.active).gray-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #757575;\n}\n*:not(.active).gray.darken-2,\n*:not(.active).gray-hover:not([disabled]):hover.darken-2,\n*:not(.active).gray-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #616161;\n}\n*:not(.active).gray.darken-3,\n*:not(.active).gray-hover:not([disabled]):hover.darken-3,\n*:not(.active).gray-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #424242;\n}\n*:not(.active).gray.darken-4,\n*:not(.active).gray-hover:not([disabled]):hover.darken-4,\n*:not(.active).gray-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #212121;\n}\n*:not(.active).blue-gray,\n*:not(.active).blue-gray-hover:not([disabled]):hover,\n*:not(.active).blue-gray-hover[hover-active]:not([disabled]) {\n  background-color: #607d8b;\n}\n*:not(.active).blue-gray.lighten-1,\n*:not(.active).blue-gray-hover:not([disabled]):hover.lighten-1,\n*:not(.active).blue-gray-hover[hover-active]:not([disabled]).lighten-1,\n*:not(.active).blue-gray.hover:not([disabled]):hover,\n*:not(.active).blue-gray-hover:not([disabled]):hover.hover:not([disabled]):hover,\n*:not(.active).blue-gray-hover[hover-active]:not([disabled]).hover:not([disabled]):hover {\n  background-color: #78909c;\n}\n*:not(.active).blue-gray.lighten-2,\n*:not(.active).blue-gray-hover:not([disabled]):hover.lighten-2,\n*:not(.active).blue-gray-hover[hover-active]:not([disabled]).lighten-2 {\n  background-color: #90a4ae;\n}\n*:not(.active).blue-gray.lighten-3,\n*:not(.active).blue-gray-hover:not([disabled]):hover.lighten-3,\n*:not(.active).blue-gray-hover[hover-active]:not([disabled]).lighten-3 {\n  background-color: #b0bec5;\n}\n*:not(.active).blue-gray.lighten-4,\n*:not(.active).blue-gray-hover:not([disabled]):hover.lighten-4,\n*:not(.active).blue-gray-hover[hover-active]:not([disabled]).lighten-4 {\n  background-color: #cfd8dc;\n}\n*:not(.active).blue-gray.lighten-5,\n*:not(.active).blue-gray-hover:not([disabled]):hover.lighten-5,\n*:not(.active).blue-gray-hover[hover-active]:not([disabled]).lighten-5 {\n  background-color: #eceff1;\n}\n*:not(.active).blue-gray.darken-1,\n*:not(.active).blue-gray-hover:not([disabled]):hover.darken-1,\n*:not(.active).blue-gray-hover[hover-active]:not([disabled]).darken-1 {\n  background-color: #546e7a;\n}\n*:not(.active).blue-gray.darken-2,\n*:not(.active).blue-gray-hover:not([disabled]):hover.darken-2,\n*:not(.active).blue-gray-hover[hover-active]:not([disabled]).darken-2 {\n  background-color: #455a64;\n}\n*:not(.active).blue-gray.darken-3,\n*:not(.active).blue-gray-hover:not([disabled]):hover.darken-3,\n*:not(.active).blue-gray-hover[hover-active]:not([disabled]).darken-3 {\n  background-color: #37474f;\n}\n*:not(.active).blue-gray.darken-4,\n*:not(.active).blue-gray-hover:not([disabled]):hover.darken-4,\n*:not(.active).blue-gray-hover[hover-active]:not([disabled]).darken-4 {\n  background-color: #263238;\n}\n.red-active.active {\n  background-color: #F44336;\n}\n.red-active.active.lighten-1,\n.red-active.active.hover:not([disabled]):hover {\n  background-color: #ef5350;\n}\n.red-active.active.lighten-2 {\n  background-color: #e57373;\n}\n.red-active.active.lighten-3 {\n  background-color: #ef9a9a;\n}\n.red-active.active.lighten-4 {\n  background-color: #ffcdd2;\n}\n.red-active.active.lighten-5 {\n  background-color: #ffebee;\n}\n.red-active.active.darken-1 {\n  background-color: #e53935;\n}\n.red-active.active.darken-2 {\n  background-color: #d32f2f;\n}\n.red-active.active.darken-3 {\n  background-color: #c62828;\n}\n.red-active.active.darken-4 {\n  background-color: #b71c1c;\n}\n.red-active.active.accent-1 {\n  background-color: #ff8a80;\n}\n.red-active.active.accent-2 {\n  background-color: #ff5252;\n}\n.red-active.active.accent-3 {\n  background-color: #ff1744;\n}\n.red-active.active.accent-4 {\n  background-color: #d50000;\n}\n.pink-active.active {\n  background-color: #E91E63;\n}\n.pink-active.active.lighten-1,\n.pink-active.active.hover:not([disabled]):hover {\n  background-color: #ec407a;\n}\n.pink-active.active.lighten-2 {\n  background-color: #f06292;\n}\n.pink-active.active.lighten-3 {\n  background-color: #f48fb1;\n}\n.pink-active.active.lighten-4 {\n  background-color: #f8bbd0;\n}\n.pink-active.active.lighten-5 {\n  background-color: #fce4ec;\n}\n.pink-active.active.darken-1 {\n  background-color: #d81b60;\n}\n.pink-active.active.darken-2 {\n  background-color: #c2185b;\n}\n.pink-active.active.darken-3 {\n  background-color: #ad1457;\n}\n.pink-active.active.darken-4 {\n  background-color: #880e4f;\n}\n.pink-active.active.accent-1 {\n  background-color: #ff80ab;\n}\n.pink-active.active.accent-2 {\n  background-color: #ff4081;\n}\n.pink-active.active.accent-3 {\n  background-color: #f50057;\n}\n.pink-active.active.accent-4 {\n  background-color: #c51162;\n}\n.purple-active.active {\n  background-color: #9c27b0;\n}\n.purple-active.active.lighten-1,\n.purple-active.active.hover:not([disabled]):hover {\n  background-color: #ab47bc;\n}\n.purple-active.active.lighten-2 {\n  background-color: #ba68c8;\n}\n.purple-active.active.lighten-3 {\n  background-color: #ce93d8;\n}\n.purple-active.active.lighten-4 {\n  background-color: #e1bee7;\n}\n.purple-active.active.lighten-5 {\n  background-color: #f3e5f5;\n}\n.purple-active.active.darken-1 {\n  background-color: #8e24aa;\n}\n.purple-active.active.darken-2 {\n  background-color: #7b1fa2;\n}\n.purple-active.active.darken-3 {\n  background-color: #6a1b9a;\n}\n.purple-active.active.darken-4 {\n  background-color: #4a148c;\n}\n.purple-active.active.accent-1 {\n  background-color: #ea80fc;\n}\n.purple-active.active.accent-2 {\n  background-color: #e040fb;\n}\n.purple-active.active.accent-3 {\n  background-color: #d500f9;\n}\n.purple-active.active.accent-4 {\n  background-color: #aa00ff;\n}\n.deep-purple-active.active {\n  background-color: #673ab7;\n}\n.deep-purple-active.active.lighten-1,\n.deep-purple-active.active.hover:not([disabled]):hover {\n  background-color: #7e57c2;\n}\n.deep-purple-active.active.lighten-2 {\n  background-color: #9575cd;\n}\n.deep-purple-active.active.lighten-3 {\n  background-color: #b39ddb;\n}\n.deep-purple-active.active.lighten-4 {\n  background-color: #d1c4e9;\n}\n.deep-purple-active.active.lighten-5 {\n  background-color: #ede7f6;\n}\n.deep-purple-active.active.darken-1 {\n  background-color: #5e35b1;\n}\n.deep-purple-active.active.darken-2 {\n  background-color: #512da8;\n}\n.deep-purple-active.active.darken-3 {\n  background-color: #4527a0;\n}\n.deep-purple-active.active.darken-4 {\n  background-color: #311b92;\n}\n.deep-purple-active.active.accent-1 {\n  background-color: #b388ff;\n}\n.deep-purple-active.active.accent-2 {\n  background-color: #7c4dff;\n}\n.deep-purple-active.active.accent-3 {\n  background-color: #651fff;\n}\n.deep-purple-active.active.accent-4 {\n  background-color: #6200ea;\n}\n.indigo-active.active {\n  background-color: #3F51b5;\n}\n.indigo-active.active.lighten-1,\n.indigo-active.active.hover:not([disabled]):hover {\n  background-color: #5c6bc0;\n}\n.indigo-active.active.lighten-2 {\n  background-color: #7986cb;\n}\n.indigo-active.active.lighten-3 {\n  background-color: #9fa8da;\n}\n.indigo-active.active.lighten-4 {\n  background-color: #c5cae9;\n}\n.indigo-active.active.lighten-5 {\n  background-color: #e8eaf6;\n}\n.indigo-active.active.darken-1 {\n  background-color: #3949ab;\n}\n.indigo-active.active.darken-2 {\n  background-color: #303f9f;\n}\n.indigo-active.active.darken-3 {\n  background-color: #283593;\n}\n.indigo-active.active.darken-4 {\n  background-color: #1a237e;\n}\n.indigo-active.active.accent-1 {\n  background-color: #8c9eff;\n}\n.indigo-active.active.accent-2 {\n  background-color: #536dfe;\n}\n.indigo-active.active.accent-3 {\n  background-color: #3d5afe;\n}\n.indigo-active.active.accent-4 {\n  background-color: #304ffe;\n}\n.blue-active.active {\n  background-color: #2196F3;\n}\n.blue-active.active.lighten-1,\n.blue-active.active.hover:not([disabled]):hover {\n  background-color: #42a5f5;\n}\n.blue-active.active.lighten-2 {\n  background-color: #64b5f6;\n}\n.blue-active.active.lighten-3 {\n  background-color: #90caf9;\n}\n.blue-active.active.lighten-4 {\n  background-color: #bbdefb;\n}\n.blue-active.active.lighten-5 {\n  background-color: #e3f2fd;\n}\n.blue-active.active.darken-1 {\n  background-color: #1e88e5;\n}\n.blue-active.active.darken-2 {\n  background-color: #1976d2;\n}\n.blue-active.active.darken-3 {\n  background-color: #1565c0;\n}\n.blue-active.active.darken-4 {\n  background-color: #0d47a1;\n}\n.blue-active.active.accent-1 {\n  background-color: #82b1ff;\n}\n.blue-active.active.accent-2 {\n  background-color: #448aff;\n}\n.blue-active.active.accent-3 {\n  background-color: #2979ff;\n}\n.blue-active.active.accent-4 {\n  background-color: #2962ff;\n}\n.light-blue-active.active {\n  background-color: #03A9F4;\n}\n.light-blue-active.active.lighten-1,\n.light-blue-active.active.hover:not([disabled]):hover {\n  background-color: #29b6f6;\n}\n.light-blue-active.active.lighten-2 {\n  background-color: #4fc3f7;\n}\n.light-blue-active.active.lighten-3 {\n  background-color: #81d4fa;\n}\n.light-blue-active.active.lighten-4 {\n  background-color: #b3e5fc;\n}\n.light-blue-active.active.lighten-5 {\n  background-color: #e1f5fe;\n}\n.light-blue-active.active.darken-1 {\n  background-color: #039be5;\n}\n.light-blue-active.active.darken-2 {\n  background-color: #0288d1;\n}\n.light-blue-active.active.darken-3 {\n  background-color: #0277bd;\n}\n.light-blue-active.active.darken-4 {\n  background-color: #01579b;\n}\n.light-blue-active.active.accent-1 {\n  background-color: #80d8ff;\n}\n.light-blue-active.active.accent-2 {\n  background-color: #40c4ff;\n}\n.light-blue-active.active.accent-3 {\n  background-color: #00b0ff;\n}\n.light-blue-active.active.accent-4 {\n  background-color: #0091ea;\n}\n.cyan-active.active {\n  background-color: #00BCD4;\n}\n.cyan-active.active.lighten-1,\n.cyan-active.active.hover:not([disabled]):hover {\n  background-color: #26c6da;\n}\n.cyan-active.active.lighten-2 {\n  background-color: #4dd0e1;\n}\n.cyan-active.active.lighten-3 {\n  background-color: #80deea;\n}\n.cyan-active.active.lighten-4 {\n  background-color: #b2ebf2;\n}\n.cyan-active.active.lighten-5 {\n  background-color: #e0f7fa;\n}\n.cyan-active.active.darken-1 {\n  background-color: #00acc1;\n}\n.cyan-active.active.darken-2 {\n  background-color: #0097a7;\n}\n.cyan-active.active.darken-3 {\n  background-color: #00838f;\n}\n.cyan-active.active.darken-4 {\n  background-color: #006064;\n}\n.cyan-active.active.accent-1 {\n  background-color: #84ffff;\n}\n.cyan-active.active.accent-2 {\n  background-color: #18ffff;\n}\n.cyan-active.active.accent-3 {\n  background-color: #00e5ff;\n}\n.cyan-active.active.accent-4 {\n  background-color: #00b8d4;\n}\n.teal-active.active {\n  background-color: #009688;\n}\n.teal-active.active.lighten-1,\n.teal-active.active.hover:not([disabled]):hover {\n  background-color: #26a69a;\n}\n.teal-active.active.lighten-2 {\n  background-color: #4db6ac;\n}\n.teal-active.active.lighten-3 {\n  background-color: #80cbc4;\n}\n.teal-active.active.lighten-4 {\n  background-color: #b2dfdb;\n}\n.teal-active.active.lighten-5 {\n  background-color: #e0f2f1;\n}\n.teal-active.active.darken-1 {\n  background-color: #00897b;\n}\n.teal-active.active.darken-2 {\n  background-color: #00796b;\n}\n.teal-active.active.darken-3 {\n  background-color: #00695c;\n}\n.teal-active.active.darken-4 {\n  background-color: #004d40;\n}\n.teal-active.active.accent-1 {\n  background-color: #a7ffeb;\n}\n.teal-active.active.accent-2 {\n  background-color: #64ffda;\n}\n.teal-active.active.accent-3 {\n  background-color: #1de9b6;\n}\n.teal-active.active.accent-4 {\n  background-color: #00bfa5;\n}\n.green-active.active {\n  background-color: #4caf50;\n}\n.green-active.active.lighten-1,\n.green-active.active.hover:not([disabled]):hover {\n  background-color: #66bb6a;\n}\n.green-active.active.lighten-2 {\n  background-color: #81c784;\n}\n.green-active.active.lighten-3 {\n  background-color: #a5d6a7;\n}\n.green-active.active.lighten-4 {\n  background-color: #c8e6c9;\n}\n.green-active.active.lighten-5 {\n  background-color: #e8f5e9;\n}\n.green-active.active.darken-1 {\n  background-color: #43a047;\n}\n.green-active.active.darken-2 {\n  background-color: #388e3c;\n}\n.green-active.active.darken-3 {\n  background-color: #2e7d32;\n}\n.green-active.active.darken-4 {\n  background-color: #1b5e20;\n}\n.green-active.active.accent-1 {\n  background-color: #b9f6ca;\n}\n.green-active.active.accent-2 {\n  background-color: #69f0ae;\n}\n.green-active.active.accent-3 {\n  background-color: #00e676;\n}\n.green-active.active.accent-4 {\n  background-color: #00c853;\n}\n.light-green-active.active {\n  background-color: #8bc34a;\n}\n.light-green-active.active.lighten-1,\n.light-green-active.active.hover:not([disabled]):hover {\n  background-color: #9ccc65;\n}\n.light-green-active.active.lighten-2 {\n  background-color: #aed581;\n}\n.light-green-active.active.lighten-3 {\n  background-color: #c5e1a5;\n}\n.light-green-active.active.lighten-4 {\n  background-color: #dcedc8;\n}\n.light-green-active.active.lighten-5 {\n  background-color: #f1f8e9;\n}\n.light-green-active.active.darken-1 {\n  background-color: #7cb342;\n}\n.light-green-active.active.darken-2 {\n  background-color: #689f38;\n}\n.light-green-active.active.darken-3 {\n  background-color: #558b2f;\n}\n.light-green-active.active.darken-4 {\n  background-color: #33691e;\n}\n.light-green-active.active.accent-1 {\n  background-color: #ccff90;\n}\n.light-green-active.active.accent-2 {\n  background-color: #b2ff59;\n}\n.light-green-active.active.accent-3 {\n  background-color: #76ff03;\n}\n.light-green-active.active.accent-4 {\n  background-color: #64dd17;\n}\n.lime-active.active {\n  background-color: #cddc39;\n}\n.lime-active.active.lighten-1,\n.lime-active.active.hover:not([disabled]):hover {\n  background-color: #d4e157;\n}\n.lime-active.active.lighten-2 {\n  background-color: #dce775;\n}\n.lime-active.active.lighten-3 {\n  background-color: #e6ee9c;\n}\n.lime-active.active.lighten-4 {\n  background-color: #f0f4c3;\n}\n.lime-active.active.lighten-5 {\n  background-color: #f9fbe7;\n}\n.lime-active.active.darken-1 {\n  background-color: #c0ca33;\n}\n.lime-active.active.darken-2 {\n  background-color: #afb42b;\n}\n.lime-active.active.darken-3 {\n  background-color: #9e9d24;\n}\n.lime-active.active.darken-4 {\n  background-color: #827717;\n}\n.lime-active.active.accent-1 {\n  background-color: #f4ff81;\n}\n.lime-active.active.accent-2 {\n  background-color: #eeff41;\n}\n.lime-active.active.accent-3 {\n  background-color: #c6ff00;\n}\n.lime-active.active.accent-4 {\n  background-color: #aeea00;\n}\n.yellow-active.active {\n  background-color: #ffeb3b;\n}\n.yellow-active.active.lighten-1,\n.yellow-active.active.hover:not([disabled]):hover {\n  background-color: #ffee58;\n}\n.yellow-active.active.lighten-2 {\n  background-color: #fff176;\n}\n.yellow-active.active.lighten-3 {\n  background-color: #fff59d;\n}\n.yellow-active.active.lighten-4 {\n  background-color: #fff9c4;\n}\n.yellow-active.active.lighten-5 {\n  background-color: #fffde7;\n}\n.yellow-active.active.darken-1 {\n  background-color: #fdd835;\n}\n.yellow-active.active.darken-2 {\n  background-color: #fbc02d;\n}\n.yellow-active.active.darken-3 {\n  background-color: #f9a825;\n}\n.yellow-active.active.darken-4 {\n  background-color: #f57f17;\n}\n.yellow-active.active.accent-1 {\n  background-color: #ffff8d;\n}\n.yellow-active.active.accent-2 {\n  background-color: #ffff00;\n}\n.yellow-active.active.accent-3 {\n  background-color: #ffea00;\n}\n.yellow-active.active.accent-4 {\n  background-color: #ffd600;\n}\n.amber-active.active {\n  background-color: #FFC107;\n}\n.amber-active.active.lighten-1,\n.amber-active.active.hover:not([disabled]):hover {\n  background-color: #ffca28;\n}\n.amber-active.active.lighten-2 {\n  background-color: #ffd54f;\n}\n.amber-active.active.lighten-3 {\n  background-color: #ffe082;\n}\n.amber-active.active.lighten-4 {\n  background-color: #ffecb3;\n}\n.amber-active.active.lighten-5 {\n  background-color: #fff8e1;\n}\n.amber-active.active.darken-1 {\n  background-color: #ffb300;\n}\n.amber-active.active.darken-2 {\n  background-color: #ffa000;\n}\n.amber-active.active.darken-3 {\n  background-color: #ff8f00;\n}\n.amber-active.active.darken-4 {\n  background-color: #ff6f00;\n}\n.amber-active.active.accent-1 {\n  background-color: #ffe57f;\n}\n.amber-active.active.accent-2 {\n  background-color: #ffd740;\n}\n.amber-active.active.accent-3 {\n  background-color: #ffc400;\n}\n.amber-active.active.accent-4 {\n  background-color: #ffab00;\n}\n.orange-active.active {\n  background-color: #FF9800;\n}\n.orange-active.active.lighten-1,\n.orange-active.active.hover:not([disabled]):hover {\n  background-color: #ffa726;\n}\n.orange-active.active.lighten-2 {\n  background-color: #ffb74d;\n}\n.orange-active.active.lighten-3 {\n  background-color: #ffcc80;\n}\n.orange-active.active.lighten-4 {\n  background-color: #ffe0b2;\n}\n.orange-active.active.lighten-5 {\n  background-color: #fff3e0;\n}\n.orange-active.active.darken-1 {\n  background-color: #fb8c00;\n}\n.orange-active.active.darken-2 {\n  background-color: #f57c00;\n}\n.orange-active.active.darken-3 {\n  background-color: #ef6c00;\n}\n.orange-active.active.darken-4 {\n  background-color: #e65100;\n}\n.orange-active.active.accent-1 {\n  background-color: #ffd180;\n}\n.orange-active.active.accent-2 {\n  background-color: #ffab40;\n}\n.orange-active.active.accent-3 {\n  background-color: #ff9100;\n}\n.orange-active.active.accent-4 {\n  background-color: #ff6d00;\n}\n.deep-orange-active.active {\n  background-color: #ff5722;\n}\n.deep-orange-active.active.lighten-1,\n.deep-orange-active.active.hover:not([disabled]):hover {\n  background-color: #ff7043;\n}\n.deep-orange-active.active.lighten-2 {\n  background-color: #ff8a65;\n}\n.deep-orange-active.active.lighten-3 {\n  background-color: #ffab91;\n}\n.deep-orange-active.active.lighten-4 {\n  background-color: #ffccbc;\n}\n.deep-orange-active.active.lighten-5 {\n  background-color: #fbe9e7;\n}\n.deep-orange-active.active.darken-1 {\n  background-color: #f4511e;\n}\n.deep-orange-active.active.darken-2 {\n  background-color: #e64a19;\n}\n.deep-orange-active.active.darken-3 {\n  background-color: #d84315;\n}\n.deep-orange-active.active.darken-4 {\n  background-color: #bf360c;\n}\n.deep-orange-active.active.accent-1 {\n  background-color: #ff9e80;\n}\n.deep-orange-active.active.accent-2 {\n  background-color: #ff6e40;\n}\n.deep-orange-active.active.accent-3 {\n  background-color: #ff3d00;\n}\n.deep-orange-active.active.accent-4 {\n  background-color: #dd2c00;\n}\n.brown-active.active {\n  background-color: #795548;\n}\n.brown-active.active.lighten-1,\n.brown-active.active.hover:not([disabled]):hover {\n  background-color: #8d6e63;\n}\n.brown-active.active.lighten-2 {\n  background-color: #a1887f;\n}\n.brown-active.active.lighten-3 {\n  background-color: #bcaaa4;\n}\n.brown-active.active.lighten-4 {\n  background-color: #d7ccc8;\n}\n.brown-active.active.lighten-5 {\n  background-color: #efebe9;\n}\n.brown-active.active.darken-1 {\n  background-color: #6d4c41;\n}\n.brown-active.active.darken-2 {\n  background-color: #5d4037;\n}\n.brown-active.active.darken-3 {\n  background-color: #4e342e;\n}\n.brown-active.active.darken-4 {\n  background-color: #3e2723;\n}\n.gray-active.active {\n  background-color: #9e9e9e;\n}\n.gray-active.active.lighten-1,\n.gray-active.active.hover:not([disabled]):hover {\n  background-color: #bdbdbd;\n}\n.gray-active.active.lighten-2 {\n  background-color: #e0e0e0;\n}\n.gray-active.active.lighten-3 {\n  background-color: #eeeeee;\n}\n.gray-active.active.lighten-4 {\n  background-color: #f5f5f5;\n}\n.gray-active.active.lighten-5 {\n  background-color: #fafafa;\n}\n.gray-active.active.darken-1 {\n  background-color: #757575;\n}\n.gray-active.active.darken-2 {\n  background-color: #616161;\n}\n.gray-active.active.darken-3 {\n  background-color: #424242;\n}\n.gray-active.active.darken-4 {\n  background-color: #212121;\n}\n.blue-gray-active.active {\n  background-color: #607d8b;\n}\n.blue-gray-active.active.lighten-1,\n.blue-gray-active.active.hover:not([disabled]):hover {\n  background-color: #78909c;\n}\n.blue-gray-active.active.lighten-2 {\n  background-color: #90a4ae;\n}\n.blue-gray-active.active.lighten-3 {\n  background-color: #b0bec5;\n}\n.blue-gray-active.active.lighten-4 {\n  background-color: #cfd8dc;\n}\n.blue-gray-active.active.lighten-5 {\n  background-color: #eceff1;\n}\n.blue-gray-active.active.darken-1 {\n  background-color: #546e7a;\n}\n.blue-gray-active.active.darken-2 {\n  background-color: #455a64;\n}\n.blue-gray-active.active.darken-3 {\n  background-color: #37474f;\n}\n.blue-gray-active.active.darken-4 {\n  background-color: #263238;\n}\n/* --- TEXT COLORS */\n.text-light-blue {\n  color: #03A9F4;\n}\n.text-light-blue.lighten-1 {\n  color: #29b6f6;\n}\n.text-light-blue.lighten-2 {\n  color: #4fc3f7;\n}\n.text-light-blue.lighten-3 {\n  color: #81d4fa;\n}\n.text-light-blue.lighten-4 {\n  color: #b3e5fc;\n}\n.text-light-blue.lighten-5 {\n  color: #e1f5fe;\n}\n.text-light-blue.darken-1 {\n  color: #039be5;\n}\n.text-light-blue.darken-2 {\n  color: #0288d1;\n}\n.text-light-blue.darken-3 {\n  color: #0277bd;\n}\n.text-light-blue.darken-4 {\n  color: #01579b;\n}\n.text-light-blue.accent-1 {\n  color: #80d8ff;\n}\n.text-light-blue.accent-2 {\n  color: #40c4ff;\n}\n.text-light-blue.accent-3 {\n  color: #00b0ff;\n}\n.text-light-blue.accent-4 {\n  color: #0091ea;\n}\n.text-cyan {\n  color: #00BCD4;\n}\n.text-cyan.lighten-1 {\n  color: #26c6da;\n}\n.text-cyan.lighten-2 {\n  color: #4dd0e1;\n}\n.text-cyan.lighten-3 {\n  color: #80deea;\n}\n.text-cyan.lighten-4 {\n  color: #b2ebf2;\n}\n.text-cyan.lighten-5 {\n  color: #e0f7fa;\n}\n.text-cyan.darken-1 {\n  color: #00acc1;\n}\n.text-cyan.darken-2 {\n  color: #0097a7;\n}\n.text-cyan.darken-3 {\n  color: #00838f;\n}\n.text-cyan.darken-4 {\n  color: #006064;\n}\n.text-cyan.accent-1 {\n  color: #84ffff;\n}\n.text-cyan.accent-2 {\n  color: #18ffff;\n}\n.text-cyan.accent-3 {\n  color: #00e5ff;\n}\n.text-cyan.accent-4 {\n  color: #00b8d4;\n}\n*.text-red,\n*.text-red-hover:not([disabled]):hover,\n*.text-red-hover[hover-active]:not([disabled]) {\n  color: #F44336;\n}\n*.text-red.lighten-1,\n*.text-red-hover:not([disabled]):hover.lighten-1,\n*.text-red-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #ef5350;\n}\n*.text-red.lighten-2,\n*.text-red-hover:not([disabled]):hover.lighten-2,\n*.text-red-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #e57373;\n}\n*.text-red.lighten-3,\n*.text-red-hover:not([disabled]):hover.lighten-3,\n*.text-red-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #ef9a9a;\n}\n*.text-red.lighten-4,\n*.text-red-hover:not([disabled]):hover.lighten-4,\n*.text-red-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #ffcdd2;\n}\n*.text-red.lighten-5,\n*.text-red-hover:not([disabled]):hover.lighten-5,\n*.text-red-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #ffebee;\n}\n*.text-red.darken-1,\n*.text-red-hover:not([disabled]):hover.darken-1,\n*.text-red-hover[hover-active]:not([disabled]).darken-1 {\n  color: #e53935;\n}\n*.text-red.darken-2,\n*.text-red-hover:not([disabled]):hover.darken-2,\n*.text-red-hover[hover-active]:not([disabled]).darken-2 {\n  color: #d32f2f;\n}\n*.text-red.darken-3,\n*.text-red-hover:not([disabled]):hover.darken-3,\n*.text-red-hover[hover-active]:not([disabled]).darken-3 {\n  color: #c62828;\n}\n*.text-red.darken-4,\n*.text-red-hover:not([disabled]):hover.darken-4,\n*.text-red-hover[hover-active]:not([disabled]).darken-4 {\n  color: #b71c1c;\n}\n*.text-red.accent-1,\n*.text-red-hover:not([disabled]):hover.accent-1,\n*.text-red-hover[hover-active]:not([disabled]).accent-1 {\n  color: #ff8a80;\n}\n*.text-red.accent-2,\n*.text-red-hover:not([disabled]):hover.accent-2,\n*.text-red-hover[hover-active]:not([disabled]).accent-2 {\n  color: #ff5252;\n}\n*.text-red.accent-3,\n*.text-red-hover:not([disabled]):hover.accent-3,\n*.text-red-hover[hover-active]:not([disabled]).accent-3 {\n  color: #ff1744;\n}\n*.text-red.accent-4,\n*.text-red-hover:not([disabled]):hover.accent-4,\n*.text-red-hover[hover-active]:not([disabled]).accent-4 {\n  color: #d50000;\n}\n*.text-pink,\n*.text-pink-hover:not([disabled]):hover,\n*.text-pink-hover[hover-active]:not([disabled]) {\n  color: #E91E63;\n}\n*.text-pink.lighten-1,\n*.text-pink-hover:not([disabled]):hover.lighten-1,\n*.text-pink-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #ec407a;\n}\n*.text-pink.lighten-2,\n*.text-pink-hover:not([disabled]):hover.lighten-2,\n*.text-pink-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #f06292;\n}\n*.text-pink.lighten-3,\n*.text-pink-hover:not([disabled]):hover.lighten-3,\n*.text-pink-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #f48fb1;\n}\n*.text-pink.lighten-4,\n*.text-pink-hover:not([disabled]):hover.lighten-4,\n*.text-pink-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #f8bbd0;\n}\n*.text-pink.lighten-5,\n*.text-pink-hover:not([disabled]):hover.lighten-5,\n*.text-pink-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #fce4ec;\n}\n*.text-pink.darken-1,\n*.text-pink-hover:not([disabled]):hover.darken-1,\n*.text-pink-hover[hover-active]:not([disabled]).darken-1 {\n  color: #d81b60;\n}\n*.text-pink.darken-2,\n*.text-pink-hover:not([disabled]):hover.darken-2,\n*.text-pink-hover[hover-active]:not([disabled]).darken-2 {\n  color: #c2185b;\n}\n*.text-pink.darken-3,\n*.text-pink-hover:not([disabled]):hover.darken-3,\n*.text-pink-hover[hover-active]:not([disabled]).darken-3 {\n  color: #ad1457;\n}\n*.text-pink.darken-4,\n*.text-pink-hover:not([disabled]):hover.darken-4,\n*.text-pink-hover[hover-active]:not([disabled]).darken-4 {\n  color: #880e4f;\n}\n*.text-pink.accent-1,\n*.text-pink-hover:not([disabled]):hover.accent-1,\n*.text-pink-hover[hover-active]:not([disabled]).accent-1 {\n  color: #ff80ab;\n}\n*.text-pink.accent-2,\n*.text-pink-hover:not([disabled]):hover.accent-2,\n*.text-pink-hover[hover-active]:not([disabled]).accent-2 {\n  color: #ff4081;\n}\n*.text-pink.accent-3,\n*.text-pink-hover:not([disabled]):hover.accent-3,\n*.text-pink-hover[hover-active]:not([disabled]).accent-3 {\n  color: #f50057;\n}\n*.text-pink.accent-4,\n*.text-pink-hover:not([disabled]):hover.accent-4,\n*.text-pink-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #c51162;\n}\n*.text-purple,\n*.text-purple-hover:not([disabled]):hover,\n*.text-purple-hover[hover-active]:not([disabled]) {\n  color: #9c27b0;\n}\n*.text-purple.lighten-1,\n*.text-purple-hover:not([disabled]):hover.lighten-1,\n*.text-purple-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #ab47bc;\n}\n*.text-purple.lighten-2,\n*.text-purple-hover:not([disabled]):hover.lighten-2,\n*.text-purple-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #ba68c8;\n}\n*.text-purple.lighten-3,\n*.text-purple-hover:not([disabled]):hover.lighten-3,\n*.text-purple-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #ce93d8;\n}\n*.text-purple.lighten-4,\n*.text-purple-hover:not([disabled]):hover.lighten-4,\n*.text-purple-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #e1bee7;\n}\n*.text-purple.lighten-5,\n*.text-purple-hover:not([disabled]):hover.lighten-5,\n*.text-purple-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #f3e5f5;\n}\n*.text-purple.darken-1,\n*.text-purple-hover:not([disabled]):hover.darken-1,\n*.text-purple-hover[hover-active]:not([disabled]).darken-1 {\n  color: #8e24aa;\n}\n*.text-purple.darken-2,\n*.text-purple-hover:not([disabled]):hover.darken-2,\n*.text-purple-hover[hover-active]:not([disabled]).darken-2 {\n  color: #7b1fa2;\n}\n*.text-purple.darken-3,\n*.text-purple-hover:not([disabled]):hover.darken-3,\n*.text-purple-hover[hover-active]:not([disabled]).darken-3 {\n  color: #6a1b9a;\n}\n*.text-purple.darken-4,\n*.text-purple-hover:not([disabled]):hover.darken-4,\n*.text-purple-hover[hover-active]:not([disabled]).darken-4 {\n  color: #4a148c;\n}\n*.text-purple.accent-1,\n*.text-purple-hover:not([disabled]):hover.accent-1,\n*.text-purple-hover[hover-active]:not([disabled]).accent-1 {\n  color: #ea80fc;\n}\n*.text-purple.accent-2,\n*.text-purple-hover:not([disabled]):hover.accent-2,\n*.text-purple-hover[hover-active]:not([disabled]).accent-2 {\n  color: #e040fb;\n}\n*.text-purple.accent-3,\n*.text-purple-hover:not([disabled]):hover.accent-3,\n*.text-purple-hover[hover-active]:not([disabled]).accent-3 {\n  color: #d500f9;\n}\n*.text-purple.accent-4,\n*.text-purple-hover:not([disabled]):hover.accent-4,\n*.text-purple-hover[hover-active]:not([disabled]).accent-4 {\n  color: #aa00ff;\n}\n*.text-deep-purple,\n*.text-deep-purple-hover:not([disabled]):hover,\n*.text-deep-purple-hover[hover-active]:not([disabled]) {\n  color: #673ab7;\n}\n*.text-deep-purple.lighten-1,\n*.text-deep-purple-hover:not([disabled]):hover.lighten-1,\n*.text-deep-purple-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #7e57c2;\n}\n*.text-deep-purple.lighten-2,\n*.text-deep-purple-hover:not([disabled]):hover.lighten-2,\n*.text-deep-purple-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #9575cd;\n}\n*.text-deep-purple.lighten-3,\n*.text-deep-purple-hover:not([disabled]):hover.lighten-3,\n*.text-deep-purple-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #b39ddb;\n}\n*.text-deep-purple.lighten-4,\n*.text-deep-purple-hover:not([disabled]):hover.lighten-4,\n*.text-deep-purple-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #d1c4e9;\n}\n*.text-deep-purple.lighten-5,\n*.text-deep-purple-hover:not([disabled]):hover.lighten-5,\n*.text-deep-purple-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #ede7f6;\n}\n*.text-deep-purple.darken-1,\n*.text-deep-purple-hover:not([disabled]):hover.darken-1,\n*.text-deep-purple-hover[hover-active]:not([disabled]).darken-1 {\n  color: #5e35b1;\n}\n*.text-deep-purple.darken-2,\n*.text-deep-purple-hover:not([disabled]):hover.darken-2,\n*.text-deep-purple-hover[hover-active]:not([disabled]).darken-2 {\n  color: #512da8;\n}\n*.text-deep-purple.darken-3,\n*.text-deep-purple-hover:not([disabled]):hover.darken-3,\n*.text-deep-purple-hover[hover-active]:not([disabled]).darken-3 {\n  color: #4527a0;\n}\n*.text-deep-purple.darken-4,\n*.text-deep-purple-hover:not([disabled]):hover.darken-4,\n*.text-deep-purple-hover[hover-active]:not([disabled]).darken-4 {\n  color: #311b92;\n}\n*.text-deep-purple.accent-1,\n*.text-deep-purple-hover:not([disabled]):hover.accent-1,\n*.text-deep-purple-hover[hover-active]:not([disabled]).accent-1 {\n  color: #b388ff;\n}\n*.text-deep-purple.accent-2,\n*.text-deep-purple-hover:not([disabled]):hover.accent-2,\n*.text-deep-purple-hover[hover-active]:not([disabled]).accent-2 {\n  color: #7c4dff;\n}\n*.text-deep-purple.accent-3,\n*.text-deep-purple-hover:not([disabled]):hover.accent-3,\n*.text-deep-purple-hover[hover-active]:not([disabled]).accent-3 {\n  color: #651fff;\n}\n*.text-deep-purple.accent-4,\n*.text-deep-purple-hover:not([disabled]):hover.accent-4,\n*.text-deep-purple-hover[hover-active]:not([disabled]).accent-4 {\n  color: #6200ea;\n}\n*.text-indigo,\n*.text-indigo-hover:not([disabled]):hover,\n*.text-indigo-hover[hover-active]:not([disabled]) {\n  color: #3F51b5;\n}\n*.text-indigo.lighten-1,\n*.text-indigo-hover:not([disabled]):hover.lighten-1,\n*.text-indigo-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #5c6bc0;\n}\n*.text-indigo.lighten-2,\n*.text-indigo-hover:not([disabled]):hover.lighten-2,\n*.text-indigo-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #7986cb;\n}\n*.text-indigo.lighten-3,\n*.text-indigo-hover:not([disabled]):hover.lighten-3,\n*.text-indigo-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #9fa8da;\n}\n*.text-indigo.lighten-4,\n*.text-indigo-hover:not([disabled]):hover.lighten-4,\n*.text-indigo-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #c5cae9;\n}\n*.text-indigo.lighten-5,\n*.text-indigo-hover:not([disabled]):hover.lighten-5,\n*.text-indigo-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #e8eaf6;\n}\n*.text-indigo.darken-1,\n*.text-indigo-hover:not([disabled]):hover.darken-1,\n*.text-indigo-hover[hover-active]:not([disabled]).darken-1 {\n  color: #3949ab;\n}\n*.text-indigo.darken-2,\n*.text-indigo-hover:not([disabled]):hover.darken-2,\n*.text-indigo-hover[hover-active]:not([disabled]).darken-2 {\n  color: #303f9f;\n}\n*.text-indigo.darken-3,\n*.text-indigo-hover:not([disabled]):hover.darken-3,\n*.text-indigo-hover[hover-active]:not([disabled]).darken-3 {\n  color: #283593;\n}\n*.text-indigo.darken-4,\n*.text-indigo-hover:not([disabled]):hover.darken-4,\n*.text-indigo-hover[hover-active]:not([disabled]).darken-4 {\n  color: #1a237e;\n}\n*.text-indigo.accent-1,\n*.text-indigo-hover:not([disabled]):hover.accent-1,\n*.text-indigo-hover[hover-active]:not([disabled]).accent-1 {\n  color: #8c9eff;\n}\n*.text-indigo.accent-2,\n*.text-indigo-hover:not([disabled]):hover.accent-2,\n*.text-indigo-hover[hover-active]:not([disabled]).accent-2 {\n  color: #536dfe;\n}\n*.text-indigo.accent-3,\n*.text-indigo-hover:not([disabled]):hover.accent-3,\n*.text-indigo-hover[hover-active]:not([disabled]).accent-3 {\n  color: #3d5afe;\n}\n*.text-indigo.accent-4,\n*.text-indigo-hover:not([disabled]):hover.accent-4,\n*.text-indigo-hover[hover-active]:not([disabled]).accent-4 {\n  color: #304ffe;\n}\n*.text-blue,\n*.text-blue-hover:not([disabled]):hover,\n*.text-blue-hover[hover-active]:not([disabled]) {\n  color: #2196F3;\n}\n*.text-blue.lighten-1,\n*.text-blue-hover:not([disabled]):hover.lighten-1,\n*.text-blue-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #42a5f5;\n}\n*.text-blue.lighten-2,\n*.text-blue-hover:not([disabled]):hover.lighten-2,\n*.text-blue-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #64b5f6;\n}\n*.text-blue.lighten-3,\n*.text-blue-hover:not([disabled]):hover.lighten-3,\n*.text-blue-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #90caf9;\n}\n*.text-blue.lighten-4,\n*.text-blue-hover:not([disabled]):hover.lighten-4,\n*.text-blue-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #bbdefb;\n}\n*.text-blue.lighten-5,\n*.text-blue-hover:not([disabled]):hover.lighten-5,\n*.text-blue-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #e3f2fd;\n}\n*.text-blue.darken-1,\n*.text-blue-hover:not([disabled]):hover.darken-1,\n*.text-blue-hover[hover-active]:not([disabled]).darken-1 {\n  color: #1e88e5;\n}\n*.text-blue.darken-2,\n*.text-blue-hover:not([disabled]):hover.darken-2,\n*.text-blue-hover[hover-active]:not([disabled]).darken-2 {\n  color: #1976d2;\n}\n*.text-blue.darken-3,\n*.text-blue-hover:not([disabled]):hover.darken-3,\n*.text-blue-hover[hover-active]:not([disabled]).darken-3 {\n  color: #1565c0;\n}\n*.text-blue.darken-4,\n*.text-blue-hover:not([disabled]):hover.darken-4,\n*.text-blue-hover[hover-active]:not([disabled]).darken-4 {\n  color: #0d47a1;\n}\n*.text-blue.accent-1,\n*.text-blue-hover:not([disabled]):hover.accent-1,\n*.text-blue-hover[hover-active]:not([disabled]).accent-1 {\n  color: #82b1ff;\n}\n*.text-blue.accent-2,\n*.text-blue-hover:not([disabled]):hover.accent-2,\n*.text-blue-hover[hover-active]:not([disabled]).accent-2 {\n  color: #448aff;\n}\n*.text-blue.accent-3,\n*.text-blue-hover:not([disabled]):hover.accent-3,\n*.text-blue-hover[hover-active]:not([disabled]).accent-3 {\n  color: #2979ff;\n}\n*.text-blue.accent-4,\n*.text-blue-hover:not([disabled]):hover.accent-4,\n*.text-blue-hover[hover-active]:not([disabled]).accent-4 {\n  color: #2962ff;\n}\n*.text-light-blue,\n*.text-light-blue-hover:not([disabled]):hover,\n*.text-light-blue-hover[hover-active]:not([disabled]) {\n  color: #03A9F4;\n}\n*.text-light-blue.lighten-1,\n*.text-light-blue-hover:not([disabled]):hover.lighten-1,\n*.text-light-blue-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #29b6f6;\n}\n*.text-light-blue.lighten-2,\n*.text-light-blue-hover:not([disabled]):hover.lighten-2,\n*.text-light-blue-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #4fc3f7;\n}\n*.text-light-blue.lighten-3,\n*.text-light-blue-hover:not([disabled]):hover.lighten-3,\n*.text-light-blue-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #81d4fa;\n}\n*.text-light-blue.lighten-4,\n*.text-light-blue-hover:not([disabled]):hover.lighten-4,\n*.text-light-blue-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #b3e5fc;\n}\n*.text-light-blue.lighten-5,\n*.text-light-blue-hover:not([disabled]):hover.lighten-5,\n*.text-light-blue-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #e1f5fe;\n}\n*.text-light-blue.darken-1,\n*.text-light-blue-hover:not([disabled]):hover.darken-1,\n*.text-light-blue-hover[hover-active]:not([disabled]).darken-1 {\n  color: #039be5;\n}\n*.text-light-blue.darken-2,\n*.text-light-blue-hover:not([disabled]):hover.darken-2,\n*.text-light-blue-hover[hover-active]:not([disabled]).darken-2 {\n  color: #0288d1;\n}\n*.text-light-blue.darken-3,\n*.text-light-blue-hover:not([disabled]):hover.darken-3,\n*.text-light-blue-hover[hover-active]:not([disabled]).darken-3 {\n  color: #0277bd;\n}\n*.text-light-blue.darken-4,\n*.text-light-blue-hover:not([disabled]):hover.darken-4,\n*.text-light-blue-hover[hover-active]:not([disabled]).darken-4 {\n  color: #01579b;\n}\n*.text-light-blue.accent-1,\n*.text-light-blue-hover:not([disabled]):hover.accent-1,\n*.text-light-blue-hover[hover-active]:not([disabled]).accent-1 {\n  color: #80d8ff;\n}\n*.text-light-blue.accent-2,\n*.text-light-blue-hover:not([disabled]):hover.accent-2,\n*.text-light-blue-hover[hover-active]:not([disabled]).accent-2 {\n  color: #40c4ff;\n}\n*.text-light-blue.accent-3,\n*.text-light-blue-hover:not([disabled]):hover.accent-3,\n*.text-light-blue-hover[hover-active]:not([disabled]).accent-3 {\n  color: #00b0ff;\n}\n*.text-light-blue.accent-4,\n*.text-light-blue-hover:not([disabled]):hover.accent-4,\n*.text-light-blue-hover[hover-active]:not([disabled]).accent-4 {\n  color: #0091ea;\n}\n*.text-cyan,\n*.text-cyan-hover:not([disabled]):hover,\n*.text-cyan-hover[hover-active]:not([disabled]) {\n  color: #00BCD4;\n}\n*.text-cyan.lighten-1,\n*.text-cyan-hover:not([disabled]):hover.lighten-1,\n*.text-cyan-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #26c6da;\n}\n*.text-cyan.lighten-2,\n*.text-cyan-hover:not([disabled]):hover.lighten-2,\n*.text-cyan-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #4dd0e1;\n}\n*.text-cyan.lighten-3,\n*.text-cyan-hover:not([disabled]):hover.lighten-3,\n*.text-cyan-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #80deea;\n}\n*.text-cyan.lighten-4,\n*.text-cyan-hover:not([disabled]):hover.lighten-4,\n*.text-cyan-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #b2ebf2;\n}\n*.text-cyan.lighten-5,\n*.text-cyan-hover:not([disabled]):hover.lighten-5,\n*.text-cyan-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #e0f7fa;\n}\n*.text-cyan.darken-1,\n*.text-cyan-hover:not([disabled]):hover.darken-1,\n*.text-cyan-hover[hover-active]:not([disabled]).darken-1 {\n  color: #00acc1;\n}\n*.text-cyan.darken-2,\n*.text-cyan-hover:not([disabled]):hover.darken-2,\n*.text-cyan-hover[hover-active]:not([disabled]).darken-2 {\n  color: #0097a7;\n}\n*.text-cyan.darken-3,\n*.text-cyan-hover:not([disabled]):hover.darken-3,\n*.text-cyan-hover[hover-active]:not([disabled]).darken-3 {\n  color: #00838f;\n}\n*.text-cyan.darken-4,\n*.text-cyan-hover:not([disabled]):hover.darken-4,\n*.text-cyan-hover[hover-active]:not([disabled]).darken-4 {\n  color: #006064;\n}\n*.text-cyan.accent-1,\n*.text-cyan-hover:not([disabled]):hover.accent-1,\n*.text-cyan-hover[hover-active]:not([disabled]).accent-1 {\n  color: #84ffff;\n}\n*.text-cyan.accent-2,\n*.text-cyan-hover:not([disabled]):hover.accent-2,\n*.text-cyan-hover[hover-active]:not([disabled]).accent-2 {\n  color: #18ffff;\n}\n*.text-cyan.accent-3,\n*.text-cyan-hover:not([disabled]):hover.accent-3,\n*.text-cyan-hover[hover-active]:not([disabled]).accent-3 {\n  color: #00e5ff;\n}\n*.text-cyan.accent-4,\n*.text-cyan-hover:not([disabled]):hover.accent-4,\n*.text-cyan-hover[hover-active]:not([disabled]).accent-4 {\n  color: #00b8d4;\n}\n*.text-teal,\n*.text-teal-hover:not([disabled]):hover,\n*.text-teal-hover[hover-active]:not([disabled]) {\n  color: #009688;\n}\n*.text-teal.lighten-1,\n*.text-teal-hover:not([disabled]):hover.lighten-1,\n*.text-teal-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #26a69a;\n}\n*.text-teal.lighten-2,\n*.text-teal-hover:not([disabled]):hover.lighten-2,\n*.text-teal-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #4db6ac;\n}\n*.text-teal.lighten-3,\n*.text-teal-hover:not([disabled]):hover.lighten-3,\n*.text-teal-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #80cbc4;\n}\n*.text-teal.lighten-4,\n*.text-teal-hover:not([disabled]):hover.lighten-4,\n*.text-teal-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #b2dfdb;\n}\n*.text-teal.lighten-5,\n*.text-teal-hover:not([disabled]):hover.lighten-5,\n*.text-teal-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #e0f2f1;\n}\n*.text-teal.darken-1,\n*.text-teal-hover:not([disabled]):hover.darken-1,\n*.text-teal-hover[hover-active]:not([disabled]).darken-1 {\n  color: #00897b;\n}\n*.text-teal.darken-2,\n*.text-teal-hover:not([disabled]):hover.darken-2,\n*.text-teal-hover[hover-active]:not([disabled]).darken-2 {\n  color: #00796b;\n}\n*.text-teal.darken-3,\n*.text-teal-hover:not([disabled]):hover.darken-3,\n*.text-teal-hover[hover-active]:not([disabled]).darken-3 {\n  color: #00695c;\n}\n*.text-teal.darken-4,\n*.text-teal-hover:not([disabled]):hover.darken-4,\n*.text-teal-hover[hover-active]:not([disabled]).darken-4 {\n  color: #004d40;\n}\n*.text-teal.accent-1,\n*.text-teal-hover:not([disabled]):hover.accent-1,\n*.text-teal-hover[hover-active]:not([disabled]).accent-1 {\n  color: #a7ffeb;\n}\n*.text-teal.accent-2,\n*.text-teal-hover:not([disabled]):hover.accent-2,\n*.text-teal-hover[hover-active]:not([disabled]).accent-2 {\n  color: #64ffda;\n}\n*.text-teal.accent-3,\n*.text-teal-hover:not([disabled]):hover.accent-3,\n*.text-teal-hover[hover-active]:not([disabled]).accent-3 {\n  color: #1de9b6;\n}\n*.text-teal.accent-4,\n*.text-teal-hover:not([disabled]):hover.accent-4,\n*.text-teal-hover[hover-active]:not([disabled]).accent-4 {\n  color: #00bfa5;\n}\n*.text-green,\n*.text-green-hover:not([disabled]):hover,\n*.text-green-hover[hover-active]:not([disabled]) {\n  color: #4caf50;\n}\n*.text-green.lighten-1,\n*.text-green-hover:not([disabled]):hover.lighten-1,\n*.text-green-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #66bb6a;\n}\n*.text-green.lighten-2,\n*.text-green-hover:not([disabled]):hover.lighten-2,\n*.text-green-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #81c784;\n}\n*.text-green.lighten-3,\n*.text-green-hover:not([disabled]):hover.lighten-3,\n*.text-green-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #a5d6a7;\n}\n*.text-green.lighten-4,\n*.text-green-hover:not([disabled]):hover.lighten-4,\n*.text-green-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #c8e6c9;\n}\n*.text-green.lighten-5,\n*.text-green-hover:not([disabled]):hover.lighten-5,\n*.text-green-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #e8f5e9;\n}\n*.text-green.darken-1,\n*.text-green-hover:not([disabled]):hover.darken-1,\n*.text-green-hover[hover-active]:not([disabled]).darken-1 {\n  color: #43a047;\n}\n*.text-green.darken-2,\n*.text-green-hover:not([disabled]):hover.darken-2,\n*.text-green-hover[hover-active]:not([disabled]).darken-2 {\n  color: #388e3c;\n}\n*.text-green.darken-3,\n*.text-green-hover:not([disabled]):hover.darken-3,\n*.text-green-hover[hover-active]:not([disabled]).darken-3 {\n  color: #2e7d32;\n}\n*.text-green.darken-4,\n*.text-green-hover:not([disabled]):hover.darken-4,\n*.text-green-hover[hover-active]:not([disabled]).darken-4 {\n  color: #1b5e20;\n}\n*.text-green.accent-1,\n*.text-green-hover:not([disabled]):hover.accent-1,\n*.text-green-hover[hover-active]:not([disabled]).accent-1 {\n  color: #b9f6ca;\n}\n*.text-green.accent-2,\n*.text-green-hover:not([disabled]):hover.accent-2,\n*.text-green-hover[hover-active]:not([disabled]).accent-2 {\n  color: #69f0ae;\n}\n*.text-green.accent-3,\n*.text-green-hover:not([disabled]):hover.accent-3,\n*.text-green-hover[hover-active]:not([disabled]).accent-3 {\n  color: #00e676;\n}\n*.text-green.accent-4,\n*.text-green-hover:not([disabled]):hover.accent-4,\n*.text-green-hover[hover-active]:not([disabled]).accent-4 {\n  color: #00c853;\n}\n*.text-light-green,\n*.text-light-green-hover:not([disabled]):hover,\n*.text-light-green-hover[hover-active]:not([disabled]) {\n  color: #8bc34a;\n}\n*.text-light-green.lighten-1,\n*.text-light-green-hover:not([disabled]):hover.lighten-1,\n*.text-light-green-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #9ccc65;\n}\n*.text-light-green.lighten-2,\n*.text-light-green-hover:not([disabled]):hover.lighten-2,\n*.text-light-green-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #aed581;\n}\n*.text-light-green.lighten-3,\n*.text-light-green-hover:not([disabled]):hover.lighten-3,\n*.text-light-green-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #c5e1a5;\n}\n*.text-light-green.lighten-4,\n*.text-light-green-hover:not([disabled]):hover.lighten-4,\n*.text-light-green-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #dcedc8;\n}\n*.text-light-green.lighten-5,\n*.text-light-green-hover:not([disabled]):hover.lighten-5,\n*.text-light-green-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #f1f8e9;\n}\n*.text-light-green.darken-1,\n*.text-light-green-hover:not([disabled]):hover.darken-1,\n*.text-light-green-hover[hover-active]:not([disabled]).darken-1 {\n  color: #7cb342;\n}\n*.text-light-green.darken-2,\n*.text-light-green-hover:not([disabled]):hover.darken-2,\n*.text-light-green-hover[hover-active]:not([disabled]).darken-2 {\n  color: #689f38;\n}\n*.text-light-green.darken-3,\n*.text-light-green-hover:not([disabled]):hover.darken-3,\n*.text-light-green-hover[hover-active]:not([disabled]).darken-3 {\n  color: #558b2f;\n}\n*.text-light-green.darken-4,\n*.text-light-green-hover:not([disabled]):hover.darken-4,\n*.text-light-green-hover[hover-active]:not([disabled]).darken-4 {\n  color: #33691e;\n}\n*.text-light-green.accent-1,\n*.text-light-green-hover:not([disabled]):hover.accent-1,\n*.text-light-green-hover[hover-active]:not([disabled]).accent-1 {\n  color: #ccff90;\n}\n*.text-light-green.accent-2,\n*.text-light-green-hover:not([disabled]):hover.accent-2,\n*.text-light-green-hover[hover-active]:not([disabled]).accent-2 {\n  color: #b2ff59;\n}\n*.text-light-green.accent-3,\n*.text-light-green-hover:not([disabled]):hover.accent-3,\n*.text-light-green-hover[hover-active]:not([disabled]).accent-3 {\n  color: #76ff03;\n}\n*.text-light-green.accent-4,\n*.text-light-green-hover:not([disabled]):hover.accent-4,\n*.text-light-green-hover[hover-active]:not([disabled]).accent-4 {\n  color: #64dd17;\n}\n*.text-lime,\n*.text-lime-hover:not([disabled]):hover,\n*.text-lime-hover[hover-active]:not([disabled]) {\n  color: #cddc39;\n}\n*.text-lime.lighten-1,\n*.text-lime-hover:not([disabled]):hover.lighten-1,\n*.text-lime-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #d4e157;\n}\n*.text-lime.lighten-2,\n*.text-lime-hover:not([disabled]):hover.lighten-2,\n*.text-lime-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #dce775;\n}\n*.text-lime.lighten-3,\n*.text-lime-hover:not([disabled]):hover.lighten-3,\n*.text-lime-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #e6ee9c;\n}\n*.text-lime.lighten-4,\n*.text-lime-hover:not([disabled]):hover.lighten-4,\n*.text-lime-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #f0f4c3;\n}\n*.text-lime.lighten-5,\n*.text-lime-hover:not([disabled]):hover.lighten-5,\n*.text-lime-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #f9fbe7;\n}\n*.text-lime.darken-1,\n*.text-lime-hover:not([disabled]):hover.darken-1,\n*.text-lime-hover[hover-active]:not([disabled]).darken-1 {\n  color: #c0ca33;\n}\n*.text-lime.darken-2,\n*.text-lime-hover:not([disabled]):hover.darken-2,\n*.text-lime-hover[hover-active]:not([disabled]).darken-2 {\n  color: #afb42b;\n}\n*.text-lime.darken-3,\n*.text-lime-hover:not([disabled]):hover.darken-3,\n*.text-lime-hover[hover-active]:not([disabled]).darken-3 {\n  color: #9e9d24;\n}\n*.text-lime.darken-4,\n*.text-lime-hover:not([disabled]):hover.darken-4,\n*.text-lime-hover[hover-active]:not([disabled]).darken-4 {\n  color: #827717;\n}\n*.text-lime.accent-1,\n*.text-lime-hover:not([disabled]):hover.accent-1,\n*.text-lime-hover[hover-active]:not([disabled]).accent-1 {\n  color: #f4ff81;\n}\n*.text-lime.accent-2,\n*.text-lime-hover:not([disabled]):hover.accent-2,\n*.text-lime-hover[hover-active]:not([disabled]).accent-2 {\n  color: #eeff41;\n}\n*.text-lime.accent-3,\n*.text-lime-hover:not([disabled]):hover.accent-3,\n*.text-lime-hover[hover-active]:not([disabled]).accent-3 {\n  color: #c6ff00;\n}\n*.text-lime.accent-4,\n*.text-lime-hover:not([disabled]):hover.accent-4,\n*.text-lime-hover[hover-active]:not([disabled]).accent-4 {\n  color: #aeea00;\n}\n*.text-yellow,\n*.text-yellow-hover:not([disabled]):hover,\n*.text-yellow-hover[hover-active]:not([disabled]) {\n  color: #ffeb3b;\n}\n*.text-yellow.lighten-1,\n*.text-yellow-hover:not([disabled]):hover.lighten-1,\n*.text-yellow-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #ffee58;\n}\n*.text-yellow.lighten-2,\n*.text-yellow-hover:not([disabled]):hover.lighten-2,\n*.text-yellow-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #fff176;\n}\n*.text-yellow.lighten-3,\n*.text-yellow-hover:not([disabled]):hover.lighten-3,\n*.text-yellow-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #fff59d;\n}\n*.text-yellow.lighten-4,\n*.text-yellow-hover:not([disabled]):hover.lighten-4,\n*.text-yellow-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #fff9c4;\n}\n*.text-yellow.lighten-5,\n*.text-yellow-hover:not([disabled]):hover.lighten-5,\n*.text-yellow-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #fffde7;\n}\n*.text-yellow.darken-1,\n*.text-yellow-hover:not([disabled]):hover.darken-1,\n*.text-yellow-hover[hover-active]:not([disabled]).darken-1 {\n  color: #fdd835;\n}\n*.text-yellow.darken-2,\n*.text-yellow-hover:not([disabled]):hover.darken-2,\n*.text-yellow-hover[hover-active]:not([disabled]).darken-2 {\n  color: #fbc02d;\n}\n*.text-yellow.darken-3,\n*.text-yellow-hover:not([disabled]):hover.darken-3,\n*.text-yellow-hover[hover-active]:not([disabled]).darken-3 {\n  color: #f9a825;\n}\n*.text-yellow.darken-4,\n*.text-yellow-hover:not([disabled]):hover.darken-4,\n*.text-yellow-hover[hover-active]:not([disabled]).darken-4 {\n  color: #f57f17;\n}\n*.text-yellow.accent-1,\n*.text-yellow-hover:not([disabled]):hover.accent-1,\n*.text-yellow-hover[hover-active]:not([disabled]).accent-1 {\n  color: #ffff8d;\n}\n*.text-yellow.accent-2,\n*.text-yellow-hover:not([disabled]):hover.accent-2,\n*.text-yellow-hover[hover-active]:not([disabled]).accent-2 {\n  color: #ffff00;\n}\n*.text-yellow.accent-3,\n*.text-yellow-hover:not([disabled]):hover.accent-3,\n*.text-yellow-hover[hover-active]:not([disabled]).accent-3 {\n  color: #ffea00;\n}\n*.text-yellow.accent-4,\n*.text-yellow-hover:not([disabled]):hover.accent-4,\n*.text-yellow-hover[hover-active]:not([disabled]).accent-4 {\n  color: #ffd600;\n}\n*.text-amber,\n*.text-amber-hover:not([disabled]):hover,\n*.text-amber-hover[hover-active]:not([disabled]) {\n  color: #FFC107;\n}\n*.text-amber.lighten-1,\n*.text-amber-hover:not([disabled]):hover.lighten-1,\n*.text-amber-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #ffca28;\n}\n*.text-amber.lighten-2,\n*.text-amber-hover:not([disabled]):hover.lighten-2,\n*.text-amber-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #ffd54f;\n}\n*.text-amber.lighten-3,\n*.text-amber-hover:not([disabled]):hover.lighten-3,\n*.text-amber-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #ffe082;\n}\n*.text-amber.lighten-4,\n*.text-amber-hover:not([disabled]):hover.lighten-4,\n*.text-amber-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #ffecb3;\n}\n*.text-amber.lighten-5,\n*.text-amber-hover:not([disabled]):hover.lighten-5,\n*.text-amber-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #fff8e1;\n}\n*.text-amber.darken-1,\n*.text-amber-hover:not([disabled]):hover.darken-1,\n*.text-amber-hover[hover-active]:not([disabled]).darken-1 {\n  color: #ffb300;\n}\n*.text-amber.darken-2,\n*.text-amber-hover:not([disabled]):hover.darken-2,\n*.text-amber-hover[hover-active]:not([disabled]).darken-2 {\n  color: #ffa000;\n}\n*.text-amber.darken-3,\n*.text-amber-hover:not([disabled]):hover.darken-3,\n*.text-amber-hover[hover-active]:not([disabled]).darken-3 {\n  color: #ff8f00;\n}\n*.text-amber.darken-4,\n*.text-amber-hover:not([disabled]):hover.darken-4,\n*.text-amber-hover[hover-active]:not([disabled]).darken-4 {\n  color: #ff6f00;\n}\n*.text-amber.accent-1,\n*.text-amber-hover:not([disabled]):hover.accent-1,\n*.text-amber-hover[hover-active]:not([disabled]).accent-1 {\n  color: #ffe57f;\n}\n*.text-amber.accent-2,\n*.text-amber-hover:not([disabled]):hover.accent-2,\n*.text-amber-hover[hover-active]:not([disabled]).accent-2 {\n  color: #ffd740;\n}\n*.text-amber.accent-3,\n*.text-amber-hover:not([disabled]):hover.accent-3,\n*.text-amber-hover[hover-active]:not([disabled]).accent-3 {\n  color: #ffc400;\n}\n*.text-amber.accent-4,\n*.text-amber-hover:not([disabled]):hover.accent-4,\n*.text-amber-hover[hover-active]:not([disabled]).accent-4 {\n  color: #ffab00;\n}\n*.text-orange,\n*.text-orange-hover:not([disabled]):hover,\n*.text-orange-hover[hover-active]:not([disabled]) {\n  color: #FF9800;\n}\n*.text-orange.lighten-1,\n*.text-orange-hover:not([disabled]):hover.lighten-1,\n*.text-orange-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #ffa726;\n}\n*.text-orange.lighten-2,\n*.text-orange-hover:not([disabled]):hover.lighten-2,\n*.text-orange-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #ffb74d;\n}\n*.text-orange.lighten-3,\n*.text-orange-hover:not([disabled]):hover.lighten-3,\n*.text-orange-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #ffcc80;\n}\n*.text-orange.lighten-4,\n*.text-orange-hover:not([disabled]):hover.lighten-4,\n*.text-orange-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #ffe0b2;\n}\n*.text-orange.lighten-5,\n*.text-orange-hover:not([disabled]):hover.lighten-5,\n*.text-orange-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #fff3e0;\n}\n*.text-orange.darken-1,\n*.text-orange-hover:not([disabled]):hover.darken-1,\n*.text-orange-hover[hover-active]:not([disabled]).darken-1 {\n  color: #fb8c00;\n}\n*.text-orange.darken-2,\n*.text-orange-hover:not([disabled]):hover.darken-2,\n*.text-orange-hover[hover-active]:not([disabled]).darken-2 {\n  color: #f57c00;\n}\n*.text-orange.darken-3,\n*.text-orange-hover:not([disabled]):hover.darken-3,\n*.text-orange-hover[hover-active]:not([disabled]).darken-3 {\n  color: #ef6c00;\n}\n*.text-orange.darken-4,\n*.text-orange-hover:not([disabled]):hover.darken-4,\n*.text-orange-hover[hover-active]:not([disabled]).darken-4 {\n  color: #e65100;\n}\n*.text-orange.accent-1,\n*.text-orange-hover:not([disabled]):hover.accent-1,\n*.text-orange-hover[hover-active]:not([disabled]).accent-1 {\n  background-color: #ffd180;\n}\n*.text-orange.accent-2,\n*.text-orange-hover:not([disabled]):hover.accent-2,\n*.text-orange-hover[hover-active]:not([disabled]).accent-2 {\n  background-color: #ffab40;\n}\n*.text-orange.accent-3,\n*.text-orange-hover:not([disabled]):hover.accent-3,\n*.text-orange-hover[hover-active]:not([disabled]).accent-3 {\n  background-color: #ff9100;\n}\n*.text-orange.accent-4,\n*.text-orange-hover:not([disabled]):hover.accent-4,\n*.text-orange-hover[hover-active]:not([disabled]).accent-4 {\n  background-color: #ff6d00;\n}\n*.text-deep-orange,\n*.text-deep-orange-hover:not([disabled]):hover,\n*.text-deep-orange-hover[hover-active]:not([disabled]) {\n  color: #ff5722;\n}\n*.text-deep-orange.lighten-1,\n*.text-deep-orange-hover:not([disabled]):hover.lighten-1,\n*.text-deep-orange-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #ff7043;\n}\n*.text-deep-orange.lighten-2,\n*.text-deep-orange-hover:not([disabled]):hover.lighten-2,\n*.text-deep-orange-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #ff8a65;\n}\n*.text-deep-orange.lighten-3,\n*.text-deep-orange-hover:not([disabled]):hover.lighten-3,\n*.text-deep-orange-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #ffab91;\n}\n*.text-deep-orange.lighten-4,\n*.text-deep-orange-hover:not([disabled]):hover.lighten-4,\n*.text-deep-orange-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #ffccbc;\n}\n*.text-deep-orange.lighten-5,\n*.text-deep-orange-hover:not([disabled]):hover.lighten-5,\n*.text-deep-orange-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #fbe9e7;\n}\n*.text-deep-orange.darken-1,\n*.text-deep-orange-hover:not([disabled]):hover.darken-1,\n*.text-deep-orange-hover[hover-active]:not([disabled]).darken-1 {\n  color: #f4511e;\n}\n*.text-deep-orange.darken-2,\n*.text-deep-orange-hover:not([disabled]):hover.darken-2,\n*.text-deep-orange-hover[hover-active]:not([disabled]).darken-2 {\n  color: #e64a19;\n}\n*.text-deep-orange.darken-3,\n*.text-deep-orange-hover:not([disabled]):hover.darken-3,\n*.text-deep-orange-hover[hover-active]:not([disabled]).darken-3 {\n  color: #d84315;\n}\n*.text-deep-orange.darken-4,\n*.text-deep-orange-hover:not([disabled]):hover.darken-4,\n*.text-deep-orange-hover[hover-active]:not([disabled]).darken-4 {\n  color: #bf360c;\n}\n*.text-deep-orange.accent-1,\n*.text-deep-orange-hover:not([disabled]):hover.accent-1,\n*.text-deep-orange-hover[hover-active]:not([disabled]).accent-1 {\n  color: #ff9e80;\n}\n*.text-deep-orange.accent-2,\n*.text-deep-orange-hover:not([disabled]):hover.accent-2,\n*.text-deep-orange-hover[hover-active]:not([disabled]).accent-2 {\n  color: #ff6e40;\n}\n*.text-deep-orange.accent-3,\n*.text-deep-orange-hover:not([disabled]):hover.accent-3,\n*.text-deep-orange-hover[hover-active]:not([disabled]).accent-3 {\n  color: #ff3d00;\n}\n*.text-deep-orange.accent-4,\n*.text-deep-orange-hover:not([disabled]):hover.accent-4,\n*.text-deep-orange-hover[hover-active]:not([disabled]).accent-4 {\n  color: #dd2c00;\n}\n*.text-brown,\n*.text-brown-hover:not([disabled]):hover,\n*.text-brown-hover[hover-active]:not([disabled]) {\n  color: #795548;\n}\n*.text-brown.lighten-1,\n*.text-brown-hover:not([disabled]):hover.lighten-1,\n*.text-brown-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #8d6e63;\n}\n*.text-brown.lighten-2,\n*.text-brown-hover:not([disabled]):hover.lighten-2,\n*.text-brown-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #a1887f;\n}\n*.text-brown.lighten-3,\n*.text-brown-hover:not([disabled]):hover.lighten-3,\n*.text-brown-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #bcaaa4;\n}\n*.text-brown.lighten-4,\n*.text-brown-hover:not([disabled]):hover.lighten-4,\n*.text-brown-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #d7ccc8;\n}\n*.text-brown.lighten-5,\n*.text-brown-hover:not([disabled]):hover.lighten-5,\n*.text-brown-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #efebe9;\n}\n*.text-brown.darken-1,\n*.text-brown-hover:not([disabled]):hover.darken-1,\n*.text-brown-hover[hover-active]:not([disabled]).darken-1 {\n  color: #6d4c41;\n}\n*.text-brown.darken-2,\n*.text-brown-hover:not([disabled]):hover.darken-2,\n*.text-brown-hover[hover-active]:not([disabled]).darken-2 {\n  color: #5d4037;\n}\n*.text-brown.darken-3,\n*.text-brown-hover:not([disabled]):hover.darken-3,\n*.text-brown-hover[hover-active]:not([disabled]).darken-3 {\n  color: #4e342e;\n}\n*.text-brown.darken-4,\n*.text-brown-hover:not([disabled]):hover.darken-4,\n*.text-brown-hover[hover-active]:not([disabled]).darken-4 {\n  color: #3e2723;\n}\n*.text-gray,\n*.text-gray-hover:not([disabled]):hover,\n*.text-gray-hover[hover-active]:not([disabled]) {\n  color: #9e9e9e;\n}\n*.text-gray.lighten-1,\n*.text-gray-hover:not([disabled]):hover.lighten-1,\n*.text-gray-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #bdbdbd;\n}\n*.text-gray.lighten-2,\n*.text-gray-hover:not([disabled]):hover.lighten-2,\n*.text-gray-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #e0e0e0;\n}\n*.text-gray.lighten-3,\n*.text-gray-hover:not([disabled]):hover.lighten-3,\n*.text-gray-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #eeeeee;\n}\n*.text-gray.lighten-4,\n*.text-gray-hover:not([disabled]):hover.lighten-4,\n*.text-gray-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #f5f5f5;\n}\n*.text-gray.lighten-5,\n*.text-gray-hover:not([disabled]):hover.lighten-5,\n*.text-gray-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #fafafa;\n}\n*.text-gray.darken-1,\n*.text-gray-hover:not([disabled]):hover.darken-1,\n*.text-gray-hover[hover-active]:not([disabled]).darken-1 {\n  color: #757575;\n}\n*.text-gray.darken-2,\n*.text-gray-hover:not([disabled]):hover.darken-2,\n*.text-gray-hover[hover-active]:not([disabled]).darken-2 {\n  color: #616161;\n}\n*.text-gray.darken-3,\n*.text-gray-hover:not([disabled]):hover.darken-3,\n*.text-gray-hover[hover-active]:not([disabled]).darken-3 {\n  color: #424242;\n}\n*.text-gray.darken-4,\n*.text-gray-hover:not([disabled]):hover.darken-4,\n*.text-gray-hover[hover-active]:not([disabled]).darken-4 {\n  color: #212121;\n}\n*.text-blue-gray,\n*.text-blue-gray-hover:not([disabled]):hover,\n*.text-blue-gray-hover[hover-active]:not([disabled]) {\n  color: #607d8b;\n}\n*.text-blue-gray.lighten-1,\n*.text-blue-gray-hover:not([disabled]):hover.lighten-1,\n*.text-blue-gray-hover[hover-active]:not([disabled]).lighten-1 {\n  color: #78909c;\n}\n*.text-blue-gray.lighten-2,\n*.text-blue-gray-hover:not([disabled]):hover.lighten-2,\n*.text-blue-gray-hover[hover-active]:not([disabled]).lighten-2 {\n  color: #90a4ae;\n}\n*.text-blue-gray.lighten-3,\n*.text-blue-gray-hover:not([disabled]):hover.lighten-3,\n*.text-blue-gray-hover[hover-active]:not([disabled]).lighten-3 {\n  color: #b0bec5;\n}\n*.text-blue-gray.lighten-4,\n*.text-blue-gray-hover:not([disabled]):hover.lighten-4,\n*.text-blue-gray-hover[hover-active]:not([disabled]).lighten-4 {\n  color: #cfd8dc;\n}\n*.text-blue-gray.lighten-5,\n*.text-blue-gray-hover:not([disabled]):hover.lighten-5,\n*.text-blue-gray-hover[hover-active]:not([disabled]).lighten-5 {\n  color: #eceff1;\n}\n*.text-blue-gray.darken-1,\n*.text-blue-gray-hover:not([disabled]):hover.darken-1,\n*.text-blue-gray-hover[hover-active]:not([disabled]).darken-1 {\n  color: #546e7a;\n}\n*.text-blue-gray.darken-2,\n*.text-blue-gray-hover:not([disabled]):hover.darken-2,\n*.text-blue-gray-hover[hover-active]:not([disabled]).darken-2 {\n  color: #455a64;\n}\n*.text-blue-gray.darken-3,\n*.text-blue-gray-hover:not([disabled]):hover.darken-3,\n*.text-blue-gray-hover[hover-active]:not([disabled]).darken-3 {\n  color: #37474f;\n}\n*.text-blue-gray.darken-4,\n*.text-blue-gray-hover:not([disabled]):hover.darken-4,\n*.text-blue-gray-hover[hover-active]:not([disabled]).darken-4 {\n  color: #263238;\n}\n.text-red-active.active {\n  color: #F44336;\n}\n.text-red-active.active.lighten-1 {\n  color: #ef5350;\n}\n.text-red-active.active.lighten-2 {\n  color: #e57373;\n}\n.text-red-active.active.lighten-3 {\n  color: #ef9a9a;\n}\n.text-red-active.active.lighten-4 {\n  color: #ffcdd2;\n}\n.text-red-active.active.lighten-5 {\n  color: #ffebee;\n}\n.text-red-active.active.darken-1 {\n  color: #e53935;\n}\n.text-red-active.active.darken-2 {\n  color: #d32f2f;\n}\n.text-red-active.active.darken-3 {\n  color: #c62828;\n}\n.text-red-active.active.darken-4 {\n  color: #b71c1c;\n}\n.text-red-active.active.accent-1 {\n  color: #ff8a80;\n}\n.text-red-active.active.accent-2 {\n  color: #ff5252;\n}\n.text-red-active.active.accent-3 {\n  color: #ff1744;\n}\n.text-red-active.active.accent-4 {\n  color: #d50000;\n}\n.text-pink-active.active {\n  color: #E91E63;\n}\n.text-pink-active.active.lighten-1 {\n  color: #ec407a;\n}\n.text-pink-active.active.lighten-2 {\n  color: #f06292;\n}\n.text-pink-active.active.lighten-3 {\n  color: #f48fb1;\n}\n.text-pink-active.active.lighten-4 {\n  color: #f8bbd0;\n}\n.text-pink-active.active.lighten-5 {\n  color: #fce4ec;\n}\n.text-pink-active.active.darken-1 {\n  color: #d81b60;\n}\n.text-pink-active.active.darken-2 {\n  color: #c2185b;\n}\n.text-pink-active.active.darken-3 {\n  color: #ad1457;\n}\n.text-pink-active.active.darken-4 {\n  color: #880e4f;\n}\n.text-pink-active.active.accent-1 {\n  color: #ff80ab;\n}\n.text-pink-active.active.accent-2 {\n  color: #ff4081;\n}\n.text-pink-active.active.accent-3 {\n  color: #f50057;\n}\n.text-pink-active.active.accent-4 {\n  background-color: #c51162;\n}\n.text-purple-active.active {\n  color: #9c27b0;\n}\n.text-purple-active.active.lighten-1 {\n  color: #ab47bc;\n}\n.text-purple-active.active.lighten-2 {\n  color: #ba68c8;\n}\n.text-purple-active.active.lighten-3 {\n  color: #ce93d8;\n}\n.text-purple-active.active.lighten-4 {\n  color: #e1bee7;\n}\n.text-purple-active.active.lighten-5 {\n  color: #f3e5f5;\n}\n.text-purple-active.active.darken-1 {\n  color: #8e24aa;\n}\n.text-purple-active.active.darken-2 {\n  color: #7b1fa2;\n}\n.text-purple-active.active.darken-3 {\n  color: #6a1b9a;\n}\n.text-purple-active.active.darken-4 {\n  color: #4a148c;\n}\n.text-purple-active.active.accent-1 {\n  color: #ea80fc;\n}\n.text-purple-active.active.accent-2 {\n  color: #e040fb;\n}\n.text-purple-active.active.accent-3 {\n  color: #d500f9;\n}\n.text-purple-active.active.accent-4 {\n  color: #aa00ff;\n}\n.text-deep-purple-active.active {\n  color: #673ab7;\n}\n.text-deep-purple-active.active.lighten-1 {\n  color: #7e57c2;\n}\n.text-deep-purple-active.active.lighten-2 {\n  color: #9575cd;\n}\n.text-deep-purple-active.active.lighten-3 {\n  color: #b39ddb;\n}\n.text-deep-purple-active.active.lighten-4 {\n  color: #d1c4e9;\n}\n.text-deep-purple-active.active.lighten-5 {\n  color: #ede7f6;\n}\n.text-deep-purple-active.active.darken-1 {\n  color: #5e35b1;\n}\n.text-deep-purple-active.active.darken-2 {\n  color: #512da8;\n}\n.text-deep-purple-active.active.darken-3 {\n  color: #4527a0;\n}\n.text-deep-purple-active.active.darken-4 {\n  color: #311b92;\n}\n.text-deep-purple-active.active.accent-1 {\n  color: #b388ff;\n}\n.text-deep-purple-active.active.accent-2 {\n  color: #7c4dff;\n}\n.text-deep-purple-active.active.accent-3 {\n  color: #651fff;\n}\n.text-deep-purple-active.active.accent-4 {\n  color: #6200ea;\n}\n.text-indigo-active.active {\n  color: #3F51b5;\n}\n.text-indigo-active.active.lighten-1 {\n  color: #5c6bc0;\n}\n.text-indigo-active.active.lighten-2 {\n  color: #7986cb;\n}\n.text-indigo-active.active.lighten-3 {\n  color: #9fa8da;\n}\n.text-indigo-active.active.lighten-4 {\n  color: #c5cae9;\n}\n.text-indigo-active.active.lighten-5 {\n  color: #e8eaf6;\n}\n.text-indigo-active.active.darken-1 {\n  color: #3949ab;\n}\n.text-indigo-active.active.darken-2 {\n  color: #303f9f;\n}\n.text-indigo-active.active.darken-3 {\n  color: #283593;\n}\n.text-indigo-active.active.darken-4 {\n  color: #1a237e;\n}\n.text-indigo-active.active.accent-1 {\n  color: #8c9eff;\n}\n.text-indigo-active.active.accent-2 {\n  color: #536dfe;\n}\n.text-indigo-active.active.accent-3 {\n  color: #3d5afe;\n}\n.text-indigo-active.active.accent-4 {\n  color: #304ffe;\n}\n.text-blue-active.active {\n  color: #2196F3;\n}\n.text-blue-active.active.lighten-1 {\n  color: #42a5f5;\n}\n.text-blue-active.active.lighten-2 {\n  color: #64b5f6;\n}\n.text-blue-active.active.lighten-3 {\n  color: #90caf9;\n}\n.text-blue-active.active.lighten-4 {\n  color: #bbdefb;\n}\n.text-blue-active.active.lighten-5 {\n  color: #e3f2fd;\n}\n.text-blue-active.active.darken-1 {\n  color: #1e88e5;\n}\n.text-blue-active.active.darken-2 {\n  color: #1976d2;\n}\n.text-blue-active.active.darken-3 {\n  color: #1565c0;\n}\n.text-blue-active.active.darken-4 {\n  color: #0d47a1;\n}\n.text-blue-active.active.accent-1 {\n  color: #82b1ff;\n}\n.text-blue-active.active.accent-2 {\n  color: #448aff;\n}\n.text-blue-active.active.accent-3 {\n  color: #2979ff;\n}\n.text-blue-active.active.accent-4 {\n  color: #2962ff;\n}\n.text-light-blue-active.active {\n  color: #03A9F4;\n}\n.text-light-blue-active.active.lighten-1 {\n  color: #29b6f6;\n}\n.text-light-blue-active.active.lighten-2 {\n  color: #4fc3f7;\n}\n.text-light-blue-active.active.lighten-3 {\n  color: #81d4fa;\n}\n.text-light-blue-active.active.lighten-4 {\n  color: #b3e5fc;\n}\n.text-light-blue-active.active.lighten-5 {\n  color: #e1f5fe;\n}\n.text-light-blue-active.active.darken-1 {\n  color: #039be5;\n}\n.text-light-blue-active.active.darken-2 {\n  color: #0288d1;\n}\n.text-light-blue-active.active.darken-3 {\n  color: #0277bd;\n}\n.text-light-blue-active.active.darken-4 {\n  color: #01579b;\n}\n.text-light-blue-active.active.accent-1 {\n  color: #80d8ff;\n}\n.text-light-blue-active.active.accent-2 {\n  color: #40c4ff;\n}\n.text-light-blue-active.active.accent-3 {\n  color: #00b0ff;\n}\n.text-light-blue-active.active.accent-4 {\n  color: #0091ea;\n}\n.text-cyan-active.active {\n  color: #00BCD4;\n}\n.text-cyan-active.active.lighten-1 {\n  color: #26c6da;\n}\n.text-cyan-active.active.lighten-2 {\n  color: #4dd0e1;\n}\n.text-cyan-active.active.lighten-3 {\n  color: #80deea;\n}\n.text-cyan-active.active.lighten-4 {\n  color: #b2ebf2;\n}\n.text-cyan-active.active.lighten-5 {\n  color: #e0f7fa;\n}\n.text-cyan-active.active.darken-1 {\n  color: #00acc1;\n}\n.text-cyan-active.active.darken-2 {\n  color: #0097a7;\n}\n.text-cyan-active.active.darken-3 {\n  color: #00838f;\n}\n.text-cyan-active.active.darken-4 {\n  color: #006064;\n}\n.text-cyan-active.active.accent-1 {\n  color: #84ffff;\n}\n.text-cyan-active.active.accent-2 {\n  color: #18ffff;\n}\n.text-cyan-active.active.accent-3 {\n  color: #00e5ff;\n}\n.text-cyan-active.active.accent-4 {\n  color: #00b8d4;\n}\n.text-teal-active.active {\n  color: #009688;\n}\n.text-teal-active.active.lighten-1 {\n  color: #26a69a;\n}\n.text-teal-active.active.lighten-2 {\n  color: #4db6ac;\n}\n.text-teal-active.active.lighten-3 {\n  color: #80cbc4;\n}\n.text-teal-active.active.lighten-4 {\n  color: #b2dfdb;\n}\n.text-teal-active.active.lighten-5 {\n  color: #e0f2f1;\n}\n.text-teal-active.active.darken-1 {\n  color: #00897b;\n}\n.text-teal-active.active.darken-2 {\n  color: #00796b;\n}\n.text-teal-active.active.darken-3 {\n  color: #00695c;\n}\n.text-teal-active.active.darken-4 {\n  color: #004d40;\n}\n.text-teal-active.active.accent-1 {\n  color: #a7ffeb;\n}\n.text-teal-active.active.accent-2 {\n  color: #64ffda;\n}\n.text-teal-active.active.accent-3 {\n  color: #1de9b6;\n}\n.text-teal-active.active.accent-4 {\n  color: #00bfa5;\n}\n.text-green-active.active {\n  color: #4caf50;\n}\n.text-green-active.active.lighten-1 {\n  color: #66bb6a;\n}\n.text-green-active.active.lighten-2 {\n  color: #81c784;\n}\n.text-green-active.active.lighten-3 {\n  color: #a5d6a7;\n}\n.text-green-active.active.lighten-4 {\n  color: #c8e6c9;\n}\n.text-green-active.active.lighten-5 {\n  color: #e8f5e9;\n}\n.text-green-active.active.darken-1 {\n  color: #43a047;\n}\n.text-green-active.active.darken-2 {\n  color: #388e3c;\n}\n.text-green-active.active.darken-3 {\n  color: #2e7d32;\n}\n.text-green-active.active.darken-4 {\n  color: #1b5e20;\n}\n.text-green-active.active.accent-1 {\n  color: #b9f6ca;\n}\n.text-green-active.active.accent-2 {\n  color: #69f0ae;\n}\n.text-green-active.active.accent-3 {\n  color: #00e676;\n}\n.text-green-active.active.accent-4 {\n  color: #00c853;\n}\n.text-light-green-active.active {\n  color: #8bc34a;\n}\n.text-light-green-active.active.lighten-1 {\n  color: #9ccc65;\n}\n.text-light-green-active.active.lighten-2 {\n  color: #aed581;\n}\n.text-light-green-active.active.lighten-3 {\n  color: #c5e1a5;\n}\n.text-light-green-active.active.lighten-4 {\n  color: #dcedc8;\n}\n.text-light-green-active.active.lighten-5 {\n  color: #f1f8e9;\n}\n.text-light-green-active.active.darken-1 {\n  color: #7cb342;\n}\n.text-light-green-active.active.darken-2 {\n  color: #689f38;\n}\n.text-light-green-active.active.darken-3 {\n  color: #558b2f;\n}\n.text-light-green-active.active.darken-4 {\n  color: #33691e;\n}\n.text-light-green-active.active.accent-1 {\n  color: #ccff90;\n}\n.text-light-green-active.active.accent-2 {\n  color: #b2ff59;\n}\n.text-light-green-active.active.accent-3 {\n  color: #76ff03;\n}\n.text-light-green-active.active.accent-4 {\n  color: #64dd17;\n}\n.text-lime-active.active {\n  color: #cddc39;\n}\n.text-lime-active.active.lighten-1 {\n  color: #d4e157;\n}\n.text-lime-active.active.lighten-2 {\n  color: #dce775;\n}\n.text-lime-active.active.lighten-3 {\n  color: #e6ee9c;\n}\n.text-lime-active.active.lighten-4 {\n  color: #f0f4c3;\n}\n.text-lime-active.active.lighten-5 {\n  color: #f9fbe7;\n}\n.text-lime-active.active.darken-1 {\n  color: #c0ca33;\n}\n.text-lime-active.active.darken-2 {\n  color: #afb42b;\n}\n.text-lime-active.active.darken-3 {\n  color: #9e9d24;\n}\n.text-lime-active.active.darken-4 {\n  color: #827717;\n}\n.text-lime-active.active.accent-1 {\n  color: #f4ff81;\n}\n.text-lime-active.active.accent-2 {\n  color: #eeff41;\n}\n.text-lime-active.active.accent-3 {\n  color: #c6ff00;\n}\n.text-lime-active.active.accent-4 {\n  color: #aeea00;\n}\n.text-yellow-active.active {\n  color: #ffeb3b;\n}\n.text-yellow-active.active.lighten-1 {\n  color: #ffee58;\n}\n.text-yellow-active.active.lighten-2 {\n  color: #fff176;\n}\n.text-yellow-active.active.lighten-3 {\n  color: #fff59d;\n}\n.text-yellow-active.active.lighten-4 {\n  color: #fff9c4;\n}\n.text-yellow-active.active.lighten-5 {\n  color: #fffde7;\n}\n.text-yellow-active.active.darken-1 {\n  color: #fdd835;\n}\n.text-yellow-active.active.darken-2 {\n  color: #fbc02d;\n}\n.text-yellow-active.active.darken-3 {\n  color: #f9a825;\n}\n.text-yellow-active.active.darken-4 {\n  color: #f57f17;\n}\n.text-yellow-active.active.accent-1 {\n  color: #ffff8d;\n}\n.text-yellow-active.active.accent-2 {\n  color: #ffff00;\n}\n.text-yellow-active.active.accent-3 {\n  color: #ffea00;\n}\n.text-yellow-active.active.accent-4 {\n  color: #ffd600;\n}\n.text-amber-active.active {\n  color: #FFC107;\n}\n.text-amber-active.active.lighten-1 {\n  color: #ffca28;\n}\n.text-amber-active.active.lighten-2 {\n  color: #ffd54f;\n}\n.text-amber-active.active.lighten-3 {\n  color: #ffe082;\n}\n.text-amber-active.active.lighten-4 {\n  color: #ffecb3;\n}\n.text-amber-active.active.lighten-5 {\n  color: #fff8e1;\n}\n.text-amber-active.active.darken-1 {\n  color: #ffb300;\n}\n.text-amber-active.active.darken-2 {\n  color: #ffa000;\n}\n.text-amber-active.active.darken-3 {\n  color: #ff8f00;\n}\n.text-amber-active.active.darken-4 {\n  color: #ff6f00;\n}\n.text-amber-active.active.accent-1 {\n  color: #ffe57f;\n}\n.text-amber-active.active.accent-2 {\n  color: #ffd740;\n}\n.text-amber-active.active.accent-3 {\n  color: #ffc400;\n}\n.text-amber-active.active.accent-4 {\n  color: #ffab00;\n}\n.text-orange-active.active {\n  color: #FF9800;\n}\n.text-orange-active.active.lighten-1 {\n  color: #ffa726;\n}\n.text-orange-active.active.lighten-2 {\n  color: #ffb74d;\n}\n.text-orange-active.active.lighten-3 {\n  color: #ffcc80;\n}\n.text-orange-active.active.lighten-4 {\n  color: #ffe0b2;\n}\n.text-orange-active.active.lighten-5 {\n  color: #fff3e0;\n}\n.text-orange-active.active.darken-1 {\n  color: #fb8c00;\n}\n.text-orange-active.active.darken-2 {\n  color: #f57c00;\n}\n.text-orange-active.active.darken-3 {\n  color: #ef6c00;\n}\n.text-orange-active.active.darken-4 {\n  color: #e65100;\n}\n.text-orange-active.active.accent-1 {\n  background-color: #ffd180;\n}\n.text-orange-active.active.accent-2 {\n  background-color: #ffab40;\n}\n.text-orange-active.active.accent-3 {\n  background-color: #ff9100;\n}\n.text-orange-active.active.accent-4 {\n  background-color: #ff6d00;\n}\n.text-deep-orange-active.active {\n  color: #ff5722;\n}\n.text-deep-orange-active.active.lighten-1 {\n  color: #ff7043;\n}\n.text-deep-orange-active.active.lighten-2 {\n  color: #ff8a65;\n}\n.text-deep-orange-active.active.lighten-3 {\n  color: #ffab91;\n}\n.text-deep-orange-active.active.lighten-4 {\n  color: #ffccbc;\n}\n.text-deep-orange-active.active.lighten-5 {\n  color: #fbe9e7;\n}\n.text-deep-orange-active.active.darken-1 {\n  color: #f4511e;\n}\n.text-deep-orange-active.active.darken-2 {\n  color: #e64a19;\n}\n.text-deep-orange-active.active.darken-3 {\n  color: #d84315;\n}\n.text-deep-orange-active.active.darken-4 {\n  color: #bf360c;\n}\n.text-deep-orange-active.active.accent-1 {\n  color: #ff9e80;\n}\n.text-deep-orange-active.active.accent-2 {\n  color: #ff6e40;\n}\n.text-deep-orange-active.active.accent-3 {\n  color: #ff3d00;\n}\n.text-deep-orange-active.active.accent-4 {\n  color: #dd2c00;\n}\n.text-brown-active.active {\n  color: #795548;\n}\n.text-brown-active.active.lighten-1 {\n  color: #8d6e63;\n}\n.text-brown-active.active.lighten-2 {\n  color: #a1887f;\n}\n.text-brown-active.active.lighten-3 {\n  color: #bcaaa4;\n}\n.text-brown-active.active.lighten-4 {\n  color: #d7ccc8;\n}\n.text-brown-active.active.lighten-5 {\n  color: #efebe9;\n}\n.text-brown-active.active.darken-1 {\n  color: #6d4c41;\n}\n.text-brown-active.active.darken-2 {\n  color: #5d4037;\n}\n.text-brown-active.active.darken-3 {\n  color: #4e342e;\n}\n.text-brown-active.active.darken-4 {\n  color: #3e2723;\n}\n.text-gray-active.active {\n  color: #9e9e9e;\n}\n.text-gray-active.active.lighten-1 {\n  color: #bdbdbd;\n}\n.text-gray-active.active.lighten-2 {\n  color: #e0e0e0;\n}\n.text-gray-active.active.lighten-3 {\n  color: #eeeeee;\n}\n.text-gray-active.active.lighten-4 {\n  color: #f5f5f5;\n}\n.text-gray-active.active.lighten-5 {\n  color: #fafafa;\n}\n.text-gray-active.active.darken-1 {\n  color: #757575;\n}\n.text-gray-active.active.darken-2 {\n  color: #616161;\n}\n.text-gray-active.active.darken-3 {\n  color: #424242;\n}\n.text-gray-active.active.darken-4 {\n  color: #212121;\n}\n.text-blue-gray-active.active {\n  color: #607d8b;\n}\n.text-blue-gray-active.active.lighten-1 {\n  color: #78909c;\n}\n.text-blue-gray-active.active.lighten-2 {\n  color: #90a4ae;\n}\n.text-blue-gray-active.active.lighten-3 {\n  color: #b0bec5;\n}\n.text-blue-gray-active.active.lighten-4 {\n  color: #cfd8dc;\n}\n.text-blue-gray-active.active.lighten-5 {\n  color: #eceff1;\n}\n.text-blue-gray-active.active.darken-1 {\n  color: #546e7a;\n}\n.text-blue-gray-active.active.darken-2 {\n  color: #455a64;\n}\n.text-blue-gray-active.active.darken-3 {\n  color: #37474f;\n}\n.text-blue-gray-active.active.darken-4 {\n  color: #263238;\n}\n";

		
(function(){
	
	var moduleFiles= [{"dev":51,"mode":16832,"nlink":5,"uid":1001,"gid":1002,"rdev":0,"blksize":1024,"ino":252093,"size":11,"blocks":3,"atimeMs":1540671811975.2349,"mtimeMs":1534429892560.1016,"ctimeMs":1534429892560.1016,"birthtimeMs":1534429892560.1016,"atime":"2018-10-27T20:23:31.975Z","mtime":"2018-08-16T14:31:32.560Z","ctime":"2018-08-16T14:31:32.560Z","birthtime":"2018-08-16T14:31:32.560Z","isdirectory":true,"isfile":false,"filename":"/kodhe-css/"},{"dev":51,"mode":16832,"nlink":3,"uid":1001,"gid":1002,"rdev":0,"blksize":1536,"ino":252094,"size":7,"blocks":3,"atimeMs":1540671811975.2349,"mtimeMs":1536207582513.3083,"ctimeMs":1536207582513.3083,"birthtimeMs":1536207582513.3083,"atime":"2018-10-27T20:23:31.975Z","mtime":"2018-09-06T04:19:42.513Z","ctime":"2018-09-06T04:19:42.513Z","birthtime":"2018-09-06T04:19:42.513Z","isdirectory":true,"isfile":false,"filename":"/kodhe-css/dynvox"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":1536,"ino":252668,"size":1379,"blocks":3,"atimeMs":1540671811975.2349,"mtimeMs":1534429896508.0798,"ctimeMs":1534429896508.0798,"birthtimeMs":1534429896508.0798,"atime":"2018-10-27T20:23:31.975Z","mtime":"2018-08-16T14:31:36.508Z","ctime":"2018-08-16T14:31:36.508Z","birthtime":"2018-08-16T14:31:36.508Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/dynvox/EscapeHtml.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":512,"ino":252673,"size":137,"blocks":2,"atimeMs":1540671811983.2349,"mtimeMs":1534429896508.0798,"ctimeMs":1534429896508.0798,"birthtimeMs":1534429896508.0798,"atime":"2018-10-27T20:23:31.983Z","mtime":"2018-08-16T14:31:36.508Z","ctime":"2018-08-16T14:31:36.508Z","birthtime":"2018-08-16T14:31:36.508Z","isdirectory":false,"isfile":true,"contents":"### dynvox\n\ndynvox permite código HTML dinámico, sin tener que manipular el DOM.<br>\nPronto se dará una documentación más detallada\n","filename":"/kodhe-css/dynvox/README.md"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":3584,"ino":252674,"size":3479,"blocks":5,"atimeMs":1540671811983.2349,"mtimeMs":1534429896508.0798,"ctimeMs":1534429896508.0798,"birthtimeMs":1534429896508.0798,"atime":"2018-10-27T20:23:31.983Z","mtime":"2018-08-16T14:31:36.508Z","ctime":"2018-08-16T14:31:36.508Z","birthtime":"2018-08-16T14:31:36.508Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/dynvox/Router.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":1536,"ino":252679,"size":1096,"blocks":2,"atimeMs":1540671812023.2358,"mtimeMs":1534429896508.0798,"ctimeMs":1534429896508.0798,"birthtimeMs":1534429896508.0798,"atime":"2018-10-27T20:23:32.023Z","mtime":"2018-08-16T14:31:36.508Z","ctime":"2018-08-16T14:31:36.508Z","birthtime":"2018-08-16T14:31:36.508Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/dynvox/init.es6"},{"dev":51,"mode":16832,"nlink":2,"uid":1001,"gid":1002,"rdev":0,"blksize":1024,"ino":252095,"size":11,"blocks":3,"atimeMs":1540671812027.2358,"mtimeMs":1536971854406.4424,"ctimeMs":1536971854406.4424,"birthtimeMs":1536971854406.4424,"atime":"2018-10-27T20:23:32.027Z","mtime":"2018-09-15T00:37:34.406Z","ctime":"2018-09-15T00:37:34.406Z","birthtime":"2018-09-15T00:37:34.406Z","isdirectory":true,"isfile":false,"filename":"/kodhe-css/dynvox/v2"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":19456,"ino":252681,"size":19075,"blocks":19,"atimeMs":1540671812027.2358,"mtimeMs":1538713513347.1787,"ctimeMs":1538713513347.1787,"birthtimeMs":1538713513347.1787,"atime":"2018-10-27T20:23:32.027Z","mtime":"2018-10-05T04:25:13.347Z","ctime":"2018-10-05T04:25:13.347Z","birthtime":"2018-10-05T04:25:13.347Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/dynvox/v2/DomParser.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":15360,"ino":252704,"size":14980,"blocks":16,"atimeMs":1540671812299.2417,"mtimeMs":1534429899828.0618,"ctimeMs":1534429899828.0618,"birthtimeMs":1534429899828.0618,"atime":"2018-10-27T20:23:32.299Z","mtime":"2018-08-16T14:31:39.828Z","ctime":"2018-08-16T14:31:39.828Z","birthtime":"2018-08-16T14:31:39.828Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/dynvox/v2/DomParser2.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":4608,"ino":252705,"size":4347,"blocks":6,"atimeMs":1540671812527.2466,"mtimeMs":1534429899828.0618,"ctimeMs":1534429899828.0618,"birthtimeMs":1534429899828.0618,"atime":"2018-10-27T20:23:32.527Z","mtime":"2018-08-16T14:31:39.828Z","ctime":"2018-08-16T14:31:39.828Z","birthtime":"2018-08-16T14:31:39.828Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/dynvox/v2/Observable.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":512,"ino":1089897,"size":43,"blocks":1,"atimeMs":1540671812583.2478,"mtimeMs":1536973076743.472,"ctimeMs":1536973076743.472,"birthtimeMs":1536973076743.472,"atime":"2018-10-27T20:23:32.583Z","mtime":"2018-09-15T00:57:56.743Z","ctime":"2018-09-15T00:57:56.743Z","birthtime":"2018-09-15T00:57:56.743Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/dynvox/v2/Proxy.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":6656,"ino":252706,"size":6348,"blocks":7,"atimeMs":1540671812583.2478,"mtimeMs":1534429899828.0618,"ctimeMs":1534429899828.0618,"birthtimeMs":1534429899828.0618,"atime":"2018-10-27T20:23:32.583Z","mtime":"2018-08-16T14:31:39.828Z","ctime":"2018-08-16T14:31:39.828Z","birthtime":"2018-08-16T14:31:39.828Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/dynvox/v2/Scope.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":4608,"ino":252707,"size":4278,"blocks":6,"atimeMs":1540671812651.2493,"mtimeMs":1534429899828.0618,"ctimeMs":1534429899828.0618,"birthtimeMs":1534429899828.0618,"atime":"2018-10-27T20:23:32.651Z","mtime":"2018-08-16T14:31:39.828Z","ctime":"2018-08-16T14:31:39.828Z","birthtime":"2018-08-16T14:31:39.828Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/dynvox/v2/Scope2.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":512,"ino":252708,"size":434,"blocks":2,"atimeMs":1540671812687.25,"mtimeMs":1534429899828.0618,"ctimeMs":1534429899828.0618,"birthtimeMs":1534429899828.0618,"atime":"2018-10-27T20:23:32.687Z","mtime":"2018-08-16T14:31:39.828Z","ctime":"2018-08-16T14:31:39.828Z","birthtime":"2018-08-16T14:31:39.828Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/dynvox/v2/ScopeObserver.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":1024,"ino":252709,"size":989,"blocks":3,"atimeMs":1540671812687.25,"mtimeMs":1534429899828.0618,"ctimeMs":1534429899828.0618,"birthtimeMs":1534429899828.0618,"atime":"2018-10-27T20:23:32.687Z","mtime":"2018-08-16T14:31:39.828Z","ctime":"2018-08-16T14:31:39.828Z","birthtime":"2018-08-16T14:31:39.828Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/dynvox/v2/_extensions.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":512,"ino":252710,"size":318,"blocks":2,"atimeMs":1540671812691.2502,"mtimeMs":1534429899828.0618,"ctimeMs":1534429899828.0618,"birthtimeMs":1534429899828.0618,"atime":"2018-10-27T20:23:32.691Z","mtime":"2018-08-16T14:31:39.828Z","ctime":"2018-08-16T14:31:39.828Z","birthtime":"2018-08-16T14:31:39.828Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/dynvox/v2/init.es6"},{"dev":51,"mode":16832,"nlink":2,"uid":1001,"gid":1002,"rdev":0,"blksize":1536,"ino":252096,"size":24,"blocks":3,"atimeMs":1540671812691.2502,"mtimeMs":1534429908344.0154,"ctimeMs":1534429908344.0154,"birthtimeMs":1534429908344.0154,"atime":"2018-10-27T20:23:32.691Z","mtime":"2018-08-16T14:31:48.344Z","ctime":"2018-08-16T14:31:48.344Z","birthtime":"2018-08-16T14:31:48.344Z","isdirectory":true,"isfile":false,"filename":"/kodhe-css/elements"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":2560,"ino":252711,"size":2238,"blocks":4,"atimeMs":1540671812691.2502,"mtimeMs":1534429899828.0618,"ctimeMs":1534429899828.0618,"birthtimeMs":1534429899828.0618,"atime":"2018-10-27T20:23:32.691Z","mtime":"2018-08-16T14:31:39.828Z","ctime":"2018-08-16T14:31:39.828Z","birthtime":"2018-08-16T14:31:39.828Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Anchor.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":2560,"ino":252712,"size":2087,"blocks":4,"atimeMs":1540671812719.2507,"mtimeMs":1534429899828.0618,"ctimeMs":1534429899828.0618,"birthtimeMs":1534429899828.0618,"atime":"2018-10-27T20:23:32.719Z","mtime":"2018-08-16T14:31:39.828Z","ctime":"2018-08-16T14:31:39.828Z","birthtime":"2018-08-16T14:31:39.828Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Card.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":7680,"ino":252713,"size":7365,"blocks":7,"atimeMs":1540671812731.251,"mtimeMs":1534429899828.0618,"ctimeMs":1534429899828.0618,"birthtimeMs":1534429899828.0618,"atime":"2018-10-27T20:23:32.731Z","mtime":"2018-08-16T14:31:39.828Z","ctime":"2018-08-16T14:31:39.828Z","birthtime":"2018-08-16T14:31:39.828Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Dropdown.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":4608,"ino":252714,"size":4214,"blocks":6,"atimeMs":1540671812767.2517,"mtimeMs":1534429899828.0618,"ctimeMs":1534429899828.0618,"birthtimeMs":1534429899828.0618,"atime":"2018-10-27T20:23:32.767Z","mtime":"2018-08-16T14:31:39.828Z","ctime":"2018-08-16T14:31:39.828Z","birthtime":"2018-08-16T14:31:39.828Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Elastic.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":1536,"ino":252715,"size":1047,"blocks":3,"atimeMs":1540671812791.2522,"mtimeMs":1534429903056.0442,"ctimeMs":1534429903056.0442,"birthtimeMs":1534429903056.0442,"atime":"2018-10-27T20:23:32.791Z","mtime":"2018-08-16T14:31:43.056Z","ctime":"2018-08-16T14:31:43.056Z","birthtime":"2018-08-16T14:31:43.056Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Element.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":3072,"ino":252723,"size":2827,"blocks":4,"atimeMs":1540671812799.2524,"mtimeMs":1538713650913.8887,"ctimeMs":1538713650913.8887,"birthtimeMs":1538713650913.8887,"atime":"2018-10-27T20:23:32.799Z","mtime":"2018-10-05T04:27:30.914Z","ctime":"2018-10-05T04:27:30.914Z","birthtime":"2018-10-05T04:27:30.914Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/HasTooltip.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":9216,"ino":252724,"size":8802,"blocks":10,"atimeMs":1540671812823.253,"mtimeMs":1534429903056.0442,"ctimeMs":1534429903056.0442,"birthtimeMs":1534429903056.0442,"atime":"2018-10-27T20:23:32.823Z","mtime":"2018-08-16T14:31:43.056Z","ctime":"2018-08-16T14:31:43.056Z","birthtime":"2018-08-16T14:31:43.056Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Input-createSelect.js"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":8192,"ino":252725,"size":8069,"blocks":8,"atimeMs":1540671812823.253,"mtimeMs":1534429903056.0442,"ctimeMs":1534429903056.0442,"birthtimeMs":1534429903056.0442,"atime":"2018-10-27T20:23:32.823Z","mtime":"2018-08-16T14:31:43.056Z","ctime":"2018-08-16T14:31:43.056Z","birthtime":"2018-08-16T14:31:43.056Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Input.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":5120,"ino":252726,"size":4772,"blocks":6,"atimeMs":1540671812875.2542,"mtimeMs":1534429903056.0442,"ctimeMs":1534429903056.0442,"birthtimeMs":1534429903056.0442,"atime":"2018-10-27T20:23:32.875Z","mtime":"2018-08-16T14:31:43.056Z","ctime":"2018-08-16T14:31:43.056Z","birthtime":"2018-08-16T14:31:43.056Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Modal.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":3072,"ino":252727,"size":2635,"blocks":4,"atimeMs":1540671812907.255,"mtimeMs":1534429903056.0442,"ctimeMs":1534429903056.0442,"birthtimeMs":1534429903056.0442,"atime":"2018-10-27T20:23:32.907Z","mtime":"2018-08-16T14:31:43.056Z","ctime":"2018-08-16T14:31:43.056Z","birthtime":"2018-08-16T14:31:43.056Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Parallax.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":3072,"ino":252728,"size":2743,"blocks":4,"atimeMs":1540671812935.2554,"mtimeMs":1534429903056.0442,"ctimeMs":1534429903056.0442,"birthtimeMs":1534429903056.0442,"atime":"2018-10-27T20:23:32.935Z","mtime":"2018-08-16T14:31:43.056Z","ctime":"2018-08-16T14:31:43.056Z","birthtime":"2018-08-16T14:31:43.056Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Pinned.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":4096,"ino":252729,"size":3787,"blocks":5,"atimeMs":1540671812947.2556,"mtimeMs":1534429903056.0442,"ctimeMs":1534429903056.0442,"birthtimeMs":1534429903056.0442,"atime":"2018-10-27T20:23:32.947Z","mtime":"2018-08-16T14:31:43.056Z","ctime":"2018-08-16T14:31:43.056Z","birthtime":"2018-08-16T14:31:43.056Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/ScrollFire.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":4608,"ino":252730,"size":4128,"blocks":5,"atimeMs":1540671812967.256,"mtimeMs":1540671808855.1677,"ctimeMs":1540671808855.1677,"birthtimeMs":1540671808855.1677,"atime":"2018-10-27T20:23:32.967Z","mtime":"2018-10-27T20:23:28.855Z","ctime":"2018-10-27T20:23:28.855Z","birthtime":"2018-10-27T20:23:28.855Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/SideNav.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":10752,"ino":252731,"size":10348,"blocks":11,"atimeMs":1540671812991.2566,"mtimeMs":1538717071317.2673,"ctimeMs":1538717071317.2673,"birthtimeMs":1538717071317.2673,"atime":"2018-10-27T20:23:32.991Z","mtime":"2018-10-05T05:24:31.317Z","ctime":"2018-10-05T05:24:31.317Z","birthtime":"2018-10-05T05:24:31.317Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Slider.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":2560,"ino":252732,"size":2118,"blocks":3,"atimeMs":1540671813067.2583,"mtimeMs":1534429903056.0442,"ctimeMs":1534429903056.0442,"birthtimeMs":1534429903056.0442,"atime":"2018-10-27T20:23:33.067Z","mtime":"2018-08-16T14:31:43.056Z","ctime":"2018-08-16T14:31:43.056Z","birthtime":"2018-08-16T14:31:43.056Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Tab.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":5120,"ino":252733,"size":4768,"blocks":6,"atimeMs":1540671813075.2583,"mtimeMs":1534429903056.0442,"ctimeMs":1534429903056.0442,"birthtimeMs":1534429903056.0442,"atime":"2018-10-27T20:23:33.075Z","mtime":"2018-08-16T14:31:43.056Z","ctime":"2018-08-16T14:31:43.056Z","birthtime":"2018-08-16T14:31:43.056Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Tabs.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":13312,"ino":252734,"size":12935,"blocks":12,"atimeMs":1540671813095.2588,"mtimeMs":1534429908344.0154,"ctimeMs":1534429908344.0154,"birthtimeMs":1534429908344.0154,"atime":"2018-10-27T20:23:33.095Z","mtime":"2018-08-16T14:31:48.344Z","ctime":"2018-08-16T14:31:48.344Z","birthtime":"2018-08-16T14:31:48.344Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Theme.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":4608,"ino":252767,"size":4160,"blocks":5,"atimeMs":1540671813115.2593,"mtimeMs":1534429908344.0154,"ctimeMs":1534429908344.0154,"birthtimeMs":1534429908344.0154,"atime":"2018-10-27T20:23:33.115Z","mtime":"2018-08-16T14:31:48.344Z","ctime":"2018-08-16T14:31:48.344Z","birthtime":"2018-08-16T14:31:48.344Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Toast.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":8704,"ino":252768,"size":8366,"blocks":8,"atimeMs":1540671813127.2595,"mtimeMs":1534429908344.0154,"ctimeMs":1534429908344.0154,"birthtimeMs":1534429908344.0154,"atime":"2018-10-27T20:23:33.127Z","mtime":"2018-08-16T14:31:48.344Z","ctime":"2018-08-16T14:31:48.344Z","birthtime":"2018-08-16T14:31:48.344Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/Tooltip.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":3072,"ino":252769,"size":2604,"blocks":4,"atimeMs":1540671813167.2603,"mtimeMs":1534429908344.0154,"ctimeMs":1534429908344.0154,"birthtimeMs":1534429908344.0154,"atime":"2018-10-27T20:23:33.167Z","mtime":"2018-08-16T14:31:48.344Z","ctime":"2018-08-16T14:31:48.344Z","birthtime":"2018-08-16T14:31:48.344Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/index.es6"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":19456,"ino":252770,"size":19027,"blocks":17,"atimeMs":1540671813171.2605,"mtimeMs":1534429908344.0154,"ctimeMs":1534429908344.0154,"birthtimeMs":1534429908344.0154,"atime":"2018-10-27T20:23:33.171Z","mtime":"2018-08-16T14:31:48.344Z","ctime":"2018-08-16T14:31:48.344Z","birthtime":"2018-08-16T14:31:48.344Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/jquery-mask.js"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":512,"ino":252771,"size":41,"blocks":1,"atimeMs":1540671813175.2605,"mtimeMs":1534429908344.0154,"ctimeMs":1534429908344.0154,"birthtimeMs":1534429908344.0154,"atime":"2018-10-27T20:23:33.175Z","mtime":"2018-08-16T14:31:48.344Z","ctime":"2018-08-16T14:31:48.344Z","birthtime":"2018-08-16T14:31:48.344Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/elements/jquery-replace.js"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":1536,"ino":252610,"size":1086,"blocks":3,"atimeMs":1540671813175.2605,"mtimeMs":1534429890856.1108,"ctimeMs":1534429890856.1108,"birthtimeMs":1534429890856.1108,"atime":"2018-10-27T20:23:33.175Z","mtime":"2018-08-16T14:31:30.856Z","ctime":"2018-08-16T14:31:30.856Z","birthtime":"2018-08-16T14:31:30.856Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/index.js"},{"dev":51,"mode":16832,"nlink":3,"uid":1001,"gid":1002,"rdev":0,"blksize":512,"ino":252097,"size":3,"blocks":1,"atimeMs":1540671813175.2605,"mtimeMs":1534429848700.3406,"ctimeMs":1534429848700.3406,"birthtimeMs":1534429848700.3406,"atime":"2018-10-27T20:23:33.175Z","mtime":"2018-08-16T14:30:48.700Z","ctime":"2018-08-16T14:30:48.700Z","birthtime":"2018-08-16T14:30:48.700Z","isdirectory":true,"isfile":false,"filename":"/kodhe-css/node_modules"},{"dev":51,"mode":16832,"nlink":2,"uid":1001,"gid":1002,"rdev":0,"blksize":512,"ino":252098,"size":3,"blocks":1,"atimeMs":1540671813175.2605,"mtimeMs":1534429908344.0154,"ctimeMs":1534429908344.0154,"birthtimeMs":1534429908344.0154,"atime":"2018-10-27T20:23:33.175Z","mtime":"2018-08-16T14:31:48.344Z","ctime":"2018-08-16T14:31:48.344Z","birthtime":"2018-08-16T14:31:48.344Z","isdirectory":true,"isfile":false,"filename":"/kodhe-css/node_modules/jquery"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":512,"ino":252772,"size":83,"blocks":1,"atimeMs":1540671813175.2605,"mtimeMs":1534429908344.0154,"ctimeMs":1534429908344.0154,"birthtimeMs":1534429908344.0154,"atime":"2018-10-27T20:23:33.175Z","mtime":"2018-08-16T14:31:48.344Z","ctime":"2018-08-16T14:31:48.344Z","birthtime":"2018-08-16T14:31:48.344Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/node_modules/jquery/index.js"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":512,"ino":252611,"size":45,"blocks":1,"atimeMs":1540672481049.6072,"mtimeMs":1534429890856.1108,"ctimeMs":1534429890856.1108,"birthtimeMs":1534429890856.1108,"atime":"2018-10-27T20:34:41.050Z","mtime":"2018-08-16T14:31:30.856Z","ctime":"2018-08-16T14:31:30.856Z","birthtime":"2018-08-16T14:31:30.856Z","isdirectory":false,"isfile":true,"contents":"{\n\t\"name\": \"kodhe-css\",\n\t\"main\": \"index.js\"\n}","filename":"/kodhe-css/package.json"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":6656,"ino":252612,"size":6275,"blocks":9,"atimeMs":1540671813175.2605,"mtimeMs":1534429890856.1108,"ctimeMs":1534429890856.1108,"birthtimeMs":1534429890856.1108,"atime":"2018-10-27T20:23:33.175Z","mtime":"2018-08-16T14:31:30.856Z","ctime":"2018-08-16T14:31:30.856Z","birthtime":"2018-08-16T14:31:30.856Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/registerelement.js"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":20992,"ino":252613,"size":20505,"blocks":16,"atimeMs":1540671813175.2605,"mtimeMs":1534429891396.108,"ctimeMs":1534429891396.108,"birthtimeMs":1534429891396.108,"atime":"2018-10-27T20:23:33.175Z","mtime":"2018-08-16T14:31:31.396Z","ctime":"2018-08-16T14:31:31.396Z","birthtime":"2018-08-16T14:31:31.396Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/vox.js"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":18944,"ino":252617,"size":18717,"blocks":17,"atimeMs":1540671813175.2605,"mtimeMs":1534429891396.108,"ctimeMs":1534429891396.108,"birthtimeMs":1534429891396.108,"atime":"2018-10-27T20:23:33.175Z","mtime":"2018-08-16T14:31:31.396Z","ctime":"2018-08-16T14:31:31.396Z","birthtime":"2018-08-16T14:31:31.396Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/waves.js"},{"dev":51,"mode":33188,"nlink":1,"uid":1001,"gid":1002,"rdev":0,"blksize":79360,"ino":252618,"size":79309,"blocks":60,"atimeMs":1540671813175.2605,"mtimeMs":1534429892560.1016,"ctimeMs":1534429892560.1016,"birthtimeMs":1534429892560.1016,"atime":"2018-10-27T20:23:33.175Z","mtime":"2018-08-16T14:31:32.560Z","ctime":"2018-08-16T14:31:32.560Z","birthtime":"2018-08-16T14:31:32.560Z","isdirectory":false,"isfile":true,"contents":"","filename":"/kodhe-css/webcomponents-lite.js"}]
	var funcs= [
		
		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			'use strict';
			var matchHtmlRegExp = /["'&<>]/;
			module.exports = escapeHtml;
			function escapeHtml(string) {
			    var str = '' + string;
			    var match = matchHtmlRegExp.exec(str);
			    if (!match) {
			        return str;
			    }
			    var escape;
			    var html = '';
			    var index = 0;
			    var lastIndex = 0;
			    for (index = match.index; index < str.length; index++) {
			        switch (str.charCodeAt(index)) {
			        case 34:
			            escape = '&quot;';
			            break;
			        case 39:
			            escape = '&#39;';
			            break;
			        case 60:
			            escape = '&lt;';
			            break;
			        case 62:
			            escape = '&gt;';
			            break;
			        default:
			            continue;
			        }
			        if (lastIndex !== index) {
			            html += str.substring(lastIndex, index);
			        }
			        lastIndex = index + 1;
			        html += escape;
			    }
			    return lastIndex !== index ? html + str.substring(lastIndex, index) : html;
			}
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var _wr = function (type) {
			    var orig = history[type];
			    return function () {
			        var rv = orig.apply(this, arguments);
			        var e = new Event(type);
			        e.arguments = arguments;
			        window.dispatchEvent(e);
			        return rv;
			    };
			};
			history.pushState = _wr('pushState');
			history.replaceState = _wr('replaceState');
			{
			    var Router = function Router() {
			        Router.$constructor ? Router.$constructor.apply(this, arguments) : Router.$superClass && Router.$superClass.apply(this, arguments);
			    };
			    Object.defineProperty(Router, '$constructor', {
			        enumerable: false,
			        value: function () {
			            this.$ = {};
			            this.$.routes = [];
			        }
			    });
			    Object.defineProperty(Router.prototype, 'attachEvents', {
			        enumerable: false,
			        value: function () {
			            if (this.attached)
			                return;
			            var hashChange = this.start.bind(this);
			            if (window.addEventListener) {
			                window.addEventListener('pushState', hashChange, false);
			                window.addEventListener('popstate', hashChange, false);
			                window.addEventListener('hashchange', hashChange, false);
			            } else if (window.attachEvent) {
			                window.attachEvent('onpushState', hashchange);
			                window.attachEvent('onpopstate', hashchange);
			                window.attachEvent('onhashchange', hashchange);
			            }
			            this.attached = true;
			        }
			    });
			    Router.prototype.__defineGetter__('location', function () {
			        return this.$.location;
			    });
			    Router.prototype.__defineGetter__('params', function () {
			        return this.$.params;
			    });
			    Object.defineProperty(Router.prototype, 'start', {
			        enumerable: false,
			        value: function (index, Uri) {
			            if (this.noprocesar)
			                return;
			            this.attachEvents();
			            if (Uri) {
			                this.noprocesar = true;
			                if (this.pushstate)
			                    history.pushState({}, '', Uri);
			                else
			                    location = Uri;
			                this.noprocesar = false;
			            }
			            var href = this.pushstate ? location.pathname + location.hash : location.hash.substring(1);
			            var url, search, hash;
			            var z = href.indexOf('?');
			            if (z >= 0)
			                search = href.substring(z);
			            var z2 = href.indexOf('#');
			            if (z2 >= 0)
			                hash = href.substring(z2);
			            if (z2 < 0)
			                z2 = href.length;
			            if (z < 0)
			                z = href.length;
			            z = Math.min(z, z2);
			            url = href.substring(0, z);
			            var ret = this.$.location && this.$.location.pathname == url;
			            this.$.location = {
			                pathname: url,
			                href: href,
			                search: search,
			                hash: hash
			            };
			            if (ret)
			                return;
			            var parts = url.split('/'), part1, part2, correcto, params = {};
			            index = index | 0;
			            for (var i = index; i < this.$.routes.length; i++) {
			                var routeA = this.$.routes[i];
			                var route = routeA.def;
			                if (route == -1)
			                    correcto = true;
			                else if (route.length == parts.length) {
			                    correcto = true;
			                    for (var y = 0; y < route.length; y++) {
			                        part1 = route[y];
			                        part2 = decodeURIComponent(parts[y]);
			                        if (!part1.param) {
			                            if (part1.name != part2) {
			                                correcto = false;
			                                break;
			                            }
			                        } else {
			                            params[part1.name] = part2;
			                        }
			                    }
			                }
			                if (correcto) {
			                    return this._execute(i, params, routeA);
			                }
			            }
			        }
			    });
			    Object.defineProperty(Router.prototype, 'redirect', {
			        enumerable: false,
			        value: function (url) {
			            if (this.pushstate)
			                history.pushState({}, '', url);
			            else
			                location = url;
			        }
			    });
			    Object.defineProperty(Router.prototype, '_execute', {
			        enumerable: false,
			        value: function (index, params, route) {
			            var continuar = function (self$0) {
			                return function () {
			                    return self$0.start(index + 1);
			                };
			            }(this);
			            var req = {
			                params: params,
			                location: this.location,
			                continue: continuar
			            };
			            this.$.params = params;
			            return route.func(req);
			        }
			    });
			    Object.defineProperty(Router.prototype, 'route', {
			        enumerable: false,
			        value: function (uri, func) {
			            var parts = uri.split('/');
			            var urldefinition = [];
			            for (var i = 0; i < parts.length; i++) {
			                var part = parts[i];
			                if (part[0] == ':') {
			                    urldefinition.push({
			                        'param': true,
			                        'name': part.substring(1)
			                    });
			                } else {
			                    urldefinition.push({ 'name': part });
			                }
			            }
			            this.$.routes.push({
			                def: urldefinition,
			                func: func
			            });
			        }
			    });
			    Object.defineProperty(Router.prototype, 'use', {
			        enumerable: false,
			        value: function (func) {
			            this.$.routes.push({
			                def: -1,
			                func: func
			            });
			        }
			    });
			}
			exports.default = Router;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			{
			    var VoxScope = function VoxScope() {
			        VoxScope.$constructor ? VoxScope.$constructor.apply(this, arguments) : VoxScope.$superClass && VoxScope.$superClass.apply(this, arguments);
			    };
			    VoxScope.__defineGetter__('EscapeHtml', function () {
			        return require('./EscapeHtml');
			    });
			    VoxScope.__defineGetter__('Router', function () {
			        return require('./Router').default;
			    });
			    VoxScope.__defineGetter__('Convert', function () {
			        return require('./Convert').default;
			    });
			}
			exports = module.exports = VoxScope;
			core.dynvox = VoxScope;
			require('./v2/init');
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var yid = 0;
			{
			    var DomParser = function DomParser() {
			        DomParser.$constructor ? DomParser.$constructor.apply(this, arguments) : DomParser.$superClass && DomParser.$superClass.apply(this, arguments);
			    };
			    Object.defineProperty(DomParser.prototype, 'parse', {
			        enumerable: false,
			        value: function (jobject, scope) {
			            if (!jobject)
			                jobject = $('html');
			            var objects = jobject.find('>*'), obj, l = [], sc;
			            for (var i = 0; i < objects.length; i++) {
			                obj = objects.eq(i);
			                l.push(obj);
			            }
			            var scopeP = scope;
			            for (var i = 0; i < l.length; i++) {
			                obj = l[i];
			                sc = obj.attr('voxs-scope');
			                scope = sc ? core.dynvox.v2.Scope.get(sc) : scopeP || core.dynvox.v2.Scope.get('default');
			                if (scope) {
			                    this.parseOne(obj, scope);
			                    this.parse(obj, scope);
			                }
			                delete l[i];
			            }
			            l = null;
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'parseOne', {
			        enumerable: false,
			        value: function (jobject, scope) {
			            if (jobject.attr('voxs-ya') !== undefined)
			                return;
			            var good, good2, gtext;
			            var atxr = jobject.get(0).attributes;
			            var attrs = [], attr;
			            for (var z = 0; z < atxr.length; z++) {
			                if (atxr[z].name.startsWith(':') || atxr[z].name.startsWith('@') || atxr[z].name.startsWith('dynamic-') || atxr[z].name.startsWith('dynvox-') || atxr[z].name.startsWith('d-') || atxr[z].name.startsWith('voxs-') || atxr[z].name == 'voxs')
			                    good = true;
			                attrs.push(atxr[z]);
			            }
			            if (jobject[0].childElementCount == 0) {
			                gtext = jobject.text();
			                if (gtext.indexOf('#{') < 0)
			                    gtext = null;
			                else
			                    good2 = true;
			            }
			            if (!good && !good2) {
			                attrs = null;
			                return;
			            }
			            if (good) {
			                if (jobject.attr('dynvox-savecode') !== undefined) {
			                    scope[jobject.attr('dynvox-savecode')] = jobject.html();
			                }
			                var for1;
			                for (var i = 0; i < attrs.length; i++) {
			                    attr = attrs[i];
			                    if (attr.name == 'event-all' || attr.name == '@@') {
			                        this.attachEventAll(scope, jobject, attr.value);
			                    } else if (attr.name.startsWith('event-')) {
			                        this.attachEvent(scope, jobject, attr.name.substring(6), attr.value);
			                    } else if (attr.name.startsWith('@')) {
			                        this.attachEvent(scope, jobject, attr.name.substring(1), attr.value);
			                    } else if (attr.name == 'voxs-if' || attr.name == 'dynamic-if' || attr.name == 'dynamic-nif' || attr.name == 'd-if' || attr.name == 'd-nif') {
			                        this.attachObserverIf(scope, jobject, attr);
			                    } else {
			                        if (attr.name == 'dynamic-repeat' || attr.name == 'voxs-repeat') {
			                            this.attachObserverArray(scope, jobject, attr);
			                        } else if (attr.name == 'd-for') {
			                            for1 = attr.value;
			                            if (for1) {
			                                for1 = for1.trim().split(/\s+/ig);
			                                if (for1[0] == 'for' && for1[2] == 'in') {
			                                    jobject.attr('dynamic-foreach', for1[1]);
			                                    jobject.attr('dynamic-in', for1.slice(3).join(' '));
			                                    this.attachObserverArray(scope, jobject, attr);
			                                } else {
			                                    console.warn('> Invalid `d-for` attribute: ', attr.value);
			                                }
			                            }
			                        } else if (attr.name.startsWith(':')) {
			                            this.attachObserverAttr(scope, jobject, attr);
			                        } else if (attr.value.indexOf('#{') >= 0) {
			                            this.attachObserverAttr(scope, jobject, attr);
			                        }
			                    }
			                }
			            }
			            if (gtext) {
			                this.attachObserverText(scope, jobject, gtext);
			            }
			            jobject.attr('voxs-ya', true);
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'attachObserverArray', {
			        enumerable: false,
			        value: function (scope, jobject, attr) {
			            var varname = jobject.attr('dynamic-foreach') || jobject.attr('voxs-var');
			            var arrname = jobject.attr('dynamic-in') || jobject.attr('voxs-name');
			            var dom = $(jobject.html()), newdom, child, childs = {}, funcs = {}, self = this, items = {};
			            jobject.html('');
			            window.DEBUG && console.info('DOM:', dom);
			            var adding = function (ev, index) {
			                var current = true, temporal, g;
			                if (index == undefined)
			                    index = ev.value.index;
			                newdom = items[index];
			                child = childs[index];
			                if (!newdom) {
			                    newdom = dom.clone();
			                    current = false;
			                    temporal = $('<div>');
			                    temporal.append(newdom);
			                }
			                items[index] = newdom;
			                if (!child) {
			                    child = scope.createChild();
			                    childs[index] = child;
			                    newdom.attr('dynamic-child-scope', true);
			                    newdom.each(function () {
			                        this['dynamic_scope'] = child;
			                    });
			                }
			                if (current) {
			                    child[varname] = ev.value;
			                } else {
			                    child[varname] = ev.value[0];
			                    child.$index = index;
			                    g = funcs[index] = function (ev) {
			                        if (g.finished)
			                            return;
			                        window.DEBUG && console.info('CAMBIANDO ...', ev, index);
			                        adding(ev, index);
			                    };
			                    g.event = arrname + '.' + child.$index;
			                    if (newdom && newdom.length) {
			                        scope.attach({
			                            event: 'change',
			                            name: arrname + '.' + child.$index,
			                            func: g,
			                            dom: newdom
			                        });
			                        self.parse(temporal, child);
			                    }
			                }
			                if (!current)
			                    jobject.append(newdom);
			            };
			            var removing = function (ev) {
			                var keys = Object.keys(items);
			                if (ev.value < keys.length) {
			                    for (var i = ev.value; i < keys.length; i++) {
			                        try {
			                            items[i].remove();
			                            funcs[i].finished = true;
			                            scope.removechange(funcs[i].event, funcs[i]);
			                            delete items[i];
			                            delete childs[i];
			                        } catch (e) {
			                            console.error('ERROR REMOVING:', e);
			                        }
			                    }
			                }
			            };
			            scope.attach({
			                event: 'push',
			                name: arrname,
			                func: adding,
			                dom: jobject
			            });
			            scope.attach({
			                event: 'change',
			                name: arrname + '.length',
			                func: removing,
			                dom: jobject
			            });
			            var j = function () {
			                var keys = Object.keys(items);
			                if (keys.length > 0) {
			                    for (var i = 0; i < keys.length; i++) {
			                        try {
			                            items[i].remove();
			                            funcs[i].finished = true;
			                            delete items[i];
			                            delete childs[i];
			                        } catch (e) {
			                            console.error('ERROR REMOVING:', e);
			                        }
			                    }
			                }
			                var parts = arrname.split('.');
			                var o = scope;
			                for (var i = 0; i < parts.length; i++) {
			                    o = o[parts[i]];
			                    if (!o)
			                        break;
			                }
			                if (o && o.length) {
			                    for (var i = 0; i < o.length; i++) {
			                        adding({
			                            value: {
			                                '0': o[i],
			                                'index': i
			                            }
			                        });
			                    }
			                }
			            };
			            j();
			            scope.attach({
			                event: 'change',
			                func: function (ev) {
			                    var t = ev.name + '.';
			                    if (arrname.startsWith(t) || arrname == ev.name) {
			                        j();
			                    }
			                },
			                dom: jobject
			            });
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'attachObserverAttr', {
			        enumerable: false,
			        value: function (scope, jobject, attr) {
			            var name = attr.name;
			            var value = attr.value;
			            if (name.startsWith('::')) {
			                name = name.substring(1);
			                value = attr.value;
			            } else if (name.startsWith(':')) {
			                name = name.substring(1);
			                value = '#{' + value + '}';
			            }
			            return this.attachObserver(scope, jobject, {
			                attr: attr,
			                text: value,
			                html: name == 'value'
			            });
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'attachObserverIf', {
			        enumerable: false,
			        value: function (scope, jobject, attr) {
			            var negate = attr.name == 'dynamic-nif' || attr.name == 'd-nif';
			            if (!negate) {
			                if (attr.value.startsWith('!')) {
			                    attr.value = attr.value.slice(1);
			                    negate = true;
			                }
			            }
			            return this.attachObserver(scope, jobject, {
			                attr: attr,
			                ifcondition: true,
			                negate: negate,
			                text: '#{' + attr.value + '}'
			            });
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'attachObserverText', {
			        enumerable: false,
			        value: function (scope, jobject, text) {
			            jobject.html('');
			            return this.attachObserver(scope, jobject, {
			                contentmode: true,
			                text: text
			            });
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'attachObserver', {
			        enumerable: false,
			        value: function (scope, jobject, options) {
			            var attr = options.attr;
			            var compilation = this.getVars(scope, options.text), var1, l, code2, put, u, disable, g;
			            var parse = function (self$0) {
			                return function () {
			                    self$0.parse(jobject, scope);
			                };
			            }(this);
			            if (compilation.vars) {
			                g = function () {
			                    if (disable) {
			                        return disable = false;
			                    }
			                    DomParser.pending.push({
			                        jobject: jobject,
			                        parse: parse,
			                        attr: attr,
			                        scope: scope,
			                        ifcondition: options.ifcondition,
			                        negate: options.negate,
			                        contentmode: options.contentmode,
			                        compilation: compilation
			                    });
			                    DomParser.enablePaint(attr && !options.ifcondition ? 10 : null);
			                };
			                scope.attach({
			                    event: 'change',
			                    func: function (ev) {
			                        var t = ev.name + '.', var1;
			                        for (var i = 0; i < compilation.vars.length; i++) {
			                            var1 = compilation.vars[i];
			                            if (var1.name.startsWith(t) || var1.name == ev.name) {
			                                g();
			                            }
			                        }
			                    },
			                    dom: jobject
			                });
			                DomParser.pending.push({
			                    jobject: jobject,
			                    attr: attr,
			                    parse: parse,
			                    scope: scope,
			                    negate: options.negate,
			                    ifcondition: options.ifcondition,
			                    contentmode: options.contentmode,
			                    compilation: compilation
			                });
			                DomParser.enablePaint(0);
			                if (options.ifcondition) {
			                    var udom = jobject.find('>*');
			                    if (udom.length > 0) {
			                        var ihtml = jobject.html();
			                        jobject.html('');
			                        udom.attr('voxs-ya', 'true');
			                        udom = null;
			                        jobject.data('delayed-dom', ihtml);
			                    }
			                } else {
			                    if (jobject.is('input,textarea,select') && compilation.vars.length == 1) {
			                        code2 = 'proxy.' + compilation.vars[0].real + '= arguments[0]';
			                        put = this.compile(scope, code2);
			                        if (jobject.is('input[type=checkbox]')) {
			                            jobject.on('change', function () {
			                                disable = true;
			                                clearTimeout(u);
			                                u = setTimeout(put.bind(put, jobject[0].checked), 1);
			                            });
			                        } else {
			                            jobject.on('change keyup', function () {
			                                disable = true;
			                                clearTimeout(u);
			                                u = setTimeout(put.bind(put, jobject.val()), 8);
			                            });
			                        }
			                    }
			                }
			            }
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'nullfunc', {
			        enumerable: false,
			        value: function () {
			            return 1;
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'compile', {
			        enumerable: false,
			        value: function (scope, text) {
			            var proxy = scope.getProxy();
			            var code = '(function(){\n\t\t\treturn ' + text + '\n\t\t})';
			            try {
			                return eval(code);
			            } catch (e) {
			                console.error('Error parsing: ', code, e);
			            }
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'getVars', {
			        enumerable: false,
			        value: function (scope, value) {
			            var i, offset = -1, y, con, con2, html, uoptions = [], newvar, vars = [], func;
			            while (true) {
			                con = undefined;
			                con2 = undefined;
			                i = value.indexOf('#{', offset);
			                y = -1;
			                html = false;
			                newvar = null;
			                if (i >= 0) {
			                    if (value[i - 1] == '#') {
			                        html = true;
			                    }
			                    y = value.indexOf('}', i + 2);
			                    if (y >= 0) {
			                        newvar = value.substring(i + 2, y);
			                        con2 = value.substring(y + 1);
			                    }
			                    con = value.substring(0, i - (html ? 1 : 0));
			                } else {
			                    con = value;
			                }
			                if (con) {
			                    uoptions.push({
			                        type: 'text',
			                        value: con
			                    });
			                }
			                if (newvar) {
			                    newvar = ' ' + newvar.replace(/[\S|\!]+([\w|\$|\-]+\.?\w?)*/ig, function (e) {
			                        var parts, real, part, important, ename, nor, ade = '';
			                        if (e.trim().startsWith('!')) {
			                            ade = '!';
			                            e = e.trim().substring(1);
			                        }
			                        if (e && !/\d/.test(e[0]) && /[\w|\$]/.test(e[0]) && e[0] != '|') {
			                            parts = e.trim().split('.');
			                            real = '';
			                            ename = '';
			                            important = '';
			                            nor = 0;
			                            for (var i = 0; i < parts.length; i++) {
			                                part = parts[i];
			                                if (part.indexOf('(') >= 0) {
			                                    if (part.endsWith('@')) {
			                                        part = part.substring(0, part.length - 1);
			                                        nor = -1;
			                                    } else {
			                                        nor++;
			                                    }
			                                }
			                                if (/\d/.test(part[0])) {
			                                    if (!nor) {
			                                        real += '[' + part + ']';
			                                    }
			                                    important += '[' + part + ']';
			                                } else if (part.indexOf('-') >= 0) {
			                                    if (!nor) {
			                                        real += '[' + JSON.stringify(part) + ']';
			                                    }
			                                    important += '[' + JSON.stringify(part) + ']';
			                                } else {
			                                    if (!nor) {
			                                        real += real ? '.' : '';
			                                        real += part;
			                                    }
			                                    important += i > 0 ? '.' : '';
			                                    important += part;
			                                }
			                                if (!nor) {
			                                    ename += ename ? '.' : '';
			                                    ename += part;
			                                }
			                                if (nor == -1)
			                                    nor = 1;
			                            }
			                            vars.push({
			                                real: real,
			                                name: ename
			                            });
			                            return ade + ' proxy' + (important[0] == '[' ? '' : '.') + important;
			                        }
			                        return ade + e;
			                    });
			                    func = this.compile(scope, newvar);
			                    uoptions.push({
			                        type: 'dynamic',
			                        value: func,
			                        html: html
			                    });
			                }
			                if (y < 0)
			                    break;
			                value = con2;
			            }
			            if (con2) {
			                uoptions.push({
			                    type: 'text',
			                    value: con2
			                });
			            }
			            return {
			                script: uoptions,
			                vars: vars
			            };
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'attachEvent', {
			        enumerable: false,
			        value: function (scope, jobject, name, value) {
			            var compilation = this.getVars(scope, '#{' + value + '}');
			            if (!compilation || !compilation.script.length)
			                return;
			            var getfunc = compilation.script[0].value;
			            jobject.on(name, function () {
			                var func = getfunc();
			                if (typeof func == 'function') {
			                    return func.apply(this, arguments);
			                }
			            });
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'getAllEvents', {
			        enumerable: false,
			        value: function (element) {
			            var result = [];
			            for (var key in element) {
			                if (key.indexOf('on') === 0) {
			                    result.push(key.slice(2));
			                }
			            }
			            return result.join(' ');
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'attachEventAll', {
			        enumerable: false,
			        value: function (scope, jobject, value) {
			            var name = this.getAllEvents(jobject[0]);
			            var compilation = this.getVars(scope, '#{' + value + '}');
			            if (!compilation || !compilation.script.length)
			                return;
			            var getfunc = compilation.script[0].value;
			            jobject.on(name, function (e) {
			                if (!e)
			                    return;
			                var events = getfunc();
			                if (!events)
			                    return;
			                var func = events[e.type];
			                if (typeof func == 'function') {
			                    return func.apply(this, arguments);
			                }
			            });
			        }
			    });
			    Object.defineProperty(DomParser, 'enablePaint', {
			        enumerable: false,
			        value: function (time) {
			            if (DomParser.paintTimeout) {
			                clearTimeout(DomParser.paintTimeout);
			            }
			            DomParser.paintTimeout = setTimeout(DomParser.executePaint, time === undefined ? 2 : 0);
			        }
			    });
			    Object.defineProperty(DomParser, 'executePaint', {
			        enumerable: false,
			        value: function () {
			            DomParser.paintTimeout = null;
			            var pending = DomParser.pending;
			            DomParser.pending = [];
			            var item, script, content = '', str, expr, textcontent;
			            for (var i = 0; i < pending.length; i++) {
			                item = pending[i];
			                content = '';
			                textcontent = false;
			                expr = null;
			                try {
			                    for (var y = 0; y < item.compilation.script.length; y++) {
			                        script = item.compilation.script[y];
			                        if (script.type == 'text') {
			                            textcontent = true;
			                            content += script.value;
			                        } else if (script.type == 'dynamic') {
			                            expr = script.value();
			                            str = expr === null || expr === undefined ? '' : expr.toString();
			                            if (!script.html && (!item.attr || item.attr.name != 'value')) {
			                                str = core.dynvox.EscapeHtml(str);
			                            }
			                            content += str;
			                        }
			                    }
			                } catch (e) {
			                    global.DEBUG && console.error('> Dyvox error: ', e);
			                }
			                var domParser;
			                if (item.attr) {
			                    if (item.ifcondition) {
			                        if (!!expr == !item.negate) {
			                            item.jobject.show();
			                            if (item.jobject.data('delayed-dom') !== undefined) {
			                                item.jobject.html(item.jobject.data('delayed-dom'));
			                                item.jobject.removeData('delayed-dom');
			                                domParser = new DomParser();
			                                domParser.parse(item.jobject, item.scope);
			                            }
			                        } else {
			                            item.jobject.hide();
			                        }
			                    } else {
			                        var name = item.attr.name;
			                        if (name.startsWith(':'))
			                            name = name.substring(1);
			                        if (name == 'value') {
			                            if (item.jobject.is('input[type=checkbox]')) {
			                                item.jobject.each(function () {
			                                    this.checked = !!expr;
			                                });
			                            } else {
			                                item.jobject.val(content);
			                            }
			                        } else if (name.startsWith('data-')) {
			                            item.jobject.attr(name, content);
			                            item.jobject.data(name.substring(5), textcontent ? content : expr);
			                        } else {
			                            if (textcontent) {
			                                item.jobject.attr(name, content);
			                            } else {
			                                if (expr === undefined)
			                                    item.jobject.removeAttr(name);
			                                else
			                                    item.jobject.attr(name, content);
			                            }
			                        }
			                    }
			                } else if (item.contentmode) {
			                    item.jobject.html(content);
			                    if (item.jobject.attr('dynvox-code') !== undefined) {
			                        item.parse();
			                    }
			                }
			            }
			        }
			    });
			}
			var removeFn = $.fn.remove;
			var emptyFn = $.fn.empty;
			var htmlFn = $.fn.html;
			var jqueryCleanData = jQuery.cleanData;
			$.fn.originalRemove = removeFn;
			var _clean = function (self) {
			    var ev, eves;
			    if (self.dynamic_events) {
			        window.DEBUG && console.info('REMOVING LISTENERS:', self.dynamic_events.length);
			        for (var i = 0; i < self.dynamic_events.length; i++) {
			            ev = self.dynamic_events[i];
			            if (ev.scope)
			                ev.scope.removeListener(ev.event, ev.func);
			            ev.scope = undefined;
			        }
			        delete self.dynamic_events;
			    }
			    if (self.dynamic_scope) {
			        delete self.dynamic_scope;
			    }
			    $(self).attr('dynamic-removed', true);
			};
			jQuery.cleanData = function (elems) {
			    for (var i = 0, elem; (elem = elems[i]) != null; i++) {
			        _clean(elem);
			    }
			    return jqueryCleanData.apply(this, arguments);
			};
			$.fn.removex = function () {
			    var g = function (self) {
			        var ev, eves;
			        if (self.dynamic_events) {
			            for (var id in self.dynamic_events) {
			                console.info('REMOVING LISTENERS:', self.dynamic_events[id].length);
			                delete self.dynamic_events[id];
			            }
			            delete self.dynamic_events;
			        }
			        if (self.dynamic_scope) {
			            delete self.dynamic_scope;
			        }
			        $(self).attr('dynamic-removed', true);
			    };
			    this.each(function () {
			        g(this);
			        var all = $(this).find(':not([dynamic-removed])');
			        if (all && all.length) {
			            for (var i = 0; i < all.length; i++) {
			                g(all[i]);
			            }
			        }
			    });
			    return removeFn.apply(this, arguments);
			};
			$.fn.voxscope = function () {
			    var e = $(this), final;
			    if (e.attr('dynamic-child-scope') !== undefined) {
			        final = e;
			    } else {
			        final = e.parents('[dynamic-child-scope]');
			    }
			    if (final && final.length) {
			        for (var i = 0; i < final.length; i++) {
			            if (final[i]['dynamic_scope'])
			                return final[i]['dynamic_scope'];
			        }
			    }
			    var sc = 'default';
			    if (e.attr('voxs-scope') !== undefined) {
			        sc = e.attr('voxs-scope');
			    } else {
			        final = e.parents('[voxs-scope]').eq(0);
			        if (final.length) {
			            sc = final.attr('voxs-scope');
			        }
			    }
			    return core.dynvox.v2.Scope.get(sc);
			};
			DomParser.pending = [];
			exports.default = DomParser;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var yid = 0;
			{
			    var DomParser = function DomParser() {
			        DomParser.$constructor ? DomParser.$constructor.apply(this, arguments) : DomParser.$superClass && DomParser.$superClass.apply(this, arguments);
			    };
			    Object.defineProperty(DomParser.prototype, 'parse', {
			        enumerable: false,
			        value: function (jobject, scope) {
			            if (!jobject)
			                jobject = $('html');
			            var objects = jobject.find('>*'), obj, l = [], sc;
			            for (var i = 0; i < objects.length; i++) {
			                obj = objects.eq(i);
			                l.push(obj);
			            }
			            var scopeP = scope;
			            for (var i = 0; i < l.length; i++) {
			                obj = l[i];
			                sc = obj.attr('voxs-scope');
			                scope = sc ? core.dynvox.v2.Scope.get(sc) : scopeP || core.dynvox.v2.Scope.get('default');
			                if (scope) {
			                    if (obj.attr('voxs') !== undefined || obj.attr('dynamic') !== undefined) {
			                        this.parseOne(obj, scope);
			                        this.parse(obj, scope);
			                    } else {
			                        this.parse(obj, scope);
			                    }
			                }
			                delete l[i];
			            }
			            l = null;
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'parseOne', {
			        enumerable: false,
			        value: function (jobject, scope) {
			            if (jobject.attr('voxs-ya') !== undefined)
			                return;
			            var atxr = jobject.get(0).attributes;
			            var attrs = [], attr;
			            for (var z = 0; z < atxr.length; z++) {
			                attrs.push(atxr[z]);
			            }
			            for (var i = 0; i < attrs.length; i++) {
			                attr = attrs[i];
			                if (attr.name == 'event-all') {
			                    this.attachEventAll(scope, jobject, attr.value);
			                } else if (attr.name.startsWith('event-')) {
			                    this.attachEvent(scope, jobject, attr.name.substring(6), attr.value);
			                } else if (attr.name == 'voxs-if' || attr.name == 'dynamic-if' || attr.name == 'dynamic-nif') {
			                    this.attachObserverIf(scope, jobject, attr);
			                } else {
			                    if (attr.name == 'dynamic-repeat' || attr.name == 'voxs-repeat') {
			                        this.attachObserverArray(scope, jobject, attr);
			                    } else if (attr.value.indexOf('#{') >= 0) {
			                        this.attachObserverAttr(scope, jobject, attr);
			                    }
			                }
			            }
			            if (jobject.find('*').length == 0) {
			                var texto = jobject.text();
			                if (texto.indexOf('#{') >= 0) {
			                    this.attachObserverText(scope, jobject, texto);
			                }
			            }
			            jobject.attr('voxs-ya', true);
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'attachObserverArray', {
			        enumerable: false,
			        value: function (scope, jobject, attr) {
			            var varname = jobject.attr('dynamic-foreach') || jobject.attr('voxs-var');
			            var arrname = jobject.attr('dynamic-in') || jobject.attr('voxs-name');
			            var uniqueId = jobject.attr('voxs-uniqueid');
			            var funiqueid = uniqueId;
			            if (!uniqueId) {
			                uniqueId = Date.now().toString(32) + yid++;
			                jobject.attr('voxs-uniqueid', uniqueId);
			                funiqueid = uniqueId;
			            }
			            var dom = $(jobject.html()), newdom, child, childs = {}, funcs = {}, self = this, items = {};
			            jobject.html('');
			            window.DEBUG && console.info('DOM:', dom);
			            var adding = function (ev, index) {
			                var current = true, temporal, g;
			                if (index == undefined)
			                    index = ev.value.index;
			                newdom = items[index];
			                child = childs[index];
			                if (!newdom) {
			                    newdom = dom.clone();
			                    current = false;
			                    temporal = $('<div>');
			                    temporal.append(newdom);
			                }
			                items[index] = newdom;
			                if (!child) {
			                    child = scope.createChild();
			                    childs[index] = child;
			                    newdom.attr('dynamic-child-scope', true);
			                    newdom.each(function () {
			                        this['dynamic-scope'] = child;
			                    });
			                }
			                if (current) {
			                    child[varname] = ev.value;
			                } else {
			                    child[varname] = ev.value[0];
			                    child.$index = index;
			                    g = funcs[index] = function (ev) {
			                        if (g.finished)
			                            return;
			                        window.DEBUG && console.info('CAMBIANDO ...', ev, index);
			                        adding(ev, index);
			                    };
			                    g.event = arrname + '.' + child.$index;
			                    scope.onchange(arrname + '.' + child.$index, g, null);
			                    self.parse(temporal, child);
			                }
			                if (!current)
			                    jobject.append(newdom);
			            };
			            var removing = function (ev) {
			                var keys = Object.keys(items);
			                if (ev.value < keys.length) {
			                    for (var i = ev.value; i < keys.length; i++) {
			                        try {
			                            items[i].remove();
			                            funcs[i].finished = true;
			                            scope.removechange(funcs[i].event, funcs[i]);
			                            delete items[i];
			                            delete childs[i];
			                        } catch (e) {
			                            console.error('ERROR REMOVING:', e);
			                        }
			                    }
			                }
			            };
			            scope.onchange(arrname, adding, 'push');
			            scope.onchange(arrname + '.length', removing, null);
			            var j = function () {
			                var keys = Object.keys(items);
			                if (keys.length > 0) {
			                    for (var i = 0; i < keys.length; i++) {
			                        try {
			                            items[i].remove();
			                            funcs[i].finished = true;
			                            delete items[i];
			                            delete childs[i];
			                        } catch (e) {
			                            console.error('ERROR REMOVING:', e);
			                        }
			                    }
			                }
			                var parts = arrname.split('.');
			                var o = scope;
			                for (var i = 0; i < parts.length; i++) {
			                    o = o[parts[i]];
			                    if (!o)
			                        break;
			                }
			                if (o && o.length) {
			                    for (var i = 0; i < o.length; i++) {
			                        adding({
			                            value: {
			                                '0': o[i],
			                                'index': i
			                            }
			                        });
			                    }
			                }
			            };
			            j();
			            scope.onchange(null, function (ev) {
			                var t = ev.name + '.';
			                if (arrname.startsWith(t) || arrname == ev.name) {
			                    j();
			                }
			            }, 'change', uniqueId + '>' + arrname);
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'attachObserverAttr', {
			        enumerable: false,
			        value: function (scope, jobject, attr) {
			            return this.attachObserver(scope, jobject, {
			                attr: attr,
			                text: attr.value
			            });
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'attachObserverIf', {
			        enumerable: false,
			        value: function (scope, jobject, attr) {
			            var negate = attr.name == 'dynamic-nif';
			            if (!negate) {
			                if (attr.value.startsWith('!')) {
			                    attr.value = attr.value.slice(1);
			                    negate = true;
			                }
			            }
			            return this.attachObserver(scope, jobject, {
			                attr: attr,
			                ifcondition: true,
			                negate: negate,
			                text: '#{' + attr.value + '}'
			            });
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'attachObserverText', {
			        enumerable: false,
			        value: function (scope, jobject, text) {
			            return this.attachObserver(scope, jobject, {
			                contentmode: true,
			                text: text
			            });
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'attachObserver', {
			        enumerable: false,
			        value: function (scope, jobject, options) {
			            var attr = options.attr;
			            var compilation = this.getVars(scope, options.text), var1, l, code2, put, u, disable, g;
			            var uniqueId = jobject.attr('voxs-uniqueid');
			            var funiqueid = uniqueId;
			            if (!uniqueId) {
			                uniqueId = Date.now().toString(32) + yid++;
			                jobject.attr('voxs-uniqueid', uniqueId);
			                funiqueid = uniqueId;
			            }
			            if (attr) {
			                uniqueId += '>' + attr.name;
			            } else if (options.contentmode) {
			                uniqueId += '>$text';
			            }
			            if (options.ifcondition)
			                uniqueId += options.negate ? '$nif' : '$if';
			            if (compilation.vars) {
			                g = function () {
			                    if (disable) {
			                        return disable = false;
			                    }
			                    l = DomParser.pending.filter(function (a) {
			                        if (a.uniqueId == uniqueId)
			                            return true;
			                    });
			                    if (l && l.length) {
			                        return;
			                    }
			                    DomParser.pending.push({
			                        uniqueId: uniqueId,
			                        jobject: jobject,
			                        attr: attr,
			                        scope: scope,
			                        ifcondition: options.ifcondition,
			                        negate: options.negate,
			                        contentmode: options.contentmode,
			                        compilation: compilation
			                    });
			                    DomParser.enablePaint();
			                };
			                scope.onchange(null, function (ev) {
			                    var t = ev.name + '.', var1;
			                    for (var i = 0; i < compilation.vars.length; i++) {
			                        var1 = compilation.vars[i];
			                        if (var1.name.startsWith(t) || var1.name == ev.name) {
			                            g();
			                        }
			                    }
			                }, 'change', uniqueId + '>' + compilation.vars[0].name);
			                DomParser.pending.push({
			                    uniqueId: uniqueId,
			                    jobject: jobject,
			                    attr: attr,
			                    negate: options.negate,
			                    ifcondition: options.ifcondition,
			                    contentmode: options.contentmode,
			                    compilation: compilation
			                });
			                DomParser.enablePaint();
			                if (options.ifcondition) {
			                } else {
			                    if (jobject.is('input,textarea,select') && compilation.vars.length == 1) {
			                        code2 = 'proxy.' + compilation.vars[0].real + '= arguments[0]';
			                        put = this.compile(scope, code2);
			                        if (jobject.is('input[type=check]')) {
			                            jobject.on('change', function () {
			                                disable = true;
			                                clearTimeout(u);
			                                u = setTimeout(put.bind(put, jobject[0].checked), 1);
			                            });
			                        } else {
			                            jobject.on('keyup', function () {
			                                disable = true;
			                                clearTimeout(u);
			                                u = setTimeout(put.bind(put, jobject.val()), 1);
			                            });
			                        }
			                    }
			                }
			            }
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'compile', {
			        enumerable: false,
			        value: function (scope, text) {
			            var proxy = scope.getProxy();
			            var code = '(function(){\n\t\t\treturn ' + text + '\n\t\t})';
			            return eval(code);
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'getVars', {
			        enumerable: false,
			        value: function (scope, value) {
			            var i, offset = -1, y, con, con2, html, uoptions = [], newvar, vars = [], func;
			            while (true) {
			                con = undefined;
			                con2 = undefined;
			                i = value.indexOf('#{', offset);
			                y = -1;
			                html = false;
			                newvar = null;
			                if (i >= 0) {
			                    if (value[i - 1] == '#') {
			                        html = true;
			                    }
			                    y = value.indexOf('}', i + 2);
			                    if (y >= 0) {
			                        newvar = value.substring(i + 2, y);
			                        con2 = value.substring(y + 1);
			                    }
			                    con = value.substring(0, i - (html ? 1 : 0));
			                } else {
			                    con = value;
			                }
			                if (con) {
			                    uoptions.push({
			                        type: 'text',
			                        value: con
			                    });
			                }
			                if (newvar) {
			                    newvar = ' ' + newvar.replace(/[\S|\!]+([\w|\$|\-]+\.?\w?)*/ig, function (e) {
			                        var parts, real, part, important, ename, nor, ade = '';
			                        if (e.trim().startsWith('!')) {
			                            ade = '!';
			                            e = e.trim().substring(1);
			                        }
			                        if (e && !/\d/.test(e[0]) && /[\w|\$]/.test(e[0])) {
			                            parts = e.trim().split('.');
			                            real = '';
			                            ename = '';
			                            important = '';
			                            nor = 0;
			                            for (var i = 0; i < parts.length; i++) {
			                                part = parts[i];
			                                if (part.indexOf('(') >= 0) {
			                                    if (part.endsWith('@')) {
			                                        part = part.substring(0, part.length - 1);
			                                        nor = -1;
			                                    } else {
			                                        nor++;
			                                    }
			                                }
			                                if (/\d/.test(part[0])) {
			                                    if (!nor) {
			                                        real += '[' + part + ']';
			                                    }
			                                    important += '[' + part + ']';
			                                } else if (part.indexOf('-') >= 0) {
			                                    if (!nor) {
			                                        real += '[' + JSON.stringify(part) + ']';
			                                    }
			                                    important += '[' + JSON.stringify(part) + ']';
			                                } else {
			                                    if (!nor) {
			                                        real += real ? '.' : '';
			                                        real += part;
			                                    }
			                                    important += i > 0 ? '.' : '';
			                                    important += part;
			                                }
			                                if (!nor) {
			                                    ename += ename ? '.' : '';
			                                    ename += part;
			                                }
			                                if (nor == -1)
			                                    nor = 1;
			                            }
			                            vars.push({
			                                real: real,
			                                name: ename
			                            });
			                            return ade + ' proxy' + (important[0] == '[' ? '' : '.') + important;
			                        }
			                        return ade + e;
			                    });
			                    func = this.compile(scope, newvar);
			                    uoptions.push({
			                        type: 'dynamic',
			                        value: func,
			                        html: html
			                    });
			                }
			                if (y < 0)
			                    break;
			                value = con2;
			            }
			            if (con2) {
			                uoptions.push({
			                    type: 'text',
			                    value: con2
			                });
			            }
			            return {
			                script: uoptions,
			                vars: vars
			            };
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'attachEvent', {
			        enumerable: false,
			        value: function (scope, jobject, name, value) {
			            var compilation = this.getVars(scope, '#{' + value + '}');
			            if (!compilation || !compilation.script.length)
			                return;
			            var getfunc = compilation.script[0].value;
			            jobject.on(name, function () {
			                var func = getfunc();
			                if (typeof func == 'function') {
			                    return func.apply(this, arguments);
			                }
			            });
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'getAllEvents', {
			        enumerable: false,
			        value: function (element) {
			            var result = [];
			            for (var key in element) {
			                if (key.indexOf('on') === 0) {
			                    result.push(key.slice(2));
			                }
			            }
			            return result.join(' ');
			        }
			    });
			    Object.defineProperty(DomParser.prototype, 'attachEventAll', {
			        enumerable: false,
			        value: function (scope, jobject, value) {
			            var name = this.getAllEvents(jobject[0]);
			            var compilation = this.getVars(scope, '#{' + value + '}');
			            if (!compilation || !compilation.script.length)
			                return;
			            var getfunc = compilation.script[0].value;
			            jobject.on(name, function (e) {
			                if (!e)
			                    return;
			                var events = getfunc();
			                var func = events[e.type];
			                if (typeof func == 'function') {
			                    return func.apply(this, arguments);
			                }
			            });
			        }
			    });
			    Object.defineProperty(DomParser, 'enablePaint', {
			        enumerable: false,
			        value: function () {
			            if (DomParser.paintTimeout) {
			                clearTimeout(DomParser.paintTimeout);
			            }
			            DomParser.paintTimeout = setTimeout(DomParser.executePaint, 60);
			        }
			    });
			    Object.defineProperty(DomParser, 'executePaint', {
			        enumerable: false,
			        value: function () {
			            DomParser.paintTimeout = null;
			            var pending = DomParser.pending;
			            DomParser.pending = [];
			            var item, script, content = '', str, expr, textcontent;
			            for (var i = 0; i < pending.length; i++) {
			                item = pending[i];
			                content = '';
			                textcontent = false;
			                expr = null;
			                try {
			                    for (var y = 0; y < item.compilation.script.length; y++) {
			                        script = item.compilation.script[y];
			                        if (script.type == 'text') {
			                            textcontent = true;
			                            content += script.value;
			                        } else if (script.type == 'dynamic') {
			                            expr = script.value();
			                            str = expr === null || expr === undefined ? '' : expr.toString();
			                            if (!script.html) {
			                                str = core.dynvox.EscapeHtml(str);
			                            }
			                            content += str;
			                        }
			                    }
			                } catch (e) {
			                    global.DEBUG && console.error('> Dyvox error: ', e);
			                }
			                var domParser;
			                if (item.attr) {
			                    if (item.ifcondition) {
			                        if (!!expr == !item.negate) {
			                            item.jobject.show();
			                            if (item.jobject.data('delayed-dom') !== undefined) {
			                                item.jobject.html(item.jobject.data('delayed-dom'));
			                                item.jobject.removeData('delayed-dom');
			                                domParser = new DomParser();
			                                domParser.parse(item.jobject, item.scope);
			                            }
			                        } else {
			                            item.jobject.hide();
			                        }
			                    } else {
			                        if (item.attr.name == 'value') {
			                            item.jobject.val(content);
			                            if (item.jobject.is('input[type=check]')) {
			                                item.jobject.each(function () {
			                                    this.checked = !!expr;
			                                });
			                            }
			                        } else if (item.attr.name.startsWith('data-')) {
			                            item.jobject.attr(item.attr.name, content);
			                            item.jobject.data(item.attr.name.substring(5), textcontent ? content : expr);
			                        } else {
			                            item.jobject.attr(item.attr.name, content);
			                        }
			                    }
			                } else if (item.contentmode) {
			                    item.jobject.html(content);
			                }
			            }
			        }
			    });
			}
			var removeFn = $.fn.remove;
			$.fn.remove = function () {
			    var e = $(this);
			    var all = $(this).find('[voxs-uniqueid]');
			    if (all && all.length) {
			        for (var i = 0; i < all.length; i++) {
			            core.dynvox.v2.Scope.removeEventsByIdentifierPrefix(all.eq(i).attr('voxs-uniqueid'));
			        }
			    }
			    all = e.filter('[voxs-uniqueid]');
			    for (var i = 0; i < all.length; i++) {
			        core.dynvox.v2.Scope.removeEventsByIdentifierPrefix(all.eq(i).attr('voxs-uniqueid'));
			    }
			    return removeFn.apply(this, arguments);
			};
			$.fn.voxscope = function () {
			    var e = $(this), final;
			    if (e.attr('dynamic-child-scope') !== undefined) {
			        final = e;
			    } else {
			        final = e.parents('[dynamic-child-scope]').eq(0);
			    }
			    if (final && final.length) {
			        return final[0]['dynamic-scope'];
			    }
			    var sc = 'default';
			    if (e.attr('voxs-scope') !== undefined) {
			        sc = e.attr('voxs-scope');
			    } else {
			        final = e.parents('[voxs-scope]').eq(0);
			        if (final.length) {
			            sc = final.attr('voxs-scope');
			        }
			    }
			    return core.dynvox.v2.Scope.get(sc);
			};
			DomParser.pending = [];
			exports.default = DomParser;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var $mod$3 = core.VW.Ecma2015.Utils.module(require('events'));
			var _id = 0;
			{
			    var Observable = function Observable() {
			        Observable.$constructor ? Observable.$constructor.apply(this, arguments) : Observable.$superClass && Observable.$superClass.apply(this, arguments);
			    };
			    Observable.prototype = Object.create($mod$3.EventEmitter.prototype);
			    Object.setPrototypeOf ? Object.setPrototypeOf(Observable, $mod$3.EventEmitter) : Observable.__proto__ = $mod$3.EventEmitter;
			    Observable.prototype.constructor = Observable;
			    Observable.$super = $mod$3.EventEmitter.prototype;
			    Observable.$superClass = $mod$3.EventEmitter;
			    Object.defineProperty(Observable, '$constructor', {
			        enumerable: false,
			        value: function (m) {
			            this.id = _id++;
			            this.m = m || {};
			            Object.defineProperty(this.m, '$$observable', {
			                enumerable: false,
			                writable: true,
			                readable: true,
			                value: this
			            });
			            $mod$3.EventEmitter.call(this);
			            Observable.$superClass.call(this);
			            this.setMaxListeners(100);
			        }
			    });
			    Observable.prototype.__defineSetter__('_name', function (value) {
			        this._iname = value;
			    });
			    Observable.prototype.__defineGetter__('_name', function () {
			        return this._iname;
			    });
			    Object.defineProperty(Observable.prototype, 'setParent', {
			        enumerable: false,
			        value: function (name, object) {
			            this._parents = this._parents || {};
			            this._parents[name + '$' + object.id] = object;
			        }
			    });
			    Object.defineProperty(Observable.prototype, 'removeParent', {
			        enumerable: false,
			        value: function (name, object) {
			            this._parents = this._parents || {};
			            delete this._parents[name + '$' + object.id];
			        }
			    });
			    Object.defineProperty(Observable.prototype, 'getProxy', {
			        enumerable: false,
			        value: function () {
			            if (this.$proxy)
			                return this.$proxy;
			            this.$proxy = new Proxy(this.m, this);
			            return this.$proxy;
			        }
			    });
			    Object.defineProperty(Observable.prototype, 'isProxyable', {
			        enumerable: false,
			        value: function (value) {
			            if (typeof value == 'object') {
			                if (value && !(value instanceof Date) && !(value instanceof global.Element))
			                    return true;
			            }
			        }
			    });
			    Object.defineProperty(Observable.prototype, 'getObservableAndObject', {
			        enumerable: false,
			        value: function (prop, value, parent) {
			            var obs;
			            if (this.isProxyable(value)) {
			                if (value.$$isobservable) {
			                    obs = value.$$observable;
			                } else {
			                    obs = value.$$observable;
			                    if (!obs)
			                        obs = new Observable(value);
			                    obs._name = prop;
			                    value = obs.getProxy();
			                }
			            }
			            if (!parent)
			                parent = this;
			            return {
			                obs: obs,
			                value: value
			            };
			        }
			    });
			    Observable.prototype.__defineGetter__('$$observable', function () {
			        return this;
			    });
			    Observable.prototype.__defineGetter__('$$isobservable', function () {
			        return true;
			    });
			    Object.defineProperty(Observable.prototype, 'get', {
			        enumerable: false,
			        value: function (m, prop) {
			            if (prop == '$$isobservable')
			                return true;
			            return m[prop];
			        }
			    });
			    Object.defineProperty(Observable.prototype, 'set', {
			        enumerable: false,
			        value: function (m, prop, value) {
			            if (prop == '$$observable') {
			                throw new core.System.Exception('Denied Exception');
			            }
			            var obs = null;
			            var obs2 = null;
			            var result;
			            result = this.getObservableAndObject(prop, value);
			            obs = result.obs;
			            if (obs) {
			                try {
			                    value = result.value;
			                } catch (e) {
			                    console.error('Error getting value:', e);
			                }
			            }
			            if (obs && obs.$$observable) {
			                window.DEBUG && console.info('SETTING PARENT', prop, this);
			                obs.setParent(prop, this);
			            }
			            var current = m[prop], keys;
			            if (current != value) {
			                if (typeof current == 'object' && current) {
			                    if (current.$$observable) {
			                        obs2 = current.$$observable;
			                    }
			                }
			                if (obs2) {
			                    if (current && value && current.$$observable && value.$$observable && current.$$observable == value.$$observable) {
			                    } else {
			                        obs2.removeParent(prop, this);
			                    }
			                }
			                m[prop] = value;
			                var safeObject = [];
			                if (value && value.$$isobservable) {
			                    if (value instanceof Array) {
			                        for (var i = 0; i < value.length; i++) {
			                            if (value[i] && !value[i].$$isobservable)
			                                value[i] = value[i];
			                        }
			                    } else {
			                        var keys = Object.keys(value), key;
			                        for (var i = 0; i < keys.length; i++) {
			                            key = keys[i];
			                            if (value[key] && !value[key].$$isobservable)
			                                value[key] = value[key];
			                        }
			                    }
			                }
			                this.emitToScope(prop, value);
			                safeObject = null;
			            }
			            return true;
			        }
			    });
			    Object.defineProperty(Observable.prototype, 'emitToScope', {
			        enumerable: false,
			        value: function (name, value, event, num) {
			            if (num === undefined) {
			                num = 0;
			            }
			            var p, emitted, rname, i, uname;
			            if (this.$$isscope) {
			                this.emit(event || 'change', {
			                    origin: this,
			                    name: name,
			                    value: value
			                });
			                return;
			            }
			            if (this._parents) {
			                for (var id in this._parents) {
			                    p = this._parents[id];
			                    i = id.indexOf('$');
			                    rname = id.substring(0, i);
			                    uname = name ? (rname ? rname + '.' : '') + name : rname;
			                    if (p.$$isscope) {
			                        p.emit(event || 'change', {
			                            origin: this,
			                            name: uname,
			                            value: value
			                        });
			                    } else {
			                        if (p == this)
			                            return console.error('CIRCULAR EMIT DETECTED');
			                        if (num > 20)
			                            return console.error(new core.System.Exception('Maximum nested level reached'));
			                        p.emitToScope(uname, value, event, num++);
			                    }
			                    emitted = true;
			                }
			            } else {
			                uname = name ? (this._iname ? this._iname + '.' : '') + name : this._iname;
			            }
			            if (!emitted) {
			                this.emit(event || 'change', {
			                    origin: this,
			                    name: uname,
			                    value: value
			                });
			            }
			        }
			    });
			}
			exports.default = Observable;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			{
			    var FProxy = function FProxy() {
			        FProxy.$constructor ? FProxy.$constructor.apply(this, arguments) : FProxy.$superClass && FProxy.$superClass.apply(this, arguments);
			    };
			}
			exports.default = FProxy;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Observable = core.dynvox.v2.Observable;
			var $mod$4 = core.VW.Ecma2015.Utils.module(require('./_extensions'));
			var id = 0;
			{
			    var Scope = function Scope() {
			        Scope.$constructor ? Scope.$constructor.apply(this, arguments) : Scope.$superClass && Scope.$superClass.apply(this, arguments);
			    };
			    Scope.prototype = Object.create(Observable.prototype);
			    Object.setPrototypeOf ? Object.setPrototypeOf(Scope, Observable) : Scope.__proto__ = Observable;
			    Scope.prototype.constructor = Scope;
			    Scope.$super = Observable.prototype;
			    Scope.$superClass = Observable;
			    Object.defineProperty(Scope, 'get', {
			        enumerable: false,
			        value: function (name) {
			            var sc = Scope.v[name] || Scope.create(name);
			            sc.makeScope();
			            var proxy = sc.getProxy();
			            return proxy;
			        }
			    });
			    Scope.prototype.__defineGetter__('$$isscope', function () {
			        return true;
			    });
			    Object.defineProperty(Scope.prototype, 'makeScope', {
			        enumerable: false,
			        value: function () {
			            this.m.id = this.id;
			            this.m.$$isscope = true;
			            this.m.onchange = this.onchange.bind(this);
			            this.m.removechange = this.removechange.bind(this);
			            this.m.emit = this.emit.bind(this);
			            this.m.on = this.on.bind(this);
			            this.m.attach = this.attach.bind(this);
			            this.m.createEvent = this.createEvent.bind(this);
			            this.m.createFilter = this.createFilter.bind(this);
			            this.m.removeListener = this.removeListener.bind(this);
			            this.m.removeEventsByPrefix = this.removeEventsByPrefix.bind(this);
			            this.m.removeEvent = this.removeEvent.bind(this);
			            this.m.once = this.once.bind(this);
			            this.m.createChild = this.createChild.bind(this);
			            this.m.global = global;
			            this.m.clone = this.clone.bind(this);
			            this.m.createVariable = this.createVariable.bind(this);
			            this.m.getProxy = this.getProxy.bind(this);
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'onchange', {
			        enumerable: false,
			        value: function (name, func, event, identifier) {
			            event = event || 'change';
			            if (typeof func != 'function')
			                return;
			            this.$$$onc = this.$$$onc || {};
			            Scope.$$$all = Scope.$$$all || {};
			            this.$$$all = this.$$$all || {};
			            this.$$$atached = this.$$$atached || {};
			            if (!identifier) {
			                this.$$$onc[event + '.' + name] = this.$$$onc[event + '.' + name] || [];
			            } else {
			                Scope.$$$all[identifier] = Scope.$$$all[identifier] || [];
			            }
			            if (!this.$$$atached[event]) {
			                this.on(event, function (self$0) {
			                    return function (ev) {
			                        var j, toremove = [];
			                        if (ev.name) {
			                            if (j = self$0.$$$onc[event + '.' + ev.name]) {
			                                j.forEach(function (a) {
			                                    a(ev);
			                                });
			                            }
			                            for (var id in self$0.$$$all) {
			                                j = Scope.$$$all[id];
			                                if (j) {
			                                    j.forEach(function (a) {
			                                        if (a.filtername) {
			                                            if (ev.name != a.filtername)
			                                                return;
			                                        }
			                                        a(ev);
			                                    });
			                                } else {
			                                    toremove.push(id);
			                                }
			                            }
			                            for (var i = 0; i < toremove.length; i++) {
			                                delete j[toremove[i]];
			                            }
			                        }
			                    };
			                }(this));
			                this.$$$atached[event] = true;
			            }
			            if (!identifier) {
			                this.$$$onc[event + '.' + name].push(func);
			            } else {
			                if (name) {
			                    func.filtername = name;
			                }
			                Scope.$$$all[identifier].push(func);
			                this.$$$all[identifier] = true;
			            }
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'attach', {
			        enumerable: false,
			        value: function (options) {
			            var dom = options.dom[0];
			            var event = options.event || 'change';
			            options.scope = this;
			            dom.dynamic_events = dom.dynamic_events || [];
			            dom.dynamic_events.push(options);
			            if (options.name) {
			                options.func = this.createFilter(options.func, options.name);
			            }
			            this.on(event, options.func);
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'createFilter', {
			        enumerable: false,
			        value: function (func, name) {
			            return function (ev) {
			                if (ev.name == name)
			                    return func(ev);
			            };
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'createEvent', {
			        enumerable: false,
			        value: function (event) {
			            var self = this;
			            return function (ev) {
			                var dom, event1, remove;
			                for (var i = 0; i < self.$$$dom.length; i++) {
			                    dom = self.$$$dom[i];
			                    if (!dom.dynamic_scope) {
			                        self.$$$dom[i] = null;
			                        remove = true;
			                    }
			                    if (dom && dom.dynamic_events && dom.dynamic_events[event]) {
			                        for (var y = 0; y < dom.dynamic_events[event].length; y++) {
			                            event1 = dom.dynamic_events[event][y];
			                            if (event1) {
			                                if (!event1.event && event == 'change' || event1.event == event) {
			                                    if (!event1.name || event1.name == ev.name) {
			                                        try {
			                                            event1.func(ev);
			                                        } catch (e) {
			                                            console.error('> Executing error: ', event, e);
			                                        }
			                                    }
			                                }
			                            }
			                        }
			                    }
			                }
			                if (remove) {
			                    self.$$$dom = self.$$$dom.filter(function (a) {
			                        return !!a;
			                    });
			                }
			            };
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'removeEventsByPrefix', {
			        enumerable: false,
			        value: function (prefix, event) {
			            var j;
			            event = event || 'change';
			            prefix = event + '.' + prefix;
			            if (this.$$$onc) {
			                for (var id in this.$$$onc) {
			                    if (id.startsWith(prefix)) {
			                        j = this.$$$onc[id];
			                        console.info('REMOVED LISTENERS: ', j.length);
			                        j.splice(0, j.length);
			                    }
			                }
			            }
			        }
			    });
			    Object.defineProperty(Scope, 'removeEventsByIdentifierPrefix', {
			        enumerable: false,
			        value: function (prefix) {
			            var j;
			            if (Scope.$$$all) {
			                for (var id in Scope.$$$all) {
			                    if (id.startsWith(prefix)) {
			                        j = Scope.$$$all[id];
			                        console.info('REMOVED LISTENERS: ', j.length);
			                        j.splice(0, j.length);
			                        delete Scope.$$$all[id];
			                    }
			                }
			            }
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'removeEvent', {
			        enumerable: false,
			        value: function (prefix, event) {
			            var j;
			            event = event || 'change';
			            prefix = event + '.' + prefix;
			            if (this.$$$onc) {
			                j = this.$$$onc[prefix];
			                console.info('REMOVED LISTENERS: ', j.length);
			                j.splice(0, j.length);
			            }
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'removechange', {
			        enumerable: false,
			        value: function (name, func, event) {
			            event = event || 'change';
			            this.removeListener(event + '.' + name, func);
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'set', {
			        enumerable: false,
			        value: function (obj, prop, value) {
			            if (prop.startsWith('$$$')) {
			                obj[prop] = value;
			                return true;
			            }
			            Observable.prototype.set.call(this, obj, prop, value);
			            return true;
			        }
			    });
			    Object.defineProperty(Scope.prototype, '_get', {
			        enumerable: false,
			        value: function (prop, ex) {
			            if (prop in this.m) {
			                ex.exists = true;
			                return this.m[prop];
			            }
			            var v;
			            if (this._parents) {
			                for (var id in this._parents) {
			                    v = this._parents[id]._get(prop, ex);
			                    if (ex.exists)
			                        return v;
			                }
			            }
			            return undefined;
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'get', {
			        enumerable: false,
			        value: function (obj, prop) {
			            if (prop == '$$isobservable')
			                return true;
			            if (prop == '$$observable')
			                return obj[prop];
			            if (prop in obj)
			                return obj[prop];
			            if (typeof prop == 'string' && prop.startsWith('$$$'))
			                return;
			            var ex = {};
			            var v = this._get(prop, ex);
			            if (ex.exists)
			                return v;
			            return undefined;
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'createChild', {
			        enumerable: false,
			        value: function () {
			            var scope = new Scope();
			            scope.makeScope();
			            scope.setParent('', this);
			            return scope.getProxy();
			        }
			    });
			    Scope.prototype.__defineGetter__('global', function () {
			        return global;
			    });
			    Object.defineProperty(Scope, 'create', {
			        enumerable: false,
			        value: function (name) {
			            return Scope.v[name] = new Scope();
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'clone', {
			        enumerable: false,
			        value: function () {
			            throw new core.System.NotImplementedException();
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'createVariable', {
			        enumerable: false,
			        value: function (name, value) {
			            this.getProxy()[name] = value;
			        }
			    });
			}
			Scope.v = {};
			exports.default = Scope;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Observable = core.dynvox.v2.Observable;
			var $mod$5 = core.VW.Ecma2015.Utils.module(require('./_extensions'));
			var id = 0;
			{
			    var Scope = function Scope() {
			        Scope.$constructor ? Scope.$constructor.apply(this, arguments) : Scope.$superClass && Scope.$superClass.apply(this, arguments);
			    };
			    Scope.prototype = Object.create(Observable.prototype);
			    Object.setPrototypeOf ? Object.setPrototypeOf(Scope, Observable) : Scope.__proto__ = Observable;
			    Scope.prototype.constructor = Scope;
			    Scope.$super = Observable.prototype;
			    Scope.$superClass = Observable;
			    Object.defineProperty(Scope, 'get', {
			        enumerable: false,
			        value: function (name) {
			            var sc = Scope.v[name] || Scope.create(name);
			            sc.makeScope();
			            var proxy = sc.getProxy();
			            return proxy;
			        }
			    });
			    Scope.prototype.__defineGetter__('$$isscope', function () {
			        return true;
			    });
			    Object.defineProperty(Scope.prototype, 'makeScope', {
			        enumerable: false,
			        value: function () {
			            this.m.id = this.id;
			            this.m.$$isscope = true;
			            this.m.onchange = this.onchange.bind(this);
			            this.m.removechange = this.removechange.bind(this);
			            this.m.emit = this.emit.bind(this);
			            this.m.on = this.on.bind(this);
			            this.m.removeListener = this.removeListener.bind(this);
			            this.m.removeEventsByPrefix = this.removeEventsByPrefix.bind(this);
			            this.m.removeEvent = this.removeEvent.bind(this);
			            this.m.once = this.once.bind(this);
			            this.m.createChild = this.createChild.bind(this);
			            this.m.global = global;
			            this.m.clone = this.clone.bind(this);
			            this.m.createVariable = this.createVariable.bind(this);
			            this.m.getProxy = this.getProxy.bind(this);
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'onchange', {
			        enumerable: false,
			        value: function (name, func, event, identifier) {
			            event = event || 'change';
			            if (typeof func != 'function')
			                return;
			            this.$$$onc = this.$$$onc || {};
			            Scope.$$$all = Scope.$$$all || {};
			            this.$$$all = this.$$$all || {};
			            this.$$$atached = this.$$$atached || {};
			            if (!identifier) {
			                this.$$$onc[event + '.' + name] = this.$$$onc[event + '.' + name] || [];
			            } else {
			                Scope.$$$all[identifier] = Scope.$$$all[identifier] || [];
			            }
			            if (!this.$$$atached[event]) {
			                this.on(event, function (self$0) {
			                    return function (ev) {
			                        var j, toremove = [];
			                        if (ev.name) {
			                            if (j = self$0.$$$onc[event + '.' + ev.name]) {
			                                j.forEach(function (a) {
			                                    a(ev);
			                                });
			                            }
			                            for (var id in self$0.$$$all) {
			                                j = Scope.$$$all[id];
			                                if (j) {
			                                    j.forEach(function (a) {
			                                        if (a.filtername) {
			                                            if (ev.name != a.filtername)
			                                                return;
			                                        }
			                                        a(ev);
			                                    });
			                                } else {
			                                    toremove.push(id);
			                                }
			                            }
			                            for (var i = 0; i < toremove.length; i++) {
			                                delete j[toremove[i]];
			                            }
			                        }
			                    };
			                }(this));
			                this.$$$atached[event] = true;
			            }
			            if (!identifier) {
			                this.$$$onc[event + '.' + name].push(func);
			            } else {
			                if (name) {
			                    func.filtername = name;
			                }
			                Scope.$$$all[identifier].push(func);
			                this.$$$all[identifier] = true;
			            }
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'removeEventsByPrefix', {
			        enumerable: false,
			        value: function (prefix, event) {
			            var j;
			            event = event || 'change';
			            prefix = event + '.' + prefix;
			            if (this.$$$onc) {
			                for (var id in this.$$$onc) {
			                    if (id.startsWith(prefix)) {
			                        j = this.$$$onc[id];
			                        console.info('REMOVED LISTENERS: ', j.length);
			                        j.splice(0, j.length);
			                    }
			                }
			            }
			        }
			    });
			    Object.defineProperty(Scope, 'removeEventsByIdentifierPrefix', {
			        enumerable: false,
			        value: function (prefix) {
			            var j;
			            if (Scope.$$$all) {
			                for (var id in Scope.$$$all) {
			                    if (id.startsWith(prefix)) {
			                        j = Scope.$$$all[id];
			                        console.info('REMOVED LISTENERS: ', j.length);
			                        j.splice(0, j.length);
			                        delete Scope.$$$all[id];
			                    }
			                }
			            }
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'removeEvent', {
			        enumerable: false,
			        value: function (prefix, event) {
			            var j;
			            event = event || 'change';
			            prefix = event + '.' + prefix;
			            if (this.$$$onc) {
			                j = this.$$$onc[prefix];
			                console.info('REMOVED LISTENERS: ', j.length);
			                j.splice(0, j.length);
			            }
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'removechange', {
			        enumerable: false,
			        value: function (name, func, event) {
			            event = event || 'change';
			            this.removeListener(event + '.' + name, func);
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'set', {
			        enumerable: false,
			        value: function (obj, prop, value) {
			            if (prop.startsWith('$$$')) {
			                obj[prop] = value;
			                return true;
			            }
			            Observable.prototype.set.call(this, obj, prop, value);
			            return true;
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'get', {
			        enumerable: false,
			        value: function (obj, prop) {
			            if (prop in obj)
			                return obj[prop];
			            if (prop.startsWith('$$$'))
			                return;
			            if (this._parents) {
			                for (var id in this._parents) {
			                    if (prop in this._parents[id].m)
			                        return this._parents[id].m[prop];
			                }
			            }
			            return undefined;
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'createChild', {
			        enumerable: false,
			        value: function () {
			            var scope = new Scope();
			            scope.makeScope();
			            scope.setParent('', this);
			            return scope.getProxy();
			        }
			    });
			    Scope.prototype.__defineGetter__('global', function () {
			        return global;
			    });
			    Object.defineProperty(Scope, 'create', {
			        enumerable: false,
			        value: function (name) {
			            return Scope.v[name] = new Scope();
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'clone', {
			        enumerable: false,
			        value: function () {
			            throw new core.System.NotImplementedException();
			        }
			    });
			    Object.defineProperty(Scope.prototype, 'createVariable', {
			        enumerable: false,
			        value: function (name, value) {
			            this.getProxy()[name] = value;
			        }
			    });
			}
			Scope.v = {};
			exports.default = Scope;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var id = 0;
			var cons = 0;
			var $mod$6 = core.VW.Ecma2015.Utils.module(require('events'));
			var NullValue = function () {
			};
			NullValue.valueOf = function () {
			    return null;
			};
			NullValue.toString = function () {
			    return '';
			};
			{
			    var ScopeBestObserver = function ScopeBestObserver() {
			        ScopeBestObserver.$constructor ? ScopeBestObserver.$constructor.apply(this, arguments) : ScopeBestObserver.$superClass && ScopeBestObserver.$superClass.apply(this, arguments);
			    };
			    ScopeBestObserver.prototype = Object.create($mod$6.EventEmitter.prototype);
			    Object.setPrototypeOf ? Object.setPrototypeOf(ScopeBestObserver, $mod$6.EventEmitter) : ScopeBestObserver.__proto__ = $mod$6.EventEmitter;
			    ScopeBestObserver.prototype.constructor = ScopeBestObserver;
			    ScopeBestObserver.$super = $mod$6.EventEmitter.prototype;
			    ScopeBestObserver.$superClass = $mod$6.EventEmitter;
			    Object.defineProperty(ScopeBestObserver, '$constructor', {
			        enumerable: false,
			        value: function (scope) {
			            this.scope = scope;
			            this.events = [];
			            ScopeBestObserver.id++;
			            this.id = ScopeBestObserver.id;
			        }
			    });
			}
			ScopeBestObserver.id = 0;
			exports.default = ScopeBestObserver;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Methods = {};
			Methods.splice = Array.prototype.splice;
			Methods.push = Array.prototype.push;
			Methods.filter = Array.prototype.filter;
			Array.prototype.splice = function (index, length) {
			    var result;
			    result = Methods.splice.apply(this, arguments);
			    if (this.$$observable) {
			        this.$$observable.emitToScope(null, arguments, 'splice');
			    }
			    return result;
			};
			Array.prototype.push = function () {
			    var result, index;
			    index = this.length;
			    result = Methods.push.apply(this, arguments);
			    if (this.$$observable) {
			        this.$$observable.emitToScope(null, {
			            '0': this[index],
			            index: index
			        }, 'push');
			    }
			    return result;
			};
			Array.prototype.toArray = function () {
			    return [].concat(this);
			};
			Array.prototype.removeAll = function () {
			    var result = this.splice(0, this.length);
			    this.length = 0;
			    return result;
			};
			Array.prototype.removeValue = function (value) {
			    var index = this.indexOf(value);
			    if (index < 0)
			        return;
			    this.splice(index, 1);
			};
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			{
			    var DynvoxV2 = function DynvoxV2() {
			        DynvoxV2.$constructor ? DynvoxV2.$constructor.apply(this, arguments) : DynvoxV2.$superClass && DynvoxV2.$superClass.apply(this, arguments);
			    };
			    DynvoxV2.__defineGetter__('Observable', function () {
			        return require('./Observable').default;
			    });
			    DynvoxV2.__defineGetter__('Scope', function () {
			        return require('./Scope').default;
			    });
			    DynvoxV2.__defineGetter__('DomParser', function () {
			        return require('./DomParser').default;
			    });
			}
			exports = module.exports = DynvoxV2;
			core.dynvox.v2 = DynvoxV2;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			{
			    var Anchor = function Anchor() {
			        Anchor.$constructor ? Anchor.$constructor.apply(this, arguments) : Anchor.$superClass && Anchor.$superClass.apply(this, arguments);
			    };
			    Anchor.prototype = Object.create(Element.prototype);
			    Object.setPrototypeOf ? Object.setPrototypeOf(Anchor, Element) : Anchor.__proto__ = Element;
			    Anchor.prototype.constructor = Anchor;
			    Anchor.$super = Element.prototype;
			    Anchor.$superClass = Element;
			    Object.defineProperty(Anchor, 'register', {
			        enumerable: false,
			        value: function () {
			            if (this.registered)
			                return;
			            $.fn.voxanchor = function () {
			                var dp = [];
			                this.each(function () {
			                    var o = $(this);
			                    var t = undefined;
			                    this.voxcss_element = this.voxcss_element || {};
			                    t = this.voxcss_element['anchor'];
			                    if (!t) {
			                        t = new Anchor(o);
			                        this.voxcss_element['anchor'] = t;
			                    }
			                    dp.push(t);
			                });
			                return dp;
			            };
			            $(function () {
			                vox.mutation.watchAppend($('body'), function (ev) {
			                    ev.jTarget.voxanchor();
			                }, '[hash-effect]');
			                $('[hash-effect]').voxanchor();
			            });
			            this.registered = true;
			        }
			    });
			    Object.defineProperty(Anchor, '$constructor', {
			        enumerable: false,
			        value: function (obj) {
			            Anchor.$superClass.call(this);
			            obj = $(obj);
			            var f = this.$ = {};
			            f.obj = obj;
			            this.obtainProps();
			            this.init();
			        }
			    });
			    Object.defineProperty(Anchor.prototype, 'init', {
			        enumerable: false,
			        value: function () {
			            this.events();
			        }
			    });
			    Object.defineProperty(Anchor.prototype, 'obtainProps', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            f.isHash = f.obj.attr('hash-effect') !== undefined;
			        }
			    });
			    Object.defineProperty(Anchor.prototype, 'hashEffect', {
			        enumerable: false,
			        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
			            var f, hash, obj, pad, offset, top, w, current, dif, time, intervals, dif1, i;
			            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
			                while (1)
			                    switch (context$1$0.prev = context$1$0.next) {
			                    case 0:
			                        if (this.it1)
			                            clearTimeout(this.it1);
			                        f = this.$;
			                        hash = f.obj.attr('href');
			                        if (!hash)
			                            hash = f.obj.data('href');
			                        else
			                            f.obj.data('href', hash);
			                        f.obj.removeAttr('href');
			                        obj = $(hash);
			                        pad = f.obj.attr('hash-padding');
			                        pad = pad | 0;
			                        offset = obj.offset();
			                        top = offset.top - pad;
			                        w = $(window);
			                        current = w.scrollTop();
			                        dif = top - current;
			                        time = f.obj.attr('hash-time');
			                        time = parseInt(time);
			                        if (isNaN(time))
			                            time = 500;
			                        intervals = time / 10;
			                        dif1 = dif / intervals;
			                        i = 0;
			                    case 19:
			                        if (!(i < intervals)) {
			                            context$1$0.next = 27;
			                            break;
			                        }
			                        current += dif1;
			                        w.scrollTop(current);
			                        context$1$0.next = 24;
			                        return regeneratorRuntime.awrap(core.VW.Task.sleep(10));
			                    case 24:
			                        i++;
			                        context$1$0.next = 19;
			                        break;
			                    case 27:
			                        w.scrollTop(top);
			                        this.it1 = setTimeout(function (self$0) {
			                            return function () {
			                                f.obj.attr('href', hash);
			                                self$0.it1 = undefined;
			                            };
			                        }(this), 100);
			                    case 29:
			                    case 'end':
			                        return context$1$0.stop();
			                    }
			            }, null, this);
			        })
			    });
			    Object.defineProperty(Anchor.prototype, 'events', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            f.obj.click(function (self$0) {
			                return function (ev) {
			                    self$0.hashEffect();
			                };
			            }(this));
			        }
			    });
			}
			exports.default = Anchor;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			{
			    var Card = function Card() {
			        Card.$constructor ? Card.$constructor.apply(this, arguments) : Card.$superClass && Card.$superClass.apply(this, arguments);
			    };
			    Card.prototype = Object.create(Element.prototype);
			    Object.setPrototypeOf ? Object.setPrototypeOf(Card, Element) : Card.__proto__ = Element;
			    Card.prototype.constructor = Card;
			    Card.$super = Element.prototype;
			    Card.$superClass = Element;
			    Object.defineProperty(Card, 'register', {
			        enumerable: false,
			        value: function () {
			            if (this.registered)
			                return;
			            $.fn.voxcard = function () {
			                var dp = [];
			                this.each(function () {
			                    var o = $(this);
			                    var t = undefined;
			                    this.voxcss_element = this.voxcss_element || {};
			                    t = this.voxcss_element['vox-card'];
			                    if (!t) {
			                        t = new Card(o);
			                        this.voxcss_element['vox-card'] = t;
			                    }
			                    dp.push(t);
			                });
			                return dp;
			            };
			            $(function () {
			                vox.mutation.watchAppend($('body'), function (ev) {
			                    ev.jTarget.voxcard();
			                }, '.card');
			                $('.card').voxcard();
			            });
			            this.registered = true;
			        }
			    });
			    Object.defineProperty(Card, '$constructor', {
			        enumerable: false,
			        value: function (obj) {
			            Card.$superClass.call(this);
			            obj = $(obj);
			            var f = this.$ = {};
			            f.obj = obj;
			            this.obtainProps();
			            this.init();
			        }
			    });
			    Object.defineProperty(Card.prototype, 'init', {
			        enumerable: false,
			        value: function () {
			            this.events();
			        }
			    });
			    Object.defineProperty(Card.prototype, 'obtainProps', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            f.reveal = f.obj.find('.card-reveal');
			            f.revealClose = f.obj.find('.card-reveal .card-title');
			            f.activator = f.obj.find('.activator');
			        }
			    });
			    Object.defineProperty(Card.prototype, 'reveal', {
			        enumerable: false,
			        value: function (event) {
			            var f = this.$, ev = this.createEvent('beforereveal', event);
			            ev.card = this;
			            this.emit(ev);
			            if (ev.defaultPrevented)
			                return;
			            f.reveal.show();
			            f.reveal.css('top', 0);
			            ev = this.createEvent('reveal');
			            ev.card = this;
			            this.emit(ev);
			        }
			    });
			    Object.defineProperty(Card.prototype, 'closeReveal', {
			        enumerable: false,
			        value: function (event) {
			            var f = this.$, ev = this.createEvent('beforeclosereveal', event);
			            ev.card = this;
			            this.emit(ev);
			            if (ev.defaultPrevented)
			                return;
			            f.reveal.css('top', '100%');
			            ev = this.createEvent('closereveal');
			            ev.card = this;
			            this.emit(ev);
			        }
			    });
			    Object.defineProperty(Card.prototype, 'events', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            f.activator.click(function (self$0) {
			                return function () {
			                    ev.preventDefault();
			                    self$0.reveal();
			                    return false;
			                };
			            }(this));
			            f.revealClose.click(function (self$0) {
			                return function () {
			                    ev.preventDefault();
			                    self$0.closeReveal();
			                    return false;
			                };
			            }(this));
			        }
			    });
			}
			exports.default = Card;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			{
			    var Dropdown = function Dropdown() {
			        Dropdown.$constructor ? Dropdown.$constructor.apply(this, arguments) : Dropdown.$superClass && Dropdown.$superClass.apply(this, arguments);
			    };
			    Dropdown.prototype = Object.create(Element.prototype);
			    Object.setPrototypeOf ? Object.setPrototypeOf(Dropdown, Element) : Dropdown.__proto__ = Element;
			    Dropdown.prototype.constructor = Dropdown;
			    Dropdown.$super = Element.prototype;
			    Dropdown.$superClass = Element;
			    Object.defineProperty(Dropdown, 'register', {
			        enumerable: false,
			        value: function () {
			            if (this.registered)
			                return;
			            $.fn.voxdropdown = function () {
			                var dp = [];
			                this.each(function () {
			                    var o = $(this);
			                    var t = undefined;
			                    this.voxcss_element = this.voxcss_element || {};
			                    t = this.voxcss_element['vox-dropdown'];
			                    if (!t) {
			                        t = new Dropdown(o);
			                        this.voxcss_element['vox-dropdown'] = t;
			                    }
			                    dp.push(t);
			                });
			                return dp;
			            };
			            Dropdown.registerWatch();
			            this.registered = true;
			        }
			    });
			    Object.defineProperty(Dropdown, 'registerWatch', {
			        enumerable: false,
			        value: function () {
			            $(function () {
			                vox.mutation.watchAppend($('body'), function (ev) {
			                    ev.jTarget.voxdropdown();
			                }, '.dropdown');
			                $('.dropdown').voxdropdown();
			            });
			        }
			    });
			    Object.defineProperty(Dropdown, '$constructor', {
			        enumerable: false,
			        value: function (obj) {
			            Dropdown.$superClass.call(this);
			            this.func = [];
			            obj = $(obj);
			            var f = this.$ = {};
			            f.obj = obj;
			            this.obtainProps();
			            this.init();
			        }
			    });
			    Object.defineProperty(Dropdown.prototype, 'init', {
			        enumerable: false,
			        value: function () {
			            this.events();
			        }
			    });
			    Object.defineProperty(Dropdown.prototype, 'obtainProps', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            if (f.obj.data('menu-selector') !== undefined)
			                f.menu = $(f.obj.data('menu-selector'));
			            else
			                f.menu = f.obj.find('.dropdown-menu');
			        }
			    });
			    Object.defineProperty(Dropdown.prototype, 'isOpened', {
			        enumerable: false,
			        value: function () {
			            return this.$.menu.hasClass('opened');
			        }
			    });
			    Object.defineProperty(Dropdown.prototype, '$pEvents', {
			        enumerable: false,
			        value: function (a) {
			            var self = this;
			            var y = function () {
			                var a0 = $(this);
			                var ev = self.createEvent('beforeselect');
			                ev.dropdown = self;
			                ev.jTarget = a0;
			                self.emit(ev);
			                if (ev.defaultPrevented)
			                    return;
			                var ev = self.createEvent('select');
			                ev.dropdown = self;
			                ev.jTarget = a0;
			                ev.value = a0.data('value');
			                self.emit(ev);
			                if (ev.defaultPrevented) {
			                    return;
			                }
			                self.close();
			            };
			            this.attachEvent(a, 'click', y);
			        }
			    });
			    Object.defineProperty(Dropdown.prototype, 'open', {
			        enumerable: false,
			        value: function (event) {
			            var f = this.$;
			            var ev = this.createEvent('beforeopen', event);
			            ev.dropdown = self;
			            this.emit(ev);
			            if (ev.defaultPrevented)
			                return;
			            var self = this;
			            f.lEvent = event ? event.type : '';
			            f.menu.addClass('opened');
			            f.menu.voxanimate(f.menu.data('ineffect') || 'fadeIn short', null, function () {
			                f.captureKeyboard = true;
			                var ev = self.createEvent('open', event);
			                ev.dropdown = self;
			                self.emit(ev);
			            });
			        }
			    });
			    Object.defineProperty(Dropdown.prototype, 'close', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            var ev = vox.platform.createEvent('beforeclose');
			            ev.dropdown = self;
			            this.emit(ev);
			            if (ev.defaultPrevented)
			                return;
			            var self = this;
			            f.lEvent = undefined;
			            f.menu.removeClass('opened');
			            f.menu.voxanimate(f.menu.data('outeffect') || 'fadeOut short', null, function () {
			                f.captureKeyboard = false;
			                var ev = vox.platform.createEvent('close');
			                ev.dropdown = self;
			                self.emit(ev);
			            });
			        }
			    });
			    Object.defineProperty(Dropdown.prototype, 'toggle', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            if (f.menu.hasClass('opened'))
			                this.close();
			            else
			                this.open();
			        }
			    });
			    Object.defineProperty(Dropdown.prototype, 'dynamicDispose', {
			        enumerable: false,
			        value: function () {
			            if (this.watchRemoveListener) {
			                this.watchRemoveListener();
			            }
			            this.watchRemoveListener = null;
			            Element.prototype.dynamicDispose.call(this);
			        }
			    });
			    Object.defineProperty(Dropdown.prototype, 'events', {
			        enumerable: false,
			        value: function () {
			            var f = this.$, ab;
			            this.watchRemoveListener = vox.mutation.watchAppend(f.menu, function (self$0) {
			                return function (ev) {
			                    self$0.$pEvents(ev.jTarget.find('>a'));
			                };
			            }(this), 'li');
			            this.$pEvents(f.menu.find('li>a'));
			            var self = this;
			            this.attachEvent($(document), 'keyup', function (ev) {
			                if (f.captureKeyboard) {
			                    ev.preventDefault();
			                    ev.dropdown = self;
			                    self.emit('keyup', ev);
			                    if (ev.defaultPrevented) {
			                        return;
			                    }
			                    if (ev.keyCode == 39) {
			                    }
			                    return false;
			                }
			            });
			            this.attachEvent($(document), 'click', function (ev) {
			                if (!self.isOpened()) {
			                    return;
			                }
			                var e = $(ev.target);
			                if (ev.target != f.obj.get(0) && f.obj.find(e).length == 0) {
			                    var ev2 = self.createEvent('outerclick');
			                    ev2.dropdown = self;
			                    ev2.target = ev.target;
			                    ev2.clickEvent = ev;
			                    self.emit(ev);
			                    if (ev.defaultPrevented)
			                        return;
			                    self.close();
			                }
			            });
			            f.btn = f.obj.find('a,.button').eq(0);
			            this.attachEvent(f.btn, 'click', function () {
			                if (f.lEvent == 'mouseenter')
			                    return self.open();
			                self.toggle();
			            });
			            var j = function (ev) {
			                if (f.obj.data('hover-activate')) {
			                    if (ev.type == 'mouseenter') {
			                        if (f.closing) {
			                            clearTimeout(f.closing);
			                            f.closing = undefined;
			                            return;
			                        }
			                        if (self.isOpened()) {
			                            return;
			                        }
			                        self.open(ev);
			                    } else if (ev.type == 'mouseleave') {
			                        if (f.lEvent != 'mouseenter')
			                            return;
			                        f.closing = setTimeout(function () {
			                            self.close();
			                            f.closing = undefined;
			                        }, 100);
			                    }
			                }
			            };
			            this.attachEvent(f.btn, 'hover', j);
			            this.attachEvent(f.menu, 'hover', j);
			        }
			    });
			}
			var doc = {};
			if (typeof document === 'object')
			    doc = document;
			exports.default = Dropdown;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			{
			    var Elastic = function Elastic() {
			        Elastic.$constructor ? Elastic.$constructor.apply(this, arguments) : Elastic.$superClass && Elastic.$superClass.apply(this, arguments);
			    };
			    Object.defineProperty(Elastic, 'getStyleObject', {
			        enumerable: false,
			        value: function () {
			            var dom = this.get(0);
			            var style;
			            var returns = {};
			            if (window.getComputedStyle) {
			                var camelize = function (a, b) {
			                    return b.toUpperCase();
			                };
			                style = window.getComputedStyle(dom, null);
			                for (var i = 0, l = style.length; i < l; i++) {
			                    var prop = style[i];
			                    var camel = prop.replace(/\-([a-z])/g, camelize);
			                    var val = style.getPropertyValue(prop);
			                    returns[camel] = val;
			                }
			                ;
			                return returns;
			            }
			            ;
			            if (style = dom.currentStyle) {
			                for (var prop in style) {
			                    returns[prop] = style[prop];
			                }
			                ;
			                return returns;
			            }
			            ;
			            return this.css();
			        }
			    });
			    Elastic.__defineGetter__('entities', function () {
			        return {
			            '&': '&amp;',
			            '<': '&lt;',
			            '>': '&gt;',
			            '"': '&quot;',
			            '\'': '&#39;',
			            '/': '&#x2F;'
			        };
			    });
			    Object.defineProperty(Elastic, 'escapeHtml', {
			        enumerable: false,
			        value: function (html) {
			            return String(html).replace(/[&<>"'\/]/g, function (self$0) {
			                return function (s) {
			                    return self$0.entities[s];
			                };
			            }(this));
			        }
			    });
			    Object.defineProperty(Elastic, 'register', {
			        enumerable: false,
			        value: function () {
			            if (this.registered)
			                return;
			            $.fn.voxelastic = function () {
			                var dp = [];
			                this.each(function () {
			                    var o = $(this);
			                    var t = undefined;
			                    this.voxcss_element = this.voxcss_element || {};
			                    t = this.voxcss_element['vox-elastic'];
			                    if (!t) {
			                        t = new Elastic(o);
			                        this.voxcss_element['vox-elastic'] = t;
			                    }
			                    dp.push(t);
			                });
			                return dp;
			            };
			            $(function () {
			                vox.mutation.watchAppend($('body'), function (ev) {
			                    ev.jTarget.voxelastic();
			                }, '.vox-textarea, .vox-elastic');
			                $('.vox-textarea, .vox-elastic').voxelastic();
			            });
			            this.registered = true;
			        }
			    });
			    Object.defineProperty(Elastic, '$constructor', {
			        enumerable: false,
			        value: function (obj) {
			            obj = $(obj);
			            var f = this.$ = {};
			            f.obj = obj;
			            this.adjust(obj);
			        }
			    });
			    Object.defineProperty(Elastic.prototype, 'refresh', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            var sxl = f.obj.get(0).sxl;
			            var refrescar = sxl.elastic ? sxl.elastic : null;
			            if (refrescar)
			                refrescar();
			            else
			                this.adjust(f.obj);
			        }
			    });
			    Object.defineProperty(Elastic.prototype, 'adjust', {
			        enumerable: false,
			        value: function (obj) {
			            var elastic = this;
			            obj.each(function () {
			                var e = $(this);
			                if (!this.sxl) {
			                    this.sxl = {};
			                }
			                var div;
			                if (!Elastic.adjustDiv) {
			                    div = $('<div>');
			                    div.addClass('sxl-elastic-provider');
			                    Elastic.adjustDiv = div;
			                    $('body').append(div);
			                } else {
			                    div = Elastic.adjustDiv;
			                }
			                div.css(Elastic.getStyleObject.call(e));
			                div.css('word-wrap', 'break-word');
			                div.css('height', 'auto');
			                div.css('position', 'fixed');
			                div.css('top', '1000%');
			                div.css('left', '1000%');
			                div.css('bottom', 'auto');
			                div.show();
			                if (!this.value) {
			                    div.html('&nbsp;');
			                } else {
			                    div.html(Elastic.escapeHtml(this.value));
			                    var di = this.value[this.value.length - 1];
			                    if (di == '\r' || di == '\n') {
			                        div.append('&nbsp;');
			                    }
			                }
			                var h = div.height();
			                e.height(h);
			                e.css('overflow', 'hidden');
			                var self = this;
			                if (!this.sxl.elastic) {
			                    this.sxl.elastic = function () {
			                        if (self.sxl.elastic.i) {
			                            clearTimeout(self.sxl.elastic.i);
			                            self.sxl.elastic.i = undefined;
			                        }
			                        self.sxl.elastic.i = setTimeout(function () {
			                            elastic.adjust($(self));
			                        }, 200);
			                    };
			                    e.bind('change click input cut paste keydown resize', this.sxl.elastic);
			                    $(window).resize(this.sxl.elastic);
			                }
			            });
			        }
			    });
			}
			exports.default = Elastic;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var events = require('events').EventEmitter;
			var vox = core.VW.Web.Vox;
			{
			    var Element = function Element() {
			        Element.$constructor ? Element.$constructor.apply(this, arguments) : Element.$superClass && Element.$superClass.apply(this, arguments);
			    };
			    Element.prototype = Object.create(events.prototype);
			    Object.setPrototypeOf ? Object.setPrototypeOf(Element, events) : Element.__proto__ = events;
			    Element.prototype.constructor = Element;
			    Element.$super = events.prototype;
			    Element.$superClass = events;
			    Object.defineProperty(Element, '$constructor', {
			        enumerable: false,
			        value: function () {
			            Element.$superClass.call(this);
			        }
			    });
			    Object.defineProperty(Element.prototype, 'attachEvent', {
			        enumerable: false,
			        value: function (obj, event, func) {
			            this.func = this.func || [];
			            this.func.push({
			                obj: obj,
			                event: event,
			                func: func
			            });
			            obj.on(event, func);
			        }
			    });
			    Object.defineProperty(Element.prototype, 'dynamicDispose', {
			        enumerable: false,
			        value: function () {
			            var ev;
			            if (this.func) {
			                for (var i = 0; i < this.func.length; i++) {
			                    ev = this.func[i];
			                    ev.obj.removeListener ? ev.obj.removeListener(ev.event, ev.func) : ev.obj.off(ev.event, ev.func);
			                    this.func[i] = undefined;
			                }
			                delete this.func;
			            }
			        }
			    });
			    Object.defineProperty(Element.prototype, 'createEvent', {
			        enumerable: false,
			        value: function (eventName, originalEvent) {
			            var ev = vox.platform.createEvent(eventName);
			            if (originalEvent)
			                ev.originalEvent = originalEvent;
			            return ev;
			        }
			    });
			    Object.defineProperty(Element.prototype, 'emit', {
			        enumerable: false,
			        value: function (ev) {
			            var name;
			            if (arguments.length < 2)
			                name = ev.type;
			            else {
			                name = arguments[0];
			                ev = arguments[1];
			            }
			            Element.$super.emit.call(this, name, ev);
			        }
			    });
			}
			exports.default = Element;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			var w = {};
			if (typeof window !== 'undefined')
			    w = window;
			function init(window) {
			    {
			        var Tooltip = function Tooltip() {
			            Tooltip.$constructor ? Tooltip.$constructor.apply(this, arguments) : Tooltip.$superClass && Tooltip.$superClass.apply(this, arguments);
			        };
			        Tooltip.prototype = Object.create(Element.prototype);
			        Object.setPrototypeOf ? Object.setPrototypeOf(Tooltip, Element) : Tooltip.__proto__ = Element;
			        Tooltip.prototype.constructor = Tooltip;
			        Tooltip.$super = Element.prototype;
			        Tooltip.$superClass = Element;
			        Object.defineProperty(Tooltip, 'createTooltip', {
			            enumerable: false,
			            value: function (obj) {
			                var s = $('<div class=\'tooltip\'></div>');
			                if (obj.data('class'))
			                    s.addClass(obj.data('class'));
			                else
			                    s.addClass('default');
			                $('body').append(s);
			                return s;
			            }
			        });
			        Object.defineProperty(Tooltip, 'register', {
			            enumerable: false,
			            value: function () {
			                if (this.registered)
			                    return;
			                $.fn.voxhastooltip = function () {
			                    var dp = [];
			                    this.each(function () {
			                        var o = $(this);
			                        var t = undefined;
			                        this.voxcss_element = this.voxcss_element || {};
			                        t = this.voxcss_element['vox-hastooltip'];
			                        if (!t) {
			                            t = new Tooltip(o);
			                            this.voxcss_element['vox-hastooltip'] = t;
			                        }
			                        dp.push(t);
			                    });
			                    return dp;
			                };
			                vox.mutation.watchAppend($('body'), function (ev) {
			                    ev.jTarget.voxhastooltip();
			                }, '[data-hover=tooltip]');
			                $('[data-hover=tooltip]').voxhastooltip();
			                this.registered = true;
			            }
			        });
			        Object.defineProperty(Tooltip, '$constructor', {
			            enumerable: false,
			            value: function (obj) {
			                Tooltip.$superClass.call(this);
			                obj = $(obj);
			                var f = this.$ = {};
			                f.obj = obj;
			                this.obtainProps();
			                this.init();
			            }
			        });
			        Object.defineProperty(Tooltip.prototype, 'obtainProps', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                var s = f.obj.attr('vox-selector');
			                if (s)
			                    f.tip = $(s);
			                else
			                    f.tip = Tooltip.createTooltip(f.obj);
			                f.tip = f.tip.voxtooltip()[0];
			            }
			        });
			        Object.defineProperty(Tooltip.prototype, 'init', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                this.events();
			            }
			        });
			        Tooltip.prototype.__defineGetter__('tooltip', function () {
			            return this.$.tip;
			        });
			        Object.defineProperty(Tooltip.prototype, 'activate', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                if (f.obj.data('html'))
			                    f.tip.html = f.obj.data('tooltip');
			                else
			                    f.tip.text = f.obj.data('tooltip');
			                f.tip.activate(f.obj);
			            }
			        });
			        Object.defineProperty(Tooltip.prototype, 'events', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                f.obj.hover(function (self$0) {
			                    return function (ev) {
			                        if (ev.type == 'mouseenter')
			                            self$0.activate();
			                        else if (ev.type = 'mouseleave')
			                            f.tip.activateClose();
			                    };
			                }(this));
			            }
			        });
			    }
			    return Tooltip;
			}
			exports.default = init(w);
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			exports.default= function($, f){

	 
    var voxselect= f.obj.find("vox-select");
    if(voxselect.length>0){

        var select= f.obj.find("select");
        if(select.length==0){
            select= $("<select>");
            // select.attr("name", voxselect.attr("name"));

            $.each(voxselect.get(0).attributes, function() {
                // this.attributes is not a plain object, but an array
                // of attribute nodes, which contain both the name and value
                if(this.specified) {
                    select.attr(this.name, this.value);
                }
            });

            var options= voxselect.find("vc-option");
            options.each(function(){
                var toption= $(this);
                var option= $("<option>");

                option.html(toption.html());
                // option.val(toption.attr("value"));

                $.each(this.attributes, function() {
                    // this.attributes is not a plain object, but an array
                    // of attribute nodes, which contain both the name and value
                    if(this.specified) {
                        option.attr(this.name, this.value);
                    }
                });

                option.data("vc-option", toption);
                select.append(option);
                f.obj.append(select);
                voxselect.hide();
            });

        }

    }


	f.voxStyle= f.obj.find("vox-css[vox-func='input-style']")
    if(f.voxStyle.length==0)
        f.voxStyle= $('<vox-css vox-type="class" vox-func="input-style" vox-selector="li>a:not([disabled]>a)">')

    var cl=[f.obj.data("activecolor")  + "-hover"]
    if(f.obj.data("activecolortext"))
        cl.push("text" + f.obj.data("select-activecolortext")  + "-hover")
    else
        cl.push("text-white-hover")



    f.voxStyle.data("value", cl.join(" "))



    f.obj.find(".select-wrapper").remove()
    f.select= f.obj.find("select")

    f.sw= $("<div>")
    f.sw.addClass("select-wrapper")
    f.sw.insertBefore(f.select)
    f.opw= $("<ul>")
    f.opw.addClass("text-"+f.obj.data("activecolor"))
    f.opw.addClass("options-wrapper")
    var i1= $("<input>")
    i1.attr("voxs", "voxs")
    i1.attr("type","text")
    i1.attr("readonly","readonly")
    f.inp= i1

    var av= f.select.val()
    var caret= $("<i class='fa fa-caret-down select-down'></i>")
    f.select.css("position", "absolute")
    f.select.css("top", "80px")
    var val=''
    f.selectDVal= ' '




    f.appendOption= function(e){
        var op= $("<li>")
        if(e.jOption){
            e.jOption.get(0).__optionwrapper= op
        }

        var vv= e.value||""
        op.attr("value", vv)

        var a= $("<a>")
        a.attr("voxs","voxs")
        a.data("value", vv)

        if(e.html)
            a.html(e.html)
        else
            a.text(e.text)


        op.append(a)
        if(e.disabled!==undefined){
            op.attr("disabled", "disabled")
        }
        if(vv==""){
           f.selectDVal= e.text
        }

        if(!av){
            if(e.selected!==undefined && e.disabled===undefined){
                op.attr("selected", "selected")
                i1.val(e.text)
                val=vv
            }
        }
        else{
            if(vv==av){
                op.attr("selected", "selected")
                i1.val(e.text)
                val=v
            }
        }
        f.opw.append(op)

        return op
    }



    f.select.find("option").each(function(){
        var e=$(this)
        f.appendOption({
            html: e.html(),
            text: e.text(),
            disabled: e.attr("disabled"),
            selected: e.attr("selected"),
            value: e.val(),
            jOption: e
        })
    })

    var adjtime=0
    var atm= function(){
      if(adjtime)
        clearTimeout(adjtime)
      adjtime= setTimeout(function(){
          f.adjustValue()
          adjtime=0
      },200)
    }


    
    if(f.scope && f.observable){


        var options=[], items=[]
        var values= {}
		var scope= f.scope
		
		var createOption= function(value){
			var option= $("<option>")
			
			/*
	        if(values[value.value])
	            return 
	          */
	          
	        values[value.value]= true
	        value.jOption= option
	        var t, item= f.appendOption(value)
	        items.push(item)
	        if(!value.text){
	            t= $("<div>")
	            t.html(value.html)
	            value.text= t.text()
	        }
	        options.push(option)
	        option.text(value.text)
	        option.val(value.value)
	        if(value.selected)
	            option.attr("selected","selected")
	        if(value.disabled)
	            option.attr("disabled","disabled")
	           
	          
	        f.select.append(option)
	        if(value.selected){
	            f.select.val(value.value)
	            f.r()
	        }
	        return item
		}
	
		var newdom, child, childs=[], newdom2
		var adding= function(ev, index){
			
			
			if(f.select.attr("dynamic-removed")!==undefined)
				return
				
			var current= true, temporal, g, dom2
			if(index==undefined)
				index= ev.value.index
			
			newdom=items[index]
			child= childs[index]
			
			if(!newdom){
				newdom= createOption(ev.value[0])
				current=false
				//temporal=$("<div>")
				//temporal.append(newdom)
			}
			else{
				// Por ahora no hace nada
				dom2= createOption(ev.value[0])
				//newdom.replaceWith(dom2)
				newdom.remove()
				newdom=dom2
			}
			items[index]= newdom
			
			
			if(!child){
				child= scope.createChild()
				childs[index]= child
				
				if(newdom){
					newdom.attr("dynamic-child-scope", true)
					newdom.each(function(){
						this["dynamic_scope"]=child
					})
				}
				
			}
			
			
			if(current ){
				child["option"]= ev.value
			}
			else{
			
				child["option"]= ev.value[0]
				child.$index= index
				
				/*g= function(ev){
					if(g.finished)
						return 
					window.DEBUG && console.info("CAMBIANDO ...", ev, index)
					adding(ev, index)
				}
				g.event= f.observable + "." + child.$index
				
				
				scope.attach({
					event: "change", 
					name: arrname + "." + child.$index, 
					func: g,
					dom: newdom
				})
				
				// parse with new scope
				self.parse(temporal, child)*/
			}
			
		}
		
		var removing= function(ev){
			var keys=Object.keys(items)
			if(ev.value< keys.length){
				for(var i=ev.value;i<keys.length;i++){
					// remove object ...
					try{
						items[keys[i]].remove()
						//funcs[i].finished= true
						//scope.removechange(funcs[i].event, funcs[i])
						
						delete items[keys[i]]
						delete childs[keys[i]]
					}catch(e){
						console.error("ERROR REMOVING:", e)
					}
				}
			}
		}
		
		
		scope.attach({
			event: "push", 
			name: f.observable,
			func: adding,
			dom: f.obj
		})
		scope.attach({
			event: "change", 
			name: f.observable + ".length", 
			func: removing,
			dom: f.obj
		})
		
		
		
		// COPIAR ACTUALES ...
		var j= function(){
			var keys=Object.keys(items)
			if(keys.length>0){
				for(var i=0;i<keys.length;i++){
					// remove object ...
					try{
						items[i].remove()
						//funcs[i].finished= true
						delete items[i]
						delete childs[i]
					}catch(e){
						console.error("ERROR REMOVING:", e)
					}
				}
			}
			
			var parts= f.observable.split(".")
			var o= scope
			for(var i=0;i<parts.length;i++){
				o= o[parts[i]]
				if(!o)
					break 
			}
			
			if(o && o.length){
				// push the array ...
				for(var i=0;i<o.length;i++){
					adding({
						value:{
							"0": o[i],
							"index": i
						}
					})
				}
				
				if(f.select.data("value")){
					setTimeout(function(){
						f.select.val(f.select.data("value"))	
					},80)
				}
			}
		}
		
		j()
		
		
		scope.attach({
			event: "change", 
			func: function(ev){
				var t= ev.name+"."
				if(f.observable.startsWith(t) || f.observable==ev.name){
					j()
				}
			},
			dom: f.obj
		})
    }


    if(!val)
        i1.val(f.selectDVal)


    f.sw.append(i1)
    f.sw.append(caret)
    f.sw.append(f.opw)
    f.sw.append(f.voxStyle)



    f.opw.addClass("dropdown-menu")
    
    
    f.dropdown= f.sw.voxdropdown()[0]
    var h= function(){
    	if(f.obj.attr("disabled")!=undefined)
			return 
        f.dropdown.toggle()
    }
    f.sw.find(".select-down").click(h)
    f.inp.click(h)
    f.label.click(h)

}

		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			var mask = require('./jquery-mask');
			exports.mask = mask;
			var fnVal = $.fn.val;
			{
			    var Input = function Input() {
			        Input.$constructor ? Input.$constructor.apply(this, arguments) : Input.$superClass && Input.$superClass.apply(this, arguments);
			    };
			    Input.prototype = Object.create(Element.prototype);
			    Object.setPrototypeOf ? Object.setPrototypeOf(Input, Element) : Input.__proto__ = Element;
			    Input.prototype.constructor = Input;
			    Input.$super = Element.prototype;
			    Input.$superClass = Element;
			    Object.defineProperty(Input, 'register', {
			        enumerable: false,
			        value: function () {
			            if (this.registered)
			                return;
			            $.fn.voxinput = function () {
			                var dp = [];
			                this.each(function () {
			                    var o = $(this);
			                    var t = undefined;
			                    this.voxcss_element = this.voxcss_element || {};
			                    t = this.voxcss_element['vox-input'];
			                    if (!t) {
			                        t = new Input(o);
			                        this.voxcss_element['vox-input'] = t;
			                    }
			                    dp.push(t);
			                });
			                return dp;
			            };
			            $(function () {
			                vox.mutation.watchAppend($('body'), function (ev) {
			                    ev.jTarget.voxinput();
			                }, '.input-field');
			                $('.input-field').voxinput();
			            });
			            var value = fnVal;
			            var replaceval = $.fn.val = function () {
			                var er, result = value.apply(this, arguments);
			                $.fn.val = value;
			                try {
			                    this.each(function () {
			                        var o = $(this);
			                        var p = o.parents('.input-field').eq(0);
			                        if (p.length > 0) {
			                            var t = p[0].voxcss_element ? p[0].voxcss_element['vox-input'] : null;
			                            if (t) {
			                                t.adjustValue();
			                                t.$.r();
			                                t.$.elasticr();
			                            }
			                        }
			                    });
			                } catch (e) {
			                    er = e;
			                }
			                $.fn.val = replaceval;
			                if (er) {
			                    throw er;
			                }
			                return result;
			            };
			            this.registered = true;
			        }
			    });
			    Object.defineProperty(Input, '$constructor', {
			        enumerable: false,
			        value: function (obj) {
			            Input.$superClass.call(this);
			            obj = $(obj);
			            var f = this.$ = {};
			            f.obj = obj;
			            this.func = [];
			            this.obtainProps();
			            this.init();
			        }
			    });
			    Object.defineProperty(Input.prototype, 'init', {
			        enumerable: false,
			        value: function () {
			            var scope, f = this.$;
			            f.observable = f.obj.data('list');
			            if (f.observable) {
			                scope = f.obj.voxscope();
			                if (scope) {
			                    f.scope = scope;
			                }
			            }
			            f.r = this.r.bind(this);
			            f.adjustValue = this.adjustValue.bind(this);
			            if (f.obj.is('.select')) {
			                require('./Input-createSelect').default($, f);
			                f.select.attr('vox-input', 'vox-input');
			            }
			            this.events();
			            f.r();
			        }
			    });
			    Object.defineProperty(Input.prototype, 'imask', {
			        enumerable: false,
			        value: function (input) {
			            setTimeout(function () {
			                var d = input.data('imask');
			                if (typeof d != 'object' && d) {
			                    input.mask(d.toString());
			                }
			            }, 30);
			        }
			    });
			    Object.defineProperty(Input.prototype, 'obtainProps', {
			        enumerable: false,
			        value: function () {
			            var f = this.$, self = this;
			            f.inp = f.obj.find('input,textarea');
			            f.inp.each(function () {
			                self.imask($(this));
			            });
			            f.label = f.obj.find('label');
			            f.label.addClass('normal');
			            f.action = f.obj.find('.action');
			            f.inp.attr('vox-input', 'vox-input');
			            if (!f.obj.data('error-color'))
			                f.obj.data('error-color', 'red');
			            if (!f.obj.data('error-color'))
			                f.obj.data('error-color', 'red');
			            if (!f.obj.data('warning-color'))
			                f.obj.data('warning-color', 'orange');
			            if (!f.obj.data('ok-color'))
			                f.obj.data('ok-color', 'green');
			        }
			    });
			    Object.defineProperty(Input.prototype, 'dynamicDispose', {
			        enumerable: false,
			        value: function () {
			            Element.prototype.dynamicDispose.call(this);
			            if (this.$) {
			                if (this.$.sw) {
			                    this.$.sw.remove();
			                }
			                for (var id in this.$) {
			                    delete this.$[id];
			                }
			                delete this.$;
			            }
			        }
			    });
			    Object.defineProperty(Input.prototype, 'adjustValue', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            if (!f.select)
			                return;
			            var va = v = f.select.val();
			            if (!v && f.select.attr('data-value')) {
			                v = f.select.attr('data-value');
			                f.select.data('value', v);
			                f.select.attr('data-value', '');
			            } else if (!v && f.select.data('value')) {
			                v = f.select.data('value');
			            }
			            if (va)
			                f.select.data('value', '');
			            f.opw.find('li').removeAttr('selected');
			            f.opw.find('li>a').removeAttr('hover-active');
			            f.opw.find('li').each(function () {
			                var l = $(this);
			                if (l.attr('value') == v || !l.attr('value') && !v) {
			                    l.attr('selected', 'selected');
			                    l.find('a').attr('hover-active', 'hover-active');
			                    f.inp.val(l.text());
			                }
			            });
			        }
			    });
			    Object.defineProperty(Input.prototype, 'r', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            if (f.inp.val() && f.inp.val().length > 0 && f.inp.attr('type') != 'search' || f.inp.attr('type') == 'date')
			                f.label.addClass('active');
			            else
			                f.label.removeClass('active');
			        }
			    });
			    Object.defineProperty(Input.prototype, 'events', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            f.elasticr = function () {
			                if (f.inp.hasClass('vox-textarea') || f.inp.hasClass('vox-elastic'))
			                    f.inp.voxelastic()[0] && f.inp.voxelastic()[0].refresh();
			            };
			            f.line = f.obj.find('.line');
			            if (f.line.length == 0) {
			                f.line = $('<div>');
			                f.line.addClass('line');
			                f.obj.append(f.line);
			            }
			            if (f.select) {
			                f.select.focus(function (ev) {
			                    f.inp.focus();
			                });
			                f.select.blur(function (ev) {
			                    f.inp.blur();
			                });
			                f.select.change(function (self$0) {
			                    return function () {
			                        f.select.data('value', f.select.val());
			                        self$0.adjustValue();
			                    };
			                }(this));
			                this.attachEvent(f.dropdown, 'select', function (ev) {
			                    f.select.val(ev.value);
			                    f.select.change();
			                });
			            }
			            f.addactive = function () {
			                if (f.inp.attr('type') != 'search') {
			                    f.label.addClass('active');
			                    f.label.removeClass('normal');
			                }
			                f.obj.addClass('active');
			            };
			            var oninput = function (self$0) {
			                return function (ev) {
			                    f.obj.removeClass('error warning ok');
			                    self$0.emit('focus', ev);
			                    if (ev.defaultPrevented)
			                        return;
			                    f.addactive();
			                    var l;
			                    if (f.lineClass) {
			                        l = 'text-' + f.lineClass;
			                        f.line.removeClass(f.lineClass);
			                        f.label.removeClass(l);
			                    }
			                    f.lineClass = f.obj.data('activecolor');
			                    f.line.addClass(f.lineClass);
			                    l = 'text-' + f.lineClass;
			                    f.label.addClass(l);
			                };
			            }(this);
			            f.inp.on('keyup input', oninput);
			            f.inp.focus(function (ev) {
			                if (f.obj.hasClass('error') || f.obj.hasClass('warning') || f.obj.hasClass('ok'))
			                    return;
			                return oninput(ev);
			            });
			            f.inp.blur(function (self$0) {
			                return function (ev) {
			                    if (f.obj.hasClass('error') || f.obj.hasClass('warning') || f.obj.hasClass('ok'))
			                        return;
			                    self$0.emit('blur', ev);
			                    if (ev.defaultPrevented)
			                        return;
			                    f.r();
			                    f.obj.removeClass('active');
			                    f.label.addClass('normal');
			                    if (f.lineClass) {
			                        l = 'text-' + f.lineClass;
			                        f.line.removeClass(f.lineClass);
			                        f.label.removeClass(l);
			                    }
			                    f.lineClass = undefined;
			                };
			            }(this));
			            this.on('change', function (self$0) {
			                return function () {
			                    if (f.select) {
			                        self$0.adjustValue();
			                    }
			                    return f.r();
			                };
			            }(this));
			        }
			    });
			}
			exports.default = Input;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			var modals = [];
			{
			    var Modal = function Modal() {
			        Modal.$constructor ? Modal.$constructor.apply(this, arguments) : Modal.$superClass && Modal.$superClass.apply(this, arguments);
			    };
			    Modal.prototype = Object.create(Element.prototype);
			    Object.setPrototypeOf ? Object.setPrototypeOf(Modal, Element) : Modal.__proto__ = Element;
			    Modal.prototype.constructor = Modal;
			    Modal.$super = Element.prototype;
			    Modal.$superClass = Element;
			    Object.defineProperty(Modal, 'checkOpened', {
			        enumerable: false,
			        value: function () {
			            var open;
			            for (var i = 0; i < modals.length; i++) {
			                open = open || modals[i].isOpened();
			                if (open)
			                    break;
			            }
			            if (!open) {
			                $('body').removeClass('modal-opened');
			                $('.modal-back').hide();
			            } else {
			                $('body').addClass('modal-opened');
			                $('.modal-back').show();
			            }
			        }
			    });
			    Object.defineProperty(Modal, 'openBack', {
			        enumerable: false,
			        value: function () {
			            $('body').addClass('modal-opened');
			            var m = $('.modal-back');
			            if (m.length == 0) {
			                m = $('<div>');
			                m.addClass('modal-back');
			                m.addClass('default');
			                $('body').append(m);
			            }
			            m.show();
			        }
			    });
			    Object.defineProperty(Modal, 'register', {
			        enumerable: false,
			        value: function () {
			            if (this.registered)
			                return;
			            $.fn.voxmodal = function () {
			                var dp = [];
			                this.each(function () {
			                    var o = $(this);
			                    var t = undefined;
			                    this.voxcss_element = this.voxcss_element || {};
			                    t = this.voxcss_element['vox-modal'];
			                    if (!t) {
			                        t = new Modal(o);
			                        this.voxcss_element['vox-modal'] = t;
			                    }
			                    dp.push(t);
			                });
			                return dp;
			            };
			            $(function () {
			                vox.mutation.watchAppend($('body'), function (ev) {
			                    ev.jTarget.voxmodal();
			                }, '.modal');
			                $('.modal').voxmodal();
			                $('[data-toggle=modal]').click(function () {
			                    var e = $(this);
			                    var s = e.attr('vox-selector');
			                    var g = $(s).eq(0);
			                    var h = g.voxmodal()[0];
			                    if (h) {
			                        h.open();
			                    }
			                });
			            });
			            this.registered = true;
			        }
			    });
			    Object.defineProperty(Modal, '$constructor', {
			        enumerable: false,
			        value: function (obj) {
			            Modal.$superClass.call(this);
			            obj = $(obj);
			            var f = this.$ = {};
			            f.obj = obj;
			            this.obtainProps();
			            this.init();
			        }
			    });
			    Object.defineProperty(Modal.prototype, 'init', {
			        enumerable: false,
			        value: function () {
			            this.events();
			        }
			    });
			    Object.defineProperty(Modal.prototype, 'obtainProps', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            f.container = f.obj.parent();
			            modals.push(this);
			        }
			    });
			    Object.defineProperty(Modal.prototype, 'isOpened', {
			        enumerable: false,
			        value: function () {
			            return this.$.obj.hasClass('opened');
			        }
			    });
			    Object.defineProperty(Modal.prototype, 'open', {
			        enumerable: false,
			        value: function (event) {
			            var f = this.$;
			            var ev = this.createEvent('beforeopen', event);
			            ev.modal = this;
			            this.emit(ev);
			            if (ev.defaultPrevented)
			                return;
			            if (f.delay) {
			                clearTimeout(f.delay);
			                f.delay = undefined;
			            }
			            Modal.openBack();
			            f.lEvent = event ? event.type : '';
			            f.obj.addClass('opened');
			            f.container.show();
			            f.obj.voxanimate(f.obj.data('ineffect') || 'bounceInUp', undefined, function (self$0) {
			                return function () {
			                    Modal.checkOpened();
			                    var ev = self$0.createEvent('open', event);
			                    ev.modal = self$0;
			                    self$0.emit(ev);
			                };
			            }(this));
			        }
			    });
			    Object.defineProperty(Modal.prototype, 'close', {
			        enumerable: false,
			        value: function (event) {
			            var f = this.$;
			            var ev = this.createEvent('beforeclose', event);
			            ev.modal = this;
			            this.emit(ev);
			            if (ev.defaultPrevented)
			                return;
			            if (f.delay) {
			                clearTimeout(f.delay);
			                f.delay = undefined;
			            }
			            f.lEvent = event ? event.type : '';
			            f.obj.removeClass('opened');
			            f.container.hide();
			            f.obj.voxanimate(f.obj.data('outeffect') || 'bounceOutDown', undefined, function (self$0) {
			                return function () {
			                    Modal.checkOpened();
			                    var ev = self$0.createEvent('close', event);
			                    ev.modal = self$0;
			                    self$0.emit(ev);
			                };
			            }(this));
			        }
			    });
			    Object.defineProperty(Modal.prototype, 'toggle', {
			        enumerable: false,
			        value: function () {
			            if (f.obj.hasClass('opened'))
			                this.close();
			            else
			                this.open();
			        }
			    });
			    Object.defineProperty(Modal.prototype, 'events', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            vox.platform.attachEvents('keyup keydown', {
			                active: function (self$0) {
			                    return function () {
			                        return self$0.isOpened();
			                    };
			                }(this),
			                processEvent: function (self$0) {
			                    return function (ev) {
			                        ev.modal = self$0;
			                        return ev;
			                    };
			                }(this),
			                self: this,
			                callback: function (self$0) {
			                    return function (ev) {
			                        if (ev.keyCode == 27 && ev.type == 'keyup') {
			                            if (!f.obj.data('escape-disabled')) {
			                                self$0.close();
			                            }
			                        }
			                    };
			                }(this)
			            });
			            vox.platform.attachOuterClick(f.obj, {
			                active: function (self$0) {
			                    return function () {
			                        return self$0.isOpened();
			                    };
			                }(this),
			                processEvent: function (self$0) {
			                    return function (ev) {
			                        var ev2 = self$0.createEvent('outerclick');
			                        ev2.modal = self$0;
			                        ev2.target = ev.target;
			                        ev2.clickEvent = ev;
			                        return ev2;
			                    };
			                }(this),
			                self: this,
			                callback: function (self$0) {
			                    return function (ev) {
			                        !f.obj.data('closeonouterclick-disabled') && self$0.close();
			                    };
			                }(this)
			            });
			        }
			    });
			}
			exports.default = Modal;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			var w = {};
			if (typeof window !== 'undefined')
			    w = window;
			function init(window) {
			    procesar = function (Parallax) {
			        Parallax.objects = [];
			    };
			    {
			        var Parallax = function Parallax() {
			            Parallax.$constructor ? Parallax.$constructor.apply(this, arguments) : Parallax.$superClass && Parallax.$superClass.apply(this, arguments);
			        };
			        Parallax.prototype = Object.create(Element.prototype);
			        Object.setPrototypeOf ? Object.setPrototypeOf(Parallax, Element) : Parallax.__proto__ = Element;
			        Parallax.prototype.constructor = Parallax;
			        Parallax.$super = Element.prototype;
			        Parallax.$superClass = Element;
			        Parallax.__defineGetter__('uid', function () {
			            Parallax.id = Parallax.id | 0;
			            return Parallax.id++;
			        });
			        Object.defineProperty(Parallax, 'register', {
			            enumerable: false,
			            value: function () {
			                if (this.registered)
			                    return;
			                $.fn.voxparallax = function () {
			                    var dp = [];
			                    this.each(function () {
			                        var o = $(this);
			                        var t = undefined;
			                        this.voxcss_element = this.voxcss_element || {};
			                        t = this.voxcss_element['vox-parallax'];
			                        if (!t) {
			                            t = new Parallax(o);
			                            this.voxcss_element['vox-parallax'] = t;
			                        }
			                        dp.push(t);
			                    });
			                    return dp;
			                };
			                $(function () {
			                    vox.mutation.watchAppend($('body'), function (ev) {
			                        ev.jTarget.voxparallax();
			                    }, '.parallax');
			                    $('.parallax').voxparallax();
			                });
			                this.registered = true;
			            }
			        });
			        Object.defineProperty(Parallax, '$constructor', {
			            enumerable: false,
			            value: function (obj) {
			                Parallax.$superClass.call(this);
			                Parallax.objects.push(this);
			                obj = $(obj);
			                var f = this.$ = {};
			                f.obj = obj;
			                this.obtainProps();
			                this.init();
			            }
			        });
			        Object.defineProperty(Parallax.prototype, 'obtainProps', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                this.id = Parallax.uid;
			                f.img = f.obj.find('.img');
			                f.scrollfire = f.obj.voxscrollfire()[0];
			            }
			        });
			        Object.defineProperty(Parallax.prototype, 'init', {
			            enumerable: false,
			            value: function () {
			                this.events();
			            }
			        });
			        Object.defineProperty(Parallax.prototype, '$scroll', {
			            enumerable: false,
			            value: function (ev) {
			                var f = this.$;
			                var h = $(window).height();
			                var hi = f.obj.outerHeight();
			                var maxRange = h + hi;
			                var off = ev.offset;
			                f.img.css('top', -(f.img.height() * 80 / 100) + 'px');
			                var percent = off * 100 / maxRange;
			                var translate = 80 * percent / 100;
			                translate = f.img.height() * translate / 100;
			                var factor = parseFloat(f.obj.data('factor'));
			                if (!factor || isNaN(factor))
			                    factor = 1.24;
			                translate *= factor;
			                f.img.css('transform', 'translate3d(0, ' + translate.toString() + 'px, 0)');
			            }
			        });
			        Object.defineProperty(Parallax.prototype, 'events', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                f.scrollfire.on('scroll', function (self$0) {
			                    return function (ev) {
			                        return self$0.$scroll(ev);
			                    };
			                }(this));
			                f.scrollfire.refresh();
			                $(window).resize();
			            }
			        });
			    }
			    procesar(Parallax);
			    return Parallax;
			}
			exports.default = init(w);
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			{
			    var Pinned = function Pinned() {
			        Pinned.$constructor ? Pinned.$constructor.apply(this, arguments) : Pinned.$superClass && Pinned.$superClass.apply(this, arguments);
			    };
			    Pinned.prototype = Object.create(Element.prototype);
			    Object.setPrototypeOf ? Object.setPrototypeOf(Pinned, Element) : Pinned.__proto__ = Element;
			    Pinned.prototype.constructor = Pinned;
			    Pinned.$super = Element.prototype;
			    Pinned.$superClass = Element;
			    Object.defineProperty(Pinned, 'register', {
			        enumerable: false,
			        value: function () {
			            if (this.registered)
			                return;
			            $.fn.voxpinned = function () {
			                var dp = [];
			                this.each(function () {
			                    var o = $(this);
			                    var t = undefined;
			                    this.voxcss_element = this.voxcss_element || {};
			                    t = this.voxcss_element['vox-pinned'];
			                    if (!t) {
			                        t = new Pinned(o);
			                        this.voxcss_element['vox-pinned'] = t;
			                    }
			                    dp.push(t);
			                });
			                return dp;
			            };
			            $(function () {
			                vox.mutation.watchAppend($('body'), function (ev) {
			                    ev.jTarget.voxpinned();
			                }, '.pinned');
			                $('.pinned').voxpinned();
			            });
			            this.registered = true;
			        }
			    });
			    Object.defineProperty(Pinned, '$constructor', {
			        enumerable: false,
			        value: function (obj) {
			            Pinned.$superClass.call(this);
			            obj = $(obj);
			            var f = this.$ = {};
			            f.obj = obj;
			            this.obtainProps();
			            this.init();
			        }
			    });
			    Object.defineProperty(Pinned.prototype, 'obtainProps', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            f.parent = f.obj.parent();
			            f.scrollfire = f.parent.voxscrollfire()[0];
			        }
			    });
			    Object.defineProperty(Pinned.prototype, 'init', {
			        enumerable: false,
			        value: function () {
			            this.events();
			        }
			    });
			    Object.defineProperty(Pinned.prototype, '$scroll', {
			        enumerable: false,
			        value: function (ev) {
			            var f = this.$, j = f.obj, j2 = f.parent, h = j.outerHeight(), h2 = j2.outerHeight();
			            var wh = $(window).outerHeight();
			            var pt = j2.attr('pinned-top') | 0;
			            wh -= pt;
			            var applyPinned = true, pos, lastpos = undefined;
			            var pinnedItems = j2.find('.pinned');
			            for (var i = 0; i < pinnedItems.length; i++) {
			                pos = pinnedItems.eq(i).position().top;
			                if (lastpos !== undefined && lastpos != pos) {
			                    applyPinned = false;
			                    break;
			                }
			                lastpos = pos;
			            }
			            if (!applyPinned) {
			                pinnedItems.css('margin-top', 0);
			                return;
			            }
			            if (h > wh) {
			                if (ev.offset >= h) {
			                    if (ev.offset > h2) {
			                        ev.offset = h2;
			                    }
			                    var a$1 = ev.offset - h;
			                    j.css('margin-top', a$1 + 'px');
			                } else {
			                    j.css('margin-top', '0');
			                }
			            } else {
			                if (ev.offset > wh) {
			                    var a$2 = ev.offset - wh;
			                    if (a$2 + h >= h2) {
			                        a$2 = h2 - h;
			                    }
			                    j.css('margin-top', a$2 + 'px');
			                } else {
			                    j.css('margin-top', '0');
			                }
			            }
			        }
			    });
			    Object.defineProperty(Pinned.prototype, 'events', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            f.scrollfire.on('scroll', function (self$0) {
			                return function (ev) {
			                    return self$0.$scroll(ev);
			                };
			            }(this));
			        }
			    });
			}
			exports.default = Pinned;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			{
			    var ScrollFire = function ScrollFire() {
			        ScrollFire.$constructor ? ScrollFire.$constructor.apply(this, arguments) : ScrollFire.$superClass && ScrollFire.$superClass.apply(this, arguments);
			    };
			    ScrollFire.prototype = Object.create(Element.prototype);
			    Object.setPrototypeOf ? Object.setPrototypeOf(ScrollFire, Element) : ScrollFire.__proto__ = Element;
			    ScrollFire.prototype.constructor = ScrollFire;
			    ScrollFire.$super = Element.prototype;
			    ScrollFire.$superClass = Element;
			    Object.defineProperty(ScrollFire, 'register', {
			        enumerable: false,
			        value: function () {
			            if (this.registered)
			                return;
			            $.fn.voxscrollfire = function () {
			                var dp = [];
			                this.each(function () {
			                    var o = $(this);
			                    var t = undefined;
			                    this.voxcss_element = this.voxcss_element || {};
			                    t = this.voxcss_element['vox-scrollfire'];
			                    if (!t) {
			                        t = new ScrollFire(o);
			                        this.voxcss_element['vox-scrollfire'] = t;
			                    }
			                    dp.push(t);
			                });
			                return dp;
			            };
			            $(function () {
			                vox.mutation.watchAppend($('body'), function (ev) {
			                    ev.jTarget.voxscrollfire();
			                }, '.scroll-fire');
			                $('.scroll-fire').voxscrollfire();
			            });
			            this.registered = true;
			        }
			    });
			    Object.defineProperty(ScrollFire, '$constructor', {
			        enumerable: false,
			        value: function (obj) {
			            ScrollFire.$superClass.call(this);
			            obj = $(obj);
			            var f = this.$ = {};
			            f.obj = obj;
			            this.init();
			        }
			    });
			    Object.defineProperty(ScrollFire.prototype, 'init', {
			        enumerable: false,
			        value: function () {
			            this.$.off = [];
			            this.events();
			        }
			    });
			    Object.defineProperty(ScrollFire.prototype, 'bindOnOffset', {
			        enumerable: false,
			        value: function (offset, callback) {
			            var f = this.$;
			            f.off.push({
			                offset: offset,
			                callback: callback,
			                scroll: -1
			            });
			        }
			    });
			    Object.defineProperty(ScrollFire.prototype, 'refresh', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            var ev = this.createEvent('scroll');
			            ev.scrollfire = this;
			            ev.resize = true;
			            f.h(ev);
			        }
			    });
			    Object.defineProperty(ScrollFire.prototype, 'events', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            var w = vox.platform.scrollObject;
			            var wo = w.get(0);
			            var g = function (self$0) {
			                return function (ev) {
			                    var he = f.obj.outerHeight();
			                    var top = f.obj.offset().top;
			                    var sTop = w.scrollTop();
			                    var windowScroll = wo.pageYOffset + wo.innerHeight;
			                    var elementOffset = f.obj.get(0).getBoundingClientRect().top + document.body.scrollTop;
			                    var offset = windowScroll - top;
			                    if (ev.resize || windowScroll >= top && sTop <= top + he) {
			                        ev.scrollfire = self$0;
			                        ev.offset = windowScroll - top;
			                        ev.type = 'scroll';
			                        self$0.emit(ev);
			                    }
			                    for (var i = 0; i < f.off.length; i++) {
			                        var o = f.off[i];
			                        if (offset >= o.offset) {
			                            if (o.scroll < o.offset && sTop <= top + he) {
			                                o.callback(ev);
			                            }
			                        }
			                        o.scroll = offset;
			                    }
			                };
			            }(this);
			            var y;
			            var h = function (ev) {
			                var delay = parseInt(f.obj.data('delay'));
			                if (!delay || isNaN(delay)) {
			                    delay = 0;
			                }
			                if (delay == 0) {
			                    return g(ev);
			                }
			                if (y) {
			                    clearTimeout(y);
			                    y = undefined;
			                }
			                y = setTimeout(function () {
			                    g(ev);
			                }, delay);
			            };
			            w.on('scroll', h);
			            f.h = h;
			            var r1 = this.refresh.bind(this);
			            f.resize_func = function () {
			                if (f.r) {
			                    clearTimeout(f.r);
			                    f.r = undefined;
			                }
			                f.r = setTimeout(r1, 100);
			            };
			            w.resize(f.resize_func);
			            setTimeout(r1, 100);
			        }
			    });
			    Object.defineProperty(ScrollFire.prototype, 'dynamicDispose', {
			        enumerable: false,
			        value: function () {
			            var w = vox.platform.scrollObject;
			            if (this.$.resize_func) {
			                w.off('resize', this.$.resize_func);
			            }
			            if (this.$.h) {
			                w.off('scroll', this.$.h);
			            }
			        }
			    });
			}
			exports.default = ScrollFire;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			var w = {};
			if (typeof window !== 'undefined')
			    w = window;
			function init(window) {
			    {
			        var SideNav = function SideNav() {
			            SideNav.$constructor ? SideNav.$constructor.apply(this, arguments) : SideNav.$superClass && SideNav.$superClass.apply(this, arguments);
			        };
			        SideNav.prototype = Object.create(Element.prototype);
			        Object.setPrototypeOf ? Object.setPrototypeOf(SideNav, Element) : SideNav.__proto__ = Element;
			        SideNav.prototype.constructor = SideNav;
			        SideNav.$super = Element.prototype;
			        SideNav.$superClass = Element;
			        Object.defineProperty(SideNav, 'init', {
			            enumerable: false,
			            value: function () {
			                if (!SideNav.overlay) {
			                    SideNav.overlay = $('#sidenav-overlay');
			                    if (SideNav.overlay.length == 0)
			                        SideNav.overlay = undefined;
			                }
			                if (!SideNav.overlay) {
			                    SideNav.overlay = $('<div>');
			                    SideNav.overlay.addClass('transitioned');
			                    SideNav.overlay.attr('id', 'sidenav-overlay');
			                    SideNav.overlay.css('opacity', 0);
			                    SideNav.overlay.hide();
			                    $('body').append(SideNav.overlay);
			                    SideNav.overlay.click(function () {
			                        if (SideNav.current)
			                            SideNav.current.close();
			                    });
			                }
			            }
			        });
			        Object.defineProperty(SideNav, 'register', {
			            enumerable: false,
			            value: function () {
			                if (this.registered)
			                    return;
			                $.fn.voxsidenav = function () {
			                    var dp = [];
			                    this.each(function () {
			                        var o = $(this);
			                        var t = undefined;
			                        this.voxcss_element = this.voxcss_element || {};
			                        t = this.voxcss_element['vox-sidenav'];
			                        if (!t) {
			                            t = new SideNav(o);
			                            this.voxcss_element['vox-sidenav'] = t;
			                        }
			                        dp.push(t);
			                    });
			                    return dp;
			                };
			                $(function () {
			                    vox.mutation.watchAppend($('body'), function (ev) {
			                        ev.jTarget.voxsidenav();
			                    }, '.side-nav');
			                    $('.side-nav').voxsidenav();
			                });
			                this.registered = true;
			            }
			        });
			        Object.defineProperty(SideNav, '$constructor', {
			            enumerable: false,
			            value: function (obj) {
			                SideNav.$superClass.call(this);
			                obj = $(obj);
			                var f = this.$ = {};
			                f.obj = obj;
			                this.obtainProps();
			                this.init();
			            }
			        });
			        Object.defineProperty(SideNav.prototype, 'obtainProps', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                var main = f.obj.data('main');
			                f.main = $(main);
			            }
			        });
			        Object.defineProperty(SideNav.prototype, 'init', {
			            enumerable: false,
			            value: function () {
			                this.events();
			            }
			        });
			        Object.defineProperty(SideNav.prototype, 'isOpened', {
			            enumerable: false,
			            value: function () {
			                return this.$.obj.hasClass('opened');
			            }
			        });
			        Object.defineProperty(SideNav.prototype, 'open', {
			            enumerable: false,
			            value: function (event) {
			                var f = this.$;
			                SideNav.overlay.show();
			                SideNav.overlay.css('opacity', 1);
			                f.obj.addClass('opened');
			                SideNav.current = this;
			                var ev = this.createEvent('open', event);
			                ev.sidenav = this;
			                this.emit(ev);
			            }
			        });
			        Object.defineProperty(SideNav.prototype, 'close', {
			            enumerable: false,
			            value: function (event) {
			                var f = this.$;
			                SideNav.overlay.css('opacity', 0);
			                setTimeout(function () {
			                    SideNav.overlay.hide();
			                }, 800);
			                f.obj.removeClass('opened');
			                SideNav.current = undefined;
			                var ev = this.createEvent('close', event);
			                ev.sidenav = this;
			                this.emit(ev);
			                setTimeout(function (self$0) {
			                    return function () {
			                        self$0.G();
			                    };
			                }(this), 500);
			            }
			        });
			        Object.defineProperty(SideNav.prototype, 'toggle', {
			            enumerable: false,
			            value: function () {
			                if (this.isOpened())
			                    this.close();
			                else
			                    this.open();
			            }
			        });
			        Object.defineProperty(SideNav.prototype, '$r', {
			            enumerable: false,
			            value: function () {
			                if (SideNav.overlay.is(':visible'))
			                    return;
			                var f = this.$;
			                var po = f.obj.position();
			                var v = true;
			                if (po.left <= -f.obj.width() || po.left >= $(window).width())
			                    v = false;
			                if (v)
			                    f.main.css('padding-left', f.obj.outerWidth());
			                else
			                    f.main.css('padding-left', 0);
			            }
			        });
			        Object.defineProperty(SideNav.prototype, 'events', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                if (f.button) {
			                    f.button.click(function (self$0) {
			                        return function () {
			                            self$0.toggle();
			                        };
			                    }(this));
			                }
			            }
			        });
			    }
			    SideNav.init();
			    return SideNav;
			}
			exports.default = init(w);
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			var w = {};
			if (typeof window !== 'undefined')
			    w = window;
			var body = $('body');
			function init(window) {
			    {
			        var Slider = function Slider() {
			            Slider.$constructor ? Slider.$constructor.apply(this, arguments) : Slider.$superClass && Slider.$superClass.apply(this, arguments);
			        };
			        Slider.prototype = Object.create(Element.prototype);
			        Object.setPrototypeOf ? Object.setPrototypeOf(Slider, Element) : Slider.__proto__ = Element;
			        Slider.prototype.constructor = Slider;
			        Slider.$super = Element.prototype;
			        Slider.$superClass = Element;
			        Object.defineProperty(Slider, 'init', {
			            enumerable: false,
			            value: function () {
			            }
			        });
			        Object.defineProperty(Slider, 'register', {
			            enumerable: false,
			            value: function () {
			                if (this.registered)
			                    return;
			                $.fn.voxslider = function () {
			                    var dp = [];
			                    this.each(function () {
			                        var o = $(this);
			                        var t = undefined;
			                        this.voxcss_element = this.voxcss_element || {};
			                        t = this.voxcss_element['vox-slider'];
			                        if (!t) {
			                            t = new Slider(o);
			                            this.voxcss_element['vox-slider'] = t;
			                        }
			                        dp.push(t);
			                    });
			                    return dp;
			                };
			                $(function () {
			                    vox.mutation.watchAppend($('body'), function (ev) {
			                        ev.jTarget.voxslider();
			                    }, '.slider');
			                    $('.slider').voxslider();
			                });
			                this.registered = true;
			            }
			        });
			        Object.defineProperty(Slider, '$constructor', {
			            enumerable: false,
			            value: function (obj) {
			                Slider.$superClass.call(this);
			                obj = $(obj);
			                var f = this.$ = {};
			                f.obj = obj;
			                this.obtainProps();
			                this.init();
			            }
			        });
			        Object.defineProperty(Slider.prototype, 'init', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                this.events();
			                if (f.obj.hasClass('discrete'))
			                    this.createSteps();
			            }
			        });
			        Slider.prototype.__defineGetter__('maxValue', function () {
			            var v = this.$.obj.data('maxvalue');
			            return v === undefined ? 100 : v;
			        });
			        Slider.prototype.__defineGetter__('minValue', function () {
			            var v = this.$.obj.data('minvalue');
			            return v === undefined ? 0 : v;
			        });
			        Slider.prototype.__defineGetter__('step', function () {
			            var v = this.$.obj.data('step');
			            return v === undefined ? 1 : v;
			        });
			        Object.defineProperty(Slider.prototype, 'createSteps', {
			            enumerable: false,
			            value: function () {
			                var f = this.$, s;
			                var minValue = parseFloat(f.obj.data('minvalue'));
			                var maxValue = parseFloat(f.obj.data('maxvalue'));
			                var step = parseFloat(f.obj.data('step'));
			                var dif = maxValue - minValue;
			                var percent = 0;
			                var stepPercent = step * 100 / dif;
			                while (percent <= 100) {
			                    s = $('<div>');
			                    s.addClass('step');
			                    s.insertAfter(f.progress);
			                    if (f.obj.hasClass('vertical')) {
			                        if (f.obj.hasClass('inverted'))
			                            s.css('top', percent + '%');
			                        else
			                            s.css('bottom', percent + '%');
			                    } else {
			                        s.css('left', percent + '%');
			                    }
			                    percent += stepPercent;
			                }
			            }
			        });
			        Object.defineProperty(Slider.prototype, 'scopeValue', {
			            enumerable: false,
			            value: function (prop, value) {
			                var props = prop.split('.');
			                var scope = this.$.scope;
			                var o = scope;
			                for (var i = 0; i < props.length - 1; i++) {
			                    if (!o)
			                        return;
			                    o = o[props[i]];
			                }
			                if (!o)
			                    return;
			                if (arguments.length == 1)
			                    return o[props[props.length - 1]];
			                else
			                    o[props[props.length - 1]] = value;
			            }
			        });
			        Object.defineProperty(Slider.prototype, 'obtainProps', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                f.line = f.obj.find('.line');
			                f.progress = f.obj.find('.progress');
			                f.ball = f.obj.find('.ball');
			                f.ball.addClass('first');
			                f.line.addClass('transitioned');
			                f.progress.addClass('transitioned');
			                f.ball.addClass('transitioned');
			                if (f.obj.data('color')) {
			                    f.ball.addClass(f.obj.data('color'));
			                    f.progress.addClass(f.obj.data('color'));
			                    f.ball.addClass('text-' + f.obj.data('color'));
			                }
			                f.span = f.ball.find('span');
			                if (!f.span.length) {
			                    f.span = $('<span>');
			                    f.span.addClass('text-' + f.obj.data('text-color'));
			                    f.ball.append(f.span);
			                }
			                var observable = f.obj.data('bind');
			                if (observable) {
			                    f.observable = observable;
			                    var scope = f.obj.voxscope();
			                    f.scope = scope;
			                    f.scope.on('change', function (self$0) {
			                        return function (args) {
			                            if (f.observable.startsWith(args.name)) {
			                                var value = self$0.scopeValue(f.observable);
			                                self$0._changeValue({ value: value });
			                            }
			                        };
			                    }(this));
			                    var value = this.scopeValue(f.observable);
			                    this._changeValue({ value: value });
			                }
			                if (f.obj.data('value'))
			                    this.set(f.obj.data('value'));
			            }
			        });
			        Object.defineProperty(Slider.prototype, 'get', {
			            enumerable: false,
			            value: function () {
			                return this.$value;
			            }
			        });
			        Object.defineProperty(Slider.prototype, 'set', {
			            enumerable: false,
			            value: function (value) {
			                var f = this.$;
			                if (f.scope) {
			                    this.scopeValue(f.observable, value);
			                } else {
			                    this._changeValue({ value: value });
			                }
			            }
			        });
			        Object.defineProperty(Slider.prototype, '_changeValue', {
			            enumerable: false,
			            value: function (ev) {
			                var f = this.$;
			                var discrete = f.obj.hasClass('discrete'), v1 = ev.value;
			                if (body.find(f.obj).length == 0)
			                    return;
			                var percent = ev.value, v = 0;
			                if (ev.value === undefined || ev.value === null)
			                    ev.value = 0;
			                v = Math.max(Math.min(ev.value, this.maxValue), this.minValue);
			                if (isNaN(v))
			                    v = this.minValue;
			                if (v == ev.value && discrete) {
			                    v1 = v;
			                    v = Math.round((v - this.minValue) / this.step) * this.step;
			                    v += this.minValue;
			                    v = Math.max(Math.min(v, this.maxValue), this.minValue);
			                }
			                if (v != ev.value && f.scope) {
			                    return f.scope.observer.assignValue({
			                        name: f.observable,
			                        value: v
			                    });
			                }
			                ev.value = v;
			                if (this.usermode) {
			                    v1 = Math.max(Math.min(v1, this.maxValue), this.minValue);
			                    percent = (v1 - this.minValue) * 100 / (this.maxValue - this.minValue);
			                    this.uservalue = v1;
			                } else {
			                    percent = (ev.value - this.minValue) * 100 / (this.maxValue - this.minValue);
			                }
			                this.$value = ev.value;
			                f.span.text(ev.value);
			                var inverted = f.obj.hasClass('inverted');
			                if (f.obj.hasClass('vertical')) {
			                    f.progress.css('top', !inverted ? 100 - percent + '%' : 0);
			                    f.progress.css('height', percent + '%');
			                    f.ball.css(inverted ? 'top' : 'bottom', percent + '%');
			                } else {
			                    f.progress.css('left', inverted ? 100 - percent + '%' : 0);
			                    f.progress.css('width', percent + '%');
			                    f.ball.css(inverted ? 'right' : 'left', percent + '%');
			                }
			            }
			        });
			        Object.defineProperty(Slider.prototype, 'next', {
			            enumerable: false,
			            value: function () {
			                this.set(this.get() + (this.step || 1));
			            }
			        });
			        Object.defineProperty(Slider.prototype, 'prev', {
			            enumerable: false,
			            value: function () {
			                this.set(this.get() - (this.step || 1));
			            }
			        });
			        Object.defineProperty(Slider.prototype, '_keyNext', {
			            enumerable: false,
			            value: function () {
			                this.clearTimeout();
			                this.next();
			                this.timeout = setTimeout(function (self$0) {
			                    return function () {
			                        self$0.next();
			                        self$0.interval = setInterval(self$0.next.bind(self$0), 100);
			                    };
			                }(this), 1000);
			            }
			        });
			        Object.defineProperty(Slider.prototype, '_keyPrev', {
			            enumerable: false,
			            value: function () {
			                this.clearTimeout();
			                this.prev();
			                this.timeout = setTimeout(function (self$0) {
			                    return function () {
			                        self$0.prev();
			                        self$0.interval = setInterval(self$0.prev.bind(self$0), 10);
			                    };
			                }(this), 1000);
			            }
			        });
			        Object.defineProperty(Slider.prototype, 'clearTimeout', {
			            enumerable: false,
			            value: function () {
			                if (this.timeout) {
			                    clearTimeout(this.timeout);
			                    this.timeout = undefined;
			                }
			                if (this.interval) {
			                    clearTimeout(this.interval);
			                    this.interval = undefined;
			                }
			            }
			        });
			        Object.defineProperty(Slider.prototype, 'mouseEvent', {
			            enumerable: false,
			            value: function (ev) {
			                var f = this.$;
			                var inverted = f.obj.hasClass('inverted');
			                var off = 0, w, pos = f.obj.offset();
			                if (f.obj.hasClass('vertical')) {
			                    off = ev.pageY - pos.top;
			                    if (!inverted)
			                        off = f.obj.outerHeight() - off;
			                    w = f.obj.outerHeight();
			                } else {
			                    off = ev.pageX - pos.left;
			                    if (inverted)
			                        off = f.obj.outerWidth() - off;
			                    w = f.obj.outerWidth();
			                }
			                var dif = this.maxValue - this.minValue;
			                var value = dif * off / w;
			                value += this.minValue;
			                if (f.obj.data('decimals') !== undefined)
			                    value = parseFloat(value.toFixed(f.obj.data('decimals') || 2));
			                else {
			                    value = parseInt(value);
			                }
			                this.set(value);
			            }
			        });
			        Object.defineProperty(Slider.prototype, 'createStyle', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                var transparent;
			                if (!(transparent = Slider.transparent)) {
			                    var $temp = $('<div style="background:none;display:none;"/>').appendTo('body');
			                    transparent = Slider.transparent = $temp.css('backgroundColor');
			                    $temp.remove();
			                }
			                var inherited = function () {
			                    var bc;
			                    $(this).parents().each(function () {
			                        bc = $(this).css('background-color');
			                        if (bc == transparent) {
			                            return inherited.call(this);
			                        } else {
			                            return bc;
			                        }
			                    });
			                    return bc;
			                };
			                var bc = f.obj.css('background-color');
			                if (bc == transparent)
			                    bc = inherited.call(f.obj.get(0));
			                if (this.style)
			                    this.style.remove();
			                if (!bc)
			                    return;
			                this.style = this.style || $('<div>');
			                var content = ['<style>'];
			                content.push('.d-' + this.id + '[disabled] .ball.first{');
			                content.push('border-color:' + bc.toString() + ';');
			                content.push('border-width:0.2em;');
			                content.push('margin:-0.6em;');
			                content.push('border-style:solid;');
			                content.push('}');
			                this.style.html(content.join('\n'));
			                $('body').append(this.style);
			            }
			        });
			        Object.defineProperty(Slider.prototype, 'events', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                Slider.count = Slider.count || 0;
			                this.id = (Date.now() + Slider.count++).toString(32);
			                f.obj.addClass('d-' + this.id);
			                this.createStyle();
			                f.ball.on('focus', function () {
			                    if (f.obj.attr('disabled') !== undefined || f.obj.attr('readonly') !== undefined)
			                        return;
			                    f.obj.addClass('focus');
			                    if (f.obj.hasClass('discrete')) {
			                    }
			                });
			                f.ball.on('blur', function () {
			                    f.obj.removeClass('focus');
			                });
			                f.obj.click(function () {
			                    if (f.obj.attr('disabled') !== undefined || f.obj.attr('readonly') !== undefined)
			                        return;
			                    f.ball.focus();
			                });
			                f.ball.on('keydown', function (self$0) {
			                    return function (ev) {
			                        if (f.obj.attr('disabled') !== undefined || f.obj.attr('readonly') !== undefined)
			                            return;
			                        var vertical = f.obj.hasClass('vertical');
			                        var inverted = f.obj.hasClass('inverted');
			                        if (!vertical && ev.keyCode == 39 || vertical && ev.keyCode == 38) {
			                            ev.preventDefault();
			                            inverted ? self$0._keyPrev() : self$0._keyNext();
			                        } else if (!vertical && ev.keyCode == 37 || vertical && ev.keyCode == 40) {
			                            ev.preventDefault();
			                            !inverted ? self$0._keyPrev() : self$0._keyNext();
			                        }
			                    };
			                }(this));
			                f.obj.on('mousedown', function (self$0) {
			                    return function (ev) {
			                        if (f.obj.attr('disabled') !== undefined || f.obj.attr('readonly') !== undefined)
			                            return;
			                        ev.preventDefault();
			                        self$0.usermode = true;
			                        f.obj.addClass('clic');
			                        f.ball.focus();
			                        self$0.mouseEvent(ev);
			                    };
			                }(this));
			                $(document).on('mouseup', function (self$0) {
			                    return function (ev) {
			                        if (f.obj.attr('disabled') !== undefined || f.obj.attr('readonly') !== undefined)
			                            return;
			                        if (self$0.usermode) {
			                            f.obj.removeClass('clic');
			                            self$0.usermode = false;
			                            self$0.set(self$0.get());
			                        }
			                    };
			                }(this));
			                $(document).on('mousemove', function (self$0) {
			                    return function (ev) {
			                        if (f.obj.attr('disabled') !== undefined || f.obj.attr('readonly') !== undefined)
			                            return;
			                        if (self$0.usermode)
			                            self$0.mouseEvent(ev);
			                    };
			                }(this));
			                f.ball.on('keyup', this.clearTimeout.bind(this));
			            }
			        });
			    }
			    Slider.init();
			    return Slider;
			}
			exports.default = init(w);
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			{
			    var Tab = function Tab() {
			        Tab.$constructor ? Tab.$constructor.apply(this, arguments) : Tab.$superClass && Tab.$superClass.apply(this, arguments);
			    };
			    Tab.prototype = Object.create(Element.prototype);
			    Object.setPrototypeOf ? Object.setPrototypeOf(Tab, Element) : Tab.__proto__ = Element;
			    Tab.prototype.constructor = Tab;
			    Tab.$super = Element.prototype;
			    Tab.$superClass = Element;
			    Object.defineProperty(Tab, '$constructor', {
			        enumerable: false,
			        value: function (obj) {
			            Tab.$superClass.call(this);
			            obj = $(obj);
			            var f = this.$ = {};
			            f.obj = obj;
			            this.obtainProps();
			            this.init();
			        }
			    });
			    Object.defineProperty(Tab.prototype, 'obtainProps', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            f.a = f.obj.find('a');
			        }
			    });
			    Object.defineProperty(Tab.prototype, 'init', {
			        enumerable: false,
			        value: function () {
			            var h = this.href();
			            h && h.hide();
			            this.events();
			        }
			    });
			    Object.defineProperty(Tab.prototype, 'href', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            var href = f.a.attr('href');
			            if (href) {
			                href = $(href);
			                f.href = href;
			            }
			            return f.href;
			        }
			    });
			    Object.defineProperty(Tab.prototype, 'unselect', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            var a0 = f.a;
			            var ev = this.createEvent('beforeunselect');
			            ev.tab = this;
			            ev.jTarget = a0;
			            this.emit(ev);
			            if (ev.defaultPrevented)
			                return false;
			            var ev = this.createEvent('unselect');
			            ev.tab = this;
			            ev.jTarget = a0;
			            this.href().hide();
			            if (f.parent) {
			                f.parent.removeIndicator();
			            }
			            this.emit(ev);
			            if (ev.defaultPrevented)
			                return;
			        }
			    });
			    Object.defineProperty(Tab.prototype, 'select', {
			        enumerable: false,
			        value: function () {
			            console.info('TAB HERE -----');
			            var f = this.$;
			            var a0 = f.a;
			            var ev = this.createEvent('beforeselect');
			            ev.tab = this;
			            ev.jTarget = a0;
			            this.emit(ev);
			            if (ev.defaultPrevented)
			                return false;
			            var ev = this.createEvent('select');
			            ev.tab = this;
			            ev.jTarget = a0;
			            if (f.parent) {
			                if (f.parent.unselect() === false) {
			                    return false;
			                }
			            }
			            this.href().show();
			            if (f.parent)
			                f.parent.addIndicator(this);
			            this.emit(ev);
			        }
			    });
			    Object.defineProperty(Tab.prototype, 'events', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            console.info('AQUI BABE...');
			            f.a.click(function (self$0) {
			                return function (ev) {
			                    if (f.obj.attr('disabled') === undefined && f.a.attr('disabled') === undefined) {
			                        ev.preventDefault();
			                        self$0.select();
			                    }
			                };
			            }(this));
			        }
			    });
			}
			exports.default = Tab;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			var Tab = core.VW.Web.Elements.Tab;
			var doc = {};
			if (typeof document !== 'undefined') {
			    doc = document;
			}
			function init(document) {
			    {
			        var Tabs = function Tabs() {
			            Tabs.$constructor ? Tabs.$constructor.apply(this, arguments) : Tabs.$superClass && Tabs.$superClass.apply(this, arguments);
			        };
			        Tabs.prototype = Object.create(Element.prototype);
			        Object.setPrototypeOf ? Object.setPrototypeOf(Tabs, Element) : Tabs.__proto__ = Element;
			        Tabs.prototype.constructor = Tabs;
			        Tabs.$super = Element.prototype;
			        Tabs.$superClass = Element;
			        Object.defineProperty(Tabs, 'register', {
			            enumerable: false,
			            value: function () {
			                if (this.registered)
			                    return;
			                $.fn.voxtabgroup = function () {
			                    var dp = [];
			                    this.each(function () {
			                        var o = $(this);
			                        var t = undefined;
			                        this.voxcss_element = this.voxcss_element || {};
			                        t = this.voxcss_element['vox-tabgroup'];
			                        if (!t) {
			                            t = new Tabs(o);
			                            this.voxcss_element['vox-tabgroup'] = t;
			                        }
			                        dp.push(t);
			                    });
			                    return dp;
			                };
			                $(function () {
			                    vox.mutation.watchAppend($('body'), function (ev) {
			                        ev.jTarget.voxtabgroup();
			                    }, '.tabs');
			                    $('.tabs').voxtabgroup();
			                });
			                if (this.registered)
			                    return;
			            }
			        });
			        Object.defineProperty(Tabs, '$constructor', {
			            enumerable: false,
			            value: function (obj) {
			                Tabs.$superClass.call(this);
			                obj = $(obj);
			                var f = this.$ = {};
			                f.obj = obj;
			                this.obtainProps();
			                this.init();
			            }
			        });
			        Object.defineProperty(Tabs.prototype, 'obtainProps', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                f.indicator = f.obj.find('.indicator');
			                if (f.indicator.length == 0) {
			                    f.indicator = $('<div>');
			                    f.indicator.addClass('indicator');
			                    f.indicator.addClass('transitioned');
			                }
			                f.indicator.hide();
			            }
			        });
			        Object.defineProperty(Tabs.prototype, 'init', {
			            enumerable: false,
			            value: function () {
			                this.$.tabs = [];
			                this.tabs();
			                this.events();
			            }
			        });
			        Object.defineProperty(Tabs.prototype, 'removeIndicator', {
			            enumerable: false,
			            value: function () {
			                this.$.indicator.hide();
			            }
			        });
			        Object.defineProperty(Tabs.prototype, 'isOpened', {
			            enumerable: false,
			            value: function () {
			                return true;
			            }
			        });
			        Object.defineProperty(Tabs.prototype, 'getTabs', {
			            enumerable: false,
			            value: function () {
			                return this.$.tabs;
			            }
			        });
			        Object.defineProperty(Tabs.prototype, 'tabs', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                var utab = f.obj.find('.tab');
			                console.info('OTABS', utab);
			                var i = 0;
			                var self = this;
			                utab.each(function () {
			                    var jtab = $(this);
			                    if (i == 0) {
			                        jtab.append(f.indicator);
			                    }
			                    var otab = new Tab(jtab);
			                    jtab.attr('vox-index', i);
			                    otab.$.index = i;
			                    otab.$.parent = self;
			                    i++;
			                    f.tabs.push(otab);
			                });
			            }
			        });
			        Object.defineProperty(Tabs.prototype, 'addIndicator', {
			            enumerable: false,
			            value: function (tab) {
			                var f = this.$;
			                var o = f.lastTab;
			                f.selectedTab = tab;
			                f.lastTab = tab;
			                var obj = tab.$.obj;
			                var left = obj.position().left;
			                f.indicator.show();
			                if (f.tabs[0]) {
			                    var nl$3 = 0;
			                    if (o)
			                        nl$3 = o.$.obj.position().left;
			                    nl$3 = nl$3.toString() + 'px';
			                    f.indicator.css('left', nl$3);
			                    f.tabs[0].$.obj.append(f.indicator);
			                }
			                f.indicator.css('width', obj.outerWidth());
			                f.indicator.voxtransition({ left: left.toString() + 'px' }, undefined, 1000, function () {
			                    f.indicator.css('left', 0);
			                    f.indicator.css('width', '100%');
			                    obj.append(f.indicator);
			                });
			            }
			        });
			        Object.defineProperty(Tabs.prototype, 'unselect', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                if (f.selectedTab) {
			                    if (f.selectedTab.unselect() !== false) {
			                        f.selectedTab = undefined;
			                    }
			                }
			            }
			        });
			        Object.defineProperty(Tabs.prototype, 'events', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                vox.platform.attachOuterClick(f.obj, {
			                    active: function (self$0) {
			                        return function () {
			                            return self$0.isOpened();
			                        };
			                    }(this),
			                    processEvent: function (self$0) {
			                        return function (ev) {
			                            var ev2 = self$0.createEvent('outerclick', ev);
			                            ev2.tabs = self$0;
			                            ev2.target = ev.target;
			                            ev2.clickEvent = ev;
			                            return ev2;
			                        };
			                    }(this),
			                    self: this,
			                    callback: function (self$0) {
			                        return function (ev) {
			                            self$0.emit(ev);
			                            if (ev.defaultPrevented)
			                                return;
			                        };
			                    }(this)
			                });
			            }
			        });
			    }
			    return Tabs;
			}
			exports.default = init(doc);
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var colors = {
			    'white': { 'default': '#ffffff' },
			    'black': { 'default': '#000000' },
			    'red': {
			        'default': '#F44336',
			        'lighten1': '#ef5350',
			        'lighten2': '#e57373',
			        'lighten3': '#ef9a9a',
			        'lighten4': '#ffcdd2',
			        'lighten5': '#ffebee',
			        'darken1': '#e53935',
			        'darken2': '#d32f2f',
			        'darken3': '#c62828',
			        'darken4': '#b71c1c',
			        'accent1': '#ff8a80',
			        'accent2': '#ff5252',
			        'accent3': '#ff1744',
			        'accent4': '#d50000'
			    },
			    'pink': {
			        'default': '#E91E63',
			        'lighten1': '#ec407a',
			        'lighten2': '#f06292',
			        'lighten3': '#f48fb1',
			        'lighten4': '#f8bbd0',
			        'lighten5': '#fce4ec',
			        'darken1': '#d81b60',
			        'darken2': '#c2185b',
			        'darken3': '#ad1457',
			        'darken4': '#880e4f',
			        'accent1': '#ff80ab',
			        'accent2': '#ff4081',
			        'accent3': '#f50057',
			        'accent4': '#c51162'
			    },
			    'purple': {
			        'default': '#9c27b0',
			        'lighten1': '#ab47bc',
			        'lighten2': '#ba68c8',
			        'lighten3': '#ce93d8',
			        'lighten4': '#e1bee7',
			        'lighten5': '#f3e5f5',
			        'darken1': '#8e24aa',
			        'darken2': '#7b1fa2',
			        'darken3': '#6a1b9a',
			        'darken4': '#4a148c',
			        'accent1': '#ea80fc',
			        'accent2': '#e040fb',
			        'accent3': '#d500f9',
			        'accent4': '#aa00ff'
			    },
			    'deeppurple': {
			        'default': '#673ab7',
			        'lighten1': '#7e57c2',
			        'lighten2': '#9575cd',
			        'lighten3': '#b39ddb',
			        'lighten4': '#d1c4e9',
			        'lighten5': '#ede7f6',
			        'darken1': '#5e35b1',
			        'darken2': '#512da8',
			        'darken3': '#4527a0',
			        'darken4': '#311b92',
			        'accent1': '#b388ff',
			        'accent2': '#7c4dff',
			        'accent3': '#651fff',
			        'accent4': '#6200ea'
			    },
			    'indigo': {
			        'default': '#3F51b5',
			        'lighten1': '#5c6bc0',
			        'lighten2': '#7986cb',
			        'lighten3': '#9fa8da',
			        'lighten4': '#c5cae9',
			        'lighten5': '#e8eaf6',
			        'darken1': '#3949ab',
			        'darken2': '#303f9f',
			        'darken3': '#283593',
			        'darken4': '#1a237e',
			        'accent1': '#8c9eff',
			        'accent2': '#536dfe',
			        'accent3': '#3d5afe',
			        'accent4': '#304ffe'
			    },
			    'blue': {
			        'default': '#2196F3',
			        'lighten1': '#42a5f5',
			        'lighten2': '#64b5f6',
			        'lighten3': '#90caf9',
			        'lighten4': '#bbdefb',
			        'lighten5': '#e3f2fd',
			        'darken1': '#1e88e5',
			        'darken2': '#1976d2',
			        'darken3': '#1565c0',
			        'darken4': '#0d47a1',
			        'accent1': '#82b1ff',
			        'accent2': '#448aff',
			        'accent3': '#2979ff',
			        'accent4': '#2962ff'
			    },
			    'lightblue': {
			        'default': '#03A9F4',
			        'lighten1': '#29b6f6',
			        'lighten2': '#4fc3f7',
			        'lighten3': '#81d4fa',
			        'lighten4': '#b3e5fc',
			        'lighten5': '#e1f5fe',
			        'darken1': '#039be5',
			        'darken2': '#0288d1',
			        'darken3': '#0277bd',
			        'darken4': '#01579b',
			        'accent1': '#80d8ff',
			        'accent2': '#40c4ff',
			        'accent3': '#00b0ff',
			        'accent4': '#0091ea'
			    },
			    'cyan': {
			        'default': '#00BCD4',
			        'lighten1': '#26c6da',
			        'lighten2': '#4dd0e1',
			        'lighten3': '#80deea',
			        'lighten4': '#b2ebf2',
			        'lighten5': '#e0f7fa',
			        'darken1': '#00acc1',
			        'darken2': '#0097a7',
			        'darken3': '#00838f',
			        'darken4': '#006064',
			        'accent1': '#84ffff',
			        'accent2': '#18ffff',
			        'accent3': '#00e5ff',
			        'accent4': '#00b8d4'
			    },
			    'teal': {
			        'default': '#009688',
			        'lighten1': '#26a69a',
			        'lighten2': '#4db6ac',
			        'lighten3': '#80cbc4',
			        'lighten4': '#b2dfdb',
			        'lighten5': '#e0f2f1',
			        'darken1': '#00897b',
			        'darken2': '#00796b',
			        'darken3': '#00695c',
			        'darken4': '#004d40',
			        'accent1': '#a7ffeb',
			        'accent2': '#64ffda',
			        'accent3': '#1de9b6',
			        'accent4': '#00bfa5'
			    },
			    'green': {
			        'default': '#4caf50',
			        'lighten1': '#66bb6a',
			        'lighten2': '#81c784',
			        'lighten3': '#a5d6a7',
			        'lighten4': '#c8e6c9',
			        'lighten5': '#e8f5e9',
			        'darken1': '#43a047',
			        'darken2': '#388e3c',
			        'darken3': '#2e7d32',
			        'darken4': '#1b5e20',
			        'accent1': '#b9f6ca',
			        'accent2': '#69f0ae',
			        'accent3': '#00e676',
			        'accent4': '#00c853'
			    },
			    'lightgreen': {
			        'default': '#8bc34a',
			        'lighten1': '#9ccc65',
			        'lighten2': '#aed581',
			        'lighten3': '#c5e1a5',
			        'lighten4': '#dcedc8',
			        'lighten5': '#f1f8e9',
			        'darken1': '#7cb342',
			        'darken2': '#689f38',
			        'darken3': '#558b2f',
			        'darken4': '#33691e',
			        'accent1': '#ccff90',
			        'accent2': '#b2ff59',
			        'accent3': '#76ff03',
			        'accent4': '#64dd17'
			    },
			    'lime': {
			        'default': '#cddc39',
			        'lighten1': '#d4e157',
			        'lighten2': '#dce775',
			        'lighten3': '#e6ee9c',
			        'lighten4': '#f0f4c3',
			        'lighten5': '#f9fbe7',
			        'darken1': '#c0ca33',
			        'darken2': '#afb42b',
			        'darken3': '#9e9d24',
			        'darken4': '#827717',
			        'accent1': '#f4ff81',
			        'accent2': '#eeff41',
			        'accent3': '#c6ff00',
			        'accent4': '#aeea00'
			    },
			    'yellow': {
			        'default': '#ffeb3b',
			        'lighten1': '#ffee58',
			        'lighten2': '#fff176',
			        'lighten3': '#fff59d',
			        'lighten4': '#fff9c4',
			        'lighten5': '#fffde7',
			        'darken1': '#fdd835',
			        'darken2': '#fbc02d',
			        'darken3': '#f9a825',
			        'darken4': '#f57f17',
			        'accent1': '#ffff8d',
			        'accent2': '#ffff00',
			        'accent3': '#ffea00',
			        'accent4': '#ffd600'
			    },
			    'amber': {
			        'default': '#FFC107',
			        'lighten1': '#ffca28',
			        'lighten2': '#ffd54f',
			        'lighten3': '#ffe082',
			        'lighten4': '#ffecb3',
			        'lighten5': '#fff8e1',
			        'darken1': '#ffb300',
			        'darken2': '#ffa000',
			        'darken3': '#ff8f00',
			        'darken4': '#ff6f00',
			        'accent1': '#ffe57f',
			        'accent2': '#ffd740',
			        'accent3': '#ffc400',
			        'accent4': '#ffab00'
			    },
			    'orange': {
			        'default': '#FF9800',
			        'lighten1': '#ffa726',
			        'lighten2': '#ffb74d',
			        'lighten3': '#ffcc80',
			        'lighten4': '#ffe0b2',
			        'lighten5': '#fff3e0',
			        'darken1': '#fb8c00',
			        'darken2': '#f57c00',
			        'darken3': '#ef6c00',
			        'darken4': '#e65100',
			        'accent1': '#ffd180',
			        'accent2': '#ffab40',
			        'accent3': '#ff9100',
			        'accent4': '#ff6d00'
			    },
			    'deeporange': {
			        'default': '#ff5722',
			        'lighten1': '#ff7043',
			        'lighten2': '#ff8a65',
			        'lighten3': '#ffab91',
			        'lighten4': '#ffccbc',
			        'lighten5': '#fbe9e7',
			        'darken1': '#f4511e',
			        'darken2': '#e64a19',
			        'darken3': '#d84315',
			        'darken4': '#bf360c',
			        'accent1': '#ff9e80',
			        'accent2': '#ff6e40',
			        'accent3': '#ff3d00',
			        'accent4': '#dd2c00'
			    },
			    'brown': {
			        'default': '#795548',
			        'lighten1': '#8d6e63',
			        'lighten2': '#a1887f',
			        'lighten3': '#bcaaa4',
			        'lighten4': '#d7ccc8',
			        'lighten5': '#efebe9',
			        'darken1': '#6d4c41',
			        'darken2': '#5d4037',
			        'darken3': '#4e342e',
			        'darken4': '#3e2723'
			    },
			    'gray': {
			        'default': '#9e9e9e',
			        'lighten1': '#bdbdbd',
			        'lighten2': '#e0e0e0',
			        'lighten3': '#eeeeee',
			        'lighten4': '#f5f5f5',
			        'lighten5': '#fafafa',
			        'darken1': '#757575',
			        'darken2': '#616161',
			        'darken3': '#424242',
			        'darken4': '#212121'
			    },
			    'bluegray': {
			        'default': '#607d8b',
			        'lighten1': '#78909c',
			        'lighten2': '#90a4ae',
			        'lighten3': '#b0bec5',
			        'lighten4': '#cfd8dc',
			        'lighten5': '#eceff1',
			        'darken1': '#546e7a',
			        'darken2': '#455a64',
			        'darken3': '#37474f',
			        'darken4': '#263238'
			    }
			};
			var Id = 0;
			{
			    var Theme = function Theme() {
			        Theme.$constructor ? Theme.$constructor.apply(this, arguments) : Theme.$superClass && Theme.$superClass.apply(this, arguments);
			    };
			    Object.defineProperty(Theme, '$constructor', {
			        enumerable: false,
			        value: function () {
			            Theme.current = this;
			        }
			    });
			    Theme.__defineGetter__('colors', function () {
			        return colors;
			    });
			    Object.defineProperty(Theme, 'createClassFromColor', {
			        enumerable: false,
			        value: function (options) {
			            var str = [];
			            var className = options.className;
			            var intention = options.intention || 'default';
			            var color = colors[options.color];
			            color = color[intention];
			            var prefix = '';
			            var sufix = ':not(.active)';
			            str.push('' + prefix + '' + className + '-active.active' + sufix + ', ' + prefix + '.' + className + '' + sufix + ', ' + prefix + '.' + className + '-hover' + sufix + ':not([disabled]):hover, ' + prefix + '.' + className + '-hover[hover-active]' + sufix + ':not([disabled])');
			            str.push('{');
			            str.push((!options.text ? 'background-' : '') + 'color: ' + color + ';');
			            str.push('}');
			            if (!options.text) {
			                className = 'text-' + className;
			                str.push('' + prefix + '' + className + '-active.active' + sufix + ', ' + prefix + '.' + className + '' + sufix + ', ' + prefix + '.' + className + '-hover' + sufix + ':not([disabled]):hover, ' + prefix + '.' + className + '-hover[hover-active]' + sufix + ':not([disabled])');
			                str.push('{');
			                str.push('color: ' + color + ';');
			                str.push('}');
			            }
			            var style = $('<style>');
			            style.html(str.join('\n'));
			            style.addClass('voxcss-theme');
			            style.attr('id', 'vox-color-themed-' + ++Id);
			            $('head').append(style);
			        }
			    });
			    Object.defineProperty(Theme, 'clear', {
			        enumerable: false,
			        value: function () {
			            $('.voxcss-theme').remove();
			            return this;
			        }
			    });
			    Object.defineProperty(Theme, 'definePalette', {
			        enumerable: false,
			        value: function (color) {
			            var c = {}, names = [
			                    'default',
			                    'lighten5',
			                    'lighten4',
			                    'lighten3',
			                    'lighten2',
			                    'lighten1',
			                    'darken4',
			                    'darken3',
			                    'darken2',
			                    'darken1'
			                ];
			            if (!color.name)
			                throw new core.System.Exception('Debe especificar el nombre del color');
			            for (var i = 0; i < names.length; i++) {
			                c[names[i]] = color[names[i]];
			                if (!c[names[i]])
			                    throw new core.System.Exception('Debe especificar la intención para ' + names[i]);
			            }
			            colors[color.name] = c;
			        }
			    });
			    Object.defineProperty(Theme, 'create', {
			        enumerable: false,
			        value: function () {
			            return new Theme();
			        }
			    });
			    Theme.__defineGetter__('defaultOptions', function () {
			        return {
			            'primaryPalette': {
			                'default': 'teal:default',
			                'hue-1': 'teal:lighten2',
			                'hue-2': 'teal:darken3',
			                'hue-3': 'teal:accent1',
			                'hue-4': 'teal:accent2',
			                'text': 'white:default',
			                'text-2': 'teal:darken-4',
			                'text-1': 'white:default',
			                'text-3': 'white:default',
			                'text-4': 'white:default'
			            },
			            'accentPalette': {
			                'default': 'pink:default',
			                'hue-1': 'pink:lighten2',
			                'hue-2': 'pink:darken3',
			                'hue-3': 'pink:accent1',
			                'hue-4': 'pink:accent2',
			                'text': 'white:default',
			                'text-1': 'red:default',
			                'text-2': 'white:default',
			                'text-3': 'white:default',
			                'text-4': 'white:default'
			            },
			            'warnPalette': {
			                'default': 'red:default',
			                'hue-1': 'red:lighten2',
			                'hue-2': 'deeporange:darken3',
			                'hue-3': 'red:accent1',
			                'hue-4': 'red:accent2',
			                'text': 'white:default',
			                'text-2': 'red:darken-4',
			                'text-1': 'white:default',
			                'text-3': 'white:default',
			                'text-4': 'white:default'
			            },
			            'backgroundPalette': {
			                'default': 'gray:lighten2',
			                'hue-1': 'white:default',
			                'hue-2': 'gray:darken2',
			                'hue-3': 'gray:darken3',
			                'hue-4': 'gray:darken3',
			                'text': 'black:default',
			                'text-1': 'gray:darken-3',
			                'text-2': 'white:default',
			                'text-3': 'white:default',
			                'text-4': 'white:default'
			            }
			        };
			    });
			    Object.defineProperty(Theme, 'default', {
			        enumerable: false,
			        value: function () {
			            var t = new Theme();
			            t.primaryPalette().accentPalette().warnPalette().backgroundPalette();
			            return t;
			        }
			    });
			    Object.defineProperty(Theme.prototype, 'primaryPalette', {
			        enumerable: false,
			        value: function (options) {
			            var defaultOptions = Theme.defaultOptions, op;
			            options = options || {};
			            options.color = options.color || defaultOptions.primaryPalette.color;
			            for (var id in defaultOptions.primaryPalette) {
			                if (id != 'color') {
			                    if (!options[id])
			                        options[id] = defaultOptions.primaryPalette[id];
			                    op = options[id].split(':');
			                    Theme.createClassFromColor({
			                        'className': 'color-' + id,
			                        'intention': op[1],
			                        'color': op[0],
			                        'text': id.startsWith('text')
			                    });
			                }
			            }
			            return this;
			        }
			    });
			    Object.defineProperty(Theme.prototype, 'accentPalette', {
			        enumerable: false,
			        value: function (options) {
			            var defaultOptions = Theme.defaultOptions, op;
			            options = options || {};
			            options.color = options.color || defaultOptions.accentPalette.color;
			            for (var id in defaultOptions.accentPalette) {
			                if (id != 'color') {
			                    if (!options[id])
			                        options[id] = defaultOptions.accentPalette[id];
			                    op = options[id].split(':');
			                    Theme.createClassFromColor({
			                        'className': 'color-accent-' + id,
			                        'intention': op[1],
			                        'color': op[0],
			                        'text': id.startsWith('text')
			                    });
			                }
			            }
			            return this;
			        }
			    });
			    Object.defineProperty(Theme.prototype, 'warnPalette', {
			        enumerable: false,
			        value: function (options) {
			            var defaultOptions = Theme.defaultOptions, op;
			            options = options || {};
			            options.color = options.color || defaultOptions.warnPalette.color;
			            for (var id in defaultOptions.warnPalette) {
			                if (id != 'color') {
			                    if (!options[id])
			                        options[id] = defaultOptions.warnPalette[id];
			                    op = options[id].split(':');
			                    Theme.createClassFromColor({
			                        'className': 'color-warning-' + id,
			                        'intention': op[1],
			                        'color': op[0],
			                        'text': id.startsWith('text')
			                    });
			                }
			            }
			            return this;
			        }
			    });
			    Object.defineProperty(Theme.prototype, 'backgroundPalette', {
			        enumerable: false,
			        value: function (options) {
			            var defaultOptions = Theme.defaultOptions, op;
			            options = options || {};
			            options.color = options.color || defaultOptions.backgroundPalette.color;
			            for (var id in defaultOptions.backgroundPalette) {
			                if (id != 'color') {
			                    if (!options[id])
			                        options[id] = defaultOptions.backgroundPalette[id];
			                    op = options[id].split(':');
			                    Theme.createClassFromColor({
			                        'className': 'color-back-' + id,
			                        'intention': op[1],
			                        'color': op[0],
			                        'text': id.startsWith('text')
			                    });
			                }
			            }
			            return this;
			        }
			    });
			}
			exports.default = Theme;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			{
			    var Toast = function Toast() {
			        Toast.$constructor ? Toast.$constructor.apply(this, arguments) : Toast.$superClass && Toast.$superClass.apply(this, arguments);
			    };
			    Toast.prototype = Object.create(Element.prototype);
			    Object.setPrototypeOf ? Object.setPrototypeOf(Toast, Element) : Toast.__proto__ = Element;
			    Toast.prototype.constructor = Toast;
			    Toast.$super = Element.prototype;
			    Toast.$superClass = Element;
			    Object.defineProperty(Toast, 'init', {
			        enumerable: false,
			        value: function () {
			            if (!Toast.container) {
			                Toast.container = $('.toast-container');
			                if (Toast.container.length == 0) {
			                    Toast.container = $('<div>');
			                    Toast.container.addClass('toast-container');
			                    Toast.container.addClass('flow-text');
			                    $('body').append(Toast.container);
			                }
			            }
			        }
			    });
			    Object.defineProperty(Toast, 'register', {
			        enumerable: false,
			        value: function () {
			            if (this.registered)
			                return;
			            Toast.init();
			            $.fn.voxtoast = function () {
			                var dp = [];
			                this.each(function () {
			                    var o = $(this);
			                    var t = undefined;
			                    this.voxcss_element = this.voxcss_element || {};
			                    t = this.voxcss_element['vox-toast'];
			                    if (!t) {
			                        t = new Toast(o);
			                        this.voxcss_element['vox-toast'] = t;
			                    }
			                    dp.push(t);
			                });
			                return dp;
			            };
			            $(function () {
			                vox.mutation.watchAppend($('body'), function (ev) {
			                    if (ev.moved == false) {
			                        ev.jTarget.voxtoast();
			                    }
			                }, '.toast');
			                $('.toast').voxtoast();
			                $('[data-toggle=toast]').click(function () {
			                    var e = $(this);
			                    var s = e.attr('vox-selector');
			                    var g = $(s).eq(0);
			                    var h = g.voxtoast()[0];
			                    if (h) {
			                        h.open();
			                    }
			                });
			            });
			            this.registered = true;
			        }
			    });
			    Object.defineProperty(Toast, '$constructor', {
			        enumerable: false,
			        value: function (obj) {
			            Toast.$superClass.call(this);
			            obj = $(obj);
			            var f = this.$ = {};
			            f.obj = obj;
			            this.obtainProps();
			            this.init();
			        }
			    });
			    Object.defineProperty(Toast.prototype, 'obtainProps', {
			        enumerable: false,
			        value: function () {
			        }
			    });
			    Object.defineProperty(Toast.prototype, 'init', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            f.obj.removeClass('toast');
			            Toast.container.append(f.obj);
			            f.obj.addClass('toast');
			            this.events();
			        }
			    });
			    Object.defineProperty(Toast.prototype, 'isOpened', {
			        enumerable: false,
			        value: function () {
			            return this.$.obj.hasClass('opened');
			        }
			    });
			    Object.defineProperty(Toast.prototype, 'open', {
			        enumerable: false,
			        value: function (event) {
			            var f = this.$;
			            var ev = this.createEvent('beforeopen', event);
			            ev.toast = this;
			            this.emit(ev);
			            if (ev.defaultPrevented)
			                return;
			            if (f.delay) {
			                clearTimeout(f.delay);
			                f.delay = undefined;
			            }
			            f.lEvent = event ? event.type : '';
			            f.obj.addClass('opened');
			            f.obj.show();
			            var ev = this.createEvent('open', event);
			            ev.toast = this;
			            this.emit(ev);
			            if (ev.defaultPrevented)
			                return;
			            var time = parseInt(f.obj.data('delay'));
			            if (isNaN(time) || !time) {
			                time = 1000;
			            }
			            f.delay = setTimeout(function (self$0) {
			                return function () {
			                    return self$0.close();
			                };
			            }(this), time);
			        }
			    });
			    Object.defineProperty(Toast.prototype, 'close', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            var ev = this.createEvent('beforeclose');
			            ev.toast = this;
			            this.emit(ev);
			            if (ev.defaultPrevented)
			                return;
			            f.lEvent = undefined;
			            f.obj.removeClass('opened');
			            f.obj.hide();
			            var ev = vox.platform.createEvent('close');
			            ev.toast = this;
			            this.emit(ev);
			        }
			    });
			    Object.defineProperty(Toast.prototype, 'toggle', {
			        enumerable: false,
			        value: function () {
			            this.isOpened() ? this.close() : this.open();
			        }
			    });
			    Object.defineProperty(Toast.prototype, 'events', {
			        enumerable: false,
			        value: function () {
			            var f = this.$;
			            vox.platform.attachOuterClick(f.obj, {
			                active: function (self$0) {
			                    return function () {
			                        return self$0.isOpened();
			                    };
			                }(this),
			                processEvent: function (self$0) {
			                    return function (ev) {
			                        var ev2 = self$0.createEvent('outerclick', ev);
			                        ev2.toast = self$0;
			                        ev2.target = ev.target;
			                        ev2.clickEvent = ev;
			                        return ev2;
			                    };
			                }(this),
			                self: this,
			                callback: function (self$0) {
			                    return function (ev) {
			                        self$0.emit(ev);
			                    };
			                }(this)
			            });
			        }
			    });
			}
			exports.default = Toast;
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var Element = require('./Element').default;
			var $ = core.VW.Web.JQuery;
			var vox = core.VW.Web.Vox;
			var w = {};
			if (typeof window !== 'undefined')
			    w = window;
			function init(window) {
			    {
			        var Tooltip = function Tooltip() {
			            Tooltip.$constructor ? Tooltip.$constructor.apply(this, arguments) : Tooltip.$superClass && Tooltip.$superClass.apply(this, arguments);
			        };
			        Tooltip.prototype = Object.create(Element.prototype);
			        Object.setPrototypeOf ? Object.setPrototypeOf(Tooltip, Element) : Tooltip.__proto__ = Element;
			        Tooltip.prototype.constructor = Tooltip;
			        Tooltip.$super = Element.prototype;
			        Tooltip.$superClass = Element;
			        Object.defineProperty(Tooltip, 'register', {
			            enumerable: false,
			            value: function () {
			                if (this.registered)
			                    return;
			                $.fn.voxtooltip = function () {
			                    var dp = [];
			                    this.each(function () {
			                        var o = $(this);
			                        var t = undefined;
			                        this.voxcss_element = this.voxcss_element || {};
			                        t = this.voxcss_element['vox-tooltip'];
			                        if (!t) {
			                            t = new Tooltip(o);
			                            this.voxcss_element['vox-tooltip'] = t;
			                        }
			                        dp.push(t);
			                    });
			                    return dp;
			                };
			                vox.mutation.watchAppend($('body'), function (ev) {
			                    ev.jTarget.voxtooltip();
			                }, '.tooltip');
			                $('.tooltip').voxtooltip();
			                this.registered = true;
			            }
			        });
			        Object.defineProperty(Tooltip, '$constructor', {
			            enumerable: false,
			            value: function (obj) {
			                Tooltip.$superClass.call(this);
			                obj = $(obj);
			                var f = this.$ = {};
			                f.obj = obj;
			                this.obtainProps();
			                this.init();
			            }
			        });
			        Tooltip.prototype.__defineGetter__('html', function () {
			            return this.$.obj.html();
			        });
			        Tooltip.prototype.__defineSetter__('html', function (value) {
			            this.$.obj.html(value);
			        });
			        Tooltip.prototype.__defineGetter__('text', function () {
			            return this.$.obj.text();
			        });
			        Tooltip.prototype.__defineSetter__('text', function (value) {
			            this.$.obj.text(value);
			        });
			        Object.defineProperty(Tooltip.prototype, 'obtainProps', {
			            enumerable: false,
			            value: function () {
			            }
			        });
			        Object.defineProperty(Tooltip.prototype, 'init', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                this.events();
			            }
			        });
			        Object.defineProperty(Tooltip.prototype, 'isOpened', {
			            enumerable: false,
			            value: function () {
			                return this.$.obj.hasClass('opened');
			            }
			        });
			        Object.defineProperty(Tooltip.prototype, 'activate', {
			            enumerable: false,
			            value: function (parent) {
			                var f = this.$;
			                if (f.activating2) {
			                    clearTimeout(f.activating2);
			                    f.activating2 = undefined;
			                }
			                if (f.activating) {
			                    clearTimeout(f.activating);
			                    f.activating = undefined;
			                }
			                var time = f.obj.data('delay');
			                if (isNaN(time) || !time)
			                    time = 500;
			                if (parent)
			                    f.lParent = parent;
			                f.activating = setTimeout(function (self$0) {
			                    return function () {
			                        return self$0.open();
			                    };
			                }(this), time);
			            }
			        });
			        Object.defineProperty(Tooltip.prototype, 'acomode', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                f.obj.addClass('activating');
			                var task = new core.VW.Task();
			                setTimeout(function () {
			                    var h = f.obj.outerHeight();
			                    var hg = $(window).height();
			                    var w = f.obj.outerWidth();
			                    var hw = $(window).width();
			                    var l = (hw - w) / 2;
			                    f.obj.css('top', 0);
			                    var f_abs = f.obj.offset().top;
			                    var lOff = 0, lFixed = 0, lh = 0, lw = 0, lLeft = 0;
			                    var maxHeight, top, bottom;
			                    if (f.lParent) {
			                        lw = f.lParent.outerWidth();
			                        lOff = f.lParent.offset().top;
			                        lLeft = f.lParent.offset().left;
			                        lFixed = lOff - f_abs;
			                        lh = f.lParent.outerHeight();
			                    }
			                    if (lFixed > hg / 2) {
			                        maxHeight = lFixed - 20;
			                        if (maxHeight < 30)
			                            maxHeight = 'auto';
			                        else
			                            maxHeight = maxHeight.toString() + 'px';
			                        top = 'initial';
			                        bottom = hg - lOff + 4 + 'px';
			                    } else {
			                        top = lOff + lh + 4;
			                        maxHeight = hg - top;
			                        if (maxHeight < 30)
			                            maxHeight = 'auto';
			                        else
			                            maxHeight = maxHeight.toString() + 'px';
			                        bottom = 'initial';
			                        top = top.toString() + 'px';
			                    }
			                    l = lLeft + lw / 2 - w / 2;
			                    if (l < 0)
			                        l = 0;
			                    f.obj.css('left', l + 'px');
			                    f.obj.css('max-height', maxHeight);
			                    f.obj.css('top', top);
			                    f.obj.css('bottom', bottom);
			                    f.obj.removeClass('activating');
			                    task.finish();
			                }, 0);
			                return task;
			            }
			        });
			        Object.defineProperty(Tooltip.prototype, 'open', {
			            enumerable: false,
			            value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(event) {
			                var f, ev, effect;
			                return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
			                    while (1)
			                        switch (context$1$0.prev = context$1$0.next) {
			                        case 0:
			                            f = this.$;
			                            if (!this.isOpened()) {
			                                context$1$0.next = 3;
			                                break;
			                            }
			                            return context$1$0.abrupt('return');
			                        case 3:
			                            f.activating = undefined;
			                            ev = this.createEvent('beforeopen', event);
			                            ev.tooltip = this;
			                            this.emit(ev);
			                            if (!ev.defaultPrevented) {
			                                context$1$0.next = 9;
			                                break;
			                            }
			                            return context$1$0.abrupt('return');
			                        case 9:
			                            if (f.delay) {
			                                clearTimeout(f.delay);
			                                f.delay = undefined;
			                            }
			                            context$1$0.next = 12;
			                            return regeneratorRuntime.awrap(this.acomode());
			                        case 12:
			                            f.lEvent = event ? event.type : '';
			                            effect = f.obj.data('ineffect') || 'fadeIn short';
			                            f.obj.addClass('opened');
			                            f.obj.voxanimate(effect, undefined, function (self$0) {
			                                return function () {
			                                    var ev = self$0.createEvent('open', event);
			                                    ev.tooltip = self$0;
			                                    self$0.emit(ev);
			                                };
			                            }(this));
			                        case 16:
			                        case 'end':
			                            return context$1$0.stop();
			                        }
			                }, null, this);
			            })
			        });
			        Object.defineProperty(Tooltip.prototype, 'close', {
			            enumerable: false,
			            value: function () {
			                if (!this.isOpened())
			                    return;
			                var f = this.$;
			                var ev = this.createEvent('beforeclose');
			                ev.tooltip = this;
			                this.emit(ev);
			                if (ev.defaultPrevented)
			                    return;
			                f.lEvent = undefined;
			                f.obj.removeClass('opened');
			                var effect = f.obj.data('outeffect') || 'fadeOut short';
			                f.obj.voxanimate(effect, undefined, function (self$0) {
			                    return function () {
			                        var ev = self$0.createEvent('close');
			                        ev.tooltip = self$0;
			                        self$0.emit(ev);
			                    };
			                }(this));
			            }
			        });
			        Object.defineProperty(Tooltip.prototype, 'toggle', {
			            enumerable: false,
			            value: function () {
			                this.isOpened() ? this.close() : this.open();
			            }
			        });
			        Object.defineProperty(Tooltip.prototype, 'activateClose', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                if (f.activating) {
			                    clearTimeout(f.activating);
			                    f.activating = undefined;
			                }
			                if (f.activating2) {
			                    clearTimeout(f.activating2);
			                    f.activating2 = undefined;
			                }
			                var time = f.obj.data('delay');
			                if (isNaN(time) || !time)
			                    time = 500;
			                f.activating2 = setTimeout(function (self$0) {
			                    return function () {
			                        return self$0.close();
			                    };
			                }(this), time);
			            }
			        });
			        Object.defineProperty(Tooltip.prototype, 'events', {
			            enumerable: false,
			            value: function () {
			                var f = this.$;
			                f.obj.hover(function (self$0) {
			                    return function (ev) {
			                        if (ev.type == 'mouseenter')
			                            self$0.activate();
			                        else if (ev.type = 'mouseleave')
			                            self$0.activateClose();
			                    };
			                }(this));
			                vox.platform.attachOuterClick(f.obj, {
			                    active: function (self$0) {
			                        return function () {
			                            return self$0.isOpened();
			                        };
			                    }(this),
			                    processEvent: function (self$0) {
			                        return function (ev) {
			                            var ev2 = self$0.createEvent('outerclick', ev);
			                            ev2.tooltip = self$0;
			                            ev2.target = ev.target;
			                            ev2.clickEvent = ev;
			                            return ev2;
			                        };
			                    }(this),
			                    self: this,
			                    callback: function (self$0) {
			                        return function (ev) {
			                            self$0.emit(ev);
			                            if (ev.defaultPrevented)
			                                return;
			                            self$0.close();
			                        };
			                    }(this)
			                });
			            }
			        });
			    }
			    return Tooltip;
			}
			exports.default = init(w);
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
			var e = core.VW.Web.Elements = core.VW.Web.Elements || {};
			exports = module.exports = core;
			e.get_Element = function () {
			    return require('./Element').default;
			};
			e.get_Card = function () {
			    return require('./Card').default;
			};
			e.get_Dropdown = function () {
			    return require('./Dropdown').default;
			};
			var Input = require('./Input');
			e.get_Input = function () {
			    return Input.default;
			};
			e.get_mask = function () {
			    return Input.mask;
			};
			e.get_Anchor = function () {
			    return require('./Anchor').default;
			};
			e.get_Modal = function () {
			    return require('./Modal').default;
			};
			e.get_ScrollFire = function () {
			    return require('./ScrollFire').default;
			};
			e.get_Parallax = function () {
			    return require('./Parallax').default;
			};
			e.get_Pinned = function () {
			    return require('./Pinned').default;
			};
			e.get_SideNav = function () {
			    return require('./SideNav').default;
			};
			e.get_Tab = function () {
			    return require('./Tab').default;
			};
			e.get_TabGroup = function () {
			    return require('./Tabs').default;
			};
			e.get_Toast = function () {
			    return require('./Toast').default;
			};
			e.get_Tooltip = function () {
			    return require('./Tooltip').default;
			};
			e.get_HasTooltip = function () {
			    return require('./HasTooltip').default;
			};
			e.get_Elastic = function () {
			    return require('./Elastic').default;
			};
			e.get_Slider = function () {
			    return require('./Slider').default;
			};
			e.get_Theme = function () {
			    return require('./Theme').default;
			};
			core.VW.Util.createProperties(e);
			e.register = function () {
			    var jqueryCleanData = jQuery.cleanData;
			    jQuery.cleanData = function (elems) {
			        var g = function (self) {
			            var ev, eves;
			            if (self.voxcss_element) {
			                for (var id in self.voxcss_element) {
			                    ev = self.voxcss_element[id];
			                    if (ev) {
			                        try {
			                            if (ev.dynamicDispose) {
			                                ev.dynamicDispose();
			                            }
			                        } catch (e) {
			                            console.error('Error in dispose method:', e);
			                        }
			                    }
			                    delete self.voxcss_element[id];
			                }
			            }
			            delete self.voxcss_element;
			        };
			        for (var i = 0, elem; (elem = elems[i]) != null; i++) {
			            g(elem);
			        }
			        return jqueryCleanData.apply(this, arguments);
			    };
			    if (!e.Theme.current)
			        e.Theme.default();
			    e.Card.register();
			    e.Dropdown.register();
			    e.Input.register();
			    e.Modal.register();
			    e.ScrollFire.register();
			    e.Parallax.register();
			    e.Pinned.register();
			    e.SideNav.register();
			    e.Slider.register();
			    e.TabGroup.register();
			    e.Toast.register();
			    e.Tooltip.register();
			    e.HasTooltip.register();
			    e.Elastic.register();
			    e.Anchor.register();
			};
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			/**
 * jquery.mask.js
 * @version: v1.13.8
 * @author: Igor Escobar
 *
 * Created by Igor Escobar on 2012-03-10. Please report any bug at http://blog.igorescobar.com
 *
 * Copyright (c) 2012 Igor Escobar http://blog.igorescobar.com
 *
 * The MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

/* jshint laxbreak: true */
/* global define, jQuery, Zepto */

'use strict';

// UMD (Universal Module Definition) patterns for JavaScript modules that work everywhere.
// https://github.com/umdjs/umd/blob/master/jqueryPluginCommonjs.js
(function (factory) {

    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery || Zepto);
    }

}(function ($) {

    var Mask = function (el, mask, options) {

        var p = {
            invalid: [],
            getCaret: function () {
                try {
                    var sel,
                        pos = 0,
                        ctrl = el.get(0),
                        dSel = document.selection,
                        cSelStart = ctrl.selectionStart;

                    // IE Support
                    if (dSel && navigator.appVersion.indexOf('MSIE 10') === -1) {
                        sel = dSel.createRange();
                        sel.moveStart('character', -p.val().length);
                        pos = sel.text.length;
                    }
                    // Firefox support
                    else if (cSelStart || cSelStart === '0') {
                        pos = cSelStart;
                    }

                    return pos;
                } catch (e) {}
            },
            setCaret: function(pos) {
                try {
                    if (el.is(':focus')) {
                        var range, ctrl = el.get(0);

                        range = ctrl.createTextRange();
                        range.collapse(true);
                        range.moveEnd('character', pos);
                        range.moveStart('character', pos);
                        range.select();
                    }
                } catch (e) {}
            },
            events: function() {
                el
                .on('keydown.mask', function(e) {
                    el.data('mask-keycode', e.keyCode || e.which);
                })
                .on($.jMaskGlobals.useInput ? 'input.mask' : 'keyup.mask', p.behaviour)
                .on('paste.mask drop.mask', function() {
                    setTimeout(function() {
                        el.keydown().keyup();
                    }, 100);
                })
                .on('change.mask', function(){
                    el.data('changed', true);
                })
                .on('blur.mask', function(){
                    if (oldValue !== p.val() && !el.data('changed')) {
                        el.trigger('change');
                    }
                    el.data('changed', false);
                })
                // it's very important that this callback remains in this position
                // otherwhise oldValue it's going to work buggy
                .on('blur.mask', function() {
                    oldValue = p.val();
                })
                // select all text on focus
                .on('focus.mask', function (e) {
                    if (options.selectOnFocus === true) {
                        $(e.target).select();
                    }
                })
                // clear the value if it not complete the mask
                .on('focusout.mask', function() {
                    if (options.clearIfNotMatch && !regexMask.test(p.val())) {
                       p.val('');
                   }
                });
            },
            getRegexMask: function() {
                var maskChunks = [], translation, pattern, optional, recursive, oRecursive, r;

                for (var i = 0; i < mask.length; i++) {
                    translation = jMask.translation[mask.charAt(i)];

                    if (translation) {

                        pattern = translation.pattern.toString().replace(/.{1}$|^.{1}/g, '');
                        optional = translation.optional;
                        recursive = translation.recursive;

                        if (recursive) {
                            maskChunks.push(mask.charAt(i));
                            oRecursive = {digit: mask.charAt(i), pattern: pattern};
                        } else {
                            maskChunks.push(!optional && !recursive ? pattern : (pattern + '?'));
                        }

                    } else {
                        maskChunks.push(mask.charAt(i).replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'));
                    }
                }

                r = maskChunks.join('');

                if (oRecursive) {
                    r = r.replace(new RegExp('(' + oRecursive.digit + '(.*' + oRecursive.digit + ')?)'), '($1)?')
                         .replace(new RegExp(oRecursive.digit, 'g'), oRecursive.pattern);
                }

                return new RegExp(r);
            },
            destroyEvents: function() {
                el.off(['input', 'keydown', 'keyup', 'paste', 'drop', 'blur', 'focusout', ''].join('.mask '));
            },
            val: function(v) {
                var isInput = el.is('input'),
                    method = isInput ? 'val' : 'text',
                    r;

                if (arguments.length > 0) {
                    if (el[method]() !== v) {
                        el[method](v);
                    }
                    r = el;
                } else {
                    r = el[method]();
                }

                return r;
            },
            getMCharsBeforeCount: function(index, onCleanVal) {
                for (var count = 0, i = 0, maskL = mask.length; i < maskL && i < index; i++) {
                    if (!jMask.translation[mask.charAt(i)]) {
                        index = onCleanVal ? index + 1 : index;
                        count++;
                    }
                }
                return count;
            },
            caretPos: function (originalCaretPos, oldLength, newLength, maskDif) {
                var translation = jMask.translation[mask.charAt(Math.min(originalCaretPos - 1, mask.length - 1))];

                return !translation ? p.caretPos(originalCaretPos + 1, oldLength, newLength, maskDif)
                                    : Math.min(originalCaretPos + newLength - oldLength - maskDif, newLength);
            },
            behaviour: function(e) {
                e = e || window.event;
                p.invalid = [];

                var keyCode = el.data('mask-keycode');

                if ($.inArray(keyCode, jMask.byPassKeys) === -1) {
                    var caretPos    = p.getCaret(),
                        currVal     = p.val(),
                        currValL    = currVal.length,
                        newVal      = p.getMasked(),
                        newValL     = newVal.length,
                        maskDif     = p.getMCharsBeforeCount(newValL - 1) - p.getMCharsBeforeCount(currValL - 1),
                        changeCaret = caretPos < currValL;

                    p.val(newVal);

                    if (changeCaret) {
                        // Avoid adjusting caret on backspace or delete
                        if (!(keyCode === 8 || keyCode === 46)) {
                            caretPos = p.caretPos(caretPos, currValL, newValL, maskDif);
                        }
                        p.setCaret(caretPos);
                    }

                    return p.callbacks(e);
                }
            },
            getMasked: function(skipMaskChars) {
                var buf = [],
                    value = p.val(),
                    m = 0, maskLen = mask.length,
                    v = 0, valLen = value.length,
                    offset = 1, addMethod = 'push',
                    resetPos = -1,
                    lastMaskChar,
                    check;

                if (options.reverse) {
                    addMethod = 'unshift';
                    offset = -1;
                    lastMaskChar = 0;
                    m = maskLen - 1;
                    v = valLen - 1;
                    check = function () {
                        return m > -1 && v > -1;
                    };
                } else {
                    lastMaskChar = maskLen - 1;
                    check = function () {
                        return m < maskLen && v < valLen;
                    };
                }

                while (check()) {
                    var maskDigit = mask.charAt(m),
                        valDigit = value.charAt(v),
                        translation = jMask.translation[maskDigit];

                    if (translation) {
                        if (valDigit.match(translation.pattern)) {
                            buf[addMethod](valDigit);
                             if (translation.recursive) {
                                if (resetPos === -1) {
                                    resetPos = m;
                                } else if (m === lastMaskChar) {
                                    m = resetPos - offset;
                                }

                                if (lastMaskChar === resetPos) {
                                    m -= offset;
                                }
                            }
                            m += offset;
                        } else if (translation.optional) {
                            m += offset;
                            v -= offset;
                        } else if (translation.fallback) {
                            buf[addMethod](translation.fallback);
                            m += offset;
                            v -= offset;
                        } else {
                          p.invalid.push({p: v, v: valDigit, e: translation.pattern});
                        }
                        v += offset;
                    } else {
                        if (!skipMaskChars) {
                            buf[addMethod](maskDigit);
                        }

                        if (valDigit === maskDigit) {
                            v += offset;
                        }

                        m += offset;
                    }
                }

                var lastMaskCharDigit = mask.charAt(lastMaskChar);
                if (maskLen === valLen + 1 && !jMask.translation[lastMaskCharDigit]) {
                    buf.push(lastMaskCharDigit);
                }

                return buf.join('');
            },
            callbacks: function (e) {
                var val = p.val(),
                    changed = val !== oldValue,
                    defaultArgs = [val, e, el, options],
                    callback = function(name, criteria, args) {
                        if (typeof options[name] === 'function' && criteria) {
                            options[name].apply(this, args);
                        }
                    };

                callback('onChange', changed === true, defaultArgs);
                callback('onKeyPress', changed === true, defaultArgs);
                callback('onComplete', val.length === mask.length, defaultArgs);
                callback('onInvalid', p.invalid.length > 0, [val, e, el, p.invalid, options]);
            }
        };

        el = $(el);
        var jMask = this, oldValue = p.val(), regexMask;

        mask = typeof mask === 'function' ? mask(p.val(), undefined, el,  options) : mask;


        // public methods
        jMask.mask = mask;
        jMask.options = options;
        jMask.remove = function() {
            var caret = p.getCaret();
            p.destroyEvents();
            p.val(jMask.getCleanVal());
            p.setCaret(caret - p.getMCharsBeforeCount(caret));
            return el;
        };

        // get value without mask
        jMask.getCleanVal = function() {
           return p.getMasked(true);
        };

       jMask.init = function(onlyMask) {
            onlyMask = onlyMask || false;
            options = options || {};

            jMask.clearIfNotMatch  = $.jMaskGlobals.clearIfNotMatch;
            jMask.byPassKeys       = $.jMaskGlobals.byPassKeys;
            jMask.translation      = $.extend({}, $.jMaskGlobals.translation, options.translation);

            jMask = $.extend(true, {}, jMask, options);

            regexMask = p.getRegexMask();

            if (onlyMask === false) {

                if (options.placeholder) {
                    el.attr('placeholder' , options.placeholder);
                }

                // this is necessary, otherwise if the user submit the form
                // and then press the "back" button, the autocomplete will erase
                // the data. Works fine on IE9+, FF, Opera, Safari.
                if (el.data('mask')) {
                  el.attr('autocomplete', 'off');
                }

                p.destroyEvents();
                p.events();

                var caret = p.getCaret();
                p.val(p.getMasked());
                p.setCaret(caret + p.getMCharsBeforeCount(caret, true));

            } else {
                p.events();
                p.val(p.getMasked());
            }
        };

        jMask.init(!el.is('input'));
    };

    $.maskWatchers = {};
    var HTMLAttributes = function () {
        var input = $(this),
            options = {},
            prefix = 'data-mask-',
            mask = input.attr('data-mask');

        if (input.attr(prefix + 'reverse')) {
            options.reverse = true;
        }

        if (input.attr(prefix + 'clearifnotmatch')) {
            options.clearIfNotMatch = true;
        }

        if (input.attr(prefix + 'selectonfocus') === 'true') {
           options.selectOnFocus = true;
        }

        if (notSameMaskObject(input, mask, options)) {
            return input.data('mask', new Mask(this, mask, options));
        }
    },
    notSameMaskObject = function(field, mask, options) {
        options = options || {};
        var maskObject = $(field).data('mask'),
            stringify = JSON.stringify,
            value = $(field).val() || $(field).text();
        try {
            if (typeof mask === 'function') {
                mask = mask(value);
            }
            return typeof maskObject !== 'object' || stringify(maskObject.options) !== stringify(options) || maskObject.mask !== mask;
        } catch (e) {}
    },
    eventSupported = function(eventName) {
        var el = document.createElement('div');
        eventName = 'on' + eventName;

        var isSupported = (eventName in el);
        if ( !isSupported ) {
            el.setAttribute(eventName, 'return;');
            isSupported = typeof el[eventName] === 'function';
        }
        el = null;

        return isSupported;
    };

    $.fn.mask = function(mask, options) {
        options = options || {};
        var selector = this.selector,
            globals = $.jMaskGlobals,
            interval = $.jMaskGlobals.watchInterval,
            maskFunction = function() {
                if (notSameMaskObject(this, mask, options)) {
                    return $(this).data('mask', new Mask(this, mask, options));
                }
            };

        $(this).each(maskFunction);

        if (selector && selector !== '' && globals.watchInputs) {
            clearInterval($.maskWatchers[selector]);
            $.maskWatchers[selector] = setInterval(function(){
                $(document).find(selector).each(maskFunction);
            }, interval);
        }
        return this;
    };

    $.fn.unmask = function() {
        clearInterval($.maskWatchers[this.selector]);
        delete $.maskWatchers[this.selector];
        return this.each(function() {
            var dataMask = $(this).data('mask');
            if (dataMask) {
                dataMask.remove().removeData('mask');
            }
        });
    };

    $.fn.cleanVal = function() {
        return this.data('mask').getCleanVal();
    };

    $.applyDataMask = function(selector) {
        selector = selector || $.jMaskGlobals.maskElements;
        var $selector = (selector instanceof $) ? selector : $(selector);
        $selector.filter($.jMaskGlobals.dataMaskAttr).each(HTMLAttributes);
    };

    var globals = {
        maskElements: 'input,td,span,div',
        dataMaskAttr: '*[data-mask]',
        dataMask: true,
        watchInterval: 300,
        watchInputs: true,
        useInput: eventSupported('input'),
        watchDataMask: false,
        byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91],
        translation: {
            '0': {pattern: /\d/},
            '9': {pattern: /\d/, optional: true},
            '#': {pattern: /\d/, recursive: true},
            'A': {pattern: /[a-zA-Z0-9]/},
            'S': {pattern: /[a-zA-Z]/}
        }
    };

    $.jMaskGlobals = $.jMaskGlobals || {};
    globals = $.jMaskGlobals = $.extend(true, {}, globals, $.jMaskGlobals);

    // looking for inputs with data-mask attribute
    if (globals.dataMask) { $.applyDataMask(); }

    setInterval(function(){
        if ($.jMaskGlobals.watchDataMask) { $.applyDataMask(); }
    }, globals.watchInterval);
}));
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			exports=module.exports=core.VW.Web.JQuery
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
function moduleVW_Web(){
	
	var c= core.VW.Web= core.VW.Web || {};
	c.JQuery= global.$
	var w= (typeof window=="object") ? window : {document:null}
	if(!(w.document && w.document.registerElement)){
		c.RegisterElement= require("./registerelement.js");
	}
	
	c.WebComponents= require("./webcomponents-lite.js");
	c.Waves= require("./waves");
	c.Vox= require("./vox.js");
	
	// add css 
	require("./elements/index")
	var script,scripts= global.$("script")
	for(var i=0;i<scripts.length;i++){
		script=scripts.eq(i)
		if(script.attr("src") && script.attr("src").indexOf("kodhe.css")>=0){
			break 
		}
	}
	
	
	require("./dynvox/init")
	var start1=function(){
		var code= global.kodhe_css_code 
		var mainpath= script.attr("mainpath") || ""
		
		if(mainpath){
			var Path= require("path")
			code= code.replace(/\.\.\/font/ig, Path.join(mainpath, "font"))
			code= code.replace(/\.\.\/webfonts/ig, Path.join(mainpath, "webfonts"))
		}
		var div= global.$("<div>")
		div.html("<style>" + code + "</style>")
		global.$("body").append(div)
		
	}
	setTimeout(start1,100)
	
	
}

moduleVW_Web()

		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			
if(!global.$)
	throw new Error("Plead add jQuery script")
module.exports= global.$
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			/*! (C) WebReflection Mit Style License */
(function(e,t,n,r){"use strict";function rt(e,t){for(var n=0,r=e.length;n<r;n++)vt(e[n],t)}function it(e){for(var t=0,n=e.length,r;t<n;t++)r=e[t],nt(r,b[ot(r)])}function st(e){return function(t){j(t)&&(vt(t,e),rt(t.querySelectorAll(w),e))}}function ot(e){var t=e.getAttribute("is"),n=e.nodeName.toUpperCase(),r=S.call(y,t?v+t.toUpperCase():d+n);return t&&-1<r&&!ut(n,t)?-1:r}function ut(e,t){return-1<w.indexOf(e+'[is="'+t+'"]')}function at(e){var t=e.currentTarget,n=e.attrChange,r=e.attrName,i=e.target;Q&&(!i||i===t)&&t.attributeChangedCallback&&r!=="style"&&e.prevValue!==e.newValue&&t.attributeChangedCallback(r,n===e[a]?null:e.prevValue,n===e[l]?null:e.newValue)}function ft(e){var t=st(e);return function(e){X.push(t,e.target)}}function lt(e){K&&(K=!1,e.currentTarget.removeEventListener(h,lt)),rt((e.target||t).querySelectorAll(w),e.detail===o?o:s),B&&pt()}function ct(e,t){var n=this;q.call(n,e,t),G.call(n,{target:n})}function ht(e,t){D(e,t),et?et.observe(e,z):(J&&(e.setAttribute=ct,e[i]=Z(e),e.addEventListener(p,G)),e.addEventListener(c,at)),e.createdCallback&&Q&&(e.created=!0,e.createdCallback(),e.created=!1)}function pt(){for(var e,t=0,n=F.length;t<n;t++)e=F[t],E.contains(e)||(n--,F.splice(t--,1),vt(e,o))}function dt(e){throw new Error("A "+e+" type is already registered")}function vt(e,t){var n,r=ot(e);-1<r&&(tt(e,b[r]),r=0,t===s&&!e[s]?(e[o]=!1,e[s]=!0,r=1,B&&S.call(F,e)<0&&F.push(e)):t===o&&!e[o]&&(e[s]=!1,e[o]=!0,r=1),r&&(n=e[t+"Callback"])&&n.call(e))}if(r in t)return;var i="__"+r+(Math.random()*1e5>>0),s="attached",o="detached",u="extends",a="ADDITION",f="MODIFICATION",l="REMOVAL",c="DOMAttrModified",h="DOMContentLoaded",p="DOMSubtreeModified",d="<",v="=",m=/^[A-Z][A-Z0-9]*(?:-[A-Z0-9]+)+$/,g=["ANNOTATION-XML","COLOR-PROFILE","FONT-FACE","FONT-FACE-SRC","FONT-FACE-URI","FONT-FACE-FORMAT","FONT-FACE-NAME","MISSING-GLYPH"],y=[],b=[],w="",E=t.documentElement,S=y.indexOf||function(e){for(var t=this.length;t--&&this[t]!==e;);return t},x=n.prototype,T=x.hasOwnProperty,N=x.isPrototypeOf,C=n.defineProperty,k=n.getOwnPropertyDescriptor,L=n.getOwnPropertyNames,A=n.getPrototypeOf,O=n.setPrototypeOf,M=!!n.__proto__,_=n.create||function mt(e){return e?(mt.prototype=e,new mt):this},D=O||(M?function(e,t){return e.__proto__=t,e}:L&&k?function(){function e(e,t){for(var n,r=L(t),i=0,s=r.length;i<s;i++)n=r[i],T.call(e,n)||C(e,n,k(t,n))}return function(t,n){do e(t,n);while((n=A(n))&&!N.call(n,t));return t}}():function(e,t){for(var n in t)e[n]=t[n];return e}),P=e.MutationObserver||e.WebKitMutationObserver,H=(e.HTMLElement||e.Element||e.Node).prototype,B=!N.call(H,E),j=B?function(e){return e.nodeType===1}:function(e){return N.call(H,e)},F=B&&[],I=H.cloneNode,q=H.setAttribute,R=H.removeAttribute,U=t.createElement,z=P&&{attributes:!0,characterData:!0,attributeOldValue:!0},W=P||function(e){J=!1,E.removeEventListener(c,W)},X,V=e.requestAnimationFrame||e.webkitRequestAnimationFrame||e.mozRequestAnimationFrame||e.msRequestAnimationFrame||function(e){setTimeout(e,10)},$=!1,J=!0,K=!0,Q=!0,G,Y,Z,et,tt,nt;O||M?(tt=function(e,t){N.call(t,e)||ht(e,t)},nt=ht):(tt=function(e,t){e[i]||(e[i]=n(!0),ht(e,t))},nt=tt),B?(J=!1,function(){var e=k(H,"addEventListener"),t=e.value,n=function(e){var t=new CustomEvent(c,{bubbles:!0});t.attrName=e,t.prevValue=this.getAttribute(e),t.newValue=null,t[l]=t.attrChange=2,R.call(this,e),this.dispatchEvent(t)},r=function(e,t){var n=this.hasAttribute(e),r=n&&this.getAttribute(e),i=new CustomEvent(c,{bubbles:!0});q.call(this,e,t),i.attrName=e,i.prevValue=n?r:null,i.newValue=t,n?i[f]=i.attrChange=1:i[a]=i.attrChange=0,this.dispatchEvent(i)},s=function(e){var t=e.currentTarget,n=t[i],r=e.propertyName,s;n.hasOwnProperty(r)&&(n=n[r],s=new CustomEvent(c,{bubbles:!0}),s.attrName=n.name,s.prevValue=n.value||null,s.newValue=n.value=t[r]||null,s.prevValue==null?s[a]=s.attrChange=0:s[f]=s.attrChange=1,t.dispatchEvent(s))};e.value=function(e,o,u){e===c&&this.attributeChangedCallback&&this.setAttribute!==r&&(this[i]={className:{name:"class",value:this.className}},this.setAttribute=r,this.removeAttribute=n,t.call(this,"propertychange",s)),t.call(this,e,o,u)},C(H,"addEventListener",e)}()):P||(E.addEventListener(c,W),E.setAttribute(i,1),E.removeAttribute(i),J&&(G=function(e){var t=this,n,r,s;if(t===e.target){n=t[i],t[i]=r=Z(t);for(s in r){if(!(s in n))return Y(0,t,s,n[s],r[s],a);if(r[s]!==n[s])return Y(1,t,s,n[s],r[s],f)}for(s in n)if(!(s in r))return Y(2,t,s,n[s],r[s],l)}},Y=function(e,t,n,r,i,s){var o={attrChange:e,currentTarget:t,attrName:n,prevValue:r,newValue:i};o[s]=e,at(o)},Z=function(e){for(var t,n,r={},i=e.attributes,s=0,o=i.length;s<o;s++)t=i[s],n=t.name,n!=="setAttribute"&&(r[n]=t.value);return r})),t[r]=function(n,r){c=n.toUpperCase(),$||($=!0,P?(et=function(e,t){function n(e,t){for(var n=0,r=e.length;n<r;t(e[n++]));}return new P(function(r){for(var i,s,o,u=0,a=r.length;u<a;u++)i=r[u],i.type==="childList"?(n(i.addedNodes,e),n(i.removedNodes,t)):(s=i.target,Q&&s.attributeChangedCallback&&i.attributeName!=="style"&&(o=s.getAttribute(i.attributeName),o!==i.oldValue&&s.attributeChangedCallback(i.attributeName,i.oldValue,o)))})}(st(s),st(o)),et.observe(t,{childList:!0,subtree:!0})):(X=[],V(function E(){while(X.length)X.shift().call(null,X.shift());V(E)}),t.addEventListener("DOMNodeInserted",ft(s)),t.addEventListener("DOMNodeRemoved",ft(o))),t.addEventListener(h,lt),t.addEventListener("readystatechange",lt),t.createElement=function(e,n){var r=U.apply(t,arguments),i=""+e,s=S.call(y,(n?v:d)+(n||i).toUpperCase()),o=-1<s;return n&&(r.setAttribute("is",n=n.toLowerCase()),o&&(o=ut(i.toUpperCase(),n))),Q=!t.createElement.innerHTMLHelper,o&&nt(r,b[s]),r},H.cloneNode=function(e){var t=I.call(this,!!e),n=ot(t);return-1<n&&nt(t,b[n]),e&&it(t.querySelectorAll(w)),t}),-2<S.call(y,v+c)+S.call(y,d+c)&&dt(n);if(!m.test(c)||-1<S.call(g,c))throw new Error("The type "+n+" is invalid");var i=function(){return f?t.createElement(l,c):t.createElement(l)},a=r||x,f=T.call(a,u),l=f?r[u].toUpperCase():c,c,p;return f&&-1<S.call(y,d+l)&&dt(l),p=y.push((f?v:d)+c)-1,w=w.concat(w.length?",":"",f?l+'[is="'+n.toLowerCase()+'"]':l),i.prototype=b[p]=T.call(a,"prototype")?a.prototype:_(H),rt(t.querySelectorAll(w),s),i}})(window,document,Object,"registerElement");
		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			/*global core*/

var Path= require("path")
//require("./Requires").module("System.String")
function init(vox, $, window, document){

    //
    core.baseUrl= "/"
    core.require= function(module){
        var src= $("<script>")
        src.attr("src", Path.join(core.baseUrl, module))
        $("head").append(src)

    }

	var Waves= core.VW.Web.Waves;
    Array.prototype.each= Array.prototype.forEach;
    $(function(){
        Waves.attach('.button');
        Waves.init();
    });

    document.createElement("vox-css");
    document.createElement("vox-action");
    document.createElement("vox-object");
    document.createElement("vox-item");
    document.createElement("vox-bindevent");


    var platform= function(){
        var self= this;
        var f={};

        self.scrollObject= $(window);


        self.getJsonResponseAsync= function(/*core.VW.Http.Request*/ request){
            request.validateStatusCode= true
            var task= request.getResponseAsync()
            //console.info("TASK: ", task)
            task.beforeExpose(function(){
                try{

                    var data= task.result.body
                    if(typeof data==="string")
                        data=JSON.parse(data)


                    if(data && data.error){
                        task.exception= data.error
                    }
                    return data
                }
                catch(e){
                    task.exception= "La respuesta no es válida. " + e.message
                }
            })

            return task
        }



        self.attachOuterClick= function(obj, pars){

            var self= pars.self;
            var isOpened= pars.active || function(){return true;}

            var y= function(ev){
                if(!isOpened()){
                    return;
                }


                var e= $(ev.target);
                if((ev.target!= obj.get(0)) && (obj.find(e).length==0)){

                    if(pars.processEvent){
                        ev=pars.processEvent(ev);
                    }
                    self.emit("outerclick", ev);
                    if(ev.defaultPrevented){
                        return;
                    }
                    if(pars.callback){
                        pars.callback(ev);
                    }
                }

            }
            $(document).bind("click", y);

        }

        self.attachEvents= function(events, pars){

            var self= pars.self;
            var isOpened= pars.active || function(){return true;}

            var y= function(ev){
                if(!isOpened()){
                    return;
                }
                if(pars.processEvent){
                    ev=pars.processEvent(ev);
                }
                self.emit(ev.type, ev);
                if(ev.defaultPrevented){
                    return;
                }
                if(pars.callback){
                    pars.callback(ev);
                }
            }
            $(document).bind(events, y);

        };

        self.transition= function(obj, values, effect, time, cb){
            var callback= cb?cb:function(){};
            var st=[];
            for(var i in values){
                st.push(i);
            }

            obj.css("transition-property", st.join(","));
            if(time){
                var timec= (time/1000);
                timec= timec.toString() + "s";
                obj.css("transition-duration", timec);
            }
            if(effect){
                obj.addClass(effect);
            }

            if(st.length==0){
                callback();
            }
            obj.css(values);

            obj.addClass("transitioned");

            setTimeout(function(){
                obj.removeClass("transitioned");
                if(effect){
                    obj.removeClass(effect);
                }
                callback();
            }, time);
        }

        self.animate= function(obj, effect, time, cb){

            callback= function(){
                if(hide){
                    obj.hide();
                }
                obj.removeClass(effect);
                if(cb){
                    cb();
                }
            }

            var hide;
            if(effect.toLowerCase().indexOf("out")>=0){

                hide= true;
                // Es efecto de salida ...
                if(!obj.is(":visible")){
                    return callback();
                }
            }
            else{
                if(obj.is(":visible")){
                    return callback();
                }
            }


            if(!obj.hasClass("animated")){
                obj.addClass("animated");
            }
            if(obj.data("last-effect")){
                obj.removeClass(obj.data("last-effect"));
            }
            obj.removeClass(effect);
            obj.show();
            obj.addClass(effect);
            obj.data("last-effect", effect);

            if(!time){
                time= parseFloat(obj.css("animation-duration"));
                if(!time){
                    time= parseFloat(obj.css("-webkit-animation-duration"));
                }
            }
            else{
                var s=(time/1000).toString() + "s";
                time /= 1000;
                obj.css({
                    "-webkit-animation-duration":s,
                    "animation-duration": s
                });
            }

            time= (time*1000) + 1;
            setTimeout(callback, time);

        }

        f.processRow= function(obj){
            var temp= $("<div>")
            obj.each(function(){
                var j= $(this);
                var col= j.find(">*");
                var cols=[];
                for(var i=0;i<col.length;i++){
                    cols.push(col.eq(i));
                    temp.append(col.eq(i))
                }
                //col.remove();
                for(var i=0;i<cols.length;i++){
                    j.append(cols[i]);
                }
            });
        };

        if (document.createEvent) {
    		self.createEvent = function(name){
    	        var evt = document.createEvent("Event");
    	        evt.initEvent(name, true, true);
    	        return evt;
    	    }

        } else if (document.createEventObject) {
        	// MSIE (NOT WORKING)
        	self.createEvent = function(name){
    	        var evt = document.createEventObject("Event");
    	        evt.type= name;
    	        return evt;
    	    }

        }

        f.bodySize= function(){
            /*
            var w= parseInt($(window).width());
            var s;

            if(w<600){
                s= "s";
            }
            else if(w<860){
                s= "sl";
            }
            else if(w<1024){
                s= "m";
            }
            else if(w<1200){
                s= "ml";
            }
            else{
                s= "l";
            }
            var b=$("body");
            b.removeClass("size-s");
            b.removeClass("size-sl");
            b.removeClass("size-m");
            b.removeClass("size-ml");
            b.removeClass("size-l");
            b.addClass("size-"+ s);
            */
            var o= $("div.size-vox"), row
            if(o.length==0){
                o= $("<div>")
                o.height(100)


                row= $("<div>")
                row.css("position","fixed")
                row.css("top", "-1000px")
                row.addClass("row")
                row.addClass("size-vox")
                row.css("margin", 0)
                o.addClass("col")
                o.addClass("s12 sl6 m4 ml3 l1")

                var items=[], g
                for(var i=0;i<8;i++){
                    g= o.clone()
                    row.append(g)
                }

                $("body").append(row)
            }

            var size=''
            if(o.height()>=800)
                size= "s"
            else if(o.height()>=400)
                size= "sl"
            else if(o.height()>=300)
                size= "m"
            else if(o.height()>=200)
                size= "ml"
            else
                size= "l"

            var b=$("body");
            b.removeClass("size-s");
            b.removeClass("size-sl");
            b.removeClass("size-m");
            b.removeClass("size-ml");
            b.removeClass("size-l");
            b.addClass("size-"+ size);




        }

        f.processScript= function(script){
            script.each(function(){
                try{
                    var s= $(this);
                    var p= s.attr("vox-name")|| "value";
                    var f= eval(s.text());
                    f.script= s;
                    if(p){
                        s.parent().data(p, f);
                    }

                    if(s.attr("vox-auto")!=undefined){
                        f(s);
                    }
                }
                catch(e){
                    console.log("Error al procesar script");
                    console.error(e);
                }
            });
        }
        f.processObjects= function(obj2){
            obj2.each(function(){

                var obj= $(this);
                if(obj.find("vox-object").length>0){
                   f.processObjects(obj.find("vox-object"));
                }
                var o={};
                if(obj.data("vox-processed")){
                    return;
                }
                obj.find(">vox-item").each(function(){
                    var g= $(this);
                    var n= g.attr("vox-name");
                    if(n){
                        o[n]= g.data("value");
                    }
                });
                var v=obj.attr("vox-name") || "value";
                obj.parent().data(v, o);
                obj.data("vox-processed", true);

            });
        }


        f.processAction= function(obj){
            obj.each(function(){
                var c= $(this);
                var s= c.attr("vox-selector");
                var v= c.data("value");
                var p= c.parent();

                var u= function(k){
                    k.each(function(){
                       v($(this));
                    });
                }
                u(p.find(s));

                vox.mutation.watchAppend(p, function(ev){
                    u(ev.jTarget);
                }, s);

            });
        }
        f.processCss= function(css){
            css.each(function(){
                var c= $(this);
                if(c.attr("vox-type")=="class"){
                    var s= c.attr("vox-selector");
                    var v= c.data("value");

                    var p= c.parent();
                    p.find(s).addClass(v);

                    vox.mutation.watchAppend(p, function(ev){
                        ev.jTarget.addClass(v);
                    }, s);
                }
                else if(c.attr("vox-type")=="style"){
                    var s= c.attr("vox-selector");
                    var v= c.data("value");

                    var p= c.parent();
                    p.find(s).css(v);

                    vox.mutation.watchAppend(p, function(ev){
                        ev.jTarget.css(v);
                    }, s);
                }
            });
        }

        self.merge= function(obj1, obj2){
            return $.merge(obj1, obj2);
        }

        f.processChipAction= function(obj){
            obj.click(function(){
                $(this).parents(".chip").eq(0).remove();
            });
        }

        f.processBindEvent= function(obj2){

			obj2.each(function(){
				var obj= $(this);
				var name= obj.attr("vox-name");
                console.log(name);
				obj.parent().bind(name, function(ev){
					var fu= obj.data("value");
					if(fu.call){
						fu(ev);
					}
				});
			});

        }

        self.start= function(){
            vox.mutation.watchAppend($("body"),function(ev){
                return f.processRow(ev.jTarget);
            } , ".row");
            f.processRow($(".row"));

            vox.mutation.watchAppend($("body"),function(ev){
                return f.processScript(ev.jTarget);
            } , "script[lang=vox]");
            f.processScript($("script[lang=vox]"));

            vox.mutation.watchAppend($("body"),function(ev){
                return f.processObjects(ev.jTarget);
            } , "vox-object");
            f.processObjects($("vox-object"));

            vox.mutation.watchAppend($("body"),function(ev){
                return f.processCss(ev.jTarget);
            } , "vox-css");
            f.processCss($("vox-css"));

            vox.mutation.watchAppend($("body"),function(ev){
                return f.processAction(ev.jTarget);
            } , "vox-action");
            f.processAction($("vox-action"));

            vox.mutation.watchAppend($("body"),function(ev){
                return f.processChipAction(ev.jTarget);
            } , ".chip .action");
            f.processChipAction($(".chip .action"));

            vox.mutation.watchAppend($("body"),function(ev){
                return f.processBindEvent(ev.jTarget);
            } , "vox-bindevent");
            f.processBindEvent($("vox-bindevent"));

            var re= function(){
                if(re.y){
                    clearTimeout(re.y);
                    re.y=undefined;
                }
                re.y= setTimeout(function(){
                    $(".window-height").height($(window).height());
                }, 100);
                f.bodySize();
            }
            $(window).resize(re);
            re();

        }
    }

    $.fn.equals = function(compareTo) {
      if (!compareTo || this.length != compareTo.length) {
        return false;
      }
      for (var i = 0; i < this.length; ++i) {
        if (this[i] !== compareTo[i]) {
          return false;
        }
      }
      return true;
    };


    var addToCallback= function(func1,func2){
        return function(){
            func1.apply(func1,arguments)
            func2.apply(func2,arguments)
        }
    }

    var mutation= function(){
        var self= this;
        var f={};
        self.observersC=self.observersC ||[]
        self.watchAppend= function(obj, callback2, filter){


			console.warn("> Deprecated. This method don't take care about memory leaks. Please use your own method, or wait to another version")

            var args={}, length


            var callback=function(ev){
                var j1= ev.jTarget.not("[vox-watched]");
                var j2= ev.jTarget.is("[vox-watched]");

                ev.moved= true;
                ev.jTarget= j2;
                if(j2.length>0){
                    callback2(ev);
                }

                ev.moved= false;
                ev.jTarget= j1;
                if(j1.length>0){
                    callback2(ev);
                }

                j1.attr("vox-matched","");


            }
            
            args.filters= [{
                callback:callback,
                filter:filter
            }]

			var removeListener, observer
			
			
			removeListener= function(){
				var index=-1
				var code,result= args.filters.filter(function(a){
					index++
					return a.callback== callback
				})
				if(result&& result[0]){
					code= result[0]
					code.callback=null
					code.filter=null 
					code=null 
					result=null
					
					args.filters.splice(index,1)
				}
				
				
				if(observer){
					// remove observer ...
					observer.disconnect()
					index=self.observers.indexOf(observer)
					if(index>=0){
						self.observers.splice(index,1)
					}
					index=-1
					result= self.observersC.filter(function(a){
						index++
						return a.observer== observer
					})
					if(result && result[0]){
						result[0]=null 
						self.observersC.splice(index,1)
					}
					result=null 
				}
				callback2=null
				callback=null
				removeListener=null
				
			}
			
			

            if(self.observersC){
                // Tratar de filtrar sihay un watchAppend al mismo objeto ...
                var filters,data=self.observersC.filter(function(a){
                    return a.target.equals(obj)
                })
                if(data&&data.length){
                    // Crear un callback a partir del anterior
                    filters=data[0].args.filters
                    if(filters.indexOf(filter)<0){
                        filters.push({
                            filter:filter,
                            callback:callback
                        })
                    }
                    //data[0].callback.f= addToCallback(data[0].callback.f || data[0].callback, callback)
                    return
                }

            }


            observer= new MutationObserver(function(events){
                events= events.filter(function(a){
                    return a.addedNodes.length
                })
                var inserted=[], ev, cache={}, node
                if(!events.length)
                    return

                for(var i=0;i<events.length;i++){
                    ev= events[i]
                    for(var y=0;y<ev.addedNodes.length;y++){
                        node= ev.addedNodes[y]
                        //if(!node._id){
                            //node._id= node._id || (i+Date.now().toString(32))
                            //if(!cache[node._id]){
                                inserted.push(ev.addedNodes[y])
                            //    cache[node._id]= true
                            //}
                        //}
                    }
                }
                inserted= $(inserted)
                var all1= inserted.find("*")
                for(var i=0;i<inserted.length;i++){
                  all1.push(inserted[i])
                }

				inserted=null
                inserted= all1
				all1=null
                setTimeout(function(){

                    var item
                    for(var y=0;y<args.filters.length;y++){
                        item= args.filters[y]
                        filter= item.filter

                        var v=true
                        //var all= inserted.find("*").filter(filter)//inserted.find(filter||"*")
                        var all =inserted.filter(filter)
                        /*for(var i=0;i<others.length;i++){
                            all= all.add(others.eq(i))
                        }*/

                        if(filter){
                          if(all.length>0 && global.debug)
                            console.info("WATCHING FILTER .....", filter, "Len:", all.length)
                          /*
                          if(others.length>0){
                             ev.jTarget= others;
                              item.callback(ev);
                          }*/
                            if(all.length>0){
                                ev.jTarget= all;
                                item.callback(ev);
                            }
                        }
                    }


					inserted=null
					ev.jTarget=null
					ev=null
					all=null

                },0)


            })

            self.observersC.push({
                target:obj,
                observer:observer,
                args:args
            })
            self.observers.push(observer)
            obj.each(function(){
                //console.info(observer)
                observer.observe(this, { childList: true, subtree: true });
            })
            
            return removeListener
            
            
        }
        
        
        

    }


    vox.platform= new platform();
    vox.mutation= new mutation();
    vox.mutation.observers=[];
    $.fn.voxanimate= function(effect, time, callback){
        vox.platform.animate(this, effect, time, callback);
    }
    $.fn.voxtransition= function(values,effect, time, callback){
        vox.platform.transition(this, values, effect, time, callback);
    }
    $(function(){
        if (window.voxpreinit) {
            window.voxpreinit();
        }
        vox.platform.start();
    });
}



var vox= exports;
//var $= core.VW.Web.JQuery;
var owindow,odocument;
if(typeof window== "object"){
	owindow=window;
}
else{
	owindow={};
}
if(typeof document== "object"){
	odocument=document;
}
else{
	odocument={};
}
init(vox, owindow.$, owindow, odocument);

		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			/*!
 * Waves v0.7.5
 * http://fian.my.id/Waves
 *
 * Copyright 2014-2016 Alfiana E. Sibuea and other contributors
 * Released under the MIT license
 * https://github.com/fians/Waves/blob/master/LICENSE
 */

;(function(window, factory) {
    'use strict';

    // AMD. Register as an anonymous module.  Wrap in function so we have access
    // to root via `this`.
    if (typeof define === 'function' && define.amd) {
        define([], function() {
            return factory.apply(window);
        });
    }

    // Node. Does not work with strict CommonJS, but only CommonJS-like
    // environments that support module.exports, like Node.
    else if (typeof exports === 'object') {
        module.exports = factory.call(window);
    }

    // Browser globals.
    else {
        window.Waves = factory.call(window);
    }
})(typeof global === 'object' ? global : this, function() {
    'use strict';

    var Waves            = Waves || {};
    var $$               = document.querySelectorAll.bind(document);
    var toString         = Object.prototype.toString;
    var isTouchAvailable = 'ontouchstart' in window;


    // Find exact position of element
    function isWindow(obj) {
        return obj !== null && obj === obj.window;
    }

    function getWindow(elem) {
        return isWindow(elem) ? elem : elem.nodeType === 9 && elem.defaultView;
    }

    function isObject(value) {
        var type = typeof value;
        return type === 'function' || type === 'object' && !!value;
    }

    function isDOMNode(obj) {
        return isObject(obj) && obj.nodeType > 0;
    }

    function getWavesElements(nodes) {
        var stringRepr = toString.call(nodes);

        if (stringRepr === '[object String]') {
            return $$(nodes);
        } else if (isObject(nodes) && /^\[object (Array|HTMLCollection|NodeList|Object)\]$/.test(stringRepr) && nodes.hasOwnProperty('length')) {
            return nodes;
        } else if (isDOMNode(nodes)) {
            return [nodes];
        }

        return [];
    }

    function offset(elem) {
        var docElem, win,
            box = { top: 0, left: 0 },
            doc = elem && elem.ownerDocument;

        docElem = doc.documentElement;

        if (typeof elem.getBoundingClientRect !== typeof undefined) {
            box = elem.getBoundingClientRect();
        }
        win = getWindow(doc);
        return {
            top: box.top + win.pageYOffset - docElem.clientTop,
            left: box.left + win.pageXOffset - docElem.clientLeft
        };
    }

    function convertStyle(styleObj) {
        var style = '';

        for (var prop in styleObj) {
            if (styleObj.hasOwnProperty(prop)) {
                style += (prop + ':' + styleObj[prop] + ';');
            }
        }

        return style;
    }

    var Effect = {

        // Effect duration
        duration: 750,

        // Effect delay (check for scroll before showing effect)
        delay: 200,

        show: function(e, element, velocity) {

            // Disable right click
            if (e.button === 2) {
                return false;
            }

            element = element || this;

            // Create ripple
            var ripple = document.createElement('div');
            ripple.className = 'waves-ripple waves-rippling';
            element.appendChild(ripple);

            // Get click coordinate and element width
            var pos       = offset(element);
            var relativeY = 0;
            var relativeX = 0;
            // Support for touch devices
            if('touches' in e && e.touches.length) {
                relativeY   = (e.touches[0].pageY - pos.top);
                relativeX   = (e.touches[0].pageX - pos.left);
            }
            //Normal case
            else {
                relativeY   = (e.pageY - pos.top);
                relativeX   = (e.pageX - pos.left);
            }
            // Support for synthetic events
            relativeX = relativeX >= 0 ? relativeX : 0;
            relativeY = relativeY >= 0 ? relativeY : 0;

            var scale     = 'scale(' + ((element.clientWidth / 100) * 3) + ')';
            var translate = 'translate(0,0)';

            if (velocity) {
                translate = 'translate(' + (velocity.x) + 'px, ' + (velocity.y) + 'px)';
            }

            // Attach data to element
            ripple.setAttribute('data-hold', Date.now());
            ripple.setAttribute('data-x', relativeX);
            ripple.setAttribute('data-y', relativeY);
            ripple.setAttribute('data-scale', scale);
            ripple.setAttribute('data-translate', translate);

            // Set ripple position
            var rippleStyle = {
                top: relativeY + 'px',
                left: relativeX + 'px'
            };

            ripple.classList.add('waves-notransition');
            ripple.setAttribute('style', convertStyle(rippleStyle));
            ripple.classList.remove('waves-notransition');

            // Scale the ripple
            rippleStyle['-webkit-transform'] = scale + ' ' + translate;
            rippleStyle['-moz-transform'] = scale + ' ' + translate;
            rippleStyle['-ms-transform'] = scale + ' ' + translate;
            rippleStyle['-o-transform'] = scale + ' ' + translate;
            rippleStyle.transform = scale + ' ' + translate;
            rippleStyle.opacity = '1';

            var duration = e.type === 'mousemove' ? 2500 : Effect.duration;
            rippleStyle['-webkit-transition-duration'] = duration + 'ms';
            rippleStyle['-moz-transition-duration']    = duration + 'ms';
            rippleStyle['-o-transition-duration']      = duration + 'ms';
            rippleStyle['transition-duration']         = duration + 'ms';

            ripple.setAttribute('style', convertStyle(rippleStyle));
        },

        hide: function(e, element) {
            element = element || this;

            var ripples = element.getElementsByClassName('waves-rippling');

            for (var i = 0, len = ripples.length; i < len; i++) {
                removeRipple(e, element, ripples[i]);
            }
        }
    };

    /**
     * Collection of wrapper for HTML element that only have single tag
     * like <input> and <img>
     */
    var TagWrapper = {

        // Wrap <input> tag so it can perform the effect
        input: function(element) {

            var parent = element.parentNode;

            // If input already have parent just pass through
            if (parent.tagName.toLowerCase() === 'i' && parent.classList.contains('waves-effect')) {
                return;
            }

            // Put element class and style to the specified parent
            var wrapper       = document.createElement('i');
            wrapper.className = element.className + ' waves-input-wrapper';
            element.className = 'waves-button-input';

            // Put element as child
            parent.replaceChild(wrapper, element);
            wrapper.appendChild(element);

            // Apply element color and background color to wrapper
            var elementStyle    = window.getComputedStyle(element, null);
            var color           = elementStyle.color;
            var backgroundColor = elementStyle.backgroundColor;

            wrapper.setAttribute('style', 'color:' + color + ';background:' + backgroundColor);
            element.setAttribute('style', 'background-color:rgba(0,0,0,0);');

        },

        // Wrap <img> tag so it can perform the effect
        img: function(element) {

            var parent = element.parentNode;

            // If input already have parent just pass through
            if (parent.tagName.toLowerCase() === 'i' && parent.classList.contains('waves-effect')) {
                return;
            }

            // Put element as child
            var wrapper  = document.createElement('i');
            parent.replaceChild(wrapper, element);
            wrapper.appendChild(element);

        }
    };

    /**
     * Hide the effect and remove the ripple. Must be
     * a separate function to pass the JSLint...
     */
    function removeRipple(e, el, ripple) {

        // Check if the ripple still exist
        if (!ripple) {
            return;
        }

        ripple.classList.remove('waves-rippling');

        var relativeX = ripple.getAttribute('data-x');
        var relativeY = ripple.getAttribute('data-y');
        var scale     = ripple.getAttribute('data-scale');
        var translate = ripple.getAttribute('data-translate');

        // Get delay beetween mousedown and mouse leave
        var diff = Date.now() - Number(ripple.getAttribute('data-hold'));
        var delay = 350 - diff;

        if (delay < 0) {
            delay = 0;
        }

        if (e.type === 'mousemove') {
            delay = 150;
        }

        // Fade out ripple after delay
        var duration = e.type === 'mousemove' ? 2500 : Effect.duration;

        setTimeout(function() {

            var style = {
                top: relativeY + 'px',
                left: relativeX + 'px',
                opacity: '0',

                // Duration
                '-webkit-transition-duration': duration + 'ms',
                '-moz-transition-duration': duration + 'ms',
                '-o-transition-duration': duration + 'ms',
                'transition-duration': duration + 'ms',
                '-webkit-transform': scale + ' ' + translate,
                '-moz-transform': scale + ' ' + translate,
                '-ms-transform': scale + ' ' + translate,
                '-o-transform': scale + ' ' + translate,
                'transform': scale + ' ' + translate
            };

            ripple.setAttribute('style', convertStyle(style));

            setTimeout(function() {
                try {
                    el.removeChild(ripple);
                } catch (e) {
                    return false;
                }
            }, duration);

        }, delay);
    }


    /**
     * Disable mousedown event for 500ms during and after touch
     */
    var TouchHandler = {

        /* uses an integer rather than bool so there's no issues with
         * needing to clear timeouts if another touch event occurred
         * within the 500ms. Cannot mouseup between touchstart and
         * touchend, nor in the 500ms after touchend. */
        touches: 0,

        allowEvent: function(e) {

            var allow = true;

            if (/^(mousedown|mousemove)$/.test(e.type) && TouchHandler.touches) {
                allow = false;
            }

            return allow;
        },
        registerEvent: function(e) {
            var eType = e.type;

            if (eType === 'touchstart') {

                TouchHandler.touches += 1; // push

            } else if (/^(touchend|touchcancel)$/.test(eType)) {

                setTimeout(function() {
                    if (TouchHandler.touches) {
                        TouchHandler.touches -= 1; // pop after 500ms
                    }
                }, 500);

            }
        }
    };


    /**
     * Delegated click handler for .waves-effect element.
     * returns null when .waves-effect element not in "click tree"
     */
    function getWavesEffectElement(e) {

        if (TouchHandler.allowEvent(e) === false) {
            return null;
        }

        var element = null;
        var target = e.target || e.srcElement;

        while (target.parentElement !== null) {
            if (target.classList.contains('waves-effect') && (!(target instanceof SVGElement))) {
                element = target;
                break;
            }
            target = target.parentElement;
        }

        return element;
    }

    /**
     * Bubble the click and show effect if .waves-effect elem was found
     */
    function showEffect(e) {

        // Disable effect if element has "disabled" property on it
        // In some cases, the event is not triggered by the current element
        // if (e.target.getAttribute('disabled') !== null) {
        //     return;
        // }

        var element = getWavesEffectElement(e);

        if (element !== null) {

            // Make it sure the element has either disabled property, disabled attribute or 'disabled' class
            if (element.disabled || element.getAttribute('disabled') || element.classList.contains('disabled')) {
                return;
            }

            TouchHandler.registerEvent(e);

            if (e.type === 'touchstart' && Effect.delay) {

                var hidden = false;

                var timer = setTimeout(function () {
                    timer = null;
                    Effect.show(e, element);
                }, Effect.delay);

                var hideEffect = function(hideEvent) {

                    // if touch hasn't moved, and effect not yet started: start effect now
                    if (timer) {
                        clearTimeout(timer);
                        timer = null;
                        Effect.show(e, element);
                    }
                    if (!hidden) {
                        hidden = true;
                        Effect.hide(hideEvent, element);
                    }
                };

                var touchMove = function(moveEvent) {
                    if (timer) {
                        clearTimeout(timer);
                        timer = null;
                    }
                    hideEffect(moveEvent);
                };

                element.addEventListener('touchmove', touchMove, false);
                element.addEventListener('touchend', hideEffect, false);
                element.addEventListener('touchcancel', hideEffect, false);

            } else {

                Effect.show(e, element);

                if (isTouchAvailable) {
                    element.addEventListener('touchend', Effect.hide, false);
                    element.addEventListener('touchcancel', Effect.hide, false);
                }

                element.addEventListener('mouseup', Effect.hide, false);
                element.addEventListener('mouseleave', Effect.hide, false);
            }
        }
    }

    Waves.init = function(options) {
        var body = document.body;

        options = options || {};

        if ('duration' in options) {
            Effect.duration = options.duration;
        }

        if ('delay' in options) {
            Effect.delay = options.delay;
        }

        if (isTouchAvailable) {
            body.addEventListener('touchstart', showEffect, false);
            body.addEventListener('touchcancel', TouchHandler.registerEvent, false);
            body.addEventListener('touchend', TouchHandler.registerEvent, false);
        }

        body.addEventListener('mousedown', showEffect, false);
    };


    /**
     * Attach Waves to dynamically loaded inputs, or add .waves-effect and other
     * waves classes to a set of elements. Set drag to true if the ripple mouseover
     * or skimming effect should be applied to the elements.
     */
    Waves.attach = function(elements, classes) {

        elements = getWavesElements(elements);

        if (toString.call(classes) === '[object Array]') {
            classes = classes.join(' ');
        }

        classes = classes ? ' ' + classes : '';

        var element, tagName;

        for (var i = 0, len = elements.length; i < len; i++) {

            element = elements[i];
            tagName = element.tagName.toLowerCase();

            if (['input', 'img'].indexOf(tagName) !== -1) {
                TagWrapper[tagName](element);
                element = element.parentElement;
            }

            if (element.className.indexOf('waves-effect') === -1) {
                element.className += ' waves-effect' + classes;
            }
        }
    };


    /**
     * Cause a ripple to appear in an element via code.
     */
    Waves.ripple = function(elements, options) {
        elements = getWavesElements(elements);
        var elementsLen = elements.length;

        options          = options || {};
        options.wait     = options.wait || 0;
        options.position = options.position || null; // default = centre of element


        if (elementsLen) {
            var element, pos, off, centre = {}, i = 0;
            var mousedown = {
                type: 'mousedown',
                button: 1
            };
            var hideRipple = function(mouseup, element) {
                return function() {
                    Effect.hide(mouseup, element);
                };
            };

            for (; i < elementsLen; i++) {
                element = elements[i];
                pos = options.position || {
                    x: element.clientWidth / 2,
                    y: element.clientHeight / 2
                };

                off      = offset(element);
                centre.x = off.left + pos.x;
                centre.y = off.top + pos.y;

                mousedown.pageX = centre.x;
                mousedown.pageY = centre.y;

                Effect.show(mousedown, element);

                if (options.wait >= 0 && options.wait !== null) {
                    var mouseup = {
                        type: 'mouseup',
                        button: 1
                    };

                    setTimeout(hideRipple(mouseup, element), options.wait);
                }
            }
        }
    };

    /**
     * Remove all ripples from an element.
     */
    Waves.calm = function(elements) {
        elements = getWavesElements(elements);
        var mouseup = {
            type: 'mouseup',
            button: 1
        };

        for (var i = 0, len = elements.length; i < len; i++) {
            Effect.hide(mouseup, elements[i]);
        }
    };

    /**
     * Deprecated API fallback
     */
    Waves.displayEffect = function(options) {
        console.error('Waves.displayEffect() has been deprecated and will be removed in future version. Please use Waves.init() to initialize Waves effect');
        Waves.init(options);
    };

    return Waves;
});

		})
			,

		(function(options){
		
			if(options.eval1){
				eval(options.eval1)
				options= undefined 
			}
			/**
 * @license
 * Copyright (c) 2014 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */
// @version 0.7.21
(function() {
  window.WebComponents = window.WebComponents || {
    flags: {}
  };
  var file = "webcomponents-lite.js";
  var script = document.querySelector('script[src*="' + file + '"]');
  var flags = {};
  if (!flags.noOpts) {
    location.search.slice(1).split("&").forEach(function(option) {
      var parts = option.split("=");
      var match;
      if (parts[0] && (match = parts[0].match(/wc-(.+)/))) {
        flags[match[1]] = parts[1] || true;
      }
    });
    if (script) {
      for (var i = 0, a; a = script.attributes[i]; i++) {
        if (a.name !== "src") {
          flags[a.name] = a.value || true;
        }
      }
    }
    if (flags.log && flags.log.split) {
      var parts = flags.log.split(",");
      flags.log = {};
      parts.forEach(function(f) {
        flags.log[f] = true;
      });
    } else {
      flags.log = {};
    }
  }
  if (flags.register) {
    window.CustomElements = window.CustomElements || {
      flags: {}
    };
    window.CustomElements.flags.register = flags.register;
  }
  WebComponents.flags = flags;
})();

(function(scope) {
  "use strict";
  var hasWorkingUrl = false;
  if (!scope.forceJURL) {
    try {
      var u = new URL("b", "http://a");
      u.pathname = "c%20d";
      hasWorkingUrl = u.href === "http://a/c%20d";
    } catch (e) {}
  }
  if (hasWorkingUrl) return;
  var relative = Object.create(null);
  relative["ftp"] = 21;
  relative["file"] = 0;
  relative["gopher"] = 70;
  relative["http"] = 80;
  relative["https"] = 443;
  relative["ws"] = 80;
  relative["wss"] = 443;
  var relativePathDotMapping = Object.create(null);
  relativePathDotMapping["%2e"] = ".";
  relativePathDotMapping[".%2e"] = "..";
  relativePathDotMapping["%2e."] = "..";
  relativePathDotMapping["%2e%2e"] = "..";
  function isRelativeScheme(scheme) {
    return relative[scheme] !== undefined;
  }
  function invalid() {
    clear.call(this);
    this._isInvalid = true;
  }
  function IDNAToASCII(h) {
    if ("" == h) {
      invalid.call(this);
    }
    return h.toLowerCase();
  }
  function percentEscape(c) {
    var unicode = c.charCodeAt(0);
    if (unicode > 32 && unicode < 127 && [ 34, 35, 60, 62, 63, 96 ].indexOf(unicode) == -1) {
      return c;
    }
    return encodeURIComponent(c);
  }
  function percentEscapeQuery(c) {
    var unicode = c.charCodeAt(0);
    if (unicode > 32 && unicode < 127 && [ 34, 35, 60, 62, 96 ].indexOf(unicode) == -1) {
      return c;
    }
    return encodeURIComponent(c);
  }
  var EOF = undefined, ALPHA = /[a-zA-Z]/, ALPHANUMERIC = /[a-zA-Z0-9\+\-\.]/;
  function parse(input, stateOverride, base) {
    function err(message) {
      errors.push(message);
    }
    var state = stateOverride || "scheme start", cursor = 0, buffer = "", seenAt = false, seenBracket = false, errors = [];
    loop: while ((input[cursor - 1] != EOF || cursor == 0) && !this._isInvalid) {
      var c = input[cursor];
      switch (state) {
       case "scheme start":
        if (c && ALPHA.test(c)) {
          buffer += c.toLowerCase();
          state = "scheme";
        } else if (!stateOverride) {
          buffer = "";
          state = "no scheme";
          continue;
        } else {
          err("Invalid scheme.");
          break loop;
        }
        break;

       case "scheme":
        if (c && ALPHANUMERIC.test(c)) {
          buffer += c.toLowerCase();
        } else if (":" == c) {
          this._scheme = buffer;
          buffer = "";
          if (stateOverride) {
            break loop;
          }
          if (isRelativeScheme(this._scheme)) {
            this._isRelative = true;
          }
          if ("file" == this._scheme) {
            state = "relative";
          } else if (this._isRelative && base && base._scheme == this._scheme) {
            state = "relative or authority";
          } else if (this._isRelative) {
            state = "authority first slash";
          } else {
            state = "scheme data";
          }
        } else if (!stateOverride) {
          buffer = "";
          cursor = 0;
          state = "no scheme";
          continue;
        } else if (EOF == c) {
          break loop;
        } else {
          err("Code point not allowed in scheme: " + c);
          break loop;
        }
        break;

       case "scheme data":
        if ("?" == c) {
          this._query = "?";
          state = "query";
        } else if ("#" == c) {
          this._fragment = "#";
          state = "fragment";
        } else {
          if (EOF != c && "	" != c && "\n" != c && "\r" != c) {
            this._schemeData += percentEscape(c);
          }
        }
        break;

       case "no scheme":
        if (!base || !isRelativeScheme(base._scheme)) {
          err("Missing scheme.");
          invalid.call(this);
        } else {
          state = "relative";
          continue;
        }
        break;

       case "relative or authority":
        if ("/" == c && "/" == input[cursor + 1]) {
          state = "authority ignore slashes";
        } else {
          err("Expected /, got: " + c);
          state = "relative";
          continue;
        }
        break;

       case "relative":
        this._isRelative = true;
        if ("file" != this._scheme) this._scheme = base._scheme;
        if (EOF == c) {
          this._host = base._host;
          this._port = base._port;
          this._path = base._path.slice();
          this._query = base._query;
          this._username = base._username;
          this._password = base._password;
          break loop;
        } else if ("/" == c || "\\" == c) {
          if ("\\" == c) err("\\ is an invalid code point.");
          state = "relative slash";
        } else if ("?" == c) {
          this._host = base._host;
          this._port = base._port;
          this._path = base._path.slice();
          this._query = "?";
          this._username = base._username;
          this._password = base._password;
          state = "query";
        } else if ("#" == c) {
          this._host = base._host;
          this._port = base._port;
          this._path = base._path.slice();
          this._query = base._query;
          this._fragment = "#";
          this._username = base._username;
          this._password = base._password;
          state = "fragment";
        } else {
          var nextC = input[cursor + 1];
          var nextNextC = input[cursor + 2];
          if ("file" != this._scheme || !ALPHA.test(c) || nextC != ":" && nextC != "|" || EOF != nextNextC && "/" != nextNextC && "\\" != nextNextC && "?" != nextNextC && "#" != nextNextC) {
            this._host = base._host;
            this._port = base._port;
            this._username = base._username;
            this._password = base._password;
            this._path = base._path.slice();
            this._path.pop();
          }
          state = "relative path";
          continue;
        }
        break;

       case "relative slash":
        if ("/" == c || "\\" == c) {
          if ("\\" == c) {
            err("\\ is an invalid code point.");
          }
          if ("file" == this._scheme) {
            state = "file host";
          } else {
            state = "authority ignore slashes";
          }
        } else {
          if ("file" != this._scheme) {
            this._host = base._host;
            this._port = base._port;
            this._username = base._username;
            this._password = base._password;
          }
          state = "relative path";
          continue;
        }
        break;

       case "authority first slash":
        if ("/" == c) {
          state = "authority second slash";
        } else {
          err("Expected '/', got: " + c);
          state = "authority ignore slashes";
          continue;
        }
        break;

       case "authority second slash":
        state = "authority ignore slashes";
        if ("/" != c) {
          err("Expected '/', got: " + c);
          continue;
        }
        break;

       case "authority ignore slashes":
        if ("/" != c && "\\" != c) {
          state = "authority";
          continue;
        } else {
          err("Expected authority, got: " + c);
        }
        break;

       case "authority":
        if ("@" == c) {
          if (seenAt) {
            err("@ already seen.");
            buffer += "%40";
          }
          seenAt = true;
          for (var i = 0; i < buffer.length; i++) {
            var cp = buffer[i];
            if ("	" == cp || "\n" == cp || "\r" == cp) {
              err("Invalid whitespace in authority.");
              continue;
            }
            if (":" == cp && null === this._password) {
              this._password = "";
              continue;
            }
            var tempC = percentEscape(cp);
            null !== this._password ? this._password += tempC : this._username += tempC;
          }
          buffer = "";
        } else if (EOF == c || "/" == c || "\\" == c || "?" == c || "#" == c) {
          cursor -= buffer.length;
          buffer = "";
          state = "host";
          continue;
        } else {
          buffer += c;
        }
        break;

       case "file host":
        if (EOF == c || "/" == c || "\\" == c || "?" == c || "#" == c) {
          if (buffer.length == 2 && ALPHA.test(buffer[0]) && (buffer[1] == ":" || buffer[1] == "|")) {
            state = "relative path";
          } else if (buffer.length == 0) {
            state = "relative path start";
          } else {
            this._host = IDNAToASCII.call(this, buffer);
            buffer = "";
            state = "relative path start";
          }
          continue;
        } else if ("	" == c || "\n" == c || "\r" == c) {
          err("Invalid whitespace in file host.");
        } else {
          buffer += c;
        }
        break;

       case "host":
       case "hostname":
        if (":" == c && !seenBracket) {
          this._host = IDNAToASCII.call(this, buffer);
          buffer = "";
          state = "port";
          if ("hostname" == stateOverride) {
            break loop;
          }
        } else if (EOF == c || "/" == c || "\\" == c || "?" == c || "#" == c) {
          this._host = IDNAToASCII.call(this, buffer);
          buffer = "";
          state = "relative path start";
          if (stateOverride) {
            break loop;
          }
          continue;
        } else if ("	" != c && "\n" != c && "\r" != c) {
          if ("[" == c) {
            seenBracket = true;
          } else if ("]" == c) {
            seenBracket = false;
          }
          buffer += c;
        } else {
          err("Invalid code point in host/hostname: " + c);
        }
        break;

       case "port":
        if (/[0-9]/.test(c)) {
          buffer += c;
        } else if (EOF == c || "/" == c || "\\" == c || "?" == c || "#" == c || stateOverride) {
          if ("" != buffer) {
            var temp = parseInt(buffer, 10);
            if (temp != relative[this._scheme]) {
              this._port = temp + "";
            }
            buffer = "";
          }
          if (stateOverride) {
            break loop;
          }
          state = "relative path start";
          continue;
        } else if ("	" == c || "\n" == c || "\r" == c) {
          err("Invalid code point in port: " + c);
        } else {
          invalid.call(this);
        }
        break;

       case "relative path start":
        if ("\\" == c) err("'\\' not allowed in path.");
        state = "relative path";
        if ("/" != c && "\\" != c) {
          continue;
        }
        break;

       case "relative path":
        if (EOF == c || "/" == c || "\\" == c || !stateOverride && ("?" == c || "#" == c)) {
          if ("\\" == c) {
            err("\\ not allowed in relative path.");
          }
          var tmp;
          if (tmp = relativePathDotMapping[buffer.toLowerCase()]) {
            buffer = tmp;
          }
          if (".." == buffer) {
            this._path.pop();
            if ("/" != c && "\\" != c) {
              this._path.push("");
            }
          } else if ("." == buffer && "/" != c && "\\" != c) {
            this._path.push("");
          } else if ("." != buffer) {
            if ("file" == this._scheme && this._path.length == 0 && buffer.length == 2 && ALPHA.test(buffer[0]) && buffer[1] == "|") {
              buffer = buffer[0] + ":";
            }
            this._path.push(buffer);
          }
          buffer = "";
          if ("?" == c) {
            this._query = "?";
            state = "query";
          } else if ("#" == c) {
            this._fragment = "#";
            state = "fragment";
          }
        } else if ("	" != c && "\n" != c && "\r" != c) {
          buffer += percentEscape(c);
        }
        break;

       case "query":
        if (!stateOverride && "#" == c) {
          this._fragment = "#";
          state = "fragment";
        } else if (EOF != c && "	" != c && "\n" != c && "\r" != c) {
          this._query += percentEscapeQuery(c);
        }
        break;

       case "fragment":
        if (EOF != c && "	" != c && "\n" != c && "\r" != c) {
          this._fragment += c;
        }
        break;
      }
      cursor++;
    }
  }
  function clear() {
    this._scheme = "";
    this._schemeData = "";
    this._username = "";
    this._password = null;
    this._host = "";
    this._port = "";
    this._path = [];
    this._query = "";
    this._fragment = "";
    this._isInvalid = false;
    this._isRelative = false;
  }
  function jURL(url, base) {
    if (base !== undefined && !(base instanceof jURL)) base = new jURL(String(base));
    this._url = url;
    clear.call(this);
    var input = url.replace(/^[ \t\r\n\f]+|[ \t\r\n\f]+$/g, "");
    parse.call(this, input, null, base);
  }
  jURL.prototype = {
    toString: function() {
      return this.href;
    },
    get href() {
      if (this._isInvalid) return this._url;
      var authority = "";
      if ("" != this._username || null != this._password) {
        authority = this._username + (null != this._password ? ":" + this._password : "") + "@";
      }
      return this.protocol + (this._isRelative ? "//" + authority + this.host : "") + this.pathname + this._query + this._fragment;
    },
    set href(href) {
      clear.call(this);
      parse.call(this, href);
    },
    get protocol() {
      return this._scheme + ":";
    },
    set protocol(protocol) {
      if (this._isInvalid) return;
      parse.call(this, protocol + ":", "scheme start");
    },
    get host() {
      return this._isInvalid ? "" : this._port ? this._host + ":" + this._port : this._host;
    },
    set host(host) {
      if (this._isInvalid || !this._isRelative) return;
      parse.call(this, host, "host");
    },
    get hostname() {
      return this._host;
    },
    set hostname(hostname) {
      if (this._isInvalid || !this._isRelative) return;
      parse.call(this, hostname, "hostname");
    },
    get port() {
      return this._port;
    },
    set port(port) {
      if (this._isInvalid || !this._isRelative) return;
      parse.call(this, port, "port");
    },
    get pathname() {
      return this._isInvalid ? "" : this._isRelative ? "/" + this._path.join("/") : this._schemeData;
    },
    set pathname(pathname) {
      if (this._isInvalid || !this._isRelative) return;
      this._path = [];
      parse.call(this, pathname, "relative path start");
    },
    get search() {
      return this._isInvalid || !this._query || "?" == this._query ? "" : this._query;
    },
    set search(search) {
      if (this._isInvalid || !this._isRelative) return;
      this._query = "?";
      if ("?" == search[0]) search = search.slice(1);
      parse.call(this, search, "query");
    },
    get hash() {
      return this._isInvalid || !this._fragment || "#" == this._fragment ? "" : this._fragment;
    },
    set hash(hash) {
      if (this._isInvalid) return;
      this._fragment = "#";
      if ("#" == hash[0]) hash = hash.slice(1);
      parse.call(this, hash, "fragment");
    },
    get origin() {
      var host;
      if (this._isInvalid || !this._scheme) {
        return "";
      }
      switch (this._scheme) {
       case "data":
       case "file":
       case "javascript":
       case "mailto":
        return "null";
      }
      host = this.host;
      if (!host) {
        return "";
      }
      return this._scheme + "://" + host;
    }
  };
  var OriginalURL = scope.URL;
  if (OriginalURL) {
    jURL.createObjectURL = function(blob) {
      return OriginalURL.createObjectURL.apply(OriginalURL, arguments);
    };
    jURL.revokeObjectURL = function(url) {
      OriginalURL.revokeObjectURL(url);
    };
  }
  scope.URL = jURL;
})(self);

if (typeof WeakMap === "undefined") {
  (function() {
    var defineProperty = Object.defineProperty;
    var counter = Date.now() % 1e9;
    var WeakMap = function() {
      this.name = "__st" + (Math.random() * 1e9 >>> 0) + (counter++ + "__");
    };
    WeakMap.prototype = {
      set: function(key, value) {
        var entry = key[this.name];
        if (entry && entry[0] === key) entry[1] = value; else defineProperty(key, this.name, {
          value: [ key, value ],
          writable: true
        });
        return this;
      },
      get: function(key) {
        var entry;
        return (entry = key[this.name]) && entry[0] === key ? entry[1] : undefined;
      },
      "delete": function(key) {
        var entry = key[this.name];
        if (!entry || entry[0] !== key) return false;
        entry[0] = entry[1] = undefined;
        return true;
      },
      has: function(key) {
        var entry = key[this.name];
        if (!entry) return false;
        return entry[0] === key;
      }
    };
    window.WeakMap = WeakMap;
  })();
}

(function(global) {
  if (global.JsMutationObserver) {
    return;
  }
  var registrationsTable = new WeakMap();
  var setImmediate;
  if (/Trident|Edge/.test(navigator.userAgent)) {
    setImmediate = setTimeout;
  } else if (window.setImmediate) {
    setImmediate = window.setImmediate;
  } else {
    var setImmediateQueue = [];
    var sentinel = String(Math.random());
    window.addEventListener("message", function(e) {
      if (e.data === sentinel) {
        var queue = setImmediateQueue;
        setImmediateQueue = [];
        queue.forEach(function(func) {
          func();
        });
      }
    });
    setImmediate = function(func) {
      setImmediateQueue.push(func);
      window.postMessage(sentinel, "*");
    };
  }
  var isScheduled = false;
  var scheduledObservers = [];
  function scheduleCallback(observer) {
    scheduledObservers.push(observer);
    if (!isScheduled) {
      isScheduled = true;
      setImmediate(dispatchCallbacks);
    }
  }
  function wrapIfNeeded(node) {
    return window.ShadowDOMPolyfill && window.ShadowDOMPolyfill.wrapIfNeeded(node) || node;
  }
  function dispatchCallbacks() {
    isScheduled = false;
    var observers = scheduledObservers;
    scheduledObservers = [];
    observers.sort(function(o1, o2) {
      return o1.uid_ - o2.uid_;
    });
    var anyNonEmpty = false;
    observers.forEach(function(observer) {
      var queue = observer.takeRecords();
      removeTransientObserversFor(observer);
      if (queue.length) {
        observer.callback_(queue, observer);
        anyNonEmpty = true;
      }
    });
    if (anyNonEmpty) dispatchCallbacks();
  }
  function removeTransientObserversFor(observer) {
    observer.nodes_.forEach(function(node) {
      var registrations = registrationsTable.get(node);
      if (!registrations) return;
      registrations.forEach(function(registration) {
        if (registration.observer === observer) registration.removeTransientObservers();
      });
    });
  }
  function forEachAncestorAndObserverEnqueueRecord(target, callback) {
    for (var node = target; node; node = node.parentNode) {
      var registrations = registrationsTable.get(node);
      if (registrations) {
        for (var j = 0; j < registrations.length; j++) {
          var registration = registrations[j];
          var options = registration.options;
          if (node !== target && !options.subtree) continue;
          var record = callback(options);
          if (record) registration.enqueue(record);
        }
      }
    }
  }
  var uidCounter = 0;
  function JsMutationObserver(callback) {
    this.callback_ = callback;
    this.nodes_ = [];
    this.records_ = [];
    this.uid_ = ++uidCounter;
  }
  JsMutationObserver.prototype = {
    observe: function(target, options) {
      target = wrapIfNeeded(target);
      if (!options.childList && !options.attributes && !options.characterData || options.attributeOldValue && !options.attributes || options.attributeFilter && options.attributeFilter.length && !options.attributes || options.characterDataOldValue && !options.characterData) {
        throw new SyntaxError();
      }
      var registrations = registrationsTable.get(target);
      if (!registrations) registrationsTable.set(target, registrations = []);
      var registration;
      for (var i = 0; i < registrations.length; i++) {
        if (registrations[i].observer === this) {
          registration = registrations[i];
          registration.removeListeners();
          registration.options = options;
          break;
        }
      }
      if (!registration) {
        registration = new Registration(this, target, options);
        registrations.push(registration);
        this.nodes_.push(target);
      }
      registration.addListeners();
    },
    disconnect: function() {
      this.nodes_.forEach(function(node) {
        var registrations = registrationsTable.get(node);
        for (var i = 0; i < registrations.length; i++) {
          var registration = registrations[i];
          if (registration.observer === this) {
            registration.removeListeners();
            registrations.splice(i, 1);
            break;
          }
        }
      }, this);
      this.records_ = [];
    },
    takeRecords: function() {
      var copyOfRecords = this.records_;
      this.records_ = [];
      return copyOfRecords;
    }
  };
  function MutationRecord(type, target) {
    this.type = type;
    this.target = target;
    this.addedNodes = [];
    this.removedNodes = [];
    this.previousSibling = null;
    this.nextSibling = null;
    this.attributeName = null;
    this.attributeNamespace = null;
    this.oldValue = null;
  }
  function copyMutationRecord(original) {
    var record = new MutationRecord(original.type, original.target);
    record.addedNodes = original.addedNodes.slice();
    record.removedNodes = original.removedNodes.slice();
    record.previousSibling = original.previousSibling;
    record.nextSibling = original.nextSibling;
    record.attributeName = original.attributeName;
    record.attributeNamespace = original.attributeNamespace;
    record.oldValue = original.oldValue;
    return record;
  }
  var currentRecord, recordWithOldValue;
  function getRecord(type, target) {
    return currentRecord = new MutationRecord(type, target);
  }
  function getRecordWithOldValue(oldValue) {
    if (recordWithOldValue) return recordWithOldValue;
    recordWithOldValue = copyMutationRecord(currentRecord);
    recordWithOldValue.oldValue = oldValue;
    return recordWithOldValue;
  }
  function clearRecords() {
    currentRecord = recordWithOldValue = undefined;
  }
  function recordRepresentsCurrentMutation(record) {
    return record === recordWithOldValue || record === currentRecord;
  }
  function selectRecord(lastRecord, newRecord) {
    if (lastRecord === newRecord) return lastRecord;
    if (recordWithOldValue && recordRepresentsCurrentMutation(lastRecord)) return recordWithOldValue;
    return null;
  }
  function Registration(observer, target, options) {
    this.observer = observer;
    this.target = target;
    this.options = options;
    this.transientObservedNodes = [];
  }
  Registration.prototype = {
    enqueue: function(record) {
      var records = this.observer.records_;
      var length = records.length;
      if (records.length > 0) {
        var lastRecord = records[length - 1];
        var recordToReplaceLast = selectRecord(lastRecord, record);
        if (recordToReplaceLast) {
          records[length - 1] = recordToReplaceLast;
          return;
        }
      } else {
        scheduleCallback(this.observer);
      }
      records[length] = record;
    },
    addListeners: function() {
      this.addListeners_(this.target);
    },
    addListeners_: function(node) {
      var options = this.options;
      if (options.attributes) node.addEventListener("DOMAttrModified", this, true);
      if (options.characterData) node.addEventListener("DOMCharacterDataModified", this, true);
      if (options.childList) node.addEventListener("DOMNodeInserted", this, true);
      if (options.childList || options.subtree) node.addEventListener("DOMNodeRemoved", this, true);
    },
    removeListeners: function() {
      this.removeListeners_(this.target);
    },
    removeListeners_: function(node) {
      var options = this.options;
      if (options.attributes) node.removeEventListener("DOMAttrModified", this, true);
      if (options.characterData) node.removeEventListener("DOMCharacterDataModified", this, true);
      if (options.childList) node.removeEventListener("DOMNodeInserted", this, true);
      if (options.childList || options.subtree) node.removeEventListener("DOMNodeRemoved", this, true);
    },
    addTransientObserver: function(node) {
      if (node === this.target) return;
      this.addListeners_(node);
      this.transientObservedNodes.push(node);
      var registrations = registrationsTable.get(node);
      if (!registrations) registrationsTable.set(node, registrations = []);
      registrations.push(this);
    },
    removeTransientObservers: function() {
      var transientObservedNodes = this.transientObservedNodes;
      this.transientObservedNodes = [];
      transientObservedNodes.forEach(function(node) {
        this.removeListeners_(node);
        var registrations = registrationsTable.get(node);
        for (var i = 0; i < registrations.length; i++) {
          if (registrations[i] === this) {
            registrations.splice(i, 1);
            break;
          }
        }
      }, this);
    },
    handleEvent: function(e) {
      e.stopImmediatePropagation();
      switch (e.type) {
       case "DOMAttrModified":
        var name = e.attrName;
        var namespace = e.relatedNode.namespaceURI;
        var target = e.target;
        var record = new getRecord("attributes", target);
        record.attributeName = name;
        record.attributeNamespace = namespace;
        var oldValue = e.attrChange === MutationEvent.ADDITION ? null : e.prevValue;
        forEachAncestorAndObserverEnqueueRecord(target, function(options) {
          if (!options.attributes) return;
          if (options.attributeFilter && options.attributeFilter.length && options.attributeFilter.indexOf(name) === -1 && options.attributeFilter.indexOf(namespace) === -1) {
            return;
          }
          if (options.attributeOldValue) return getRecordWithOldValue(oldValue);
          return record;
        });
        break;

       case "DOMCharacterDataModified":
        var target = e.target;
        var record = getRecord("characterData", target);
        var oldValue = e.prevValue;
        forEachAncestorAndObserverEnqueueRecord(target, function(options) {
          if (!options.characterData) return;
          if (options.characterDataOldValue) return getRecordWithOldValue(oldValue);
          return record;
        });
        break;

       case "DOMNodeRemoved":
        this.addTransientObserver(e.target);

       case "DOMNodeInserted":
        var changedNode = e.target;
        var addedNodes, removedNodes;
        if (e.type === "DOMNodeInserted") {
          addedNodes = [ changedNode ];
          removedNodes = [];
        } else {
          addedNodes = [];
          removedNodes = [ changedNode ];
        }
        var previousSibling = changedNode.previousSibling;
        var nextSibling = changedNode.nextSibling;
        var record = getRecord("childList", e.target.parentNode);
        record.addedNodes = addedNodes;
        record.removedNodes = removedNodes;
        record.previousSibling = previousSibling;
        record.nextSibling = nextSibling;
        forEachAncestorAndObserverEnqueueRecord(e.relatedNode, function(options) {
          if (!options.childList) return;
          return record;
        });
      }
      clearRecords();
    }
  };
  global.JsMutationObserver = JsMutationObserver;
  if (!global.MutationObserver) {
    global.MutationObserver = JsMutationObserver;
    JsMutationObserver._isPolyfilled = true;
  }
})(self);

(function() {
  var needsTemplate = typeof HTMLTemplateElement === "undefined";
  var needsCloning = function() {
    if (!needsTemplate) {
      var frag = document.createDocumentFragment();
      var t = document.createElement("template");
      frag.appendChild(t);
      t.content.appendChild(document.createElement("div"));
      var clone = frag.cloneNode(true);
      return clone.firstChild.content.childNodes.length === 0;
    }
  }();
  var TEMPLATE_TAG = "template";
  var TemplateImpl = function() {};
  if (needsTemplate) {
    var contentDoc = document.implementation.createHTMLDocument("template");
    var canDecorate = true;
    var templateStyle = document.createElement("style");
    templateStyle.textContent = TEMPLATE_TAG + "{display:none;}";
    var head = document.head;
    head.insertBefore(templateStyle, head.firstElementChild);
    TemplateImpl.prototype = Object.create(HTMLElement.prototype);
    TemplateImpl.decorate = function(template) {
      if (template.content) {
        return;
      }
      template.content = contentDoc.createDocumentFragment();
      var child;
      while (child = template.firstChild) {
        template.content.appendChild(child);
      }
      if (canDecorate) {
        try {
          Object.defineProperty(template, "innerHTML", {
            get: function() {
              var o = "";
              for (var e = this.content.firstChild; e; e = e.nextSibling) {
                o += e.outerHTML || escapeData(e.data);
              }
              return o;
            },
            set: function(text) {
              contentDoc.body.innerHTML = text;
              TemplateImpl.bootstrap(contentDoc);
              while (this.content.firstChild) {
                this.content.removeChild(this.content.firstChild);
              }
              while (contentDoc.body.firstChild) {
                this.content.appendChild(contentDoc.body.firstChild);
              }
            },
            configurable: true
          });
          template.cloneNode = function(deep) {
            return TemplateImpl.cloneNode(this, deep);
          };
        } catch (err) {
          canDecorate = false;
        }
      }
      TemplateImpl.bootstrap(template.content);
    };
    TemplateImpl.bootstrap = function(doc) {
      var templates = doc.querySelectorAll(TEMPLATE_TAG);
      for (var i = 0, l = templates.length, t; i < l && (t = templates[i]); i++) {
        TemplateImpl.decorate(t);
      }
    };
    document.addEventListener("DOMContentLoaded", function() {
      TemplateImpl.bootstrap(document);
    });
    var createElement = document.createElement;
    document.createElement = function() {
      "use strict";
      var el = createElement.apply(document, arguments);
      if (el.localName == "template") {
        TemplateImpl.decorate(el);
      }
      return el;
    };
    var escapeDataRegExp = /[&\u00A0<>]/g;
    function escapeReplace(c) {
      switch (c) {
       case "&":
        return "&amp;";

       case "<":
        return "&lt;";

       case ">":
        return "&gt;";

       case " ":
        return "&nbsp;";
      }
    }
    function escapeData(s) {
      return s.replace(escapeDataRegExp, escapeReplace);
    }
  }
  if (needsTemplate || needsCloning) {
    var nativeCloneNode = Node.prototype.cloneNode;
    TemplateImpl.cloneNode = function(template, deep) {
      var clone = nativeCloneNode.call(template);
      if (this.decorate) {
        this.decorate(clone);
      }
      if (deep) {
        clone.content.appendChild(nativeCloneNode.call(template.content, true));
        this.fixClonedDom(clone.content, template.content);
      }
      return clone;
    };
    TemplateImpl.fixClonedDom = function(clone, source) {
      var s$ = source.querySelectorAll(TEMPLATE_TAG);
      var t$ = clone.querySelectorAll(TEMPLATE_TAG);
      for (var i = 0, l = t$.length, t, s; i < l; i++) {
        s = s$[i];
        t = t$[i];
        if (this.decorate) {
          this.decorate(s);
        }
        t.parentNode.replaceChild(s.cloneNode(true), t);
      }
    };
    var originalImportNode = document.importNode;
    Node.prototype.cloneNode = function(deep) {
      var dom = nativeCloneNode.call(this, deep);
      if (deep) {
        TemplateImpl.fixClonedDom(dom, this);
      }
      return dom;
    };
    document.importNode = function(element, deep) {
      if (element.localName === TEMPLATE_TAG) {
        return TemplateImpl.cloneNode(element, deep);
      } else {
        var dom = originalImportNode.call(document, element, deep);
        if (deep) {
          TemplateImpl.fixClonedDom(dom, element);
        }
        return dom;
      }
    };
    if (needsCloning) {
      HTMLTemplateElement.prototype.cloneNode = function(deep) {
        return TemplateImpl.cloneNode(this, deep);
      };
    }
  }
  if (needsTemplate) {
    HTMLTemplateElement = TemplateImpl;
  }
})();

(function(scope) {
  "use strict";
  if (!window.performance) {
    var start = Date.now();
    window.performance = {
      now: function() {
        return Date.now() - start;
      }
    };
  }
  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function() {
      var nativeRaf = window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame;
      return nativeRaf ? function(callback) {
        return nativeRaf(function() {
          callback(performance.now());
        });
      } : function(callback) {
        return window.setTimeout(callback, 1e3 / 60);
      };
    }();
  }
  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function() {
      return window.webkitCancelAnimationFrame || window.mozCancelAnimationFrame || function(id) {
        clearTimeout(id);
      };
    }();
  }
  var workingDefaultPrevented = function() {
    var e = document.createEvent("Event");
    e.initEvent("foo", true, true);
    e.preventDefault();
    return e.defaultPrevented;
  }();
  if (!workingDefaultPrevented) {
    var origPreventDefault = Event.prototype.preventDefault;
    Event.prototype.preventDefault = function() {
      if (!this.cancelable) {
        return;
      }
      origPreventDefault.call(this);
      Object.defineProperty(this, "defaultPrevented", {
        get: function() {
          return true;
        },
        configurable: true
      });
    };
  }
  var isIE = /Trident/.test(navigator.userAgent);
  if (!window.CustomEvent || isIE && typeof window.CustomEvent !== "function") {
    window.CustomEvent = function(inType, params) {
      params = params || {};
      var e = document.createEvent("CustomEvent");
      e.initCustomEvent(inType, Boolean(params.bubbles), Boolean(params.cancelable), params.detail);
      return e;
    };
    window.CustomEvent.prototype = window.Event.prototype;
  }
  if (!window.Event || isIE && typeof window.Event !== "function") {
    var origEvent = window.Event;
    window.Event = function(inType, params) {
      params = params || {};
      var e = document.createEvent("Event");
      e.initEvent(inType, Boolean(params.bubbles), Boolean(params.cancelable));
      return e;
    };
    window.Event.prototype = origEvent.prototype;
  }
})(window.WebComponents);

window.HTMLImports = window.HTMLImports || {
  flags: {}
};

(function(scope) {
  var IMPORT_LINK_TYPE = "import";
  var useNative = Boolean(IMPORT_LINK_TYPE in document.createElement("link"));
  var hasShadowDOMPolyfill = Boolean(window.ShadowDOMPolyfill);
  var wrap = function(node) {
    return hasShadowDOMPolyfill ? window.ShadowDOMPolyfill.wrapIfNeeded(node) : node;
  };
  var rootDocument = wrap(document);
  var currentScriptDescriptor = {
    get: function() {
      var script = window.HTMLImports.currentScript || document.currentScript || (document.readyState !== "complete" ? document.scripts[document.scripts.length - 1] : null);
      return wrap(script);
    },
    configurable: true
  };
  Object.defineProperty(document, "_currentScript", currentScriptDescriptor);
  Object.defineProperty(rootDocument, "_currentScript", currentScriptDescriptor);
  var isIE = /Trident/.test(navigator.userAgent);
  function whenReady(callback, doc) {
    doc = doc || rootDocument;
    whenDocumentReady(function() {
      watchImportsLoad(callback, doc);
    }, doc);
  }
  var requiredReadyState = isIE ? "complete" : "interactive";
  var READY_EVENT = "readystatechange";
  function isDocumentReady(doc) {
    return doc.readyState === "complete" || doc.readyState === requiredReadyState;
  }
  function whenDocumentReady(callback, doc) {
    if (!isDocumentReady(doc)) {
      var checkReady = function() {
        if (doc.readyState === "complete" || doc.readyState === requiredReadyState) {
          doc.removeEventListener(READY_EVENT, checkReady);
          whenDocumentReady(callback, doc);
        }
      };
      doc.addEventListener(READY_EVENT, checkReady);
    } else if (callback) {
      callback();
    }
  }
  function markTargetLoaded(event) {
    event.target.__loaded = true;
  }
  function watchImportsLoad(callback, doc) {
    var imports = doc.querySelectorAll("link[rel=import]");
    var parsedCount = 0, importCount = imports.length, newImports = [], errorImports = [];
    function checkDone() {
      if (parsedCount == importCount && callback) {
        callback({
          allImports: imports,
          loadedImports: newImports,
          errorImports: errorImports
        });
      }
    }
    function loadedImport(e) {
      markTargetLoaded(e);
      newImports.push(this);
      parsedCount++;
      checkDone();
    }
    function errorLoadingImport(e) {
      errorImports.push(this);
      parsedCount++;
      checkDone();
    }
    if (importCount) {
      for (var i = 0, imp; i < importCount && (imp = imports[i]); i++) {
        if (isImportLoaded(imp)) {
          newImports.push(this);
          parsedCount++;
          checkDone();
        } else {
          imp.addEventListener("load", loadedImport);
          imp.addEventListener("error", errorLoadingImport);
        }
      }
    } else {
      checkDone();
    }
  }
  function isImportLoaded(link) {
    return useNative ? link.__loaded || link.import && link.import.readyState !== "loading" : link.__importParsed;
  }
  if (useNative) {
    new MutationObserver(function(mxns) {
      for (var i = 0, l = mxns.length, m; i < l && (m = mxns[i]); i++) {
        if (m.addedNodes) {
          handleImports(m.addedNodes);
        }
      }
    }).observe(document.head, {
      childList: true
    });
    function handleImports(nodes) {
      for (var i = 0, l = nodes.length, n; i < l && (n = nodes[i]); i++) {
        if (isImport(n)) {
          handleImport(n);
        }
      }
    }
    function isImport(element) {
      return element.localName === "link" && element.rel === "import";
    }
    function handleImport(element) {
      var loaded = element.import;
      if (loaded) {
        markTargetLoaded({
          target: element
        });
      } else {
        element.addEventListener("load", markTargetLoaded);
        element.addEventListener("error", markTargetLoaded);
      }
    }
    (function() {
      if (document.readyState === "loading") {
        var imports = document.querySelectorAll("link[rel=import]");
        for (var i = 0, l = imports.length, imp; i < l && (imp = imports[i]); i++) {
          handleImport(imp);
        }
      }
    })();
  }
  whenReady(function(detail) {
    window.HTMLImports.ready = true;
    window.HTMLImports.readyTime = new Date().getTime();
    var evt = rootDocument.createEvent("CustomEvent");
    evt.initCustomEvent("HTMLImportsLoaded", true, true, detail);
    rootDocument.dispatchEvent(evt);
  });
  scope.IMPORT_LINK_TYPE = IMPORT_LINK_TYPE;
  scope.useNative = useNative;
  scope.rootDocument = rootDocument;
  scope.whenReady = whenReady;
  scope.isIE = isIE;
})(window.HTMLImports);

(function(scope) {
  var modules = [];
  var addModule = function(module) {
    modules.push(module);
  };
  var initializeModules = function() {
    modules.forEach(function(module) {
      module(scope);
    });
  };
  scope.addModule = addModule;
  scope.initializeModules = initializeModules;
})(window.HTMLImports);

window.HTMLImports.addModule(function(scope) {
  var CSS_URL_REGEXP = /(url\()([^)]*)(\))/g;
  var CSS_IMPORT_REGEXP = /(@import[\s]+(?!url\())([^;]*)(;)/g;
  var path = {
    resolveUrlsInStyle: function(style, linkUrl) {
      var doc = style.ownerDocument;
      var resolver = doc.createElement("a");
      style.textContent = this.resolveUrlsInCssText(style.textContent, linkUrl, resolver);
      return style;
    },
    resolveUrlsInCssText: function(cssText, linkUrl, urlObj) {
      var r = this.replaceUrls(cssText, urlObj, linkUrl, CSS_URL_REGEXP);
      r = this.replaceUrls(r, urlObj, linkUrl, CSS_IMPORT_REGEXP);
      return r;
    },
    replaceUrls: function(text, urlObj, linkUrl, regexp) {
      return text.replace(regexp, function(m, pre, url, post) {
        var urlPath = url.replace(/["']/g, "");
        if (linkUrl) {
          urlPath = new URL(urlPath, linkUrl).href;
        }
        urlObj.href = urlPath;
        urlPath = urlObj.href;
        return pre + "'" + urlPath + "'" + post;
      });
    }
  };
  scope.path = path;
});

window.HTMLImports.addModule(function(scope) {
  var xhr = {
    async: true,
    ok: function(request) {
      return request.status >= 200 && request.status < 300 || request.status === 304 || request.status === 0;
    },
    load: function(url, next, nextContext) {
      var request = new XMLHttpRequest();
      if (scope.flags.debug || scope.flags.bust) {
        url += "?" + Math.random();
      }
      request.open("GET", url, xhr.async);
      request.addEventListener("readystatechange", function(e) {
        if (request.readyState === 4) {
          var redirectedUrl = null;
          try {
            var locationHeader = request.getResponseHeader("Location");
            if (locationHeader) {
              redirectedUrl = locationHeader.substr(0, 1) === "/" ? location.origin + locationHeader : locationHeader;
            }
          } catch (e) {
            console.error(e.message);
          }
          next.call(nextContext, !xhr.ok(request) && request, request.response || request.responseText, redirectedUrl);
        }
      });
      request.send();
      return request;
    },
    loadDocument: function(url, next, nextContext) {
      this.load(url, next, nextContext).responseType = "document";
    }
  };
  scope.xhr = xhr;
});

window.HTMLImports.addModule(function(scope) {
  var xhr = scope.xhr;
  var flags = scope.flags;
  var Loader = function(onLoad, onComplete) {
    this.cache = {};
    this.onload = onLoad;
    this.oncomplete = onComplete;
    this.inflight = 0;
    this.pending = {};
  };
  Loader.prototype = {
    addNodes: function(nodes) {
      this.inflight += nodes.length;
      for (var i = 0, l = nodes.length, n; i < l && (n = nodes[i]); i++) {
        this.require(n);
      }
      this.checkDone();
    },
    addNode: function(node) {
      this.inflight++;
      this.require(node);
      this.checkDone();
    },
    require: function(elt) {
      var url = elt.src || elt.href;
      elt.__nodeUrl = url;
      if (!this.dedupe(url, elt)) {
        this.fetch(url, elt);
      }
    },
    dedupe: function(url, elt) {
      if (this.pending[url]) {
        this.pending[url].push(elt);
        return true;
      }
      var resource;
      if (this.cache[url]) {
        this.onload(url, elt, this.cache[url]);
        this.tail();
        return true;
      }
      this.pending[url] = [ elt ];
      return false;
    },
    fetch: function(url, elt) {
      flags.load && console.log("fetch", url, elt);
      if (!url) {
        setTimeout(function() {
          this.receive(url, elt, {
            error: "href must be specified"
          }, null);
        }.bind(this), 0);
      } else if (url.match(/^data:/)) {
        var pieces = url.split(",");
        var header = pieces[0];
        var body = pieces[1];
        if (header.indexOf(";base64") > -1) {
          body = atob(body);
        } else {
          body = decodeURIComponent(body);
        }
        setTimeout(function() {
          this.receive(url, elt, null, body);
        }.bind(this), 0);
      } else {
        var receiveXhr = function(err, resource, redirectedUrl) {
          this.receive(url, elt, err, resource, redirectedUrl);
        }.bind(this);
        xhr.load(url, receiveXhr);
      }
    },
    receive: function(url, elt, err, resource, redirectedUrl) {
      this.cache[url] = resource;
      var $p = this.pending[url];
      for (var i = 0, l = $p.length, p; i < l && (p = $p[i]); i++) {
        this.onload(url, p, resource, err, redirectedUrl);
        this.tail();
      }
      this.pending[url] = null;
    },
    tail: function() {
      --this.inflight;
      this.checkDone();
    },
    checkDone: function() {
      if (!this.inflight) {
        this.oncomplete();
      }
    }
  };
  scope.Loader = Loader;
});

window.HTMLImports.addModule(function(scope) {
  var Observer = function(addCallback) {
    this.addCallback = addCallback;
    this.mo = new MutationObserver(this.handler.bind(this));
  };
  Observer.prototype = {
    handler: function(mutations) {
      for (var i = 0, l = mutations.length, m; i < l && (m = mutations[i]); i++) {
        if (m.type === "childList" && m.addedNodes.length) {
          this.addedNodes(m.addedNodes);
        }
      }
    },
    addedNodes: function(nodes) {
      if (this.addCallback) {
        this.addCallback(nodes);
      }
      for (var i = 0, l = nodes.length, n, loading; i < l && (n = nodes[i]); i++) {
        if (n.children && n.children.length) {
          this.addedNodes(n.children);
        }
      }
    },
    observe: function(root) {
      this.mo.observe(root, {
        childList: true,
        subtree: true
      });
    }
  };
  scope.Observer = Observer;
});

window.HTMLImports.addModule(function(scope) {
  var path = scope.path;
  var rootDocument = scope.rootDocument;
  var flags = scope.flags;
  var isIE = scope.isIE;
  var IMPORT_LINK_TYPE = scope.IMPORT_LINK_TYPE;
  var IMPORT_SELECTOR = "link[rel=" + IMPORT_LINK_TYPE + "]";
  var importParser = {
    documentSelectors: IMPORT_SELECTOR,
    importsSelectors: [ IMPORT_SELECTOR, "link[rel=stylesheet]:not([type])", "style:not([type])", "script:not([type])", 'script[type="application/javascript"]', 'script[type="text/javascript"]' ].join(","),
    map: {
      link: "parseLink",
      script: "parseScript",
      style: "parseStyle"
    },
    dynamicElements: [],
    parseNext: function() {
      var next = this.nextToParse();
      if (next) {
        this.parse(next);
      }
    },
    parse: function(elt) {
      if (this.isParsed(elt)) {
        flags.parse && console.log("[%s] is already parsed", elt.localName);
        return;
      }
      var fn = this[this.map[elt.localName]];
      if (fn) {
        this.markParsing(elt);
        fn.call(this, elt);
      }
    },
    parseDynamic: function(elt, quiet) {
      this.dynamicElements.push(elt);
      if (!quiet) {
        this.parseNext();
      }
    },
    markParsing: function(elt) {
      flags.parse && console.log("parsing", elt);
      this.parsingElement = elt;
    },
    markParsingComplete: function(elt) {
      elt.__importParsed = true;
      this.markDynamicParsingComplete(elt);
      if (elt.__importElement) {
        elt.__importElement.__importParsed = true;
        this.markDynamicParsingComplete(elt.__importElement);
      }
      this.parsingElement = null;
      flags.parse && console.log("completed", elt);
    },
    markDynamicParsingComplete: function(elt) {
      var i = this.dynamicElements.indexOf(elt);
      if (i >= 0) {
        this.dynamicElements.splice(i, 1);
      }
    },
    parseImport: function(elt) {
      elt.import = elt.__doc;
      if (window.HTMLImports.__importsParsingHook) {
        window.HTMLImports.__importsParsingHook(elt);
      }
      if (elt.import) {
        elt.import.__importParsed = true;
      }
      this.markParsingComplete(elt);
      if (elt.__resource && !elt.__error) {
        elt.dispatchEvent(new CustomEvent("load", {
          bubbles: false
        }));
      } else {
        elt.dispatchEvent(new CustomEvent("error", {
          bubbles: false
        }));
      }
      if (elt.__pending) {
        var fn;
        while (elt.__pending.length) {
          fn = elt.__pending.shift();
          if (fn) {
            fn({
              target: elt
            });
          }
        }
      }
      this.parseNext();
    },
    parseLink: function(linkElt) {
      if (nodeIsImport(linkElt)) {
        this.parseImport(linkElt);
      } else {
        linkElt.href = linkElt.href;
        this.parseGeneric(linkElt);
      }
    },
    parseStyle: function(elt) {
      var src = elt;
      elt = cloneStyle(elt);
      src.__appliedElement = elt;
      elt.__importElement = src;
      this.parseGeneric(elt);
    },
    parseGeneric: function(elt) {
      this.trackElement(elt);
      this.addElementToDocument(elt);
    },
    rootImportForElement: function(elt) {
      var n = elt;
      while (n.ownerDocument.__importLink) {
        n = n.ownerDocument.__importLink;
      }
      return n;
    },
    addElementToDocument: function(elt) {
      var port = this.rootImportForElement(elt.__importElement || elt);
      port.parentNode.insertBefore(elt, port);
    },
    trackElement: function(elt, callback) {
      var self = this;
      var done = function(e) {
        elt.removeEventListener("load", done);
        elt.removeEventListener("error", done);
        if (callback) {
          callback(e);
        }
        self.markParsingComplete(elt);
        self.parseNext();
      };
      elt.addEventListener("load", done);
      elt.addEventListener("error", done);
      if (isIE && elt.localName === "style") {
        var fakeLoad = false;
        if (elt.textContent.indexOf("@import") == -1) {
          fakeLoad = true;
        } else if (elt.sheet) {
          fakeLoad = true;
          var csr = elt.sheet.cssRules;
          var len = csr ? csr.length : 0;
          for (var i = 0, r; i < len && (r = csr[i]); i++) {
            if (r.type === CSSRule.IMPORT_RULE) {
              fakeLoad = fakeLoad && Boolean(r.styleSheet);
            }
          }
        }
        if (fakeLoad) {
          setTimeout(function() {
            elt.dispatchEvent(new CustomEvent("load", {
              bubbles: false
            }));
          });
        }
      }
    },
    parseScript: function(scriptElt) {
      var script = document.createElement("script");
      script.__importElement = scriptElt;
      script.src = scriptElt.src ? scriptElt.src : generateScriptDataUrl(scriptElt);
      scope.currentScript = scriptElt;
      this.trackElement(script, function(e) {
        if (script.parentNode) {
          script.parentNode.removeChild(script);
        }
        scope.currentScript = null;
      });
      this.addElementToDocument(script);
    },
    nextToParse: function() {
      this._mayParse = [];
      return !this.parsingElement && (this.nextToParseInDoc(rootDocument) || this.nextToParseDynamic());
    },
    nextToParseInDoc: function(doc, link) {
      if (doc && this._mayParse.indexOf(doc) < 0) {
        this._mayParse.push(doc);
        var nodes = doc.querySelectorAll(this.parseSelectorsForNode(doc));
        for (var i = 0, l = nodes.length, n; i < l && (n = nodes[i]); i++) {
          if (!this.isParsed(n)) {
            if (this.hasResource(n)) {
              return nodeIsImport(n) ? this.nextToParseInDoc(n.__doc, n) : n;
            } else {
              return;
            }
          }
        }
      }
      return link;
    },
    nextToParseDynamic: function() {
      return this.dynamicElements[0];
    },
    parseSelectorsForNode: function(node) {
      var doc = node.ownerDocument || node;
      return doc === rootDocument ? this.documentSelectors : this.importsSelectors;
    },
    isParsed: function(node) {
      return node.__importParsed;
    },
    needsDynamicParsing: function(elt) {
      return this.dynamicElements.indexOf(elt) >= 0;
    },
    hasResource: function(node) {
      if (nodeIsImport(node) && node.__doc === undefined) {
        return false;
      }
      return true;
    }
  };
  function nodeIsImport(elt) {
    return elt.localName === "link" && elt.rel === IMPORT_LINK_TYPE;
  }
  function generateScriptDataUrl(script) {
    var scriptContent = generateScriptContent(script);
    return "data:text/javascript;charset=utf-8," + encodeURIComponent(scriptContent);
  }
  function generateScriptContent(script) {
    return script.textContent + generateSourceMapHint(script);
  }
  function generateSourceMapHint(script) {
    var owner = script.ownerDocument;
    owner.__importedScripts = owner.__importedScripts || 0;
    var moniker = script.ownerDocument.baseURI;
    var num = owner.__importedScripts ? "-" + owner.__importedScripts : "";
    owner.__importedScripts++;
    return "\n//# sourceURL=" + moniker + num + ".js\n";
  }
  function cloneStyle(style) {
    var clone = style.ownerDocument.createElement("style");
    clone.textContent = style.textContent;
    path.resolveUrlsInStyle(clone);
    return clone;
  }
  scope.parser = importParser;
  scope.IMPORT_SELECTOR = IMPORT_SELECTOR;
});

window.HTMLImports.addModule(function(scope) {
  var flags = scope.flags;
  var IMPORT_LINK_TYPE = scope.IMPORT_LINK_TYPE;
  var IMPORT_SELECTOR = scope.IMPORT_SELECTOR;
  var rootDocument = scope.rootDocument;
  var Loader = scope.Loader;
  var Observer = scope.Observer;
  var parser = scope.parser;
  var importer = {
    documents: {},
    documentPreloadSelectors: IMPORT_SELECTOR,
    importsPreloadSelectors: [ IMPORT_SELECTOR ].join(","),
    loadNode: function(node) {
      importLoader.addNode(node);
    },
    loadSubtree: function(parent) {
      var nodes = this.marshalNodes(parent);
      importLoader.addNodes(nodes);
    },
    marshalNodes: function(parent) {
      return parent.querySelectorAll(this.loadSelectorsForNode(parent));
    },
    loadSelectorsForNode: function(node) {
      var doc = node.ownerDocument || node;
      return doc === rootDocument ? this.documentPreloadSelectors : this.importsPreloadSelectors;
    },
    loaded: function(url, elt, resource, err, redirectedUrl) {
      flags.load && console.log("loaded", url, elt);
      elt.__resource = resource;
      elt.__error = err;
      if (isImportLink(elt)) {
        var doc = this.documents[url];
        if (doc === undefined) {
          doc = err ? null : makeDocument(resource, redirectedUrl || url);
          if (doc) {
            doc.__importLink = elt;
            this.bootDocument(doc);
          }
          this.documents[url] = doc;
        }
        elt.__doc = doc;
      }
      parser.parseNext();
    },
    bootDocument: function(doc) {
      this.loadSubtree(doc);
      this.observer.observe(doc);
      parser.parseNext();
    },
    loadedAll: function() {
      parser.parseNext();
    }
  };
  var importLoader = new Loader(importer.loaded.bind(importer), importer.loadedAll.bind(importer));
  importer.observer = new Observer();
  function isImportLink(elt) {
    return isLinkRel(elt, IMPORT_LINK_TYPE);
  }
  function isLinkRel(elt, rel) {
    return elt.localName === "link" && elt.getAttribute("rel") === rel;
  }
  function hasBaseURIAccessor(doc) {
    return !!Object.getOwnPropertyDescriptor(doc, "baseURI");
  }
  function makeDocument(resource, url) {
    var doc = document.implementation.createHTMLDocument(IMPORT_LINK_TYPE);
    doc._URL = url;
    var base = doc.createElement("base");
    base.setAttribute("href", url);
    if (!doc.baseURI && !hasBaseURIAccessor(doc)) {
      Object.defineProperty(doc, "baseURI", {
        value: url
      });
    }
    var meta = doc.createElement("meta");
    meta.setAttribute("charset", "utf-8");
    doc.head.appendChild(meta);
    doc.head.appendChild(base);
    doc.body.innerHTML = resource;
    if (window.HTMLTemplateElement && HTMLTemplateElement.bootstrap) {
      HTMLTemplateElement.bootstrap(doc);
    }
    return doc;
  }
  if (!document.baseURI) {
    var baseURIDescriptor = {
      get: function() {
        var base = document.querySelector("base");
        return base ? base.href : window.location.href;
      },
      configurable: true
    };
    Object.defineProperty(document, "baseURI", baseURIDescriptor);
    Object.defineProperty(rootDocument, "baseURI", baseURIDescriptor);
  }
  scope.importer = importer;
  scope.importLoader = importLoader;
});

window.HTMLImports.addModule(function(scope) {
  var parser = scope.parser;
  var importer = scope.importer;
  var dynamic = {
    added: function(nodes) {
      var owner, parsed, loading;
      for (var i = 0, l = nodes.length, n; i < l && (n = nodes[i]); i++) {
        if (!owner) {
          owner = n.ownerDocument;
          parsed = parser.isParsed(owner);
        }
        loading = this.shouldLoadNode(n);
        if (loading) {
          importer.loadNode(n);
        }
        if (this.shouldParseNode(n) && parsed) {
          parser.parseDynamic(n, loading);
        }
      }
    },
    shouldLoadNode: function(node) {
      return node.nodeType === 1 && matches.call(node, importer.loadSelectorsForNode(node));
    },
    shouldParseNode: function(node) {
      return node.nodeType === 1 && matches.call(node, parser.parseSelectorsForNode(node));
    }
  };
  importer.observer.addCallback = dynamic.added.bind(dynamic);
  var matches = HTMLElement.prototype.matches || HTMLElement.prototype.matchesSelector || HTMLElement.prototype.webkitMatchesSelector || HTMLElement.prototype.mozMatchesSelector || HTMLElement.prototype.msMatchesSelector;
});

(function(scope) {
  var initializeModules = scope.initializeModules;
  var isIE = scope.isIE;
  if (scope.useNative) {
    return;
  }
  initializeModules();
  var rootDocument = scope.rootDocument;
  function bootstrap() {
    window.HTMLImports.importer.bootDocument(rootDocument);
  }
  if (document.readyState === "complete" || document.readyState === "interactive" && !window.attachEvent) {
    bootstrap();
  } else {
    document.addEventListener("DOMContentLoaded", bootstrap);
  }
})(window.HTMLImports);

window.CustomElements = window.CustomElements || {
  flags: {}
};

(function(scope) {
  var flags = scope.flags;
  var modules = [];
  var addModule = function(module) {
    modules.push(module);
  };
  var initializeModules = function() {
    modules.forEach(function(module) {
      module(scope);
    });
  };
  scope.addModule = addModule;
  scope.initializeModules = initializeModules;
  scope.hasNative = Boolean(document.registerElement);
  scope.isIE = /Trident/.test(navigator.userAgent);
  scope.useNative = !flags.register && scope.hasNative && !window.ShadowDOMPolyfill && (!window.HTMLImports || window.HTMLImports.useNative);
})(window.CustomElements);

window.CustomElements.addModule(function(scope) {
  var IMPORT_LINK_TYPE = window.HTMLImports ? window.HTMLImports.IMPORT_LINK_TYPE : "none";
  function forSubtree(node, cb) {
    findAllElements(node, function(e) {
      if (cb(e)) {
        return true;
      }
      forRoots(e, cb);
    });
    forRoots(node, cb);
  }
  function findAllElements(node, find, data) {
    var e = node.firstElementChild;
    if (!e) {
      e = node.firstChild;
      while (e && e.nodeType !== Node.ELEMENT_NODE) {
        e = e.nextSibling;
      }
    }
    while (e) {
      if (find(e, data) !== true) {
        findAllElements(e, find, data);
      }
      e = e.nextElementSibling;
    }
    return null;
  }
  function forRoots(node, cb) {
    var root = node.shadowRoot;
    while (root) {
      forSubtree(root, cb);
      root = root.olderShadowRoot;
    }
  }
  function forDocumentTree(doc, cb) {
    _forDocumentTree(doc, cb, []);
  }
  function _forDocumentTree(doc, cb, processingDocuments) {
    doc = window.wrap(doc);
    if (processingDocuments.indexOf(doc) >= 0) {
      return;
    }
    processingDocuments.push(doc);
    var imports = doc.querySelectorAll("link[rel=" + IMPORT_LINK_TYPE + "]");
    for (var i = 0, l = imports.length, n; i < l && (n = imports[i]); i++) {
      if (n.import) {
        _forDocumentTree(n.import, cb, processingDocuments);
      }
    }
    cb(doc);
  }
  scope.forDocumentTree = forDocumentTree;
  scope.forSubtree = forSubtree;
});

window.CustomElements.addModule(function(scope) {
  var flags = scope.flags;
  var forSubtree = scope.forSubtree;
  var forDocumentTree = scope.forDocumentTree;
  function addedNode(node, isAttached) {
    return added(node, isAttached) || addedSubtree(node, isAttached);
  }
  function added(node, isAttached) {
    if (scope.upgrade(node, isAttached)) {
      return true;
    }
    if (isAttached) {
      attached(node);
    }
  }
  function addedSubtree(node, isAttached) {
    forSubtree(node, function(e) {
      if (added(e, isAttached)) {
        return true;
      }
    });
  }
  var hasThrottledAttached = window.MutationObserver._isPolyfilled && flags["throttle-attached"];
  scope.hasPolyfillMutations = hasThrottledAttached;
  scope.hasThrottledAttached = hasThrottledAttached;
  var isPendingMutations = false;
  var pendingMutations = [];
  function deferMutation(fn) {
    pendingMutations.push(fn);
    if (!isPendingMutations) {
      isPendingMutations = true;
      setTimeout(takeMutations);
    }
  }
  function takeMutations() {
    isPendingMutations = false;
    var $p = pendingMutations;
    for (var i = 0, l = $p.length, p; i < l && (p = $p[i]); i++) {
      p();
    }
    pendingMutations = [];
  }
  function attached(element) {
    if (hasThrottledAttached) {
      deferMutation(function() {
        _attached(element);
      });
    } else {
      _attached(element);
    }
  }
  function _attached(element) {
    if (element.__upgraded__ && !element.__attached) {
      element.__attached = true;
      if (element.attachedCallback) {
        element.attachedCallback();
      }
    }
  }
  function detachedNode(node) {
    detached(node);
    forSubtree(node, function(e) {
      detached(e);
    });
  }
  function detached(element) {
    if (hasThrottledAttached) {
      deferMutation(function() {
        _detached(element);
      });
    } else {
      _detached(element);
    }
  }
  function _detached(element) {
    if (element.__upgraded__ && element.__attached) {
      element.__attached = false;
      if (element.detachedCallback) {
        element.detachedCallback();
      }
    }
  }
  function inDocument(element) {
    var p = element;
    var doc = window.wrap(document);
    while (p) {
      if (p == doc) {
        return true;
      }
      p = p.parentNode || p.nodeType === Node.DOCUMENT_FRAGMENT_NODE && p.host;
    }
  }
  function watchShadow(node) {
    if (node.shadowRoot && !node.shadowRoot.__watched) {
      flags.dom && console.log("watching shadow-root for: ", node.localName);
      var root = node.shadowRoot;
      while (root) {
        observe(root);
        root = root.olderShadowRoot;
      }
    }
  }
  function handler(root, mutations) {
    if (flags.dom) {
      var mx = mutations[0];
      if (mx && mx.type === "childList" && mx.addedNodes) {
        if (mx.addedNodes) {
          var d = mx.addedNodes[0];
          while (d && d !== document && !d.host) {
            d = d.parentNode;
          }
          var u = d && (d.URL || d._URL || d.host && d.host.localName) || "";
          u = u.split("/?").shift().split("/").pop();
        }
      }
      console.group("mutations (%d) [%s]", mutations.length, u || "");
    }
    var isAttached = inDocument(root);
    mutations.forEach(function(mx) {
      if (mx.type === "childList") {
        forEach(mx.addedNodes, function(n) {
          if (!n.localName) {
            return;
          }
          addedNode(n, isAttached);
        });
        forEach(mx.removedNodes, function(n) {
          if (!n.localName) {
            return;
          }
          detachedNode(n);
        });
      }
    });
    flags.dom && console.groupEnd();
  }
  function takeRecords(node) {
    node = window.wrap(node);
    if (!node) {
      node = window.wrap(document);
    }
    while (node.parentNode) {
      node = node.parentNode;
    }
    var observer = node.__observer;
    if (observer) {
      handler(node, observer.takeRecords());
      takeMutations();
    }
  }
  var forEach = Array.prototype.forEach.call.bind(Array.prototype.forEach);
  function observe(inRoot) {
    if (inRoot.__observer) {
      return;
    }
    var observer = new MutationObserver(handler.bind(this, inRoot));
    observer.observe(inRoot, {
      childList: true,
      subtree: true
    });
    inRoot.__observer = observer;
  }
  function upgradeDocument(doc) {
    doc = window.wrap(doc);
    flags.dom && console.group("upgradeDocument: ", doc.baseURI.split("/").pop());
    var isMainDocument = doc === window.wrap(document);
    addedNode(doc, isMainDocument);
    observe(doc);
    flags.dom && console.groupEnd();
  }
  function upgradeDocumentTree(doc) {
    forDocumentTree(doc, upgradeDocument);
  }
  var originalCreateShadowRoot = Element.prototype.createShadowRoot;
  if (originalCreateShadowRoot) {
    Element.prototype.createShadowRoot = function() {
      var root = originalCreateShadowRoot.call(this);
      window.CustomElements.watchShadow(this);
      return root;
    };
  }
  scope.watchShadow = watchShadow;
  scope.upgradeDocumentTree = upgradeDocumentTree;
  scope.upgradeDocument = upgradeDocument;
  scope.upgradeSubtree = addedSubtree;
  scope.upgradeAll = addedNode;
  scope.attached = attached;
  scope.takeRecords = takeRecords;
});

window.CustomElements.addModule(function(scope) {
  var flags = scope.flags;
  function upgrade(node, isAttached) {
    if (node.localName === "template") {
      if (window.HTMLTemplateElement && HTMLTemplateElement.decorate) {
        HTMLTemplateElement.decorate(node);
      }
    }
    if (!node.__upgraded__ && node.nodeType === Node.ELEMENT_NODE) {
      var is = node.getAttribute("is");
      var definition = scope.getRegisteredDefinition(node.localName) || scope.getRegisteredDefinition(is);
      if (definition) {
        if (is && definition.tag == node.localName || !is && !definition.extends) {
          return upgradeWithDefinition(node, definition, isAttached);
        }
      }
    }
  }
  function upgradeWithDefinition(element, definition, isAttached) {
    flags.upgrade && console.group("upgrade:", element.localName);
    if (definition.is) {
      element.setAttribute("is", definition.is);
    }
    implementPrototype(element, definition);
    element.__upgraded__ = true;
    created(element);
    if (isAttached) {
      scope.attached(element);
    }
    scope.upgradeSubtree(element, isAttached);
    flags.upgrade && console.groupEnd();
    return element;
  }
  function implementPrototype(element, definition) {
    if (Object.__proto__) {
      element.__proto__ = definition.prototype;
    } else {
      customMixin(element, definition.prototype, definition.native);
      element.__proto__ = definition.prototype;
    }
  }
  function customMixin(inTarget, inSrc, inNative) {
    var used = {};
    var p = inSrc;
    while (p !== inNative && p !== HTMLElement.prototype) {
      var keys = Object.getOwnPropertyNames(p);
      for (var i = 0, k; k = keys[i]; i++) {
        if (!used[k]) {
          Object.defineProperty(inTarget, k, Object.getOwnPropertyDescriptor(p, k));
          used[k] = 1;
        }
      }
      p = Object.getPrototypeOf(p);
    }
  }
  function created(element) {
    if (element.createdCallback) {
      element.createdCallback();
    }
  }
  scope.upgrade = upgrade;
  scope.upgradeWithDefinition = upgradeWithDefinition;
  scope.implementPrototype = implementPrototype;
});

window.CustomElements.addModule(function(scope) {
  var isIE = scope.isIE;
  var upgradeDocumentTree = scope.upgradeDocumentTree;
  var upgradeAll = scope.upgradeAll;
  var upgradeWithDefinition = scope.upgradeWithDefinition;
  var implementPrototype = scope.implementPrototype;
  var useNative = scope.useNative;
  function register(name, options) {
    var definition = options || {};
    if (!name) {
      throw new Error("document.registerElement: first argument `name` must not be empty");
    }
    if (name.indexOf("-") < 0) {
      throw new Error("document.registerElement: first argument ('name') must contain a dash ('-'). Argument provided was '" + String(name) + "'.");
    }
    if (isReservedTag(name)) {
      throw new Error("Failed to execute 'registerElement' on 'Document': Registration failed for type '" + String(name) + "'. The type name is invalid.");
    }
    if (getRegisteredDefinition(name)) {
      throw new Error("DuplicateDefinitionError: a type with name '" + String(name) + "' is already registered");
    }
    if (!definition.prototype) {
      definition.prototype = Object.create(HTMLElement.prototype);
    }
    definition.__name = name.toLowerCase();
    definition.lifecycle = definition.lifecycle || {};
    definition.ancestry = ancestry(definition.extends);
    resolveTagName(definition);
    resolvePrototypeChain(definition);
    overrideAttributeApi(definition.prototype);
    registerDefinition(definition.__name, definition);
    definition.ctor = generateConstructor(definition);
    definition.ctor.prototype = definition.prototype;
    definition.prototype.constructor = definition.ctor;
    if (scope.ready) {
      upgradeDocumentTree(document);
    }
    return definition.ctor;
  }
  function overrideAttributeApi(prototype) {
    if (prototype.setAttribute._polyfilled) {
      return;
    }
    var setAttribute = prototype.setAttribute;
    prototype.setAttribute = function(name, value) {
      changeAttribute.call(this, name, value, setAttribute);
    };
    var removeAttribute = prototype.removeAttribute;
    prototype.removeAttribute = function(name) {
      changeAttribute.call(this, name, null, removeAttribute);
    };
    prototype.setAttribute._polyfilled = true;
  }
  function changeAttribute(name, value, operation) {
    name = name.toLowerCase();
    var oldValue = this.getAttribute(name);
    operation.apply(this, arguments);
    var newValue = this.getAttribute(name);
    if (this.attributeChangedCallback && newValue !== oldValue) {
      this.attributeChangedCallback(name, oldValue, newValue);
    }
  }
  function isReservedTag(name) {
    for (var i = 0; i < reservedTagList.length; i++) {
      if (name === reservedTagList[i]) {
        return true;
      }
    }
  }
  var reservedTagList = [ "annotation-xml", "color-profile", "font-face", "font-face-src", "font-face-uri", "font-face-format", "font-face-name", "missing-glyph" ];
  function ancestry(extnds) {
    var extendee = getRegisteredDefinition(extnds);
    if (extendee) {
      return ancestry(extendee.extends).concat([ extendee ]);
    }
    return [];
  }
  function resolveTagName(definition) {
    var baseTag = definition.extends;
    for (var i = 0, a; a = definition.ancestry[i]; i++) {
      baseTag = a.is && a.tag;
    }
    definition.tag = baseTag || definition.__name;
    if (baseTag) {
      definition.is = definition.__name;
    }
  }
  function resolvePrototypeChain(definition) {
    if (!Object.__proto__) {
      var nativePrototype = HTMLElement.prototype;
      if (definition.is) {
        var inst = document.createElement(definition.tag);
        nativePrototype = Object.getPrototypeOf(inst);
      }
      var proto = definition.prototype, ancestor;
      var foundPrototype = false;
      while (proto) {
        if (proto == nativePrototype) {
          foundPrototype = true;
        }
        ancestor = Object.getPrototypeOf(proto);
        if (ancestor) {
          proto.__proto__ = ancestor;
        }
        proto = ancestor;
      }
      if (!foundPrototype) {
        console.warn(definition.tag + " prototype not found in prototype chain for " + definition.is);
      }
      definition.native = nativePrototype;
    }
  }
  function instantiate(definition) {
    return upgradeWithDefinition(domCreateElement(definition.tag), definition);
  }
  var registry = {};
  function getRegisteredDefinition(name) {
    if (name) {
      return registry[name.toLowerCase()];
    }
  }
  function registerDefinition(name, definition) {
    registry[name] = definition;
  }
  function generateConstructor(definition) {
    return function() {
      return instantiate(definition);
    };
  }
  var HTML_NAMESPACE = "http://www.w3.org/1999/xhtml";
  function createElementNS(namespace, tag, typeExtension) {
    if (namespace === HTML_NAMESPACE) {
      return createElement(tag, typeExtension);
    } else {
      return domCreateElementNS(namespace, tag);
    }
  }
  function createElement(tag, typeExtension) {
    if (tag) {
      tag = tag.toLowerCase();
    }
    if (typeExtension) {
      typeExtension = typeExtension.toLowerCase();
    }
    var definition = getRegisteredDefinition(typeExtension || tag);
    if (definition) {
      if (tag == definition.tag && typeExtension == definition.is) {
        return new definition.ctor();
      }
      if (!typeExtension && !definition.is) {
        return new definition.ctor();
      }
    }
    var element;
    if (typeExtension) {
      element = createElement(tag);
      element.setAttribute("is", typeExtension);
      return element;
    }
    element = domCreateElement(tag);
    if (tag.indexOf("-") >= 0) {
      implementPrototype(element, HTMLElement);
    }
    return element;
  }
  var domCreateElement = document.createElement.bind(document);
  var domCreateElementNS = document.createElementNS.bind(document);
  var isInstance;
  if (!Object.__proto__ && !useNative) {
    isInstance = function(obj, ctor) {
      if (obj instanceof ctor) {
        return true;
      }
      var p = obj;
      while (p) {
        if (p === ctor.prototype) {
          return true;
        }
        p = p.__proto__;
      }
      return false;
    };
  } else {
    isInstance = function(obj, base) {
      return obj instanceof base;
    };
  }
  function wrapDomMethodToForceUpgrade(obj, methodName) {
    var orig = obj[methodName];
    obj[methodName] = function() {
      var n = orig.apply(this, arguments);
      upgradeAll(n);
      return n;
    };
  }
  wrapDomMethodToForceUpgrade(Node.prototype, "cloneNode");
  wrapDomMethodToForceUpgrade(document, "importNode");
  if (isIE) {
    (function() {
      var importNode = document.importNode;
      document.importNode = function() {
        var n = importNode.apply(document, arguments);
        if (n.nodeType == n.DOCUMENT_FRAGMENT_NODE) {
          var f = document.createDocumentFragment();
          f.appendChild(n);
          return f;
        } else {
          return n;
        }
      };
    })();
  }
  document.registerElement = register;
  document.createElement = createElement;
  document.createElementNS = createElementNS;
  scope.registry = registry;
  scope.instanceof = isInstance;
  scope.reservedTagList = reservedTagList;
  scope.getRegisteredDefinition = getRegisteredDefinition;
  document.register = document.registerElement;
});

(function(scope) {
  var useNative = scope.useNative;
  var initializeModules = scope.initializeModules;
  var isIE = scope.isIE;
  if (useNative) {
    var nop = function() {};
    scope.watchShadow = nop;
    scope.upgrade = nop;
    scope.upgradeAll = nop;
    scope.upgradeDocumentTree = nop;
    scope.upgradeSubtree = nop;
    scope.takeRecords = nop;
    scope.instanceof = function(obj, base) {
      return obj instanceof base;
    };
  } else {
    initializeModules();
  }
  var upgradeDocumentTree = scope.upgradeDocumentTree;
  var upgradeDocument = scope.upgradeDocument;
  if (!window.wrap) {
    if (window.ShadowDOMPolyfill) {
      window.wrap = window.ShadowDOMPolyfill.wrapIfNeeded;
      window.unwrap = window.ShadowDOMPolyfill.unwrapIfNeeded;
    } else {
      window.wrap = window.unwrap = function(node) {
        return node;
      };
    }
  }
  if (window.HTMLImports) {
    window.HTMLImports.__importsParsingHook = function(elt) {
      if (elt.import) {
        upgradeDocument(wrap(elt.import));
      }
    };
  }
  function bootstrap() {
    upgradeDocumentTree(window.wrap(document));
    window.CustomElements.ready = true;
    var requestAnimationFrame = window.requestAnimationFrame || function(f) {
      setTimeout(f, 16);
    };
    requestAnimationFrame(function() {
      setTimeout(function() {
        window.CustomElements.readyTime = Date.now();
        if (window.HTMLImports) {
          window.CustomElements.elapsed = window.CustomElements.readyTime - window.HTMLImports.readyTime;
        }
        document.dispatchEvent(new CustomEvent("WebComponentsReady", {
          bubbles: true
        }));
      });
    });
  }
  if (document.readyState === "complete" || scope.flags.eager) {
    bootstrap();
  } else if (document.readyState === "interactive" && !window.attachEvent && (!window.HTMLImports || window.HTMLImports.ready)) {
    bootstrap();
  } else {
    var loadEvent = window.HTMLImports && !window.HTMLImports.ready ? "HTMLImportsLoaded" : "DOMContentLoaded";
    window.addEventListener(loadEvent, bootstrap);
  }
})(window.CustomElements);

(function(scope) {
  var style = document.createElement("style");
  style.textContent = "" + "body {" + "transition: opacity ease-in 0.2s;" + " } \n" + "body[unresolved] {" + "opacity: 0; display: block; overflow: hidden; position: relative;" + " } \n";
  var head = document.querySelector("head");
  head.insertBefore(style, head.firstChild);
})(window.WebComponents);
		})
			
	]
	var n
	for(var i=0;i<moduleFiles.length;i++){
		if(moduleFiles[i].isfile && !moduleFiles[i].contents)
			moduleFiles[i].contentFunc= funcs.shift()
		n= moduleFiles[i].filename
		if(n.endsWith('/'))
			n= n.substring(0,n.length-1)
		window.___Module.vfsSystem[n]=moduleFiles[i]
	}
	window.___module.require('/' + "kodhe-css")

})()
			