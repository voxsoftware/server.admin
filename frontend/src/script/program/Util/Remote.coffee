#global $
G= require(__dirname + "/../default").WebSite
v= require(__dirname + "/../default").Util


class Remote extends v.Module

	request: (method, module, args, events={})->

		u = location.origin
		s1= if module.indexOf("?")>=0 then "&" else "?"
		s2= if events.prefix then events.prefix else @app.apiUrl + "/"
		
		if @app.port1
			s2= "http://127.0.0.1:#{@app.port1}#{s2}"
		
		url=  s2 + module + s1 + "src-url=" + encodeURIComponent(u)
		if global.localStorage and global.localStorage[@app.id+"-sid"] 
			url+="&sid=" + global.localStorage[@app.id+"-sid"] 
		
		if global.localStorage and global.localStorage[this.app.id+"-kac"]
			url+="&kodhe_apps_code="+global.localStorage[this.app.id + "-kac"]
		
		if method.toUpperCase()!="GET" 
			req= new core.VW.Http.Request(url)
		

		if args and  typeof args.submit is "function"
			if method.toUpperCase()=="GET"
				args= this.app.formArgs(args)
			else
				req.form= args
			
		

		if args and typeof args.submit isnt "function"

			if method.toUpperCase()=="GET"
				for id, val of args
					url+= "&" + @encodeURIComponent(id, val)
				
			else
				if (global.FormData and args instanceof global.FormData) or 
						(global.File && args instanceof global.File) or (global.Blob && args instanceof global.Blob) 
					req.literal= yes 
					req.body= args
				
				else
					req.contentType= "application/json"
					req.body= args
				
		
		if method.toUpperCase()=="GET"
			req= new core.VW.Http.Request(url)
			
		req.method=method
		req.withCredentials= if events.credentials isnt undefined then events.credentials else false
		req.nowait= @_nowait
		req.events= events
		req.timeout=99999999999
		@_nowait= no 
		@emit "request", req
		@currentRequest= req


		try
			task= core.VW.Web.Vox.platform.getJsonResponseAsync(req)
			data= await task
			@procesar data
		
		catch e
			ee= e
		
		@emit "request:complete",req
		throw ee if ee
		data
	

	encodeURIComponent:(id, arg)->
		
		if arg instanceof Array
			for ar,i in args
				str+="&" if i > 0
				str+= @encodeURIComponent id+"[]", ar
		else
			str= id + "=" + ( encodeURIComponent arg )
		str
	

	procesar: (data)->
		
		if typeof data=="string"

			dr= data.split("-")
			if dr.length>2 and not isNaN(parseInt(dr[0]))
				date= new Date(data)
			
			if date and not isNaN(date.getTime())
				data= new v.Datetime(core.VW.Moment(date))
			
			else
				l= data.split(" ")
				if(l.length==2)
					d= l[0].split("-")
					if(d.length==3)
						d= l[1].split(":")
						if(d.length==3)
							if data=="0000-00-00 00:00:00"
								data=undefined
							else
								d= new v.Datetime(core.VW.Moment(data+"Z"))
								data= d if d.isValid()
									
		else if data and typeof data is "object"
			if data.$oid
				data= new v.ObjectId(data.$oid)
			
			else
				for id, val of data
					data[id]= @procesar val
				
	
				if data instanceof Array
					for val, i in data
						data[i]= @procesar val 
					
		data
	

	__get_nowait: ->
		@_nowait= true
		@
	

	get: (module, args, options)->
		@request("GET", module, args, options)
	

	post: (module, args, options)->
		@request("POST", module, args, options)
	

	put: (module, args, options)->
		@request("PUT", module, args, options)
	

	patch: (module, args, options)->
		@request("PATCH", module, args, options)
	

	delete: (module, args, options)->
		@request("DELETE", module, args, options)
	
	global.resolveProps @, @::
export default Remote
