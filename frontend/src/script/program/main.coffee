DefaultNamespace=require("./default")
Path= require("path")

resolveProps= (cl)->
	props= {}
	keys= Object.getOwnPropertyNames cl
	for id in keys 
		val= cl[id]
		if id.startsWith "__get_"
			name= id.substring	6
			props[name]= props[name] ? {}
			props[name].get= val 
			delete cl[id]
		else if id.startsWith "__set_"
			name= id.substring	6
			props[name]= props[name] ? {}
			props[name].set= val 
			delete cl[id]
	for id,prop of props 
		Object.defineProperty cl, id, prop

global.resolveProps= (args...)->
	for arg in args 
		resolveProps arg
	return


createfunc= (name,file)->
	iname= "$" + name
	()->
		if not @[iname]
			e= require file 
			@[iname]= e.default or e 
		@[iname]

if global.___Module and global.___Module.vfsSystem
	noop= ()->
		
	for id,val of global.___Module.vfsSystem
		if id.startsWith("/program") and id isnt "/program"
			parts= id.split "/"
			json= DefaultNamespace 
			for i in [2...parts.length]
				if not json[parts[i]] 
					json[parts[i]]= noop 
				json= json[parts[i]]
			if parts.length > 3 
				if val.isdirectory
					json[parts[parts.length-1]] = noop
				else 
					n= Path.basename parts[parts.length-1], Path.extname(parts[parts.length-1])
					json.__defineGetter__ n, createfunc(n,id)

global.program= DefaultNamespace
export default DefaultNamespace