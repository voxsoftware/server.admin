# global $
G = require(__dirname + "/../default").WebSite
U = require(__dirname + "/../default").Util

class IndexController extends U.Module 


	ui: ()->
		@hashChangedPre()
		@attachEvents()
		$(".await-action").removeAttr("disabled")
		@hashChangedPost()
	

	hashChangedPre: ()->
		scope = @app.scope
		scope.hashArgs = @hashArgs = @app.getParameters()

	

	hashChangedPost: ()->
		scope = @app.scope
		scope.selected = []
		scope.args = {}
		@init()
	

	
	attachEvents: ()->
		scope = @app.scope
		self = @
		
		

	__get_mainCode: ()->
		"""
		var F= async function(body){
			var global=F.global
			try{
				body.app= app 
			}catch(e){}
			global.publicContext.UserFunction.global= global
			return await global.publicContext.UserFunction(body.func_name).invoke(body)
		}
		F
		"""
		
	


	abrirNavegador: ()->
		
		scope= @app.scope
		###
		await this.app.remote.post("function/c/browser.open", {
			idSite: this.app.id,
			//code: scope.userinfo.code,
			email:scope.email,
			password: scope.password
		}, {
			prefix: 'http://127.0.0.1:49603/site/'+this.app.id+'/api/'
		})
		###
	



	
	init: ->
		scope = @app.scope
		remote = yes 
		scope.opcion = 0
		try 
			scope.instalando = true
	
			scope.config= await this.app.remote.post "function/c/api.init", {}, 
				prefix: "http://127.0.0.1:49603/site/#{this.app.id}/api/"
			
		finally 
			scope.instalando= false 


	secureTask: (promise)->
		task = new core.VW.Task()
		promise
			.then (result) ->
				task.result = result
				task.finish()
			.catch (er) ->
				task.exception = er
				task.finish()
		task
		
	global.resolveProps @, @::
	
export default IndexController
