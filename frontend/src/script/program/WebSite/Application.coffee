#global $


G= require(__dirname + "/../default").WebSite
U= require(__dirname + "/../default").Util
import {EventEmitter} from 'events'

class Application	extends EventEmitter
	
	constructor: ()->
		super()
		@id= 'org.kodhe.serveradmin'
		
		
	@__get_current: ()->
		Application.$current= new Application if not Application.$current 
		#core.dynvox.v2.Scope.get("default").prefix= global.prefix 
		Application.$current 
	
	__get_configuration: ()->
		@getModule "configuracion"
	
	getParameters: ()->
		@getModule("routemanager").getParameters()
	
	hashChanged: (f)->
		self= @
		if not @$changed
			global.onhashchange= ()->
				args= arguments
				func?.apply? self, args for func in self.$changed
			@changed=[]
		@changed.push f 
				
	getModule: (module)->
		name= module.toUpperCase() 
		if not @$cacheNames
			@$cacheNames= {}
			for id of G 
				@$cacheNames[id.toUpperCase()]= G[id]
		if @$cacheNames[name]
			return @create @$cacheNames[name], name
	

	initScope:(options)->
		# create VueApp first time 
		if @$scope 
			@$scope.$destroy()
		if options.data 
			# get colors 
			backColors= {}
			textColors= {}
			for id, val of global.ThemeColors
				backColors[id]= {}
				textColors[id]= {}
				for id2, val2 of val 
					backColors[id][id2]= val2.join(" ")
					val2[0] += "--text"
					textColors[id][id2]= val2.join(" ")
				
			options.data.colors= 
				background: backColors
				text: textColors
		

		return  @$scope= new Vue options
		
	
	
	__get_scope: ()->
		return @$scope
		
		#scope= $("body").attr("voxs-scope") or "default"
		#sc= core.dynvox.v2.Scope.get(scope)
		#if not sc.observer 
		#	sc.observer= new G.Observer sc 
		#sc 
	
	__get_currentModule: ()->
		m= $("module")
		m.eq(m.length-1).text()
	

	__get_messageManager: ()->
		@$message= @create G.MessageManager if not @$message
		@$message



	__get_apiUrl: ()->
		global.app.apiUrl
	

	__get_uiUrl: ()->
		global.app.uiUrl
	

	__get_assetUrl: ()->
		global.app.assetUrl
	

	__get_routes: ()->
		global.app.routes
	


	beginRequest: (req)->

		
		if not req.nowait
			$(".await-action").attr("disabled", "disabled")

		progress= $(".loading-progress")
		progress.removeClass("transitioned")
		progress.css("width", 0)
		$(".loading-container").show()
		progress.addClass("transitioned")
		progress.css("transition-duration", "6s")
		progress.css("width", "80%")

	

	endRequest: ()->
		progress= $(".loading-progress")
		progress.css("transition-duration", "0.7s")
		progress.css("width", "100%")
		setTimeout ()->
			$(".loading-container").hide()
		, 800
		$(".await-action").removeAttr("disabled")
	

	__get_remote: ()->
		if not @$remote
			@$remote= new U.Remote(@)
			@$remote.on "request", @beginRequest.bind(@)
			@$remote.on "request:complete", @.endRequest.bind(@)
		
		@$remote
	
	
	reset: ()->

		@$changed= []
		if @currentModuleO
			@currentModuleO.abort()
		@$scope.$destroy() if @$scope 		

	isForm: (form)->
		typeof form.serialize is "function"
	

	formArgs: (form)->
		data= form.serializeArray()
		obj={}
		for d in data
			obj[d.name]= d.value
		
		obj
	

	getAssetUrl: (url)->
		this.assetUrl + url
	


	redirect: (route,parameters)->
		@.getModule("routemanager").redirect(route, parameters)


	cargarConfigSiesNecesario: ()->

		config= {}
		if localStorage
			config.lang= localStorage.lang
		

		if not config.lang
			config= config= {lang:'es'}
			localStorage.lang= config.lang
		
		global.applicationConfig= config


	__get_port1: ->
		global.localStorage[@id+"-port1"]
	
	parseSearch: ()->
		if global.location.search
			s= global.location.search.substring(1)
			parts= s.split("&")
			obj= {}
			for part in parts 
				cc=part.split("=")
				obj[cc[0]]= decodeURIComponent(cc[1])
			
			#this.scope.search= obj
			if obj.kodhe_apps_code
				global.localStorage[this.id+"-kac"]= obj.kodhe_apps_code
			
			if obj.port
				global.localStorage[@id+"-port1"]= obj.port
	
	
	center: ()->
		bounds = global._require("electron").remote.screen.getPrimaryDisplay().bounds
		x = bounds.x + ((bounds.width - window.outerWidth) / 2)
		y = bounds.y + ((bounds.height - window.outerHeight) / 2)
		window.moveTo(x,y)
	

	start: ()->
		module= @currentModule
		@parseSearch()


		$(".await-action").attr("disabled", "disabled")
		await @cargarConfigSiesNecesario()
		
		if module
			@emit "change", module
			
			module= @getModule(module)
			@currentModuleO= module
			module?.ui?()

		$("form a.button[type=submit]").click ()->
			a= $(this)
			a.parents("form").eq(0).submit()
		


	uiRequired: ()->
		inputs= $(".input-field")
		self= this
		inputs.each ()->
			inp= $(this)
			t= inp.find("input,select").eq(0)
			if t.data("required") isnt undefined
				span= $("<span>")
				span.text(" * ")
				span.addClass("text-color-warn")
				inp.find("label").append(span)
			

	create: (clase, name)->
		@cached={} if not @cached
		name= clase.name if not name 
		@cached[name]= new clase @ if not @cached[name]
		@cached[name]


	deleteFromCache: (module)->
		if @cached 
			for id of @cached 
				delete @cached[id] if @cached[id] is module 
		module?.dispose?()
		




	clearCache: ()->
		for id of @cached 
			delete @cached[id] if id isnt "routemanager"

		for id of @$cacheNames
			delete @$cachedNames[id] if id isnt "routemanager"
		

	global.resolveProps @, @::
				
	
###
class ObservableList extends Array
	constructor: ()->
		super ...arguments
		console.warn("> Deprecated. Please consider using Array constructor. ObservableList it's not present on new versions of dynvox")
	
	
	toArray: ()->
		Array.prototype.slice.call(@, 0, @length)
	
	toJSON: ()->
		@.toArray()
	

core.dynvox.ObservableList= ObservableList
###

export default Application
