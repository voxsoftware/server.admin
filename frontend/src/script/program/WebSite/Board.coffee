# global $
G = require(__dirname + "/../default").WebSite
U = require(__dirname + "/../default").Util

class Board extends U.Module 


	

	hashChangedPre: ()->
		scope = @app.scope
		scope.hashArgs = @hashArgs = @app.getParameters()

	

	hashChangedPost: ()->
		scope = @app.scope
		scope.selected = []
		scope.args = {}
		@init()
	

	
	attachEvents: ()->
		scope = @app.scope
		#await @app.remote.post "function/c/api.init"
		scope.href= (ev)->
			ev?.preventDefault?()
			href= this.href 
			if href
				global.history.pushState {}, "", href
				$(".side-nav").voxsidenav()[0].close()
		scope.goback= ()->global.history.back()
		scope.menuClick= ()->
			$(".side-nav").voxsidenav()[0].toggle()
	
	init: ->
		scope = @app.scope


	secureTask: (promise)->
		task = new core.VW.Task()
		promise
			.then (result) ->
				task.result = result
				task.finish()
			.catch (er) ->
				task.exception = er
				task.finish()
		task
		
	global.resolveProps @, @::
	
export default Board
