
routerC= (router,app)->
	prefix= global.prefix 
	IDIndex= global.IDIndex or "0"
	routeManager= app.getModule "routeManager"
	
	currentuser= ()->
		m= app.getModule "logincontroller"
		u=await m.user()
		global["1user"]=u 
		#return global["1user"]
	
	main= (args)->
		user= await currentuser()
		return app.redirect "ui/login" if not user 
		routeManager.view prefix + "/dynamic/" + IDIndex + "/components/inicio"
	
	router.route route, main  for route in [prefix or "/", prefix, prefix + "/ui/main"] 
	router.route prefix + "/ui/login", (args)->
		user= await currentuser()
		return app.redirect "ui/main" if user 
		routeManager.view prefix + "/dynamic/" + IDIndex + "/components/login"
	
	router.route prefix + "/ui/terminal", (args)->
		routeManager.view prefix + "/dynamic/" + IDIndex + "/components/terminal"
	
	router.route prefix + "/ui/x11", (args)->
		routeManager.view prefix + "/dynamic/" + IDIndex + "/components/x11"
		
	

	
export routes= routerC
