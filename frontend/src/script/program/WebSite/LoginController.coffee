# global $
G = require(__dirname + "/../default").WebSite
U = require(__dirname + "/../default").Util

class LoginController extends U.Module 


	ui: ()->
		@hashChangedPre()
		@attachEvents()
		$(".await-action").removeAttr("disabled")
		@hashChangedPost()
	

	hashChangedPre: ()->
		scope = @app.scope
		scope.hashArgs = @hashArgs = @app.getParameters()

	

	hashChangedPost: ()->
		scope = @app.scope
		scope.selected = []
		scope.args = {}
		@init()
	

	
	attachEvents: ()->
		scope = @app.scope
		self = @
		scope.login_submit= @tryMode.bind @, @login.bind(@)
		
	
	tryMode: (func, args...)->
		try 
			return await func.apply this, args
		catch e 
			console.error e
			@app.messageManager.error e.message or e.toString()
	
	
	
	user: ()->
		# get current user
		scope=@app.scope
		scope.user = global["1user"] if global["1user"]
		return scope.user if scope.user 
		
		if not @app._inited 
			await @app.remote.post "function/c/app.init"
			@app._inited = true
		
		user= await @app.remote.post "function/c/user@current"
		if user 
			scope.user=user 
			return user 
		
	
	login: (ev)->
		ev?.preventDefault?()
		scope=@app.scope
		tok= await @app.remote.post "function/c/user@login", scope.login
		if tok?.sid 
			localStorage["#{@app.id}-sid"]= tok.sid
			@app.redirect "ui/main"


	init: ->
		scope = @app.scope
		await this.app.remote.post "function/c/app.init"
		scope.login= {}


	secureTask: (promise)->
		task = new core.VW.Task()
		promise
			.then (result) ->
				task.result = result
				task.finish()
			.catch (er) ->
				task.exception = er
				task.finish()
		task
		
	global.resolveProps @, @::
	
export default LoginController
