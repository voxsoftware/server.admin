# global $
G = require(__dirname + "/../default").WebSite
U = require(__dirname + "/../default").Util

class TerminalController extends U.Module 

	ui: ()->
		@hashChangedPre()
		@attachEvents()
		$(".await-action").removeAttr("disabled")
		@hashChangedPost()
	

	hashChangedPre: ()->
		#scope = @app.scope
		@hashArgs = @app.getParameters()

	

	hashChangedPost: ()->
		@init()
	

	
	attachEvents: ()->
		#scope = @app.scope		
		
		# init scope 
		scope= @app.initScope 
			el: '#t-main'
			data: 
				args:
					password: 'admin'
					host: global.location.host
				show_password: false
				connect: @.connect.bind @

		
		
		
	tryMode: (func, args...)->
		try 
			return await func.apply this, args
		catch e 
			console.error e
			@app.messageManager.error e.message or e.toString()
	
	
	
	connect: ()->
		scope=  @app.scope 
		args= scope.args 
		
		if @socket
			@socket.close()
			delete @cid
			
		
		host= args.host 
		@socket= global.io host,
			transports: [ 'websocket' ]
		
		# attach to site 
		@socket.on("connect", @.socketconnect.bind(@))
	
	
	socketconnect: ()->
		args= @app.scope.args
		#self= @
		if not @cid
			@cid= @uniqid()
			window.terminal= @terminal
			@term_write = (data)=>
				lines= data.split(/\r\n|\r|\n/)
				for line, i in lines
					line= line.trim() if line
					console.info "Line", line
					if line 
						@terminal.write line + (if i isnt (lines.length-1) then "\r\n" else "")
			
			@socket.on "stderr_#{@cid}", @term_write
			@socket.on "stdout_#{@cid}", @term_write
			
			# on app change disconnect 
			socket= @socket
			@app.once "change", ()->
				console.log "Closing socket ..."
				socket.close()			
				
		@terminal.writeln "Connected to host"
		@socket.emit "event", 
			site: "server.admin"
			method: "console.attach"
			cid: @cid
			body: 
				stderr: "stderr_#{@cid}"
				stdout: "stdout_#{@cid}"
				encoding: 'binary'
				password: args.password
			wait: yes
	
	uniqid:(a='', b=no)->
		c= Date.now() / 100
		d= c.toString(16).split(".").join("")
		while d.length < 14 
			d += "0"
		
		e= ''
		if b 
			e= '.'
			f= Math.round(Math.random * 100000000 )
			e += f
		
		return a + d + e
		
	
	init: ->
		scope = @app.scope
		console.log "Initing ..."
		
		while not global.Terminal
			await core.VW.Task.sleep 10
		
		await core.VW.Task.sleep 1000
		this.terminal= new global.Terminal()
		this.terminal.open(document.getElementById("term"))
		
		
		scope.args.host= global.location.host if not scope.args.host
		scope.args.password= "admin" if not scope.args.password
		
		
			

	secureTask: (promise)->
		task = new core.VW.Task()
		promise
			.then (result) ->
				task.result = result
				task.finish()
			.catch (er) ->
				task.exception = er
				task.finish()
		task
		
	global.resolveProps @, @::
	
export default TerminalController
