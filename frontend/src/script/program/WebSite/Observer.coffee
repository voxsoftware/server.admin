
class Observer
	constructor: (scope)->
		@scope=scope	
	
	
	getValue: (str)->
		console.warn("> Deprecated, please consider not using Observer with newer version of scope")
		props= str.split(".")
		o= this.scope
		for prop in props
			o=o[prop]
			breal if not o
		
		return o
	
	
	onValueChanged: (name, event)->
		console.warn("> Deprecated, please consider not using Observer for attach events with newer version of scope")
		console.warn("> Deprecated, Newer versions of dynvox take care about memory leaks, this method no")


		@scope.on "change", (ev)->
			if ev.name==name 
				return event(ev)
		
	
	
	assignValue: (args)->
		
		console.warn("> Deprecated, please consider not using Observer for attach events with newer version of scope")
		props= args.name.split(".")
		o= @scope
		for prop in props 
			o=o[prop]
			break if not o
			
		if o
			o[props[props.length-1]]= args.value
		
		return args.value
	
	

export default Observer