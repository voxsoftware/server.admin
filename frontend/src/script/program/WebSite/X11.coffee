# global $
G = require(__dirname + "/../default").WebSite
U = require(__dirname + "/../default").Util

class X11 extends U.Module 

	ui: ()->
		@hashChangedPre()
		@attachEvents()
		$(".await-action").removeAttr("disabled")
		@hashChangedPost()
	

	hashChangedPre: ()->
		#scope = @app.scope
		@hashArgs = @app.getParameters()

	

	hashChangedPost: ()->
		@init()
	

	
	attachEvents: ()->	
		
		# init scope 
		self= @
		scope= @app.initScope 
			el: '#t-main'
			data: 
				images: 0
				args:
					password: 'admin'
					host: global.location.host
				show_password: false
				show_style: "display:none"
				connected: false
				connect: @.connect.bind @

		# attach events from mouse 
		mousemove= (e)->
			if scope.connected
				if mousemove.int 
					clearTimeout mousemove.int 

				mousemove.int= setTimeout ()->
					self.socket.emit "move", 
						xpercent: (e.pageX / $("#image").width()) * 100
						ypercent: (e.pageY / $("#image").height()) * 100
				, 5 
		
		mousedown= (e)->
			if scope.connected
				self.socket.emit "mousedown", e.which
				e.preventDefault()
		
		mouseup= (e)->
			if scope.connected
				self.socket.emit "mouseup", e.which
				e.preventDefault()
		realCodes= 
			"Slash": 45
			"Period": 46
			"Comma": 44
			"Semicolon": 241
			"Quote": 123
			"BracketLeft": 65105
			"BracketRight": 43
			"Backslash": 125
			"Backquote": 124
			"Minus": 92
			"Equal": 191
			"Backspace": 65288
			"F1": 65470
			"F2": 65471
			"F3": 65472
			"F4": 65473
			"F5": 65474
			"F6": 65475
			"F7": 65476
			"F8": 65477
			"F9": 65478
			"F10": 65479
			"F11": 65480
			"F12": 65481
			"AltRight": 65027
			"AltLeft": 65013
			"ControlLeft": 65507
			"ControlRight": 65508
			"ShiftLeft": 65505
			"ShiftRight": 65506
			"MetaLeft": 65515
			"Home": 65360
			"End": 65367
			"Insert": 65379
			"Escape": 65307
			"Delete": 65535
			"ArrowLeft": 65361
			"ArrowUp": 65362
			"ArrowRight": 65363
			"CapsLock": 65509
			"ArrowDown": 65364
			"Enter": 65293
			"Tab": 65289



		keydown= (e)->
			if scope.connected
								
				realcode= realCodes[e.originalEvent.code]
				kcode= realcode or e.which 
				if keydown.lastcode is kcode 
					self.socket.emit "keyup", kcode 
				self.socket.emit "keydown", kcode 
				keydown.lastcode= kcode							
				e.preventDefault()

				#console.log eljk
						
		keyup= (e)->
			if scope.connected
				keydown.lastcode= null
				realcode= realCodes[e.originalEvent.code]
				kcode= realcode or e.which 
				self.socket.emit "keyup", kcode
				e.preventDefault()
		


		document.oncontextmenu = ()->no
		$(document).mousemove mousemove
		$(document).mousedown mousedown
		$(document).mouseup mouseup
		$(document).keydown keydown
		$(document).keyup keyup

		
	tryMode: (func, args...)->
		try 
			return await func.apply this, args
		catch e 
			console.error e
			@app.messageManager.error e.message or e.toString()
	
	
	
	connect: ()->
		scope=  @app.scope 
		args= scope.args 
		
		if @socket
			@socket.close()
			delete @cid
			
		
		host= args.host 
		@socket= global.io host,
			transports: [ 'websocket' ]
		
		# attach to site 
		scope.connected= no 
		@socket.on("connect", @.socketconnect.bind(@))
	
	
	

	socketconnect: ()->
		scope= @app.scope
		args= @app.scope.args
		format= "jpeg"
		Buffer= core.basic.get_buffer().Buffer
		bufcount= 10
		if not @cid
			@cid= @uniqid()
			image= null 

			socket= @socket
			
			@socket.on "cache.image", (y)->
				console.log "from cache"
				image.src= buffering[y]
			
			buffering= @buffering= []
			
			@socket.on "image", (src)->
				if not image 
					image= document.getElementById "image"	
				scope.images++
				buf= new Buffer(src,'binary')
				buf= "data:image/#{format};base64," + buf.toString('base64')
				image.src= buf
				buffering.push buf 
				if buffering.length > bufcount
					buffering.shift()

				#socket.emit "continue"
			
			# on app change disconnect 
			
			@app.once "change", ()->
				console.log "Closing socket ..."
				socket.close()			
		

		scope.connected= yes
		@socket.emit "event", 
			site: "server.admin"
			method: "x11@X.capture"
			cid: @cid
			body: 
				bufferCount: bufcount
				encoding: 'binary'
				format: format
				password: args.password
			wait: no
	
	uniqid:(a='', b=no)->
		c= Date.now() / 100
		d= c.toString(16).split(".").join("")
		while d.length < 14 
			d += "0"
		
		e= ''
		if b 
			e= '.'
			f= Math.round(Math.random * 100000000 )
			e += f
		
		return a + d + e
		
	
	init: ->
		scope = @app.scope
		console.log "Initing ..."

		
		scope.args.host= global.location.host if not scope.args.host
		scope.args.password= "admin" if not scope.args.password
		
		
			

	secureTask: (promise)->
		task = new core.VW.Task()
		promise
			.then (result) ->
				task.result = result
				task.finish()
			.catch (er) ->
				task.exception = er
				task.finish()
		task
		
	global.resolveProps @, @::
	
export default X11
