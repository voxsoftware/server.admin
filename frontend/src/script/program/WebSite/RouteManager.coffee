

exe= (code)->
	eval code


#global 
G= require(__dirname + "/../default").WebSite
U= require(__dirname + "/../default").Util





class RouteManager extends U.Module
	view: (view)->
		if not view.endsWith(".html")
			view+= ".html"
		req= new core.VW.Http.Request(view)
		req.async= yes
		res= await req.getResponseAsync()
		await this.processHtml(res.body)

	
	enableLoading: ()->
		$(".loading-container,.loading-cover").show()
		progress= $(".loading-progress")
		if not this.loading
			progress.css("transition-duration", "6s")
			progress.css("width", "80%")

			this.loading= yes
			this.task= new core.VW.Task()
			this.task.oncomplete= ()->
				progress.css("transition-duration", "0.7s")
				progress.css("width", "100%")
				setTimeout ()->
					$(".loading-container,.loading-cover").hide()
				, 800



	deleteAttributes: (element)->
		attributes= element.get(0).attributes
		for attr in attributes
			element.removeAttr attr.name
		
	


	copyAttributes: (element, element2)->
		attributes= element.get(0).attributes
		for attr in attributes
			element2.attr attr.name, attr.value
		
	

	replaceAttributes: (element, element2, ignore=[])->

		attributes = if element[0] then element.get(0).attributes else []
		attributes2 = if element2[0] then element2.get(0).attributes else []
		e1byname={}
		
		for attr in attributes
			e1byname[attr.name]=attr.value
		
		e1keys= Object.keys e1byname
		for attr in attributes2
			n= attr.name
			v= attr.value
			if n not in e1keys
				element2.removeAttr n
			
			else if e1byname[n] isnt v and n not in ignore
				element2.attr n, e1byname[n]
			
		
	
	processHtml: (html)->

		contenido= html.replace /(\<html\s+|\<\/html\>|\<head\s+|\<\/head\>|\<body\s+|\<\/body\>)/ig, (v)->
			v= v.toLowerCase()
			if v.indexOf("html") >= 0
				return v.replace("html", "vhtml")

			if v.indexOf("head") >= 0
				return v.replace("head", "vhead")

			if v.indexOf("body") >= 0
				return v.replace("body", "vbody")
		
		
		$("iframe").attr("src","null")
		$("iframe").remove()

		jq= $(contenido)
		body= jq.find("vbody>*")
		main0= jq.find(".main-0")
		main02= $(".main-0")
		main1= jq.find(".main-1")
		main12= $(".main-1")
		window.pe= body
		
		this.app.reset()
		
		if main1.length+main12.length >= 2
			#main12.voxanimate("fadeOut", 300)
			main12.hide()
			
			await core.VW.Task.sleep(300)
			main12.html(main1.html())

			toAnimate= main12
		
		else if main0.length+main02.length >= 2
			#main02.voxanimate("fadeOut", 300)
			main12.hide()
			
			await core.VW.Task.sleep(300)
			main02.html(main0.html())

			toAnimate= main02

		else
			toremove= $("body>*").not(".inmovible")
			toremove.remove()
			
			for i in [0...body.length]
				$("body").append body.eq(i)
			
		


		
		html1= jq.find("vhead").html()
		


		meta= jq.find("vhead meta,vhead title")
		jq.find("vhead title").html($("title").html())
		$("head meta,head title").remove()
		for i in [0...meta.length]
			$("head").append(meta.eq(i))
			

		@replaceAttributes(jq, $("html"))


		scripts= jq.find("vhead script")
		scripts.each ()->
			s= $ @
			code= s.html()
			if code
				exe code
			code=null
			s=null

		if toAnimate
			await core.VW.Task.sleep(500)
			#toAnimate.voxanimate("fadeIn", 300)
			toAnimate.show()
		
		if this.task
			this.task.finish()

		html=null
		html1=null
		jq=null
	



	init: (pushstate)->
		
		pushstate= not not pushstate
		@router= new core.dynvox.Router()
		issite= location.pathname.startsWith("/site/")
		@prefix=''
		if issite
			prefix= location.pathname.split("/").slice(0,3).join("/")
		
		@prefix=prefix
		
		@router.pushstate= pushstate
		@pushstate= pushstate

		
		for id,r of G.Routes
			if r?.routes
				r.routes @router, @app, prefix
			
		@router.use ()=>
			@view prefix+"/dynamic/01/404"
		
		@router.start()
	


	getParameters: ->
		hashStr= @router.location.hash
		if hashStr
			hashStr= hashStr.substring(1)

		hash={}
		if hashStr
			parts= hashStr.split("&")
			for part in parts 
				p=part.split("=")
				hash[p[0]]= p[1]
			
		

		params= @router.params
		o= {}
		for id,param of params
			o[id]= param
		
		for id,val of hash
			o[id]= val if not o[id]
		
		o



	redirect: (route, parameters)->
		if @prefix
			route= @prefix.substring(1) + "/" + route
		
		
		url= (if this.pushstate then "/" else "#/") + route
		if parameters
			str=[]
			for id,param of parameters
				str.push id + "=" + param
			
			url+= "#" + str.join("&")
		

		base= @app.uiUrl
		@router.start undefined, url
	
export default RouteManager
